package co.stutzen.tomatotail.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Bill;
import co.stutzen.tomatotail.entity.BillWithBLOBs;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.BillDetailService;

@Controller
@RequestMapping("/bill")
public class BillDetailController extends SWFController<Bill>{

	@Autowired
	private BillDetailService billDetailService;

	@Autowired
	private UserMapper userMapper;

	private static Logger log = LogManager.getRootLogger();

	@Autowired
	public BillDetailController(SWFService<Bill> swfService) {
		super(swfService);
	}

	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={java.lang.Exception.class})
	@RequestMapping("/save")
	@ResponseBody
	// Save the bill details in tbl_bill & tbl_bill_detail
	public Result save(@RequestBody BillWithBLOBs bill,
			HttpSession session) {
		Result r = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		bill.setCreatedBy(userId);
		bill.setModifiedBy(userId);
		if(bill.getIsActive() == null)
			bill.setIsActive(true);
		log.info("save () ===> Bill Save ==>"+bill);
		billDetailService.create(bill);
		// save bill details here
		if (bill.getBillDetailList().size() > 0)
			billDetailService.detailSave(bill, userId);
		if(bill.getId() >0){
			r.setMessage("Bill saved ");
			r.setSuccess(true);
		}else{
			r.setMessage("Bill not saved ");
			r.setSuccess(false);
		}
		return r;
	}

	@Transactional(propagation=Propagation.REQUIRED, rollbackFor={java.lang.Exception.class})
	@RequestMapping("/statusUpdate")
	@ResponseBody
	//
	public Result statusUpdate(int billId, String status, HttpSession session){
		Result r = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		int updateCount = billDetailService.updateStatus(billId, status, userId);
		if(updateCount >0){
			r.setMessage("Bill status updated");
			r.setSuccess(true);
		}else{
			r.setMessage("Bill status not updated");
			r.setSuccess(false);
		}
		return r;
	}

	@RequestMapping("/listAll")
	@ResponseBody
	// Fetch the records from tbl_bill & tbl_bill_detail
	public Result billList(
			@RequestParam(value = "status", required = false, defaultValue = "") String status,
			@RequestParam(value = "outletId", required = false, defaultValue = "") String outletId,
			@RequestParam(value = "billId", required = false, defaultValue = "") String billId,
			@RequestParam(value = "toDate", required = false, defaultValue = "") String toDate,
			@RequestParam(value = "fromDate", required = false, defaultValue = "") String fromDate,
			@RequestParam(value = "start", required = false, defaultValue = "0") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "100") String limit){
		Result r = new Result();
		int begin = Integer.parseInt(start);
		int end = Integer.parseInt(limit);
		int billid = 0;
		int outletid = 0;
		if (!billId.isEmpty())
			billid = Integer.parseInt(billId);
		if (!outletId.isEmpty())
			outletid = Integer.parseInt(outletId);
		if(toDate.isEmpty())
			toDate = fromDate;
		r.addData("list", billDetailService.getBillList(status, outletid,
				billid, fromDate, toDate, begin, end));
		begin = 0;
		end = 0;
		r.addData(
				"total",
				billDetailService.getBillList(status, outletid, billid,
						fromDate, toDate, begin, end).size());
		r.setSuccess(true);
		return r;
	}
}
