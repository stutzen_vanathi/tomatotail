package co.stutzen.tomatotail.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Brand;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.BrandService;

@Controller
@RequestMapping("/brand")
public class BrandController extends SWFController<Brand> {

	@Autowired
	BrandService brandService;
	
	@Autowired
	private UserMapper userMapper;
	
	private static Logger log = LogManager.getRootLogger();

	@Autowired
	public BrandController(BrandService brandService) {
		super((SWFService<Brand>) brandService);
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public Result save(@RequestBody Brand brand, HttpSession session) {
		Result result = new Result();
		Brand duplicateBrand = brandService.duplicateBrand(brand.getName());
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		/*Here createdBy & updatedBy has String data type */
		brand.setCreatedby(String.valueOf((int)session.getAttribute("userId")));
		brand.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		if (duplicateBrand == null) {
			brand = brandService.create(brand);
			log.info("save () ===> BRAND NAME ==>"+brand.getName()+"MANUFACTURER ID ==>"+brand.getManufacturerid());
			result.setMessage("Brand information added successfully..");
			result.addData("brandid", brand.getBrandid());
			result.setSuccess(true);
		} else {
			result.setMessage("BrandName already exists");
			result.setSuccess(false);
			result.addData("brandId", duplicateBrand);
		}
		return result;
	}

	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	@ResponseBody
	public Result update(@RequestBody Brand brand, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		/*Here createdBy & updatedBy has String data type */
		brand.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		Brand duplicateBrand = brandService.duplicateBrand(brand.getName());
		if (duplicateBrand == null || (duplicateBrand != null && brand.getBrandid() == duplicateBrand.getBrandid())) {
			brandService.update(brand);
			log.info("update () ===> BRAND ID"+brand.getBrandid()+"BRAND NAME ==>"+brand.getName()+"MANUFACTURER ID ==>"+brand.getManufacturerid());
			result.setMessage("Brand information updated successfully..");
			result.setSuccess(true);
		} else {
			result.setMessage("Brand name already exist..");
			result.setSuccess(false);
		}
		return result;
	}

	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(@RequestBody Brand brand) {
		Result result = new Result();
		brandService.delete(brand.getBrandid());
		result.setMessage("Brand information deleted successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/brandList")
	@ResponseBody
	public Result listOfBrand(@RequestParam int start, @RequestParam int limit) {
		Result result = new Result();
		if (limit == 0) {
			limit = 100;
		}
		result.addData("list", brandService.brandList(start, limit));
		result.addData("total", brandService.getAll().size());
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/searchByName")
	@ResponseBody
	public Result searchBrandList(
			@RequestParam(value = "brandName", required = false, defaultValue = "") String brandName,
			@RequestParam(value = "manuName", required = false, defaultValue = "") String manuName,
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "") String limit) {
		Result result = new Result();
		log.info("searchBrandList () ===>"+"BRAND NAME ==>"+brandName+"MANUFACTURER NAME ==> "+manuName);
		int begin =0;
		 int end = 0;
		 	if(limit.isEmpty() || start.isEmpty()){
				begin =0;
				end = 100;
			}else{
				begin =Integer.parseInt(start);
				end = Integer.parseInt(limit);
			}
		result.addData("list", brandService.listOfBrand(brandName, manuName, begin, end));
		begin = 0;
		end = 0;
		result.addData("total", brandService.listOfBrand(brandName, manuName, begin, end).size());
		result.setSuccess(true);
		return result;
	}
}
