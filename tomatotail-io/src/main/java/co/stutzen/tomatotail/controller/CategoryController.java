package co.stutzen.tomatotail.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Category;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.CategoryService;

@Controller
@RequestMapping("/category")
public class CategoryController extends SWFController<Category> {

	@Autowired
	CategoryService categoryService;

	@Autowired
	private UserMapper userMapper;
	
	private static Logger log = LogManager.getRootLogger();
	
	@Autowired
	public CategoryController(CategoryService categoryService) {
		super((SWFService<Category>) categoryService);
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public Result save(@RequestBody Category category, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);	
		Category duplicateCategory = categoryService.duplicateCategoryName(category.getName());
		category.setCreatedby(String.valueOf((int)session.getAttribute("userId")));
		category.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		if (duplicateCategory == null) {
			log.info("save () ===> CATEGORY NAME ==>"+category.getName()+"IS SUB CATEGORY ==>"+category.getIsParent()+"PARENT ID ==>"+category.getParentId());
			categoryService.create(category);
			result.addData("id", category.getCategoryid());
			result.setMessage("Category information added successfully..");
			result.setSuccess(true);
		} else {
			result.setMessage("CategoryName already exists");
			result.setSuccess(false);
		}
		return result;
	}

	@RequestMapping("/modify")
	@ResponseBody
	public Result update(@RequestBody Category category, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);	
		category.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		Category duplicateCategory = categoryService.duplicateCategoryName(category.getName());
		if (duplicateCategory == null || (duplicateCategory != null && duplicateCategory.getCategoryid() == category.getCategoryid())) {
			log.info("update () ===> CATEGORY ID"+category.getCategoryid()+"CATEGORY NAME ==>"+category.getName()+"IS SUB CATEGORY ==>"+category.getIsParent()+"PARENT ID ==>"+category.getParentId());
			categoryService.update(category);
			result.setMessage("Category information updated successfully..");
			result.setSuccess(true);
		} else {
			result.setMessage("Category Name already exist..");
			result.setSuccess(false);
		}
		return result;
	}

	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(@RequestBody Category category) {
		Result result = new Result();
		categoryService.delete(category.getCategoryid());
		result.setMessage("Category information deleted successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/parentCategorys")
	@ResponseBody
	public Result listOfCategory() {
		Result result = new Result();
		result.addData("list", categoryService.listOfCategory());
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/categoryList")
	@ResponseBody
	public Result searchCategoryList( 
			@RequestParam(value = "categoryName", required = false, defaultValue = "") String categoryName,
			@RequestParam(value = "categoryId", required = false, defaultValue = "") String categoryId,
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "") String limit) {
		Result result = new Result();
		log.info("searchCategoryList () ===> CATEGORY NAME ==>"+categoryName+"CATEGORY ID"+categoryId);
		int id = 0;
		int begin = 0;
		int end = 0;
		if ((start.isEmpty() && limit.isEmpty()) || limit.isEmpty()) {
			begin = 0;
			end = 100;
		}
		if (!start.isEmpty())
			begin = Integer.parseInt(start);
		if (!limit.isEmpty())
			end = Integer.parseInt(limit);
		
		if(!categoryId.isEmpty())
			id = Integer.parseInt(categoryId);
		result.addData("list", categoryService.categoryList(categoryName, id, begin, end));
		begin =0;
		end =0;
		result.addData("total", categoryService.categoryList(categoryName, id, begin, end).size());
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/subCategorys")
	@ResponseBody
	public Result subCategoryList(
			@RequestParam(value = "categoryId", required = false, defaultValue = "") String categoryId) {
		Result result = new Result();
		log.info("subCategoryList () ===> CATEGORY ID ==>"+categoryId);
		if (!categoryId.isEmpty())result.addData("list", categoryService.getSubCategorysByParentId(categoryId));
		else
			result.addData("list", categoryService.getSubCategoryList());
		result.setSuccess(true);
		return result;
	}
}
