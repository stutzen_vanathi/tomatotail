package co.stutzen.tomatotail.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;

import co.stutzen.tomatotail.entity.ChannelPartner;
import co.stutzen.tomatotail.entity.ChannelPartnerStoreMapping;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.ChannelPartnerService;

@Controller
@RequestMapping("/channelPartner")
public class ChannelPartnerController extends SWFController<ChannelPartner> {

	@Autowired
	ChannelPartnerService partnerService;

	@Autowired
	private UserMapper userMapper;

	private static Logger log = LogManager.getRootLogger();

	@Autowired
	public ChannelPartnerController(ChannelPartnerService partnerService) {
		super(partnerService);
	}

	@RequestMapping("/save")
	@ResponseBody
	public Result saveChannelPartner(@RequestBody ChannelPartner cp, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		boolean issuccess = false;
		cp.setCreatedby(String.valueOf((int)session.getAttribute("userId")));
		cp.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		log.info("save () ===> CP CITY ==>"+cp.getCity()+ "CP ADDRESS"+cp.getAddress());
		partnerService.create(cp);
		issuccess = saveOrUpdate(cp);
		if (issuccess) {
			result.setMessage("Channel Partner & Store Mapping information saved sucessfully..");
			result.setSuccess(true);
		} else {
			result.setMessage("Channel Partner & Store Mapping information not saved sucessfully..");
			result.setSuccess(false);
		}
		return result;
	}

	@RequestMapping("/updateChannelPartner")
	@ResponseBody
	public Result updateChannelPartner(@RequestBody ChannelPartner cp, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		boolean issuccess = false;
		cp.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		log.info("save () ===> CP ID ==> "+cp.getPartnerId()+"CP CITY ==>"+cp.getCity()+ "CP ADDRESS"+cp.getAddress());
		partnerService.update(cp);
		issuccess = saveOrUpdate(cp);
		if (issuccess) {
			result.setMessage("Channel Partner & Store mapping information updated sucessfully..");
			result.setSuccess(true);
		} else {
			result.setMessage("Channel Partner & Store mapping information updated sucessfully..");
			result.setSuccess(true);
		}
		return result;
	}

	@RequestMapping("/channelPartnerById")
	@ResponseBody
	public Result channelPartnerDetailsById(@RequestParam("partnerId") int partnerId) {
		Result result = new Result();
		log.info("channelPartnerDetailsById () ==> CP ID ==>"+partnerId);
		result.addData("partnerDetails",partnerService.channelPartnerById(partnerId));
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/listOfPartner")
	@ResponseBody
	public Result listOfChannelPartner(@RequestParam int start, @RequestParam int limit) {
		Result result = new Result();
		if (limit == 0) {
			limit = 100;
		}
		result.addData("list", partnerService.channelPartnerList(start, limit));
		result.addData("total", partnerService.getAll().size());
		return result;
	}

	public boolean saveOrUpdate(ChannelPartner channelPartner) {
		ChannelPartnerStoreMapping partnerStoreMapping = new ChannelPartnerStoreMapping();
		partnerStoreMapping.setPartnerid(channelPartner.getPartnerId());
		partnerStoreMapping.setStoreList(channelPartner.getStoreList());
		boolean isSuccess = partnerService.saveStorewithPartner(partnerStoreMapping);
		return isSuccess;
	}

	@RequestMapping("/listAll")
	@ResponseBody
	public Result channelPartnerList() {
		Result result = new Result();
		result.addData("list", partnerService.channelPartners());
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/skuList")
	@ResponseBody
	public Result listOfSkuForChannelPartner(
			@RequestParam(value = "partnerId", required = false, defaultValue = "") String partnerId,
			@RequestParam(value = "sku", required = false, defaultValue = "") String sku,
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "") String limit) {
		Result result = new Result();
		log.info("listOfSkuForChannelPartner () ===> CP ID ==>"+partnerId + "SKU ==>"+sku);
		int id = 0;
		int begin = 0;
		int end = 0;
		if (!partnerId.isEmpty())
			id = Integer.parseInt(partnerId);
		if ((start.isEmpty() && limit.isEmpty()) || limit.isEmpty()) {
			begin = 0;
			end = 100;
		}
		if (!start.isEmpty())
			begin = Integer.parseInt(start);
		if (!limit.isEmpty())
			end = Integer.parseInt(limit);
		result.addData("list", partnerService.skuDetailsForPartner(id, sku, begin, end));
		begin = 0;
		end = 0;
		result.addData("total", partnerService.skuDetailsForPartner(id, sku, begin, end).size());
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/activeList")
	@ResponseBody
	public Result ActiveChannelPartnerList(
			@RequestParam(value = "partnerId", required = false, defaultValue = "") String partnerId,
			@RequestParam(value = "name", required = false, defaultValue = "") String name,
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "") String limit) {
		Result result = new Result();
		log.info("ActiveChannelPartnerList()===> partnerId==>" + partnerId
				+ "name==>" + name);
		int id = 0;
		int begin = 0;
		int end = 0;
		if (!partnerId.isEmpty())
			id = Integer.parseInt(partnerId);
		if ((start.isEmpty() && limit.isEmpty()) || limit.isEmpty()) {
			begin = 0;
			end = 100;
		}
		if (!start.isEmpty())
			begin = Integer.parseInt(start);
		if (!limit.isEmpty())
			end = Integer.parseInt(limit);
		result.addData("list",
				partnerService.getActiveList(id, name, begin, end));
		begin = 0;
		end = 0;
		result.addData("total",
				partnerService.getActiveList(id, name, begin, end).size());
		result.setSuccess(true);
		return result;

	}
}
