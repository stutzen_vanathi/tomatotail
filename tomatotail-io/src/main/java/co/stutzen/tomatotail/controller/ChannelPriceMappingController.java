package co.stutzen.tomatotail.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.ChannelPriceMapping;
import co.stutzen.tomatotail.entity.Product;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.ChannelPriceMappingService;

@Controller
@RequestMapping("/purchasePrice")
public class ChannelPriceMappingController extends
		SWFController<ChannelPriceMapping> {

	@Autowired
	private ChannelPriceMappingService mappingService;

	@Autowired
	private UserMapper userMapper;
	
	private static Logger log = LogManager.getRootLogger();
	
	@Autowired
	public ChannelPriceMappingController(ChannelPriceMappingService mappingService) {
		super((SWFService<ChannelPriceMapping>) mappingService);
	}

	@RequestMapping("/listOfProduct")
	@ResponseBody
	public Result listOfProduct(@RequestParam("name") String name,@RequestParam int start, @RequestParam int limit) {
		Result result = new Result();
		log.info("listOfProduct () ===> PROD NAME ==>"+name);
		List<Product> listOfProduct = mappingService.listOfProductByName(name,start, limit);
		if (listOfProduct.size() > 0) {
			result.addData("list", listOfProduct);
			start = 0;
			limit = 0;
			result.addData("total", mappingService.listOfProductByName(name, start, limit).size());
		} else {
			result.setMessage("Product Records Not Found...");
		}
		result.setSuccess(true);
		return result;
	}

	@RequestMapping(value="/save", method = RequestMethod.POST)
	@ResponseBody
	public Result savePurchasePrice( @RequestBody List<ChannelPriceMapping> pm, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);	
		for (ChannelPriceMapping cpm : pm) {
			cpm.setCreatedby(String.valueOf((int)session.getAttribute("userId")));
			cpm.setModifiedby(String.valueOf((int)session.getAttribute("userId")));
			ChannelPriceMapping price = mappingService.duplicatePriceMapping(cpm.getPartnerid(), cpm.getProductid());
			log.info("savePurchasePrice () ==> CP ID "+ cpm.getPartnerid() + "PRODUCT ID ==>"+cpm.getProductid() +"PURCHASE PRICE ==>"+cpm.getPurchaseprice()+ "MRP PRICE"+cpm.getMrp_Price()+ "SELLING PRICE ==>"+cpm.getSellingprice());
			if (price == null)
				mappingService.create(cpm);
		}
		result.setSuccess(true);
		result.setMessage("Product's Purchase Price saved successfully...");
		return result;
	}

	@RequestMapping("/partnerId")
	@ResponseBody
	public Result productPurchasePriceById(@RequestParam("partnerId") int partnerId, @RequestParam int start,@RequestParam int limit) {
		Result result = new Result();
		log.info("productPurchasePriceById() ===> CP ID ==>"+ partnerId);
		if (limit == 0) {
			limit = 100;
		}
		List<Product> list = mappingService.productPurchasePriceById(partnerId,start, limit);
		if (list.size() > 0) {
			result.addData("list", list);
			start = 0;
			limit = 0;
			result.addData(
					"total", mappingService.productPurchasePriceById(partnerId, start, limit).size());
		} else {
			result.setMessage("Product Records Not Found...");
		}
		result.setSuccess(true);
		return result;
	}

	@RequestMapping(value="/updatePrice", method = RequestMethod.POST)
	@ResponseBody
	public Result updatePurchasePrice(@RequestBody ChannelPriceMapping cpm, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		cpm.setModifiedby(String.valueOf((int)session.getAttribute("userId")));
		boolean isSuccess = false;
		/* Check duplicate product with parnter price mapping */
		ChannelPriceMapping price = mappingService.duplicatePriceMapping(cpm.getPartnerid(), cpm.getProductid());
		if (price == null || (price != null && price.getId() == cpm.getId())) {
			log.info("updatePurchasePrice () ==> PRICE MAPPING ID ==>"+cpm.getId()+"CP ID "+ cpm.getPartnerid() + "PRODUCT ID ==>"+cpm.getProductid() +"PURCHASE PRICE ==>"+cpm.getPurchaseprice()+ "MRP PRICE"+cpm.getMrp_Price()+ "SELLING PRICE ==>"+cpm.getSellingprice());
			mappingService.update(cpm);
			isSuccess = true;
			result.setMessage("Product's Purchase Price updated successfully...");
		} else {
			result.setSuccess(isSuccess);
			result.setMessage("Product Price with Partner mapping already exist");
		}
		return result;
	}
	/*update product purchase & selling price in Product Price Screen*/
	@RequestMapping(value ="/modify", method = RequestMethod.POST)
	@ResponseBody
	public Result updatePurchasePrice( @RequestBody List<ChannelPriceMapping> priceMapping, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		boolean isSuccess = false;
		for (ChannelPriceMapping cpm : priceMapping) {
			cpm.setModifiedby(String.valueOf((int)session.getAttribute("userId")));
			ChannelPriceMapping price = mappingService.duplicatePriceMapping(cpm.getPartnerid(), cpm.getProductid());
			if (price == null || (price != null && price.getId() == cpm.getId())) {
				if (cpm.getId() == 0){
					System.out.println(cpm.getId());
					cpm.setCreatedby(String.valueOf((int)session.getAttribute("userId")));
					log.info(" [SAVE] updatePurchasePrice () ==> CP ID "+ cpm.getPartnerid() + "PRODUCT ID ==>"+cpm.getProductid() +"PURCHASE PRICE ==>"+cpm.getPurchaseprice()+ "MRP PRICE"+cpm.getMrp_Price()+ "SELLING PRICE ==>"+cpm.getSellingprice());
					mappingService.create(cpm);
					result.setMessage("Product's Purchase Price saved successfully...");
				}
				else{
					System.out.println(cpm.getId()+">>>");
					log.info("[UPDATE] updatePurchasePrice () ==> PRICE MAPPING ID ==>"+cpm.getId()+"CP ID "+ cpm.getPartnerid() + "PRODUCT ID ==>"+cpm.getProductid() +"PURCHASE PRICE ==>"+cpm.getPurchaseprice()+ "MRP PRICE"+cpm.getMrp_Price()+ "SELLING PRICE ==>"+cpm.getSellingprice());
					mappingService.update(cpm);
					result.setMessage("Product's Purchase Price updated successfully...");
				}
				isSuccess = true;
			}
		}
		result.setSuccess(isSuccess);
		return result;
	}
}
