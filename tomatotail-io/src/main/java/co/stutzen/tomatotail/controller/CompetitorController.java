package co.stutzen.tomatotail.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Competitor;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.CompetitorService;

@Controller
@RequestMapping("/competitor")
public class CompetitorController extends SWFController<Competitor> {

	@Autowired
	CompetitorService competitorService;

	@Autowired
	private UserMapper userMapper;
	
	private static Logger log = LogManager.getRootLogger();
	
	@Autowired
	public CompetitorController(CompetitorService competitorService) {
		super((SWFService<Competitor>) competitorService);
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public Result save(@RequestBody Competitor competitor, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);	
		competitor.setCreatedby(String.valueOf((int)session.getAttribute("userId")));
		competitor.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		log.info("save () ==> COMPETITOR NAME ==>"+competitor.getName()+"COMPETITOR ADDRESS ==>"+competitor.getAddress());
		competitorService.create(competitor);
		result.setMessage("Competitor information added successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping(value ="/update", method = RequestMethod.POST)
	@ResponseBody
	public Result update(@RequestBody Competitor competitor, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);	
		competitor.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		log.info("update () ==> COMPETITOR ID ==>" +competitor.getCompetitorid()+"COMPETITOR NAME ==>"+competitor.getName()+"COMPETITOR ADDRESS ==>"+competitor.getAddress());
		competitorService.update(competitor);
		result.setMessage("Competitor information updated successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(@RequestBody Competitor competitor) {
		Result result = new Result();
		competitorService.delete(competitor.getCompetitorid());
		result.setMessage("Competitor information deleted successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/find")
	@ResponseBody
	public Result listOfCompetitorByName(@RequestParam("competitorName") String competitorName, @RequestParam("start") int start, @RequestParam("limit") int limit) {
		Result result = new Result();
		log.info("listOfCompetitorByName () ===> COMPETITOR NAME ==>"+competitorName);
		if (limit == 0) {
			limit = 100;
		}
		result.addData("list", competitorService.listOfCompetitorByName(competitorName, start, limit));
		result.addData("total", competitorService.listOfCompetitorByName(competitorName, 0, 0).size());
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/competitorList")
	@ResponseBody
	public Result listOfCompetitor() {
		Result result = new Result();
		result.addData("list", competitorService.getAll());
		result.addData("total", competitorService.getAll().size());
		result.setSuccess(true);
		return result;
	}

}
