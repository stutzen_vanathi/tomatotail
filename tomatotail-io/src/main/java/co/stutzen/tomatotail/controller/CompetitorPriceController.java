package co.stutzen.tomatotail.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.CompetitorPrice;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.CompetitorPriceService;

@Controller
@RequestMapping("/competitorPrice")
public class CompetitorPriceController extends SWFController<CompetitorPrice> {

	@Autowired
	CompetitorPriceService competitorPriceService;

	@Autowired
	private UserMapper userMapper;
	
	private static Logger log = LogManager.getRootLogger();
	
	@Autowired
	public CompetitorPriceController(CompetitorPriceService competitorPriceService) {
		super((SWFService<CompetitorPrice>) competitorPriceService);
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public Result save(@RequestBody List<CompetitorPrice> competitorPrice,@RequestParam("productId") Integer productId, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);	
		for(CompetitorPrice cp:competitorPrice){
			cp.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
			cp.setProductid(productId);
			System.out.println("Comp id"+cp.getId());
			if(cp.getId() == null){
				log.info(" [SAVE] save () ===> PRODUCT ID ==>"+ cp.getProductid() + "DISCOUNT PRICE ==>" + cp.getDiscountprice() + "COMPETITOR PRICE ==>"+cp.getCompetitorprice());
				cp.setCreatedby(String.valueOf((int)session.getAttribute("userId")));
				competitorPriceService.create(cp);
			}else if(cp.getId() != null && cp.getId() > 0){
				log.info(" [UPDATE] save () ===> COMPETITOR ID ==>"+cp.getCompetitorId()+ "PRODUCT ID ==>"+ cp.getProductid() + "DISCOUNT PRICE ==>" + cp.getDiscountprice() + "COMPETITOR PRICE ==>"+cp.getCompetitorprice());
				competitorPriceService.update(cp);
			}
		}
		result.setMessage("CompetitorPrice information added successfully..");
		result.setSuccess(true);
		return result;
	}


	@RequestMapping("/update")
	@ResponseBody
	public Result update(@RequestBody CompetitorPrice cp, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		cp.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		log.info(" [UPDATE] save () ===> COMPETITOR ID ==>"+cp.getCompetitorId()+ "PRODUCT ID ==>"+ cp.getProductid() + "DISCOUNT PRICE ==>" + cp.getDiscountprice() + "COMPETITOR PRICE ==>"+cp.getCompetitorprice());
		competitorPriceService.update(cp);
		result.setMessage("CompetitorPrice information updated successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(@RequestBody CompetitorPrice competitorPrice) {
		Result result = new Result();
		competitorPriceService.delete(competitorPrice.getId());
		result.setMessage("CompetitorPrice information deleted successfully..");
		result.setSuccess(true);
		return result;
	}


	@RequestMapping("/searchByName")
	@ResponseBody
	public Result listOfCompetitorPriceBySearch(@RequestParam("manuName")String manuName,
			@RequestParam("productName") String productName,@RequestParam("brandName") String brandName,@RequestParam("start")int start, @RequestParam("limit") int limit) {
		Result result = new Result();
		log.info("listOfCompetitorPriceBySearch () ===> MANU NAME ==>"+manuName+ "PROD NAME ==>"+productName+"BRAND NAME ==>"+brandName);
		result.addData("list",competitorPriceService.listOfCompetitorPriceBySearch(manuName, productName, brandName, start, limit));
		start = 0;
		limit = 0;
		result.addData("total",competitorPriceService.listOfCompetitorPriceBySearch(manuName, productName, brandName, start, limit).size());
		result.setSuccess(true);
		return result;
	}

}
