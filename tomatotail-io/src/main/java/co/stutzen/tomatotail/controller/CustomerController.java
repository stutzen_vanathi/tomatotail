package co.stutzen.tomatotail.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Customer;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController extends SWFController<Customer> {

	@Autowired
	private CustomerService service;
	
	@Autowired
	private UserMapper userMapper;
	
	private static Logger log = LogManager.getRootLogger();
	

	@Autowired
	public CustomerController(CustomerService service) {
		super((SWFService<Customer>) service);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody Result save(HttpSession session, @RequestBody Customer customer) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		customer.setCreatedBy((int)session.getAttribute("userId"));
		customer.setModifiedBy((int)session.getAttribute("userId"));
		log.info("save () ===> Customer Name ==>" +customer.getName());
		service.saveCustomer(customer);
		result.setMessage("Customer Details saved successfully.");
		result.addData("customerId", customer.getId());
		result.setSuccess(true);
		return result;
	}

	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	public @ResponseBody Result update(HttpSession session, @RequestBody Customer customer) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		customer.setModifiedBy((int)session.getAttribute("userId"));
		log.info("update () ===> Customer Id ==>" +customer.getId());
		service.updateCustomer(customer);
		result.setMessage("Customer Details updated successfully.");
		result.addData("customerId", customer.getId());
		result.setSuccess(true);
		return result;
	}

	@RequestMapping(value = "/listAll")
	public @ResponseBody Result customerList(
			@RequestParam(value = "name", required = false, defaultValue = "") String name,
			@RequestParam(value = "mobileNo", required = false, defaultValue = "") String mobileNo,
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "") String limit) {
		log.info("customerList () ===> NAME ==>"+name+"MOBILE NO ==>"+mobileNo);
		int begin = 0;
		int end = 0;
		if ((start.isEmpty() && limit.isEmpty()) || limit.isEmpty()) {
			begin = 0;
			end = 100;
		}
		if (!start.isEmpty())
			begin = Integer.parseInt(start);
		if (!limit.isEmpty())
			end = Integer.parseInt(limit);
		Result result = new Result();
		result.addData("list", service.getCustomerList(name, mobileNo, begin, end));
		begin = 0;
		end = 0;
		result.addData("total", service.getCustomerList(name, mobileNo, begin, end).size());
		result.setSuccess(true);
		return result;
	}

}
