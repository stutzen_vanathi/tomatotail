package co.stutzen.tomatotail.controller;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.CsvToBean;
import au.com.bytecode.opencsv.bean.HeaderColumnNameMappingStrategy;
import co.stutzen.tomatotail.entity.ExternalProductInfoWithBLOBs;
import co.stutzen.tomatotail.mapper.ExternalProductInfoMapper;
import co.stutzen.tomatotail.service.ExternalProductInfoService;

@Controller
@RequestMapping("/fileParser")
public class FileParser extends SWFController<ExternalProductInfoWithBLOBs> {

	@Autowired
	ExternalProductInfoMapper productInfo;

	@Autowired
	ExternalProductInfoService productInfoService;

	@Autowired
	public FileParser(ExternalProductInfoService productInfoService) {
		super((SWFService<ExternalProductInfoWithBLOBs>) productInfoService);
	}

	@RequestMapping("/save")
	public Result fileParser() {
		Result result = new Result();
		boolean isSuccess = false;
		int recordCount = 0;
		String msg = "";
		/* Map CSV file to Bean */
		CsvToBean<ExternalProductInfoWithBLOBs> csvToBean = new CsvToBean<ExternalProductInfoWithBLOBs>();
		CSVReader csvReader = null;
		InputStream in = getClass().getClassLoader().getResourceAsStream("Tomatotail.properties");
		Properties p = new Properties();
		try {
			p.load(in);
			String fileLocation = p.getProperty("file_location");
			csvReader = new CSVReader(new FileReader(fileLocation));
		} catch (IOException e) {
			System.out.println(e.getMessage());
		  }
		List<ExternalProductInfoWithBLOBs> productDetails = csvToBean.parse(setColumMapping(), csvReader);

		for (Object obj : productDetails) {
			ExternalProductInfoWithBLOBs product = (ExternalProductInfoWithBLOBs) obj;
			try{
			productInfo.insert(product);
			productInfoService.insertProductInfoToLocalProductTbl(product);
			isSuccess = true;
			recordCount ++;
			msg = recordCount +" "+ "records inserted successfuly"; 
			}catch(Exception e){
				isSuccess = false;
				msg = "Error occured while inserting  "+recordCount +"  th record";	
			}
		}
		result.setMessage(msg);
		result.setSuccess(isSuccess);
		return result;
	}

	private HeaderColumnNameMappingStrategy<ExternalProductInfoWithBLOBs> setColumMapping() {
		HeaderColumnNameMappingStrategy<ExternalProductInfoWithBLOBs> strategy = new HeaderColumnNameMappingStrategy<ExternalProductInfoWithBLOBs>();
		strategy.setType(ExternalProductInfoWithBLOBs.class);
		return strategy;
	}

}