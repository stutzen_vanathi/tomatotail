package co.stutzen.tomatotail.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.xmlpull.v1.XmlPullParserException;

import co.stutzen.tomatotail.entity.MagentoSalesOrder;
import co.stutzen.tomatotail.service.MagentoSalesOrderService;
import co.stutzen.tomatotail.util.CommonWSUtil;
import co.stutzen.tomatotail.util.KSoapUtil;

@Controller
@RequestMapping("/magento")
public class MagentoOrderController {

	@Autowired
	private CommonWSUtil wsUtil;

	@Autowired
	private MagentoSalesOrderService orderService;

	@RequestMapping("/orderList")
	@ResponseBody
	public Result getOrderList() {

		Result result = new Result();
		String methodName = "salesOrderList";
		Boolean isSuccess = false;
		String msg = " ";
		// get local db count
		int localSalesOrderCount = orderService.count();
		SoapObject orderResp = getSoapResponse(methodName, localSalesOrderCount);

		// save incrementid in order tbl
		List<MagentoSalesOrder> orders = orderService.saveOrderList(orderResp,
				localSalesOrderCount);

		// Save child info & update order tbl
		if (orders.size() != 0) {
			isSuccess = orderService.saveOrUpdateOrderDetails(orders, true);
			if (isSuccess)
				msg = msg + "Saved successfully";
			else
				msg = msg + "Not Saved successfully";
		} else if (orders.size() == 0) {
			isSuccess = true;
			msg = msg + "No Records to Save OR Update ";
		}
		
		result.setSuccess(isSuccess);
		result.setMessage(msg);
		return result;
	}

	private SoapObject getSoapResponse(String methodName,int localSalesOrderCount) {

		SoapObject orderResp = null;
		SoapSerializationEnvelope env = KSoapUtil.getEnvelope();

		SoapObject orderRequest = new SoapObject(wsUtil.nameSpace(), methodName);
		orderRequest.addProperty("sessionId", wsUtil.getSessionId());

		if (localSalesOrderCount > 0) {
			// Get recent updated date from db
			Date updatedDate = orderService.getRecentUpdatedDate();
			Timestamp recentUpdatedDate = new Timestamp(updatedDate.getTime());
			String condition = "gt";
			String filterBy = "updated_at";
			// complexFilterArray level
			SoapObject complexFilterArray = new SoapObject(wsUtil.nameSpace(), "complexFilterArray");
			complexFilterArray.addProperty("item", wsUtil.getComplexFilter(filterBy, condition, recentUpdatedDate.toString()));

			SoapObject complex_filter = new SoapObject(wsUtil.nameSpace(),"complex_filter");
			complex_filter.addProperty("complex_filter", complexFilterArray);
			System.out.println("##########");
			// Adding filters to request
			orderRequest.addProperty("filters", complex_filter);

		}
		
		env.setOutputSoapObject(orderRequest);
		try {
			// Webservice call
			wsUtil.getHttpTransport().call("", env);

			// Retrieval of resp
			orderResp = (SoapObject) env.getResponse();
			System.out.println("##########"+localSalesOrderCount);

			System.out.println("##########"+orderResp);
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return orderResp;
	}

	@RequestMapping("/productInfo")
	@ResponseBody
	public Result getProductInfo() {

		Result result = new Result();
		SoapObject productInfoRes = null;

		SoapSerializationEnvelope env = KSoapUtil.getEnvelope();
		SoapObject productInfoReq = new SoapObject(wsUtil.nameSpace(),"catalogProductList");
		productInfoReq.addProperty("sessionId", wsUtil.getSessionId());

		env.setOutputSoapObject(productInfoReq);
		try {
			// Webservice call
			wsUtil.getHttpTransport().call("", env);
			// Retrieval of resp
			productInfoRes = (SoapObject) env.getResponse();

		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(productInfoRes);
		result.addData("ProductInfo", productInfoRes);
		return result;
	}

	/*
	 * @RequestMapping("/productWithCategory")
	 * 
	 * @ResponseBody public Result getProductListByCategory() throws
	 * HttpResponseException, IOException, XmlPullParserException {
	 * 
	 * SoapObject orderResp = null; Result result = new Result();
	 * SoapSerializationEnvelope env = KSoapUtil.getEnvelope();
	 * 
	 * SoapObject orderRequest = new SoapObject(TTContants.MAGENTO_NAMESPACE,
	 * "catalogProductList"); orderRequest.addProperty("sessionId",
	 * wsUtil.getSession());
	 * 
	 * SoapObject associativeEntity = new
	 * SoapObject(TTContants.MAGENTO_NAMESPACE, "associativeEntity");
	 * associativeEntity.addProperty("key", "in");
	 * associativeEntity.addProperty("value", "13,10");
	 * 
	 * // complexFilter level SoapObject complexFilter = new
	 * SoapObject(TTContants.MAGENTO_NAMESPACE, "complexFilter");
	 * complexFilter.addProperty("key", "category_ids");
	 * complexFilter.addProperty("value", associativeEntity);
	 * 
	 * // complexFilterArray level SoapObject complexFilterArray = new
	 * SoapObject( TTContants.MAGENTO_NAMESPACE, "complexFilterArray");
	 * complexFilterArray.addProperty("item",
	 * KSoapUtil.getComplexFilter("category_ids", "in", "13,10"));
	 * 
	 * SoapObject complex_filter = new SoapObject( TTContants.MAGENTO_NAMESPACE,
	 * "complex_filter"); complex_filter.addProperty("complex_filter",
	 * complexFilterArray);
	 * 
	 * // Adding filters to request orderRequest.addProperty("filters",
	 * complex_filter);
	 * 
	 * env.setOutputSoapObject(orderRequest);
	 * 
	 * // Webservice call wsUtil.getHttpTransport().call("", env);
	 * 
	 * // Retrieval of resp orderResp = (SoapObject) env.getResponse();
	 * 
	 * System.out.println("Response::" + orderResp);
	 * System.out.println(orderResp); result.setMessage("Success");
	 * 
	 * return result; }
	 */
}
