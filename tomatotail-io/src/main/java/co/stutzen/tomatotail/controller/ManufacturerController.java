package co.stutzen.tomatotail.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Manufacturer;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.ManufacturerService;

@Controller
@RequestMapping("/manufacturer")
public class ManufacturerController extends SWFController<Manufacturer> {

	@Autowired
	public ManufacturerService manufacturerService;
	
	@Autowired
	private UserMapper userMapper;
	
	private static Logger log = LogManager.getRootLogger();
	

	@Autowired
	public ManufacturerController(ManufacturerService manufacturerService) {
		super((SWFService<Manufacturer>) manufacturerService);
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public Result save(@RequestBody Manufacturer manufacturer, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		manufacturer.setCreatedby(String.valueOf((int)session.getAttribute("userId")));
		manufacturer.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		Manufacturer duplicateManufacturer = manufacturerService.duplicateManufacturer(manufacturer.getName());
		if (duplicateManufacturer == null) {
			log.info("save () ===> NAME ==>"+manufacturer.getName()+ " ADDRESS ==>"+ manufacturer.getAddress());
			manufacturerService.create(manufacturer);
			result.setMessage("Manufacturer information added successfully..");
			result.setSuccess(true);
		} else {
			result.setMessage("ManufacturerName already exists");
			result.setSuccess(false);
		}
		return result;
	}

	@RequestMapping("/modify")
	@ResponseBody
	public Result update(@RequestBody Manufacturer manufacturer, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		manufacturer.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		log.info("save () ===> MANU ID ==>"+manufacturer.getManufacturerid()+"NAME ==>"+manufacturer.getName()+ " ADDRESS ==>"+ manufacturer.getAddress());
		manufacturerService.updateManufacturer(manufacturer);
		result.setMessage("Manufacturer information updated successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(@RequestBody Manufacturer manufacturer) {
		Result result = new Result();
		manufacturerService.delete(manufacturer.getManufacturerid());
		result.setMessage("Manufacturer information deleted successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/manufacturerList")
	public @ResponseBody
	Result listOfManufacturer(@RequestParam int start, @RequestParam int limit) {
		Result result = new Result();
		if (limit == 0) {
			limit = 100;
		}
		result.addData("list", manufacturerService.manufacturerList(start, limit));
		result.addData("total", manufacturerService.getAll().size());
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/find")
	@ResponseBody
	public Result manufactureByName(
			@RequestParam("manufacturerName") String manufacturerName, @RequestParam("start") int start, @RequestParam("limit") int limit) {
		Result result = new Result();
		log.info("manufactureByName () ==> MANU NAME ==>"+manufacturerName);
		if (limit == 0) {
			limit = 100;
		}
		result.addData("list", manufacturerService.manufacturerByName(manufacturerName, start, limit));
		start = 0;
		limit = 0;
		result.addData("total", manufacturerService.manufacturerByName(manufacturerName, start, limit).size());
		result.setSuccess(true);
		return result;
	}

}
