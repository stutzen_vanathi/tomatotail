package co.stutzen.tomatotail.controller;

import java.math.BigDecimal;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.MagentoOrderItem;
import co.stutzen.tomatotail.entity.MagentoSalesOrder;
import co.stutzen.tomatotail.entity.Order;
import co.stutzen.tomatotail.entity.ProductManufactureDate;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.MagentoSalesOrderService;
import co.stutzen.tomatotail.service.OrderService;
import co.stutzen.tomatotail.util.CommonConstants;

@Controller
@RequestMapping("/order")
public class OrderController extends SWFController<MagentoSalesOrder> {

	@Autowired
	private MagentoSalesOrderService magentoOrderService;

	@Autowired
	private OrderService orderService;
	
	@Autowired
	private UserMapper userMapper;
	
	private static Logger log = LogManager.getRootLogger();
	

	@Autowired
	public OrderController(MagentoSalesOrderService orderService) {
		super((SWFService<MagentoSalesOrder>) orderService);
	}

	@RequestMapping("/orderList")
	@ResponseBody
	public Result salesOrderList(@RequestParam int start, @RequestParam int limit) {
		Result result = new Result();
		if (limit == 0) {
			limit = 100;
		}
		result.addData("list", magentoOrderService.getOrderList(start, limit));
		result.addData("total", magentoOrderService.getAll().size());
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/search")
	@ResponseBody
	public Result salesOrderListBySearch(@RequestParam("storeId") String storeId, @RequestParam("status") String status,
			@RequestParam("start") int start, @RequestParam("limit") int limit) {
		log.info("salesOrderListBySearch () ===> STORE ID"+storeId +"STATUS ==>"+status);
		Result result = new Result();
		int id;
		if (status.isEmpty()) {
			status = "";
		} else if (storeId.isEmpty()) {
			storeId = "0";
		}
		if (status.isEmpty() && storeId.isEmpty()) {
			result.addData("list", magentoOrderService.getOrderList(start, limit));
			result.addData("total", magentoOrderService.getAll().size());
		} else {
			id = Integer.valueOf(storeId);
			result.addData("list", magentoOrderService.getOrdersBySearch(id,status, start, limit));
			start = 0;
			limit = 0;
			result.addData("total", magentoOrderService.getOrdersBySearch(id, status, start, limit).size());
		}
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/orderById")
	@ResponseBody
	public Result salesOrderByOrderId(@RequestParam("orderId") int orderId) {
		Result result = new Result();
		log.info("salesOrderByOrderId () ===> ORDER ID"+orderId);
		result.addData("order",magentoOrderService.getOrderDetailsByOrderId(orderId));
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/updateByOrderId")
	@ResponseBody
	public Result updateOrderStatusById(@RequestBody MagentoSalesOrder salesOrder) {
		Result result = new Result();
		magentoOrderService.updateOrderStatusById(salesOrder);
		result.setMessage("Order status updated successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/manufactureDate")
	@ResponseBody
	public Result saveManufactureDate(@RequestBody ProductManufactureDate p) {
		Result result = new Result();
		magentoOrderService.saveManufactureDate(p);
		result.setMessage("ManufactureDate saved successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/product")
	@ResponseBody
	public Result saveProduct(@RequestBody MagentoOrderItem orderItem, @RequestParam String status) {
		Result result = new Result();
		int productStatus = Integer.parseInt(status);
		magentoOrderService.saveOrderedItem(orderItem, productStatus);
		result.setMessage("Newly ordered item is saved successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/save")
	@ResponseBody
	public Result saveOrder(@RequestBody Order order, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		order.setCreatedBy((int)session.getAttribute("userId"));
		order.setModifiedBy((int)session.getAttribute("userId"));
		orderService.saveOrder(order);
		result.addData("orderId", order.getId());
		result.setMessage("Ordered item is saved successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/updateOrder")
	@ResponseBody
	public Result updateOrderStatus(@RequestBody Order order, HttpSession session,
			@RequestParam(value = "statusMode", required = false, defaultValue = "") String statusMode) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		order.setModifiedBy((int)session.getAttribute("userId"));
		String status = " ";
		if (!statusMode.isEmpty()) {
			int mode = Integer.parseInt(statusMode);
			if (mode == CommonConstants.REVISED_MODE)
				status = CommonConstants.REVISED;
			order.setStatus(status);
		}
		log.info("updateOrderStatus () ===> ORDER ID ==>" +order.getId()+"OUTLET ID ==>"+order.getOutletId()+"ADDRESS ID ==>"+order.getAddressId());
		orderService.updateOrder(order);
		result.addData("orderId", order.getId());
		result.setMessage("Ordered item updated successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/updateStatus")
	@ResponseBody
	public Result updateOrderStatusById(HttpSession session, @RequestParam int orderId, @RequestParam int statusMode) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		String status = "";
		if (statusMode == CommonConstants.ACCEPT_MODE)
			status = CommonConstants.APPROVED;
		else if (statusMode == CommonConstants.REJECT_MODE)
			status = CommonConstants.REJECTED;
		log.info("updateOrderStatus () ===> ORDER ID ==>" +orderId+"STATUS ==>"+statusMode);
		orderService.updateOrderStatus(orderId, userId, status);
		result.setMessage("Ordered item has been " + " " + status);
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/listAll")
	@ResponseBody
	public Result orderList(
			@RequestParam(value = "orderId", required = false, defaultValue = "") String orderId,
			@RequestParam(value = "status", required = false, defaultValue = "") String status,
			@RequestParam(value = "date", required = false, defaultValue = "") String date,
			@RequestParam(value = "name", required = false, defaultValue = "") String name,
			@RequestParam(value = "sourceType", required = false, defaultValue = "") String sourceType,
			@RequestParam(value = "outletId", required = false, defaultValue = "") String outletId,
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "") String limit) {
		Result result = new Result();
		log.info("orderList () ===> ORDER ID ==>"+orderId+"STATUS ==>"+status+ " DATE ==> "+ date+ "OUTLET ID ==>"+outletId + "NAME ==>"+name);
		int begin = 0;
		int end = 0;
		int id = 0;
		int id2 = 0;
		if (!orderId.isEmpty())
			id = Integer.parseInt(orderId);
		if (!outletId.isEmpty())
			id2 = Integer.parseInt(outletId);
		if ((start.isEmpty() && limit.isEmpty()) || limit.isEmpty()) {
			begin = 0;
			end = 100;
		}
		if (!start.isEmpty())
			begin = Integer.parseInt(start);
		if (!limit.isEmpty())
			end = Integer.parseInt(limit);
		if (id != 0) {
			BigDecimal totalPrice = orderService.getOrderTotalPrice(id);
			result.addData("totalPrice", totalPrice);
		}
		result.addData("list", orderService.orderList(id, status, date, name, id2, sourceType, begin, end));
		begin = 0;
		end = 0;
		result.addData("total", orderService.orderList(id, status, date, name, id2, sourceType, begin, end).size());
		result.setSuccess(true);
		return result;
	}
	
	//Ordered list to make bill
	@RequestMapping("/orderedItem")
	@ResponseBody
	public Result orderedItemList(
			@RequestParam(value = "orderId", required = false, defaultValue = "") String orderId,
			@RequestParam(value = "status", required = false, defaultValue = "") String status,
			@RequestParam(value = "date", required = false, defaultValue = "") String date,
			@RequestParam(value = "outletId", required = false, defaultValue = "") String outletId,
			@RequestParam(value = "start", required = false, defaultValue = "0") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "100") String limit) {
		Result result = new Result();
		log.info("orderList () ===> ORDER ID ==>"+orderId+"STATUS ==>"+status+ " DATE ==> "+ date+ "OUTLET ID ==>"+outletId );
		int begin = Integer.parseInt(start);
		int end = Integer.parseInt(limit);
		int oId = 0;
		int outId = 0;
		if (!orderId.isEmpty())
			oId = Integer.parseInt(orderId);
		if (!outletId.isEmpty())
			outId = Integer.parseInt(outletId);
		if (oId != 0) {
			BigDecimal totalPrice = orderService.getOrderTotalPrice(oId);
			result.addData("totalPrice", totalPrice);
		}
		result.addData("list", orderService.orderedItemList(oId, status, date, outId, begin, end));
		begin = 0;
		end = 0;
		result.addData("total", orderService.orderedItemList(oId, status, date, outId, begin, end).size());
		result.setSuccess(true);
		return result;
	}
}
