package co.stutzen.tomatotail.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.ProductWithBLOBs;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.ProductService;

@Controller
@RequestMapping("/product")
public class ProductController extends SWFController<ProductWithBLOBs> {

	@Autowired
	ProductService productService;
	
	@Autowired
	private UserMapper userMapper;
	
	private static Logger log = LogManager.getRootLogger();

	@Autowired
	public ProductController(ProductService productSservice) {
		super((SWFService<ProductWithBLOBs>) productSservice);
	}

	@RequestMapping(value = "/productNameDuplicate")
	@ResponseBody
	public Result checkProductName(@RequestParam("productName") String productName, @RequestParam("manufacturerId") Integer manufacturerId) {
		Result result = new Result();
		log.info("PRODUCT NAME ==>"+productName+"MANUFACTURER ID ==>"+manufacturerId);
		List<ProductWithBLOBs> list = productService.duplicateProductList(productName, manufacturerId);
		ProductWithBLOBs p = new ProductWithBLOBs();
		p.setName(productName);
		list.add(p);
		result.addData("list", list);
		result.setSuccess(true);
		return result;
	}

	@RequestMapping(value = "/save")
	@ResponseBody
	public Result save(@RequestBody ProductWithBLOBs product, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		product.setCreatedby(String.valueOf((int)session.getAttribute("userId")));
		product.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		String duplicateSku = productService.duplicateProductSku(product.getSKU());
		if (duplicateSku == null) {
			log.info("save () ===> PRODUCT NAME ==>"+product.getName()+"SKU ==>"+ product.getProductSku());
			productService.saveProduct(product);
			result.setMessage("Product information added successfully..");
			result.setSuccess(true);
		} else {
			result.setMessage("Product's SKU already exist..");
			result.setSuccess(false);
		}
		result.addData("productId", product.getProductid());
		return result;
	}

	@RequestMapping(value="/updatedata", method = RequestMethod.POST)
	@ResponseBody
	public Result update(@RequestBody ProductWithBLOBs product, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		product.setUpdatedby(String.valueOf((int)session.getAttribute("userId")));
		log.info("update () ===> PRODUCT ID"+product.getProductid() +"PRODUCT NAME ==>"+product.getName()+"SKU ==>"+ product.getProductSku());
		productService.updateProduct(product);
		result.setMessage("Product information updated successfully..");
		result.setSuccess(true);
		result.addData("productId", product.getProductid());
		return result;
	}

	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(@RequestBody ProductWithBLOBs product) {
		Result result = new Result();
		productService.delete(product.getProductid());
		result.setMessage("Product information deleted successfully..");
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/searchByName")
	@ResponseBody
	public Result listOfProductBySearch(
			@RequestParam("brandName") String brandName,
			@RequestParam("manuName") String manuName,
			@RequestParam("productName") String productName,
			@RequestParam("start") int start, @RequestParam("limit") int limit) {
		Result result = new Result();
		log.info("listOfProductBySearch () ===> BRAND NAME ==>"+brandName + "MANU NAME ==>"+manuName+"PRODUCT NAME ==>"+productName);
		if (limit == 0) {
			limit = 100;
		}
		result.addData("list", productService.listOfProduct(productName,brandName, manuName, start, limit));
		start = 0;
		limit = 0;
		result.addData("total",productService.listOfProduct(productName, brandName, manuName,start, limit).size());
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/productWithCompetitor")
	@ResponseBody
	public Result productWithCompetitor(@RequestParam("productId") int productId) {
		Result result = new Result();
		log.info("productWithCompetitor () ===> PRODUCT ID ==>"+productId);
		result.addData("list",productService.productWithCompetitorPrices(productId));
		result.setSuccess(true);
		return result;
	}

	@RequestMapping("/productList")
	@ResponseBody
	public Result productList(
			@RequestParam(value = "productName", required = false, defaultValue = "") String productName,
			@RequestParam(value = "partnerId", required = false, defaultValue = "") String partnerId,
			@RequestParam(value = "sku", required = false, defaultValue = "") String sku,
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "") String limit) {
		Result result = new Result();
		log.info("productList () ===> PARTNER ID ==>"+partnerId + "PRODUCT NAME ==>"+productName+"SKU ==>"+sku);
		int begin = 0;
		int end = 0;
		int id = 0;
		if(!partnerId.isEmpty())
			id = Integer.parseInt(partnerId);
		if ((start.isEmpty() && limit.isEmpty()) || limit.isEmpty()) {
			begin = 0;
			end = 100;
		}
		if (!start.isEmpty())
			begin = Integer.parseInt(start);
		if (!limit.isEmpty())
			end = Integer.parseInt(limit);
		result.addData("list", productService.productList(productName, id, sku, begin, end));
		begin = 0;
		end = 0;
		result.addData("total", productService.productList(productName, id, sku, begin, end).size());
		result.setSuccess(true);
		return result;
	}
	
	@RequestMapping("/listAll")
	@ResponseBody
	public Result productListAll(
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "") String limit) {
		Result result = new Result();
		int begin = 0;
		int end = 0;
		if ((start.isEmpty() && limit.isEmpty()) || limit.isEmpty()) {
			begin = 0;
			end = 100;
		}
		if (!start.isEmpty())
			begin = Integer.parseInt(start);
		if (!limit.isEmpty())
			end = Integer.parseInt(limit);
		result.addData("list", productService.productListAll(begin, end));
		begin = 0;
		end = 0;
		result.addData("total", productService.productListAll(begin, end).size());
		result.setSuccess(true);
		return result;
	}

	
}
