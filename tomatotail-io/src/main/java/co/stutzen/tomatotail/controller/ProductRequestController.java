package co.stutzen.tomatotail.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.ProductRequestWithBLOBs;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.ProductRequestService;
import co.stutzen.tomatotail.util.CommonConstants;

@Controller
@RequestMapping("/prodRequest")
public class ProductRequestController extends SWFController<ProductRequestWithBLOBs>{

	@Autowired
	private ProductRequestService requestService;
	
	@Autowired
	private UserMapper userMapper;
	
	private static Logger log = LogManager.getRootLogger();
	
	@Autowired
	public ProductRequestController(ProductRequestService requestService) {
		super((SWFService<ProductRequestWithBLOBs>)requestService);
	}
	
	@RequestMapping (value = "/saveOrUpdate", method=RequestMethod.POST)
	@ResponseBody
	public Result save(@RequestBody List<ProductRequestWithBLOBs> prodReq, HttpSession session){
		Result result = new  Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);	
		String msg = "";
		for(ProductRequestWithBLOBs prodRequest : prodReq){
			log.info("[LIST]save () ===> PRODUCT NAME ==>"+prodRequest.getProductName() + "PartnerId ==>"+prodRequest.getPartnerId());
			prodRequest.setModifiedBy(((int)session.getAttribute("userId")));
			if(prodRequest.getId() != null && prodRequest.getId() != 0){
				requestService.modify(prodRequest);
				msg = "updated";
			}
			if(prodRequest.getId() != null && prodRequest.getId() == 0){
				prodRequest.setCreatedBy(((int)session.getAttribute("userId")));
				prodRequest.setStatus(CommonConstants.INITIATED);
				requestService.save(prodRequest);
				msg = "created";
			}
		}
		result.setMessage("Product Request has been "+msg+" Scuccessfully" );
		result.setSuccess(true);
		return result;
	}
	
	@RequestMapping (value = "/listAll")
	@ResponseBody
	public Result list(
			@RequestParam(value = "prodReqId", required = false, defaultValue = "") String prodReqId,
			@RequestParam(value = "partnerCity", required = false, defaultValue = "") String partnerCity,
			@RequestParam(value = "prodName", required = false, defaultValue = "") String prodName,
			@RequestParam(value = "partnerId", required = false, defaultValue = "") String partnerId,
			@RequestParam(value = "status", required = false, defaultValue = "") String status,
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "") String limit) {
		Result result = new  Result();
		log.info("list () ===> PRODUCT CITY ==>"+partnerCity + "PARTNER ID ==>"+partnerId + "PRODUCT REQ ID ==>"+prodReqId + "PRODUCT NAME ==>"+prodName);
		int begin = 0;
		int end = 0;
		if(limit.isEmpty() || start.isEmpty()){
			begin =0;
			end = 100;
		}else{
			begin =Integer.parseInt(start);
			end = Integer.parseInt(limit);
		}	
		result.addData("list", requestService.listAll(prodReqId, partnerId, partnerCity, prodName, status, begin, end));
		begin = 0;
		end = 0;
		result.addData("total", requestService.listAll(prodReqId, partnerId, partnerCity, prodName, status, begin, end).size());
		result.setSuccess(true);
		return result;
	}
	
	@RequestMapping (value = "/statusUpdate")
	@ResponseBody
	public Result statusUpdate(@RequestParam(value = "prodReqId", required = false) int prodReqId,
			@RequestParam(value = "status", required = false, defaultValue = "") String status, HttpSession session) {
		Result result = new  Result();
		log.info("statusUpdate () ===>" + "STATUS ==>"+status + "PRODUCT REQ ID"+prodReqId);
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		requestService.updateStatus(prodReqId, status, userId);
		result.setMessage("Status has updated successfuly");
		result.setSuccess(true);
		return result;
	}
}
