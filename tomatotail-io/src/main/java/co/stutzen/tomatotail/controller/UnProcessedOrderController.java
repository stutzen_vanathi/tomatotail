package co.stutzen.tomatotail.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.UnProcessedOrder;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.UnProcessedOrderService;
import co.stutzen.tomatotail.util.CommonConstants;

@Controller
@RequestMapping("/unProcessedOrder")
public class UnProcessedOrderController extends SWFController<UnProcessedOrder> {

	@Autowired
	private UnProcessedOrderService unProcessedOrderService;

	@Autowired
	private UserMapper userMapper;
	
	private static Logger log = LogManager.getRootLogger();
	
	@Autowired
	public UnProcessedOrderController(UnProcessedOrderService unProcessedOrder) {
		super((SWFService<UnProcessedOrder>) unProcessedOrder);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody Result save(HttpSession session, @RequestBody UnProcessedOrder unProcessedOrder) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		unProcessedOrder.setCreatedBy((int)session.getAttribute("userId"));
		unProcessedOrder.setModifiedBy((int)session.getAttribute("userId"));
		unProcessedOrder.setStatus(CommonConstants.INITIATED);
		log.info("save () ===> ADDRESS ID ==>"+unProcessedOrder.getAddressId()+"CUSTOMER ID ==>"+unProcessedOrder.getCustomerId()+"ORDER DETAILS ==>"+unProcessedOrder.getOrderDetails());
		unProcessedOrderService.create(unProcessedOrder);
		result.setMessage("UnProcessedOrder saved successfully.");
		result.setSuccess(true);
		return result;
	}
	
	@RequestMapping(value = "/listAll")
	public @ResponseBody Result unprocessedOrderList(
			@RequestParam(value = "unProcessedOrderId", required = false, defaultValue = "") String unProcessedOrderId,
			@RequestParam(value = "date", required = false, defaultValue = "") String date,
			@RequestParam(value = "status", required = false, defaultValue = "") String status,
			@RequestParam(value = "name", required = false, defaultValue = "") String name,
			@RequestParam(value = "mobile", required = false, defaultValue = "") String mobile,
			@RequestParam(value = "start", required = false, defaultValue = "") String start,
			@RequestParam(value = "limit", required = false, defaultValue = "") String limit) {
		log.info("unprocessedOrderList () ===> ORDER ID ==>"+unProcessedOrderId+"DATE ==>"+date+"STATUS ==>"+status+"NMAE ==>"+name+"MOBILE ==>"+mobile);
		int begin = 0;
		int end = 0;
		int id = 0;
		if(!unProcessedOrderId.isEmpty())
			id = Integer.parseInt(unProcessedOrderId);
		if ((start.isEmpty() && limit.isEmpty()) || limit.isEmpty()) {
			begin = 0;
			end = 100;
		}
		if (!start.isEmpty())
			begin = Integer.parseInt(start);
		if (!limit.isEmpty())
			end = Integer.parseInt(limit);
		Result result = new Result();
		result.addData("list", unProcessedOrderService.getUnProcessedOrderList(id,date, status, name, mobile,begin, end));
		begin = 0;
		end = 0;
		result.addData("total", unProcessedOrderService.getUnProcessedOrderList(id,date, status,  name, mobile,begin, end).size());
		result.setSuccess(true);
		return result;
	}
}
