package co.stutzen.tomatotail.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.controller.SWFController;

import co.stutzen.tomatotail.entity.User;
import co.stutzen.tomatotail.entity.UserRole;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController extends SWFController<User> {

	@Autowired
	private UserService userService;

	@Autowired
	private UserMapper userMapper;

	private static Logger log = LogManager.getRootLogger();


	@Autowired
	public UserController(UserService userService) {
		super(userService);
	}

	/*@RequestMapping("/list")
	public @ResponseBody Result get() {
		return new Result();
	}*/

	@RequestMapping("/save")
	@ResponseBody
	public Result save(@RequestBody User user, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		user.setCreatedBy((int)session.getAttribute("userId"));
		user.setUpdatedBy((int)session.getAttribute("userId"));
		User duplicateEmail = userService.duplicateEmail(user.getEmail());
		User duplicateUser = userService.duplicateUser(user.getUsername());
		if (duplicateEmail != null || duplicateUser != null) {
			String msg = "";
			if (duplicateEmail != null)
				msg = msg + "Email,";
			if (duplicateUser != null)
				msg = msg + "UserName ";
			result.setMessage(msg + "is already exist..");
			result.setSuccess(false);
		}
		if (duplicateEmail == null && duplicateUser == null) {
			log.info("save () ===> USER NAME ==>"+user.getUsername()+"NAME ==>"+user.getName());
			userService.create(user);
			UserRole role = new UserRole();
			role.setUsername(user.getUsername());
			role.setRolename(user.getRole().getRolename());
			userService.saveRole(role);
			result.setMessage("User information saved successfully...");
			result.setSuccess(true);
		}
		return result;
	}

	@Override
	@RequestMapping("/modify")
	@ResponseBody
	public Result update(@RequestBody User user, HttpSession session) {
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		user.setUpdatedBy((int)session.getAttribute("userId"));
		User duplicateEmail = userService.duplicateEmail(user.getEmail());
		User duplicateUser = userService.duplicateUser(user.getUsername());
		String oldUserName = userService.oldUserName(user.getUserid());
		if ((duplicateEmail != null && duplicateEmail.getUserid() != user.getUserid())
				|| (duplicateUser != null && duplicateUser.getUserid() != user.getUserid())) {
			String msg = "";
			if (duplicateEmail != null && duplicateEmail.getUserid() != user.getUserid())
				msg = msg + "Email,";
			if (duplicateUser != null && duplicateUser.getUserid() != user.getUserid())
				msg = msg + "UserName ";
			result.setMessage(msg + "is already exist..");
			result.setSuccess(false);
			return result;
		}
		if ((duplicateEmail == null || (duplicateEmail != null && duplicateEmail.getUserid() == user.getUserid()))
				|| (duplicateUser == null || (duplicateUser != null && duplicateUser.getUserid() == user.getUserid()))) {
			log.info("update () ===> USER ID ==>"+user.getUserid()+"USER NAME ==>"+user.getUsername()+"NAME ==>"+user.getName());
			userService.updateUser(user);
			userService.updateRole(user, oldUserName);
			result.setMessage("User information updated successfully...");
			result.setSuccess(true);
		}
		return result;
	}

	@RequestMapping("/changePassword")
	@ResponseBody
	public Result changePassword(@RequestBody User user, HttpSession session) {
		boolean isSuccess = false;
		String msg = null;
		Result result = new Result();
		int userId = 0;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			userId = userMapper.getUserIdByUserName(authentication.getName());
		session.setAttribute("userId", userId);
		user.setUpdatedBy((int)session.getAttribute("userId"));
		user.setIsactive(Byte.parseByte("1"));
		User userObjFromDB = userService.getPasswordByUserName(user.getUsername());
		if (!user.getNewPassword().equals(user.getConfirmPassword())) {
			msg = "New password and confirm password is not matched..";
		} else {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			userObjFromDB.setPassword(passwordEncoder.encode(user.getNewPassword()));
			userObjFromDB.setUsername(user.getUsername());
			userObjFromDB.setUserid(userObjFromDB.getUserid());
			/*
			 * userObjFromDB.setUpdatedBy(Integer.parseInt(session
			 * .getAttribute("userId").toString()));
			 */
			log.info("changePassword () ===> USER NAME ==>"+user.getUsername()+"NAME ==>"+user.getName());
			userService.updatePassword(userObjFromDB);
			isSuccess = true;
			msg = "Password updated successfully..";
		}
		result.setMessage(msg);
		result.setSuccess(isSuccess);
		return result;
	}

	@RequestMapping(value = "/userInfo")
	@ResponseBody
	public Result userInfo(
			@RequestParam(value = "userId", required = false, defaultValue = "0") String userId,
			HttpSession session) {
		Result result = new Result();
		int id = 0;
		User user = null;
		if (session != null && session.getAttribute("userId") != null) {
			id = (Integer) session.getAttribute("userId");
		} else if (!userId.equalsIgnoreCase("0")) {
			id = Integer.parseInt(userId);
		}
		user = userService.getUserInfo(id);
		result.addData("user", user);
		result.setSuccess(true);
		return result;
	}
}
