package co.stutzen.tomatotail.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChannelPartner {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_channel_partner.partner_id
	 * 
	 * @mbggenerated
	 */
	private Integer partnerId;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_channel_partner.name
	 * 
	 * @mbggenerated
	 */
	private String name;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_channel_partner.address
	 * 
	 * @mbggenerated
	 */
	private String address;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_channel_partner.createdby
	 * 
	 * @mbggenerated
	 */
	private String createdby;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_channel_partner.updatedby
	 * 
	 * @mbggenerated
	 */
	private String updatedby;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_channel_partner.createdtime
	 * 
	 * @mbggenerated
	 */
	private Date createdtime;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_channel_partner.updatedtime
	 * 
	 * @mbggenerated
	 */
	private Date updatedtime;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_channel_partner.isactive
	 * 
	 * @mbggenerated
	 */
	private Byte isactive;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_channel_partner.phone_number
	 * 
	 * @mbggenerated
	 */
	private String phoneNumber;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_channel_partner.mobile_number
	 * 
	 * @mbggenerated
	 */
	private String mobileNumber;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_channel_partner.city
	 * 
	 * @mbggenerated
	 */
	private String city;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_channel_partner.comments
	 * 
	 * @mbggenerated
	 */
	private String comments;

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_channel_partner.partner_id
	 * 
	 * @return the value of tbl_channel_partner.partner_id
	 * @mbggenerated
	 */
	public Integer getPartnerId() {
		return partnerId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_channel_partner.partner_id
	 * 
	 * @param partnerId
	 *            the value for tbl_channel_partner.partner_id
	 * @mbggenerated
	 */
	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_channel_partner.name
	 * 
	 * @return the value of tbl_channel_partner.name
	 * @mbggenerated
	 */
	public String getName() {
		return name;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_channel_partner.name
	 * 
	 * @param name
	 *            the value for tbl_channel_partner.name
	 * @mbggenerated
	 */
	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_channel_partner.address
	 * 
	 * @return the value of tbl_channel_partner.address
	 * @mbggenerated
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_channel_partner.address
	 * 
	 * @param address
	 *            the value for tbl_channel_partner.address
	 * @mbggenerated
	 */
	public void setAddress(String address) {
		this.address = address == null ? null : address.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_channel_partner.createdby
	 * 
	 * @return the value of tbl_channel_partner.createdby
	 * @mbggenerated
	 */
	public String getCreatedby() {
		return createdby;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_channel_partner.createdby
	 * 
	 * @param createdby
	 *            the value for tbl_channel_partner.createdby
	 * @mbggenerated
	 */
	public void setCreatedby(String createdby) {
		this.createdby = createdby == null ? null : createdby.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_channel_partner.updatedby
	 * 
	 * @return the value of tbl_channel_partner.updatedby
	 * @mbggenerated
	 */
	public String getUpdatedby() {
		return updatedby;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_channel_partner.updatedby
	 * 
	 * @param updatedby
	 *            the value for tbl_channel_partner.updatedby
	 * @mbggenerated
	 */
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby == null ? null : updatedby.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_channel_partner.createdtime
	 * 
	 * @return the value of tbl_channel_partner.createdtime
	 * @mbggenerated
	 */
	public Date getCreatedtime() {
		return createdtime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_channel_partner.createdtime
	 * 
	 * @param createdtime
	 *            the value for tbl_channel_partner.createdtime
	 * @mbggenerated
	 */
	public void setCreatedtime(Date createdtime) {
		this.createdtime = createdtime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_channel_partner.updatedtime
	 * 
	 * @return the value of tbl_channel_partner.updatedtime
	 * @mbggenerated
	 */
	public Date getUpdatedtime() {
		return updatedtime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_channel_partner.updatedtime
	 * 
	 * @param updatedtime
	 *            the value for tbl_channel_partner.updatedtime
	 * @mbggenerated
	 */
	public void setUpdatedtime(Date updatedtime) {
		this.updatedtime = updatedtime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_channel_partner.isactive
	 * 
	 * @return the value of tbl_channel_partner.isactive
	 * @mbggenerated
	 */
	public Byte getIsactive() {
		return isactive;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_channel_partner.isactive
	 * 
	 * @param isactive
	 *            the value for tbl_channel_partner.isactive
	 * @mbggenerated
	 */
	public void setIsactive(Byte isactive) {
		this.isactive = isactive;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_channel_partner.phone_number
	 * 
	 * @return the value of tbl_channel_partner.phone_number
	 * @mbggenerated
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_channel_partner.phone_number
	 * 
	 * @param phoneNumber
	 *            the value for tbl_channel_partner.phone_number
	 * @mbggenerated
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber == null ? null : phoneNumber.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_channel_partner.mobile_number
	 * 
	 * @return the value of tbl_channel_partner.mobile_number
	 * @mbggenerated
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_channel_partner.mobile_number
	 * 
	 * @param mobileNumber
	 *            the value for tbl_channel_partner.mobile_number
	 * @mbggenerated
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber == null ? null : mobileNumber.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_channel_partner.city
	 * 
	 * @return the value of tbl_channel_partner.city
	 * @mbggenerated
	 */
	public String getCity() {
		return city;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_channel_partner.city
	 * 
	 * @param city
	 *            the value for tbl_channel_partner.city
	 * @mbggenerated
	 */
	public void setCity(String city) {
		this.city = city == null ? null : city.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_channel_partner.comments
	 * 
	 * @return the value of tbl_channel_partner.comments
	 * @mbggenerated
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_channel_partner.comments
	 * 
	 * @param comments
	 *            the value for tbl_channel_partner.comments
	 * @mbggenerated
	 */
	public void setComments(String comments) {
		this.comments = comments == null ? null : comments.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_channel_partner
	 * 
	 * @mbggenerated
	 */
	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		ChannelPartner other = (ChannelPartner) that;
		return (this.getPartnerId() == null ? other.getPartnerId() == null
				: this.getPartnerId().equals(other.getPartnerId()))
				&& (this.getName() == null ? other.getName() == null : this
						.getName().equals(other.getName()))
				&& (this.getAddress() == null ? other.getAddress() == null
						: this.getAddress().equals(other.getAddress()))
				&& (this.getCreatedby() == null ? other.getCreatedby() == null
						: this.getCreatedby().equals(other.getCreatedby()))
				&& (this.getUpdatedby() == null ? other.getUpdatedby() == null
						: this.getUpdatedby().equals(other.getUpdatedby()))
				&& (this.getCreatedtime() == null ? other.getCreatedtime() == null
						: this.getCreatedtime().equals(other.getCreatedtime()))
				&& (this.getUpdatedtime() == null ? other.getUpdatedtime() == null
						: this.getUpdatedtime().equals(other.getUpdatedtime()))
				&& (this.getIsactive() == null ? other.getIsactive() == null
						: this.getIsactive().equals(other.getIsactive()))
				&& (this.getPhoneNumber() == null ? other.getPhoneNumber() == null
						: this.getPhoneNumber().equals(other.getPhoneNumber()))
				&& (this.getMobileNumber() == null ? other.getMobileNumber() == null
						: this.getMobileNumber()
								.equals(other.getMobileNumber()))
				&& (this.getCity() == null ? other.getCity() == null : this
						.getCity().equals(other.getCity()))
				&& (this.getComments() == null ? other.getComments() == null
						: this.getComments().equals(other.getComments()));
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_channel_partner
	 * 
	 * @mbggenerated
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((getPartnerId() == null) ? 0 : getPartnerId().hashCode());
		result = prime * result
				+ ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result
				+ ((getAddress() == null) ? 0 : getAddress().hashCode());
		result = prime * result
				+ ((getCreatedby() == null) ? 0 : getCreatedby().hashCode());
		result = prime * result
				+ ((getUpdatedby() == null) ? 0 : getUpdatedby().hashCode());
		result = prime
				* result
				+ ((getCreatedtime() == null) ? 0 : getCreatedtime().hashCode());
		result = prime
				* result
				+ ((getUpdatedtime() == null) ? 0 : getUpdatedtime().hashCode());
		result = prime * result
				+ ((getIsactive() == null) ? 0 : getIsactive().hashCode());
		result = prime
				* result
				+ ((getPhoneNumber() == null) ? 0 : getPhoneNumber().hashCode());
		result = prime
				* result
				+ ((getMobileNumber() == null) ? 0 : getMobileNumber()
						.hashCode());
		result = prime * result
				+ ((getCity() == null) ? 0 : getCity().hashCode());
		result = prime * result
				+ ((getComments() == null) ? 0 : getComments().hashCode());
		return result;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_channel_partner
	 * 
	 * @mbggenerated
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", partnerId=").append(partnerId);
		sb.append(", name=").append(name);
		sb.append(", address=").append(address);
		sb.append(", createdby=").append(createdby);
		sb.append(", updatedby=").append(updatedby);
		sb.append(", createdtime=").append(createdtime);
		sb.append(", updatedtime=").append(updatedtime);
		sb.append(", isactive=").append(isactive);
		sb.append(", phoneNumber=").append(phoneNumber);
		sb.append(", mobileNumber=").append(mobileNumber);
		sb.append(", city=").append(city);
		sb.append(", comments=").append(comments);
		sb.append("]");
		return sb.toString();
	}

	private List<Store> storeList = new ArrayList<Store>();

	private List<Product> products = new ArrayList<Product>();

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<Store> getStoreList() {
		return storeList;
	}

	public void setStoreList(List<Store> storeList) {
		this.storeList = storeList;
	}

	private List<ChannelPartnerStoreMapping> partnerStoreList = new ArrayList<ChannelPartnerStoreMapping>();

	public List<ChannelPartnerStoreMapping> getPartnerStoreList() {
		return partnerStoreList;
	}

	public void setPartnerStoreList(
			List<ChannelPartnerStoreMapping> partnerStoreList) {
		this.partnerStoreList = partnerStoreList;
	}

}