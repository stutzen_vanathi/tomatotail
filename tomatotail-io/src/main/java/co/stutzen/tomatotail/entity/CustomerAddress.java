package co.stutzen.tomatotail.entity;

import java.util.Date;

public class CustomerAddress {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.id
	 * @mbggenerated
	 */
	private Integer id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.customer_id
	 * @mbggenerated
	 */
	private Integer customerId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.address
	 * @mbggenerated
	 */
	private String address;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.city
	 * @mbggenerated
	 */
	private String city;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.email
	 * @mbggenerated
	 */
	private String email;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.pincode
	 * @mbggenerated
	 */
	private String pincode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.state
	 * @mbggenerated
	 */
	private String state;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.country
	 * @mbggenerated
	 */
	private String country;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.created_by
	 * @mbggenerated
	 */
	private Integer createdBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.modified_by
	 * @mbggenerated
	 */
	private Integer modifiedBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.created_on
	 * @mbggenerated
	 */
	private Date createdOn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.modified_on
	 * @mbggenerated
	 */
	private Date modifiedOn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.is_active
	 * @mbggenerated
	 */
	private Boolean isActive;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_address.comments
	 * @mbggenerated
	 */
	private String comments;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.id
	 * @return  the value of tbl_address.id
	 * @mbggenerated
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.id
	 * @param id  the value for tbl_address.id
	 * @mbggenerated
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.customer_id
	 * @return  the value of tbl_address.customer_id
	 * @mbggenerated
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.customer_id
	 * @param customerId  the value for tbl_address.customer_id
	 * @mbggenerated
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.address
	 * @return  the value of tbl_address.address
	 * @mbggenerated
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.address
	 * @param address  the value for tbl_address.address
	 * @mbggenerated
	 */
	public void setAddress(String address) {
		this.address = address == null ? null : address.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.city
	 * @return  the value of tbl_address.city
	 * @mbggenerated
	 */
	public String getCity() {
		return city;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.city
	 * @param city  the value for tbl_address.city
	 * @mbggenerated
	 */
	public void setCity(String city) {
		this.city = city == null ? null : city.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.email
	 * @return  the value of tbl_address.email
	 * @mbggenerated
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.email
	 * @param email  the value for tbl_address.email
	 * @mbggenerated
	 */
	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.pincode
	 * @return  the value of tbl_address.pincode
	 * @mbggenerated
	 */
	public String getPincode() {
		return pincode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.pincode
	 * @param pincode  the value for tbl_address.pincode
	 * @mbggenerated
	 */
	public void setPincode(String pincode) {
		this.pincode = pincode == null ? null : pincode.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.state
	 * @return  the value of tbl_address.state
	 * @mbggenerated
	 */
	public String getState() {
		return state;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.state
	 * @param state  the value for tbl_address.state
	 * @mbggenerated
	 */
	public void setState(String state) {
		this.state = state == null ? null : state.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.country
	 * @return  the value of tbl_address.country
	 * @mbggenerated
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.country
	 * @param country  the value for tbl_address.country
	 * @mbggenerated
	 */
	public void setCountry(String country) {
		this.country = country == null ? null : country.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.created_by
	 * @return  the value of tbl_address.created_by
	 * @mbggenerated
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.created_by
	 * @param createdBy  the value for tbl_address.created_by
	 * @mbggenerated
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.modified_by
	 * @return  the value of tbl_address.modified_by
	 * @mbggenerated
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.modified_by
	 * @param modifiedBy  the value for tbl_address.modified_by
	 * @mbggenerated
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.created_on
	 * @return  the value of tbl_address.created_on
	 * @mbggenerated
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.created_on
	 * @param createdOn  the value for tbl_address.created_on
	 * @mbggenerated
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.modified_on
	 * @return  the value of tbl_address.modified_on
	 * @mbggenerated
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.modified_on
	 * @param modifiedOn  the value for tbl_address.modified_on
	 * @mbggenerated
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.is_active
	 * @return  the value of tbl_address.is_active
	 * @mbggenerated
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.is_active
	 * @param isActive  the value for tbl_address.is_active
	 * @mbggenerated
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_address.comments
	 * @return  the value of tbl_address.comments
	 * @mbggenerated
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_address.comments
	 * @param comments  the value for tbl_address.comments
	 * @mbggenerated
	 */
	public void setComments(String comments) {
		this.comments = comments == null ? null : comments.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		CustomerAddress other = (CustomerAddress) that;
		return (this.getId() == null ? other.getId() == null : this.getId()
				.equals(other.getId()))
				&& (this.getCustomerId() == null ? other.getCustomerId() == null
						: this.getCustomerId().equals(other.getCustomerId()))
				&& (this.getAddress() == null ? other.getAddress() == null
						: this.getAddress().equals(other.getAddress()))
				&& (this.getCity() == null ? other.getCity() == null : this
						.getCity().equals(other.getCity()))
				&& (this.getEmail() == null ? other.getEmail() == null : this
						.getEmail().equals(other.getEmail()))
				&& (this.getPincode() == null ? other.getPincode() == null
						: this.getPincode().equals(other.getPincode()))
				&& (this.getState() == null ? other.getState() == null : this
						.getState().equals(other.getState()))
				&& (this.getCountry() == null ? other.getCountry() == null
						: this.getCountry().equals(other.getCountry()))
				&& (this.getCreatedBy() == null ? other.getCreatedBy() == null
						: this.getCreatedBy().equals(other.getCreatedBy()))
				&& (this.getModifiedBy() == null ? other.getModifiedBy() == null
						: this.getModifiedBy().equals(other.getModifiedBy()))
				&& (this.getCreatedOn() == null ? other.getCreatedOn() == null
						: this.getCreatedOn().equals(other.getCreatedOn()))
				&& (this.getModifiedOn() == null ? other.getModifiedOn() == null
						: this.getModifiedOn().equals(other.getModifiedOn()))
				&& (this.getIsActive() == null ? other.getIsActive() == null
						: this.getIsActive().equals(other.getIsActive()))
				&& (this.getComments() == null ? other.getComments() == null
						: this.getComments().equals(other.getComments()));
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result
				+ ((getCustomerId() == null) ? 0 : getCustomerId().hashCode());
		result = prime * result
				+ ((getAddress() == null) ? 0 : getAddress().hashCode());
		result = prime * result
				+ ((getCity() == null) ? 0 : getCity().hashCode());
		result = prime * result
				+ ((getEmail() == null) ? 0 : getEmail().hashCode());
		result = prime * result
				+ ((getPincode() == null) ? 0 : getPincode().hashCode());
		result = prime * result
				+ ((getState() == null) ? 0 : getState().hashCode());
		result = prime * result
				+ ((getCountry() == null) ? 0 : getCountry().hashCode());
		result = prime * result
				+ ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
		result = prime * result
				+ ((getModifiedBy() == null) ? 0 : getModifiedBy().hashCode());
		result = prime * result
				+ ((getCreatedOn() == null) ? 0 : getCreatedOn().hashCode());
		result = prime * result
				+ ((getModifiedOn() == null) ? 0 : getModifiedOn().hashCode());
		result = prime * result
				+ ((getIsActive() == null) ? 0 : getIsActive().hashCode());
		result = prime * result
				+ ((getComments() == null) ? 0 : getComments().hashCode());
		return result;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", customerId=").append(customerId);
		sb.append(", address=").append(address);
		sb.append(", city=").append(city);
		sb.append(", email=").append(email);
		sb.append(", pincode=").append(pincode);
		sb.append(", state=").append(state);
		sb.append(", country=").append(country);
		sb.append(", createdBy=").append(createdBy);
		sb.append(", modifiedBy=").append(modifiedBy);
		sb.append(", createdOn=").append(createdOn);
		sb.append(", modifiedOn=").append(modifiedOn);
		sb.append(", isActive=").append(isActive);
		sb.append(", comments=").append(comments);
		sb.append("]");
		return sb.toString();
	}
}