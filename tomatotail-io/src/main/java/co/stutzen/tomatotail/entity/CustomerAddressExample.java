package co.stutzen.tomatotail.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CustomerAddressExample {
    /**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	public CustomerAddressExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value,
				String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1,
				Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(Integer value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(Integer value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(Integer value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(Integer value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(Integer value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<Integer> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<Integer> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(Integer value1, Integer value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(Integer value1, Integer value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andCustomerIdIsNull() {
			addCriterion("customer_id is null");
			return (Criteria) this;
		}

		public Criteria andCustomerIdIsNotNull() {
			addCriterion("customer_id is not null");
			return (Criteria) this;
		}

		public Criteria andCustomerIdEqualTo(Integer value) {
			addCriterion("customer_id =", value, "customerId");
			return (Criteria) this;
		}

		public Criteria andCustomerIdNotEqualTo(Integer value) {
			addCriterion("customer_id <>", value, "customerId");
			return (Criteria) this;
		}

		public Criteria andCustomerIdGreaterThan(Integer value) {
			addCriterion("customer_id >", value, "customerId");
			return (Criteria) this;
		}

		public Criteria andCustomerIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("customer_id >=", value, "customerId");
			return (Criteria) this;
		}

		public Criteria andCustomerIdLessThan(Integer value) {
			addCriterion("customer_id <", value, "customerId");
			return (Criteria) this;
		}

		public Criteria andCustomerIdLessThanOrEqualTo(Integer value) {
			addCriterion("customer_id <=", value, "customerId");
			return (Criteria) this;
		}

		public Criteria andCustomerIdIn(List<Integer> values) {
			addCriterion("customer_id in", values, "customerId");
			return (Criteria) this;
		}

		public Criteria andCustomerIdNotIn(List<Integer> values) {
			addCriterion("customer_id not in", values, "customerId");
			return (Criteria) this;
		}

		public Criteria andCustomerIdBetween(Integer value1, Integer value2) {
			addCriterion("customer_id between", value1, value2, "customerId");
			return (Criteria) this;
		}

		public Criteria andCustomerIdNotBetween(Integer value1, Integer value2) {
			addCriterion("customer_id not between", value1, value2,
					"customerId");
			return (Criteria) this;
		}

		public Criteria andAddressIsNull() {
			addCriterion("address is null");
			return (Criteria) this;
		}

		public Criteria andAddressIsNotNull() {
			addCriterion("address is not null");
			return (Criteria) this;
		}

		public Criteria andAddressEqualTo(String value) {
			addCriterion("address =", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressNotEqualTo(String value) {
			addCriterion("address <>", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressGreaterThan(String value) {
			addCriterion("address >", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressGreaterThanOrEqualTo(String value) {
			addCriterion("address >=", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressLessThan(String value) {
			addCriterion("address <", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressLessThanOrEqualTo(String value) {
			addCriterion("address <=", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressLike(String value) {
			addCriterion("address like", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressNotLike(String value) {
			addCriterion("address not like", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressIn(List<String> values) {
			addCriterion("address in", values, "address");
			return (Criteria) this;
		}

		public Criteria andAddressNotIn(List<String> values) {
			addCriterion("address not in", values, "address");
			return (Criteria) this;
		}

		public Criteria andAddressBetween(String value1, String value2) {
			addCriterion("address between", value1, value2, "address");
			return (Criteria) this;
		}

		public Criteria andAddressNotBetween(String value1, String value2) {
			addCriterion("address not between", value1, value2, "address");
			return (Criteria) this;
		}

		public Criteria andCityIsNull() {
			addCriterion("city is null");
			return (Criteria) this;
		}

		public Criteria andCityIsNotNull() {
			addCriterion("city is not null");
			return (Criteria) this;
		}

		public Criteria andCityEqualTo(String value) {
			addCriterion("city =", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityNotEqualTo(String value) {
			addCriterion("city <>", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityGreaterThan(String value) {
			addCriterion("city >", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityGreaterThanOrEqualTo(String value) {
			addCriterion("city >=", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityLessThan(String value) {
			addCriterion("city <", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityLessThanOrEqualTo(String value) {
			addCriterion("city <=", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityLike(String value) {
			addCriterion("city like", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityNotLike(String value) {
			addCriterion("city not like", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityIn(List<String> values) {
			addCriterion("city in", values, "city");
			return (Criteria) this;
		}

		public Criteria andCityNotIn(List<String> values) {
			addCriterion("city not in", values, "city");
			return (Criteria) this;
		}

		public Criteria andCityBetween(String value1, String value2) {
			addCriterion("city between", value1, value2, "city");
			return (Criteria) this;
		}

		public Criteria andCityNotBetween(String value1, String value2) {
			addCriterion("city not between", value1, value2, "city");
			return (Criteria) this;
		}

		public Criteria andEmailIsNull() {
			addCriterion("email is null");
			return (Criteria) this;
		}

		public Criteria andEmailIsNotNull() {
			addCriterion("email is not null");
			return (Criteria) this;
		}

		public Criteria andEmailEqualTo(String value) {
			addCriterion("email =", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailNotEqualTo(String value) {
			addCriterion("email <>", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailGreaterThan(String value) {
			addCriterion("email >", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailGreaterThanOrEqualTo(String value) {
			addCriterion("email >=", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailLessThan(String value) {
			addCriterion("email <", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailLessThanOrEqualTo(String value) {
			addCriterion("email <=", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailLike(String value) {
			addCriterion("email like", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailNotLike(String value) {
			addCriterion("email not like", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailIn(List<String> values) {
			addCriterion("email in", values, "email");
			return (Criteria) this;
		}

		public Criteria andEmailNotIn(List<String> values) {
			addCriterion("email not in", values, "email");
			return (Criteria) this;
		}

		public Criteria andEmailBetween(String value1, String value2) {
			addCriterion("email between", value1, value2, "email");
			return (Criteria) this;
		}

		public Criteria andEmailNotBetween(String value1, String value2) {
			addCriterion("email not between", value1, value2, "email");
			return (Criteria) this;
		}

		public Criteria andPincodeIsNull() {
			addCriterion("pincode is null");
			return (Criteria) this;
		}

		public Criteria andPincodeIsNotNull() {
			addCriterion("pincode is not null");
			return (Criteria) this;
		}

		public Criteria andPincodeEqualTo(String value) {
			addCriterion("pincode =", value, "pincode");
			return (Criteria) this;
		}

		public Criteria andPincodeNotEqualTo(String value) {
			addCriterion("pincode <>", value, "pincode");
			return (Criteria) this;
		}

		public Criteria andPincodeGreaterThan(String value) {
			addCriterion("pincode >", value, "pincode");
			return (Criteria) this;
		}

		public Criteria andPincodeGreaterThanOrEqualTo(String value) {
			addCriterion("pincode >=", value, "pincode");
			return (Criteria) this;
		}

		public Criteria andPincodeLessThan(String value) {
			addCriterion("pincode <", value, "pincode");
			return (Criteria) this;
		}

		public Criteria andPincodeLessThanOrEqualTo(String value) {
			addCriterion("pincode <=", value, "pincode");
			return (Criteria) this;
		}

		public Criteria andPincodeLike(String value) {
			addCriterion("pincode like", value, "pincode");
			return (Criteria) this;
		}

		public Criteria andPincodeNotLike(String value) {
			addCriterion("pincode not like", value, "pincode");
			return (Criteria) this;
		}

		public Criteria andPincodeIn(List<String> values) {
			addCriterion("pincode in", values, "pincode");
			return (Criteria) this;
		}

		public Criteria andPincodeNotIn(List<String> values) {
			addCriterion("pincode not in", values, "pincode");
			return (Criteria) this;
		}

		public Criteria andPincodeBetween(String value1, String value2) {
			addCriterion("pincode between", value1, value2, "pincode");
			return (Criteria) this;
		}

		public Criteria andPincodeNotBetween(String value1, String value2) {
			addCriterion("pincode not between", value1, value2, "pincode");
			return (Criteria) this;
		}

		public Criteria andStateIsNull() {
			addCriterion("state is null");
			return (Criteria) this;
		}

		public Criteria andStateIsNotNull() {
			addCriterion("state is not null");
			return (Criteria) this;
		}

		public Criteria andStateEqualTo(String value) {
			addCriterion("state =", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotEqualTo(String value) {
			addCriterion("state <>", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateGreaterThan(String value) {
			addCriterion("state >", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateGreaterThanOrEqualTo(String value) {
			addCriterion("state >=", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateLessThan(String value) {
			addCriterion("state <", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateLessThanOrEqualTo(String value) {
			addCriterion("state <=", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateLike(String value) {
			addCriterion("state like", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotLike(String value) {
			addCriterion("state not like", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateIn(List<String> values) {
			addCriterion("state in", values, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotIn(List<String> values) {
			addCriterion("state not in", values, "state");
			return (Criteria) this;
		}

		public Criteria andStateBetween(String value1, String value2) {
			addCriterion("state between", value1, value2, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotBetween(String value1, String value2) {
			addCriterion("state not between", value1, value2, "state");
			return (Criteria) this;
		}

		public Criteria andCountryIsNull() {
			addCriterion("country is null");
			return (Criteria) this;
		}

		public Criteria andCountryIsNotNull() {
			addCriterion("country is not null");
			return (Criteria) this;
		}

		public Criteria andCountryEqualTo(String value) {
			addCriterion("country =", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryNotEqualTo(String value) {
			addCriterion("country <>", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryGreaterThan(String value) {
			addCriterion("country >", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryGreaterThanOrEqualTo(String value) {
			addCriterion("country >=", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryLessThan(String value) {
			addCriterion("country <", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryLessThanOrEqualTo(String value) {
			addCriterion("country <=", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryLike(String value) {
			addCriterion("country like", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryNotLike(String value) {
			addCriterion("country not like", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryIn(List<String> values) {
			addCriterion("country in", values, "country");
			return (Criteria) this;
		}

		public Criteria andCountryNotIn(List<String> values) {
			addCriterion("country not in", values, "country");
			return (Criteria) this;
		}

		public Criteria andCountryBetween(String value1, String value2) {
			addCriterion("country between", value1, value2, "country");
			return (Criteria) this;
		}

		public Criteria andCountryNotBetween(String value1, String value2) {
			addCriterion("country not between", value1, value2, "country");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNull() {
			addCriterion("created_by is null");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNotNull() {
			addCriterion("created_by is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedByEqualTo(Integer value) {
			addCriterion("created_by =", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotEqualTo(Integer value) {
			addCriterion("created_by <>", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThan(Integer value) {
			addCriterion("created_by >", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThanOrEqualTo(Integer value) {
			addCriterion("created_by >=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThan(Integer value) {
			addCriterion("created_by <", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThanOrEqualTo(Integer value) {
			addCriterion("created_by <=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByIn(List<Integer> values) {
			addCriterion("created_by in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotIn(List<Integer> values) {
			addCriterion("created_by not in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByBetween(Integer value1, Integer value2) {
			addCriterion("created_by between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotBetween(Integer value1, Integer value2) {
			addCriterion("created_by not between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByIsNull() {
			addCriterion("modified_by is null");
			return (Criteria) this;
		}

		public Criteria andModifiedByIsNotNull() {
			addCriterion("modified_by is not null");
			return (Criteria) this;
		}

		public Criteria andModifiedByEqualTo(Integer value) {
			addCriterion("modified_by =", value, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByNotEqualTo(Integer value) {
			addCriterion("modified_by <>", value, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByGreaterThan(Integer value) {
			addCriterion("modified_by >", value, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByGreaterThanOrEqualTo(Integer value) {
			addCriterion("modified_by >=", value, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByLessThan(Integer value) {
			addCriterion("modified_by <", value, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByLessThanOrEqualTo(Integer value) {
			addCriterion("modified_by <=", value, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByIn(List<Integer> values) {
			addCriterion("modified_by in", values, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByNotIn(List<Integer> values) {
			addCriterion("modified_by not in", values, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByBetween(Integer value1, Integer value2) {
			addCriterion("modified_by between", value1, value2, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByNotBetween(Integer value1, Integer value2) {
			addCriterion("modified_by not between", value1, value2,
					"modifiedBy");
			return (Criteria) this;
		}

		public Criteria andCreatedOnIsNull() {
			addCriterion("created_on is null");
			return (Criteria) this;
		}

		public Criteria andCreatedOnIsNotNull() {
			addCriterion("created_on is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedOnEqualTo(Date value) {
			addCriterion("created_on =", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnNotEqualTo(Date value) {
			addCriterion("created_on <>", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnGreaterThan(Date value) {
			addCriterion("created_on >", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnGreaterThanOrEqualTo(Date value) {
			addCriterion("created_on >=", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnLessThan(Date value) {
			addCriterion("created_on <", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnLessThanOrEqualTo(Date value) {
			addCriterion("created_on <=", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnIn(List<Date> values) {
			addCriterion("created_on in", values, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnNotIn(List<Date> values) {
			addCriterion("created_on not in", values, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnBetween(Date value1, Date value2) {
			addCriterion("created_on between", value1, value2, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnNotBetween(Date value1, Date value2) {
			addCriterion("created_on not between", value1, value2, "createdOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnIsNull() {
			addCriterion("modified_on is null");
			return (Criteria) this;
		}

		public Criteria andModifiedOnIsNotNull() {
			addCriterion("modified_on is not null");
			return (Criteria) this;
		}

		public Criteria andModifiedOnEqualTo(Date value) {
			addCriterion("modified_on =", value, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnNotEqualTo(Date value) {
			addCriterion("modified_on <>", value, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnGreaterThan(Date value) {
			addCriterion("modified_on >", value, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnGreaterThanOrEqualTo(Date value) {
			addCriterion("modified_on >=", value, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnLessThan(Date value) {
			addCriterion("modified_on <", value, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnLessThanOrEqualTo(Date value) {
			addCriterion("modified_on <=", value, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnIn(List<Date> values) {
			addCriterion("modified_on in", values, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnNotIn(List<Date> values) {
			addCriterion("modified_on not in", values, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnBetween(Date value1, Date value2) {
			addCriterion("modified_on between", value1, value2, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnNotBetween(Date value1, Date value2) {
			addCriterion("modified_on not between", value1, value2,
					"modifiedOn");
			return (Criteria) this;
		}

		public Criteria andIsActiveIsNull() {
			addCriterion("is_active is null");
			return (Criteria) this;
		}

		public Criteria andIsActiveIsNotNull() {
			addCriterion("is_active is not null");
			return (Criteria) this;
		}

		public Criteria andIsActiveEqualTo(Boolean value) {
			addCriterion("is_active =", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveNotEqualTo(Boolean value) {
			addCriterion("is_active <>", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveGreaterThan(Boolean value) {
			addCriterion("is_active >", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveGreaterThanOrEqualTo(Boolean value) {
			addCriterion("is_active >=", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveLessThan(Boolean value) {
			addCriterion("is_active <", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveLessThanOrEqualTo(Boolean value) {
			addCriterion("is_active <=", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveIn(List<Boolean> values) {
			addCriterion("is_active in", values, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveNotIn(List<Boolean> values) {
			addCriterion("is_active not in", values, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveBetween(Boolean value1, Boolean value2) {
			addCriterion("is_active between", value1, value2, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveNotBetween(Boolean value1, Boolean value2) {
			addCriterion("is_active not between", value1, value2, "isActive");
			return (Criteria) this;
		}

		public Criteria andCommentsIsNull() {
			addCriterion("comments is null");
			return (Criteria) this;
		}

		public Criteria andCommentsIsNotNull() {
			addCriterion("comments is not null");
			return (Criteria) this;
		}

		public Criteria andCommentsEqualTo(String value) {
			addCriterion("comments =", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsNotEqualTo(String value) {
			addCriterion("comments <>", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsGreaterThan(String value) {
			addCriterion("comments >", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsGreaterThanOrEqualTo(String value) {
			addCriterion("comments >=", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsLessThan(String value) {
			addCriterion("comments <", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsLessThanOrEqualTo(String value) {
			addCriterion("comments <=", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsLike(String value) {
			addCriterion("comments like", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsNotLike(String value) {
			addCriterion("comments not like", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsIn(List<String> values) {
			addCriterion("comments in", values, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsNotIn(List<String> values) {
			addCriterion("comments not in", values, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsBetween(String value1, String value2) {
			addCriterion("comments between", value1, value2, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsNotBetween(String value1, String value2) {
			addCriterion("comments not between", value1, value2, "comments");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table tbl_address
	 * @mbggenerated
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue,
				String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table tbl_address
     *
     * @mbggenerated do_not_delete_during_merge
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }
}