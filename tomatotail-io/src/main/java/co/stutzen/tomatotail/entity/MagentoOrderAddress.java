package co.stutzen.tomatotail.entity;

import java.util.Date;

public class MagentoOrderAddress {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.id
	 * @mbggenerated
	 */
	private Integer id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.order_id
	 * @mbggenerated
	 */
	private Integer orderid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.created_at
	 * @mbggenerated
	 */
	private Date createdat;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.updated_at
	 * @mbggenerated
	 */
	private Date updatedat;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.is_active
	 * @mbggenerated
	 */
	private Byte isactive;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.address_type
	 * @mbggenerated
	 */
	private String addresstype;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.firstname
	 * @mbggenerated
	 */
	private String firstname;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.lastname
	 * @mbggenerated
	 */
	private String lastname;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.company
	 * @mbggenerated
	 */
	private String company;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.street
	 * @mbggenerated
	 */
	private String street;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.city
	 * @mbggenerated
	 */
	private String city;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.region
	 * @mbggenerated
	 */
	private String region;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.postcode
	 * @mbggenerated
	 */
	private String postcode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.country_id
	 * @mbggenerated
	 */
	private String countryid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.telephone
	 * @mbggenerated
	 */
	private String telephone;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.fax
	 * @mbggenerated
	 */
	private String fax;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.region_id
	 * @mbggenerated
	 */
	private Integer region_id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_address.address_id
	 * @mbggenerated
	 */
	private Integer address_id;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.id
	 * @return  the value of tbl_order_address.id
	 * @mbggenerated
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.id
	 * @param id  the value for tbl_order_address.id
	 * @mbggenerated
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.order_id
	 * @return  the value of tbl_order_address.order_id
	 * @mbggenerated
	 */
	public Integer getOrderid() {
		return orderid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.order_id
	 * @param orderid  the value for tbl_order_address.order_id
	 * @mbggenerated
	 */
	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.created_at
	 * @return  the value of tbl_order_address.created_at
	 * @mbggenerated
	 */
	public Date getCreatedat() {
		return createdat;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.created_at
	 * @param createdat  the value for tbl_order_address.created_at
	 * @mbggenerated
	 */
	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.updated_at
	 * @return  the value of tbl_order_address.updated_at
	 * @mbggenerated
	 */
	public Date getUpdatedat() {
		return updatedat;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.updated_at
	 * @param updatedat  the value for tbl_order_address.updated_at
	 * @mbggenerated
	 */
	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.is_active
	 * @return  the value of tbl_order_address.is_active
	 * @mbggenerated
	 */
	public Byte getIsactive() {
		return isactive;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.is_active
	 * @param isactive  the value for tbl_order_address.is_active
	 * @mbggenerated
	 */
	public void setIsactive(Byte isactive) {
		this.isactive = isactive;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.address_type
	 * @return  the value of tbl_order_address.address_type
	 * @mbggenerated
	 */
	public String getAddresstype() {
		return addresstype;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.address_type
	 * @param addresstype  the value for tbl_order_address.address_type
	 * @mbggenerated
	 */
	public void setAddresstype(String addresstype) {
		this.addresstype = addresstype == null ? null : addresstype.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.firstname
	 * @return  the value of tbl_order_address.firstname
	 * @mbggenerated
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.firstname
	 * @param firstname  the value for tbl_order_address.firstname
	 * @mbggenerated
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname == null ? null : firstname.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.lastname
	 * @return  the value of tbl_order_address.lastname
	 * @mbggenerated
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.lastname
	 * @param lastname  the value for tbl_order_address.lastname
	 * @mbggenerated
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname == null ? null : lastname.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.company
	 * @return  the value of tbl_order_address.company
	 * @mbggenerated
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.company
	 * @param company  the value for tbl_order_address.company
	 * @mbggenerated
	 */
	public void setCompany(String company) {
		this.company = company == null ? null : company.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.street
	 * @return  the value of tbl_order_address.street
	 * @mbggenerated
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.street
	 * @param street  the value for tbl_order_address.street
	 * @mbggenerated
	 */
	public void setStreet(String street) {
		this.street = street == null ? null : street.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.city
	 * @return  the value of tbl_order_address.city
	 * @mbggenerated
	 */
	public String getCity() {
		return city;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.city
	 * @param city  the value for tbl_order_address.city
	 * @mbggenerated
	 */
	public void setCity(String city) {
		this.city = city == null ? null : city.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.region
	 * @return  the value of tbl_order_address.region
	 * @mbggenerated
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.region
	 * @param region  the value for tbl_order_address.region
	 * @mbggenerated
	 */
	public void setRegion(String region) {
		this.region = region == null ? null : region.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.postcode
	 * @return  the value of tbl_order_address.postcode
	 * @mbggenerated
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.postcode
	 * @param postcode  the value for tbl_order_address.postcode
	 * @mbggenerated
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode == null ? null : postcode.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.country_id
	 * @return  the value of tbl_order_address.country_id
	 * @mbggenerated
	 */
	public String getCountryid() {
		return countryid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.country_id
	 * @param countryid  the value for tbl_order_address.country_id
	 * @mbggenerated
	 */
	public void setCountryid(String countryid) {
		this.countryid = countryid == null ? null : countryid.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.telephone
	 * @return  the value of tbl_order_address.telephone
	 * @mbggenerated
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.telephone
	 * @param telephone  the value for tbl_order_address.telephone
	 * @mbggenerated
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone == null ? null : telephone.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.fax
	 * @return  the value of tbl_order_address.fax
	 * @mbggenerated
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.fax
	 * @param fax  the value for tbl_order_address.fax
	 * @mbggenerated
	 */
	public void setFax(String fax) {
		this.fax = fax == null ? null : fax.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.region_id
	 * @return  the value of tbl_order_address.region_id
	 * @mbggenerated
	 */
	public Integer getRegion_id() {
		return region_id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.region_id
	 * @param region_id  the value for tbl_order_address.region_id
	 * @mbggenerated
	 */
	public void setRegion_id(Integer region_id) {
		this.region_id = region_id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_address.address_id
	 * @return  the value of tbl_order_address.address_id
	 * @mbggenerated
	 */
	public Integer getAddress_id() {
		return address_id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_address.address_id
	 * @param address_id  the value for tbl_order_address.address_id
	 * @mbggenerated
	 */
	public void setAddress_id(Integer address_id) {
		this.address_id = address_id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_address
	 * @mbggenerated
	 */
	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		MagentoOrderAddress other = (MagentoOrderAddress) that;
		return (this.getId() == null ? other.getId() == null : this.getId()
				.equals(other.getId()))
				&& (this.getOrderid() == null ? other.getOrderid() == null
						: this.getOrderid().equals(other.getOrderid()))
				&& (this.getCreatedat() == null ? other.getCreatedat() == null
						: this.getCreatedat().equals(other.getCreatedat()))
				&& (this.getUpdatedat() == null ? other.getUpdatedat() == null
						: this.getUpdatedat().equals(other.getUpdatedat()))
				&& (this.getIsactive() == null ? other.getIsactive() == null
						: this.getIsactive().equals(other.getIsactive()))
				&& (this.getAddresstype() == null ? other.getAddresstype() == null
						: this.getAddresstype().equals(other.getAddresstype()))
				&& (this.getFirstname() == null ? other.getFirstname() == null
						: this.getFirstname().equals(other.getFirstname()))
				&& (this.getLastname() == null ? other.getLastname() == null
						: this.getLastname().equals(other.getLastname()))
				&& (this.getCompany() == null ? other.getCompany() == null
						: this.getCompany().equals(other.getCompany()))
				&& (this.getStreet() == null ? other.getStreet() == null : this
						.getStreet().equals(other.getStreet()))
				&& (this.getCity() == null ? other.getCity() == null : this
						.getCity().equals(other.getCity()))
				&& (this.getRegion() == null ? other.getRegion() == null : this
						.getRegion().equals(other.getRegion()))
				&& (this.getPostcode() == null ? other.getPostcode() == null
						: this.getPostcode().equals(other.getPostcode()))
				&& (this.getCountryid() == null ? other.getCountryid() == null
						: this.getCountryid().equals(other.getCountryid()))
				&& (this.getTelephone() == null ? other.getTelephone() == null
						: this.getTelephone().equals(other.getTelephone()))
				&& (this.getFax() == null ? other.getFax() == null : this
						.getFax().equals(other.getFax()))
				&& (this.getRegion_id() == null ? other.getRegion_id() == null
						: this.getRegion_id().equals(other.getRegion_id()))
				&& (this.getAddress_id() == null ? other.getAddress_id() == null
						: this.getAddress_id().equals(other.getAddress_id()));
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_address
	 * @mbggenerated
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result
				+ ((getOrderid() == null) ? 0 : getOrderid().hashCode());
		result = prime * result
				+ ((getCreatedat() == null) ? 0 : getCreatedat().hashCode());
		result = prime * result
				+ ((getUpdatedat() == null) ? 0 : getUpdatedat().hashCode());
		result = prime * result
				+ ((getIsactive() == null) ? 0 : getIsactive().hashCode());
		result = prime
				* result
				+ ((getAddresstype() == null) ? 0 : getAddresstype().hashCode());
		result = prime * result
				+ ((getFirstname() == null) ? 0 : getFirstname().hashCode());
		result = prime * result
				+ ((getLastname() == null) ? 0 : getLastname().hashCode());
		result = prime * result
				+ ((getCompany() == null) ? 0 : getCompany().hashCode());
		result = prime * result
				+ ((getStreet() == null) ? 0 : getStreet().hashCode());
		result = prime * result
				+ ((getCity() == null) ? 0 : getCity().hashCode());
		result = prime * result
				+ ((getRegion() == null) ? 0 : getRegion().hashCode());
		result = prime * result
				+ ((getPostcode() == null) ? 0 : getPostcode().hashCode());
		result = prime * result
				+ ((getCountryid() == null) ? 0 : getCountryid().hashCode());
		result = prime * result
				+ ((getTelephone() == null) ? 0 : getTelephone().hashCode());
		result = prime * result
				+ ((getFax() == null) ? 0 : getFax().hashCode());
		result = prime * result
				+ ((getRegion_id() == null) ? 0 : getRegion_id().hashCode());
		result = prime * result
				+ ((getAddress_id() == null) ? 0 : getAddress_id().hashCode());
		return result;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_address
	 * @mbggenerated
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", orderid=").append(orderid);
		sb.append(", createdat=").append(createdat);
		sb.append(", updatedat=").append(updatedat);
		sb.append(", isactive=").append(isactive);
		sb.append(", addresstype=").append(addresstype);
		sb.append(", firstname=").append(firstname);
		sb.append(", lastname=").append(lastname);
		sb.append(", company=").append(company);
		sb.append(", street=").append(street);
		sb.append(", city=").append(city);
		sb.append(", region=").append(region);
		sb.append(", postcode=").append(postcode);
		sb.append(", countryid=").append(countryid);
		sb.append(", telephone=").append(telephone);
		sb.append(", fax=").append(fax);
		sb.append(", region_id=").append(region_id);
		sb.append(", address_id=").append(address_id);
		sb.append("]");
		return sb.toString();
	}
}