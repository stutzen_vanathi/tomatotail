package co.stutzen.tomatotail.entity;

public class MagentoOrderItemWithBLOBs extends MagentoOrderItem {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_item.product_options
	 * @mbggenerated
	 */
	private String productoptions;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_item.weee_tax_applied
	 * @mbggenerated
	 */
	private String weeetaxapplied;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_item.product_options
	 * @return  the value of tbl_order_item.product_options
	 * @mbggenerated
	 */
	public String getProductoptions() {
		return productoptions;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_item.product_options
	 * @param productoptions  the value for tbl_order_item.product_options
	 * @mbggenerated
	 */
	public void setProductoptions(String productoptions) {
		this.productoptions = productoptions == null ? null : productoptions
				.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_item.weee_tax_applied
	 * @return  the value of tbl_order_item.weee_tax_applied
	 * @mbggenerated
	 */
	public String getWeeetaxapplied() {
		return weeetaxapplied;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_item.weee_tax_applied
	 * @param weeetaxapplied  the value for tbl_order_item.weee_tax_applied
	 * @mbggenerated
	 */
	public void setWeeetaxapplied(String weeetaxapplied) {
		this.weeetaxapplied = weeetaxapplied == null ? null : weeetaxapplied
				.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_item
	 * @mbggenerated
	 */
	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		MagentoOrderItemWithBLOBs other = (MagentoOrderItemWithBLOBs) that;
		return (this.getId() == null ? other.getId() == null : this.getId()
				.equals(other.getId()))
				&& (this.getItemid() == null ? other.getItemid() == null : this
						.getItemid().equals(other.getItemid()))
				&& (this.getOrderid() == null ? other.getOrderid() == null
						: this.getOrderid().equals(other.getOrderid()))
				&& (this.getQuoteitemid() == null ? other.getQuoteitemid() == null
						: this.getQuoteitemid().equals(other.getQuoteitemid()))
				&& (this.getCreatedat() == null ? other.getCreatedat() == null
						: this.getCreatedat().equals(other.getCreatedat()))
				&& (this.getUpdatedat() == null ? other.getUpdatedat() == null
						: this.getUpdatedat().equals(other.getUpdatedat()))
				&& (this.getProductid() == null ? other.getProductid() == null
						: this.getProductid().equals(other.getProductid()))
				&& (this.getProducttype() == null ? other.getProducttype() == null
						: this.getProducttype().equals(other.getProducttype()))
				&& (this.getWeight() == null ? other.getWeight() == null : this
						.getWeight().equals(other.getWeight()))
				&& (this.getIsvirtual() == null ? other.getIsvirtual() == null
						: this.getIsvirtual().equals(other.getIsvirtual()))
				&& (this.getSku() == null ? other.getSku() == null : this
						.getSku().equals(other.getSku()))
				&& (this.getName() == null ? other.getName() == null : this
						.getName().equals(other.getName()))
				&& (this.getAppliedruleids() == null ? other
						.getAppliedruleids() == null : this.getAppliedruleids()
						.equals(other.getAppliedruleids()))
				&& (this.getFreeshipping() == null ? other.getFreeshipping() == null
						: this.getFreeshipping()
								.equals(other.getFreeshipping()))
				&& (this.getIsqtydecimal() == null ? other.getIsqtydecimal() == null
						: this.getIsqtydecimal()
								.equals(other.getIsqtydecimal()))
				&& (this.getNodiscount() == null ? other.getNodiscount() == null
						: this.getNodiscount().equals(other.getNodiscount()))
				&& (this.getQtycanceled() == null ? other.getQtycanceled() == null
						: this.getQtycanceled().equals(other.getQtycanceled()))
				&& (this.getQtyinvoiced() == null ? other.getQtyinvoiced() == null
						: this.getQtyinvoiced().equals(other.getQtyinvoiced()))
				&& (this.getQtyordered() == null ? other.getQtyordered() == null
						: this.getQtyordered().equals(other.getQtyordered()))
				&& (this.getQtyrefunded() == null ? other.getQtyrefunded() == null
						: this.getQtyrefunded().equals(other.getQtyrefunded()))
				&& (this.getQtyshipped() == null ? other.getQtyshipped() == null
						: this.getQtyshipped().equals(other.getQtyshipped()))
				&& (this.getCost() == null ? other.getCost() == null : this
						.getCost().equals(other.getCost()))
				&& (this.getPrice() == null ? other.getPrice() == null : this
						.getPrice().equals(other.getPrice()))
				&& (this.getBaseprice() == null ? other.getBaseprice() == null
						: this.getBaseprice().equals(other.getBaseprice()))
				&& (this.getOriginalprice() == null ? other.getOriginalprice() == null
						: this.getOriginalprice().equals(
								other.getOriginalprice()))
				&& (this.getBaseoriginalprice() == null ? other
						.getBaseoriginalprice() == null : this
						.getBaseoriginalprice().equals(
								other.getBaseoriginalprice()))
				&& (this.getTaxpercent() == null ? other.getTaxpercent() == null
						: this.getTaxpercent().equals(other.getTaxpercent()))
				&& (this.getTaxamount() == null ? other.getTaxamount() == null
						: this.getTaxamount().equals(other.getTaxamount()))
				&& (this.getBasetaxamount() == null ? other.getBasetaxamount() == null
						: this.getBasetaxamount().equals(
								other.getBasetaxamount()))
				&& (this.getTaxinvoiced() == null ? other.getTaxinvoiced() == null
						: this.getTaxinvoiced().equals(other.getTaxinvoiced()))
				&& (this.getBasetaxinvoiced() == null ? other
						.getBasetaxinvoiced() == null : this
						.getBasetaxinvoiced()
						.equals(other.getBasetaxinvoiced()))
				&& (this.getDiscountpercent() == null ? other
						.getDiscountpercent() == null : this
						.getDiscountpercent()
						.equals(other.getDiscountpercent()))
				&& (this.getDiscountamount() == null ? other
						.getDiscountamount() == null : this.getDiscountamount()
						.equals(other.getDiscountamount()))
				&& (this.getBasediscountamount() == null ? other
						.getBasediscountamount() == null : this
						.getBasediscountamount().equals(
								other.getBasediscountamount()))
				&& (this.getDiscountinvoiced() == null ? other
						.getDiscountinvoiced() == null : this
						.getDiscountinvoiced().equals(
								other.getDiscountinvoiced()))
				&& (this.getBasediscountinvoiced() == null ? other
						.getBasediscountinvoiced() == null : this
						.getBasediscountinvoiced().equals(
								other.getBasediscountinvoiced()))
				&& (this.getAmountrefunded() == null ? other
						.getAmountrefunded() == null : this.getAmountrefunded()
						.equals(other.getAmountrefunded()))
				&& (this.getBaseamountrefunded() == null ? other
						.getBaseamountrefunded() == null : this
						.getBaseamountrefunded().equals(
								other.getBaseamountrefunded()))
				&& (this.getRowtotal() == null ? other.getRowtotal() == null
						: this.getRowtotal().equals(other.getRowtotal()))
				&& (this.getBaserowtotal() == null ? other.getBaserowtotal() == null
						: this.getBaserowtotal()
								.equals(other.getBaserowtotal()))
				&& (this.getRowinvoiced() == null ? other.getRowinvoiced() == null
						: this.getRowinvoiced().equals(other.getRowinvoiced()))
				&& (this.getBaserowinvoiced() == null ? other
						.getBaserowinvoiced() == null : this
						.getBaserowinvoiced()
						.equals(other.getBaserowinvoiced()))
				&& (this.getRowweight() == null ? other.getRowweight() == null
						: this.getRowweight().equals(other.getRowweight()))
				&& (this.getGiftmessageid() == null ? other.getGiftmessageid() == null
						: this.getGiftmessageid().equals(
								other.getGiftmessageid()))
				&& (this.getGiftmessage() == null ? other.getGiftmessage() == null
						: this.getGiftmessage().equals(other.getGiftmessage()))
				&& (this.getGiftmessageavailable() == null ? other
						.getGiftmessageavailable() == null : this
						.getGiftmessageavailable().equals(
								other.getGiftmessageavailable()))
				&& (this.getBasetaxbeforediscount() == null ? other
						.getBasetaxbeforediscount() == null : this
						.getBasetaxbeforediscount().equals(
								other.getBasetaxbeforediscount()))
				&& (this.getTaxbeforediscount() == null ? other
						.getTaxbeforediscount() == null : this
						.getTaxbeforediscount().equals(
								other.getTaxbeforediscount()))
				&& (this.getWeeetaxappliedamount() == null ? other
						.getWeeetaxappliedamount() == null : this
						.getWeeetaxappliedamount().equals(
								other.getWeeetaxappliedamount()))
				&& (this.getWeeetaxappliedrowamount() == null ? other
						.getWeeetaxappliedrowamount() == null : this
						.getWeeetaxappliedrowamount().equals(
								other.getWeeetaxappliedrowamount()))
				&& (this.getBaseweeetaxappliedamount() == null ? other
						.getBaseweeetaxappliedamount() == null : this
						.getBaseweeetaxappliedamount().equals(
								other.getBaseweeetaxappliedamount()))
				&& (this.getBaseweeetaxappliedrowamount() == null ? other
						.getBaseweeetaxappliedrowamount() == null : this
						.getBaseweeetaxappliedrowamount().equals(
								other.getBaseweeetaxappliedrowamount()))
				&& (this.getWeeetaxdisposition() == null ? other
						.getWeeetaxdisposition() == null : this
						.getWeeetaxdisposition().equals(
								other.getWeeetaxdisposition()))
				&& (this.getWeeetaxrowdisposition() == null ? other
						.getWeeetaxrowdisposition() == null : this
						.getWeeetaxrowdisposition().equals(
								other.getWeeetaxrowdisposition()))
				&& (this.getBaseweeetaxdisposition() == null ? other
						.getBaseweeetaxdisposition() == null : this
						.getBaseweeetaxdisposition().equals(
								other.getBaseweeetaxdisposition()))
				&& (this.getBaseweeetaxrowdisposition() == null ? other
						.getBaseweeetaxrowdisposition() == null : this
						.getBaseweeetaxrowdisposition().equals(
								other.getBaseweeetaxrowdisposition()))
				&& (this.getProductoptions() == null ? other
						.getProductoptions() == null : this.getProductoptions()
						.equals(other.getProductoptions()))
				&& (this.getWeeetaxapplied() == null ? other
						.getWeeetaxapplied() == null : this.getWeeetaxapplied()
						.equals(other.getWeeetaxapplied()));
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_item
	 * @mbggenerated
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result
				+ ((getItemid() == null) ? 0 : getItemid().hashCode());
		result = prime * result
				+ ((getOrderid() == null) ? 0 : getOrderid().hashCode());
		result = prime
				* result
				+ ((getQuoteitemid() == null) ? 0 : getQuoteitemid().hashCode());
		result = prime * result
				+ ((getCreatedat() == null) ? 0 : getCreatedat().hashCode());
		result = prime * result
				+ ((getUpdatedat() == null) ? 0 : getUpdatedat().hashCode());
		result = prime * result
				+ ((getProductid() == null) ? 0 : getProductid().hashCode());
		result = prime
				* result
				+ ((getProducttype() == null) ? 0 : getProducttype().hashCode());
		result = prime * result
				+ ((getWeight() == null) ? 0 : getWeight().hashCode());
		result = prime * result
				+ ((getIsvirtual() == null) ? 0 : getIsvirtual().hashCode());
		result = prime * result
				+ ((getSku() == null) ? 0 : getSku().hashCode());
		result = prime * result
				+ ((getName() == null) ? 0 : getName().hashCode());
		result = prime
				* result
				+ ((getAppliedruleids() == null) ? 0 : getAppliedruleids()
						.hashCode());
		result = prime
				* result
				+ ((getFreeshipping() == null) ? 0 : getFreeshipping()
						.hashCode());
		result = prime
				* result
				+ ((getIsqtydecimal() == null) ? 0 : getIsqtydecimal()
						.hashCode());
		result = prime * result
				+ ((getNodiscount() == null) ? 0 : getNodiscount().hashCode());
		result = prime
				* result
				+ ((getQtycanceled() == null) ? 0 : getQtycanceled().hashCode());
		result = prime
				* result
				+ ((getQtyinvoiced() == null) ? 0 : getQtyinvoiced().hashCode());
		result = prime * result
				+ ((getQtyordered() == null) ? 0 : getQtyordered().hashCode());
		result = prime
				* result
				+ ((getQtyrefunded() == null) ? 0 : getQtyrefunded().hashCode());
		result = prime * result
				+ ((getQtyshipped() == null) ? 0 : getQtyshipped().hashCode());
		result = prime * result
				+ ((getCost() == null) ? 0 : getCost().hashCode());
		result = prime * result
				+ ((getPrice() == null) ? 0 : getPrice().hashCode());
		result = prime * result
				+ ((getBaseprice() == null) ? 0 : getBaseprice().hashCode());
		result = prime
				* result
				+ ((getOriginalprice() == null) ? 0 : getOriginalprice()
						.hashCode());
		result = prime
				* result
				+ ((getBaseoriginalprice() == null) ? 0
						: getBaseoriginalprice().hashCode());
		result = prime * result
				+ ((getTaxpercent() == null) ? 0 : getTaxpercent().hashCode());
		result = prime * result
				+ ((getTaxamount() == null) ? 0 : getTaxamount().hashCode());
		result = prime
				* result
				+ ((getBasetaxamount() == null) ? 0 : getBasetaxamount()
						.hashCode());
		result = prime
				* result
				+ ((getTaxinvoiced() == null) ? 0 : getTaxinvoiced().hashCode());
		result = prime
				* result
				+ ((getBasetaxinvoiced() == null) ? 0 : getBasetaxinvoiced()
						.hashCode());
		result = prime
				* result
				+ ((getDiscountpercent() == null) ? 0 : getDiscountpercent()
						.hashCode());
		result = prime
				* result
				+ ((getDiscountamount() == null) ? 0 : getDiscountamount()
						.hashCode());
		result = prime
				* result
				+ ((getBasediscountamount() == null) ? 0
						: getBasediscountamount().hashCode());
		result = prime
				* result
				+ ((getDiscountinvoiced() == null) ? 0 : getDiscountinvoiced()
						.hashCode());
		result = prime
				* result
				+ ((getBasediscountinvoiced() == null) ? 0
						: getBasediscountinvoiced().hashCode());
		result = prime
				* result
				+ ((getAmountrefunded() == null) ? 0 : getAmountrefunded()
						.hashCode());
		result = prime
				* result
				+ ((getBaseamountrefunded() == null) ? 0
						: getBaseamountrefunded().hashCode());
		result = prime * result
				+ ((getRowtotal() == null) ? 0 : getRowtotal().hashCode());
		result = prime
				* result
				+ ((getBaserowtotal() == null) ? 0 : getBaserowtotal()
						.hashCode());
		result = prime
				* result
				+ ((getRowinvoiced() == null) ? 0 : getRowinvoiced().hashCode());
		result = prime
				* result
				+ ((getBaserowinvoiced() == null) ? 0 : getBaserowinvoiced()
						.hashCode());
		result = prime * result
				+ ((getRowweight() == null) ? 0 : getRowweight().hashCode());
		result = prime
				* result
				+ ((getGiftmessageid() == null) ? 0 : getGiftmessageid()
						.hashCode());
		result = prime
				* result
				+ ((getGiftmessage() == null) ? 0 : getGiftmessage().hashCode());
		result = prime
				* result
				+ ((getGiftmessageavailable() == null) ? 0
						: getGiftmessageavailable().hashCode());
		result = prime
				* result
				+ ((getBasetaxbeforediscount() == null) ? 0
						: getBasetaxbeforediscount().hashCode());
		result = prime
				* result
				+ ((getTaxbeforediscount() == null) ? 0
						: getTaxbeforediscount().hashCode());
		result = prime
				* result
				+ ((getWeeetaxappliedamount() == null) ? 0
						: getWeeetaxappliedamount().hashCode());
		result = prime
				* result
				+ ((getWeeetaxappliedrowamount() == null) ? 0
						: getWeeetaxappliedrowamount().hashCode());
		result = prime
				* result
				+ ((getBaseweeetaxappliedamount() == null) ? 0
						: getBaseweeetaxappliedamount().hashCode());
		result = prime
				* result
				+ ((getBaseweeetaxappliedrowamount() == null) ? 0
						: getBaseweeetaxappliedrowamount().hashCode());
		result = prime
				* result
				+ ((getWeeetaxdisposition() == null) ? 0
						: getWeeetaxdisposition().hashCode());
		result = prime
				* result
				+ ((getWeeetaxrowdisposition() == null) ? 0
						: getWeeetaxrowdisposition().hashCode());
		result = prime
				* result
				+ ((getBaseweeetaxdisposition() == null) ? 0
						: getBaseweeetaxdisposition().hashCode());
		result = prime
				* result
				+ ((getBaseweeetaxrowdisposition() == null) ? 0
						: getBaseweeetaxrowdisposition().hashCode());
		result = prime
				* result
				+ ((getProductoptions() == null) ? 0 : getProductoptions()
						.hashCode());
		result = prime
				* result
				+ ((getWeeetaxapplied() == null) ? 0 : getWeeetaxapplied()
						.hashCode());
		return result;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_item
	 * @mbggenerated
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", productoptions=").append(productoptions);
		sb.append(", weeetaxapplied=").append(weeetaxapplied);
		sb.append("]");
		return sb.toString();
	}
}