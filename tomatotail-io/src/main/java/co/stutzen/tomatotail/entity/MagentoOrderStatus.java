package co.stutzen.tomatotail.entity;

import java.util.Date;

public class MagentoOrderStatus {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_status_history.id
	 * @mbggenerated
	 */
	private Integer id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_status_history.order_id
	 * @mbggenerated
	 */
	private Integer orderid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_status_history.created_at
	 * @mbggenerated
	 */
	private Date createdat;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_status_history.updated_at
	 * @mbggenerated
	 */
	private Date updatedat;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_status_history.updated_by
	 * @mbggenerated
	 */
	private String updatedby;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_status_history.is_active
	 * @mbggenerated
	 */
	private Byte isactive;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_status_history.is_customer_notified
	 * @mbggenerated
	 */
	private Byte iscustomernotified;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_status_history.order_status
	 * @mbggenerated
	 */
	private String order_status;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_status_history.is_magento
	 * @mbggenerated
	 */
	private Byte ismagento;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_status_history.comments
	 * @mbggenerated
	 */
	private String comments;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_status_history.id
	 * @return  the value of tbl_order_status_history.id
	 * @mbggenerated
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_status_history.id
	 * @param id  the value for tbl_order_status_history.id
	 * @mbggenerated
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_status_history.order_id
	 * @return  the value of tbl_order_status_history.order_id
	 * @mbggenerated
	 */
	public Integer getOrderid() {
		return orderid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_status_history.order_id
	 * @param orderid  the value for tbl_order_status_history.order_id
	 * @mbggenerated
	 */
	public void setOrderid(Integer orderid) {
		this.orderid = orderid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_status_history.created_at
	 * @return  the value of tbl_order_status_history.created_at
	 * @mbggenerated
	 */
	public Date getCreatedat() {
		return createdat;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_status_history.created_at
	 * @param createdat  the value for tbl_order_status_history.created_at
	 * @mbggenerated
	 */
	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_status_history.updated_at
	 * @return  the value of tbl_order_status_history.updated_at
	 * @mbggenerated
	 */
	public Date getUpdatedat() {
		return updatedat;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_status_history.updated_at
	 * @param updatedat  the value for tbl_order_status_history.updated_at
	 * @mbggenerated
	 */
	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_status_history.updated_by
	 * @return  the value of tbl_order_status_history.updated_by
	 * @mbggenerated
	 */
	public String getUpdatedby() {
		return updatedby;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_status_history.updated_by
	 * @param updatedby  the value for tbl_order_status_history.updated_by
	 * @mbggenerated
	 */
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby == null ? null : updatedby.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_status_history.is_active
	 * @return  the value of tbl_order_status_history.is_active
	 * @mbggenerated
	 */
	public Byte getIsactive() {
		return isactive;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_status_history.is_active
	 * @param isactive  the value for tbl_order_status_history.is_active
	 * @mbggenerated
	 */
	public void setIsactive(Byte isactive) {
		this.isactive = isactive;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_status_history.is_customer_notified
	 * @return  the value of tbl_order_status_history.is_customer_notified
	 * @mbggenerated
	 */
	public Byte getIscustomernotified() {
		return iscustomernotified;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_status_history.is_customer_notified
	 * @param iscustomernotified  the value for tbl_order_status_history.is_customer_notified
	 * @mbggenerated
	 */
	public void setIscustomernotified(Byte iscustomernotified) {
		this.iscustomernotified = iscustomernotified;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_status_history.order_status
	 * @return  the value of tbl_order_status_history.order_status
	 * @mbggenerated
	 */
	public String getOrder_status() {
		return order_status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_status_history.order_status
	 * @param order_status  the value for tbl_order_status_history.order_status
	 * @mbggenerated
	 */
	public void setOrder_status(String order_status) {
		this.order_status = order_status == null ? null : order_status.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_status_history.is_magento
	 * @return  the value of tbl_order_status_history.is_magento
	 * @mbggenerated
	 */
	public Byte getIsmagento() {
		return ismagento;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_status_history.is_magento
	 * @param ismagento  the value for tbl_order_status_history.is_magento
	 * @mbggenerated
	 */
	public void setIsmagento(Byte ismagento) {
		this.ismagento = ismagento;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_status_history.comments
	 * @return  the value of tbl_order_status_history.comments
	 * @mbggenerated
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_status_history.comments
	 * @param comments  the value for tbl_order_status_history.comments
	 * @mbggenerated
	 */
	public void setComments(String comments) {
		this.comments = comments == null ? null : comments.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_status_history
	 * @mbggenerated
	 */
	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		MagentoOrderStatus other = (MagentoOrderStatus) that;
		return (this.getId() == null ? other.getId() == null : this.getId()
				.equals(other.getId()))
				&& (this.getOrderid() == null ? other.getOrderid() == null
						: this.getOrderid().equals(other.getOrderid()))
				&& (this.getCreatedat() == null ? other.getCreatedat() == null
						: this.getCreatedat().equals(other.getCreatedat()))
				&& (this.getUpdatedat() == null ? other.getUpdatedat() == null
						: this.getUpdatedat().equals(other.getUpdatedat()))
				&& (this.getUpdatedby() == null ? other.getUpdatedby() == null
						: this.getUpdatedby().equals(other.getUpdatedby()))
				&& (this.getIsactive() == null ? other.getIsactive() == null
						: this.getIsactive().equals(other.getIsactive()))
				&& (this.getIscustomernotified() == null ? other
						.getIscustomernotified() == null : this
						.getIscustomernotified().equals(
								other.getIscustomernotified()))
				&& (this.getOrder_status() == null ? other.getOrder_status() == null
						: this.getOrder_status()
								.equals(other.getOrder_status()))
				&& (this.getIsmagento() == null ? other.getIsmagento() == null
						: this.getIsmagento().equals(other.getIsmagento()))
				&& (this.getComments() == null ? other.getComments() == null
						: this.getComments().equals(other.getComments()));
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_status_history
	 * @mbggenerated
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result
				+ ((getOrderid() == null) ? 0 : getOrderid().hashCode());
		result = prime * result
				+ ((getCreatedat() == null) ? 0 : getCreatedat().hashCode());
		result = prime * result
				+ ((getUpdatedat() == null) ? 0 : getUpdatedat().hashCode());
		result = prime * result
				+ ((getUpdatedby() == null) ? 0 : getUpdatedby().hashCode());
		result = prime * result
				+ ((getIsactive() == null) ? 0 : getIsactive().hashCode());
		result = prime
				* result
				+ ((getIscustomernotified() == null) ? 0
						: getIscustomernotified().hashCode());
		result = prime
				* result
				+ ((getOrder_status() == null) ? 0 : getOrder_status()
						.hashCode());
		result = prime * result
				+ ((getIsmagento() == null) ? 0 : getIsmagento().hashCode());
		result = prime * result
				+ ((getComments() == null) ? 0 : getComments().hashCode());
		return result;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_status_history
	 * @mbggenerated
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", orderid=").append(orderid);
		sb.append(", createdat=").append(createdat);
		sb.append(", updatedat=").append(updatedat);
		sb.append(", updatedby=").append(updatedby);
		sb.append(", isactive=").append(isactive);
		sb.append(", iscustomernotified=").append(iscustomernotified);
		sb.append(", order_status=").append(order_status);
		sb.append(", ismagento=").append(ismagento);
		sb.append(", comments=").append(comments);
		sb.append("]");
		return sb.toString();
	}

	
}