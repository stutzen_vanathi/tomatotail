package co.stutzen.tomatotail.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class MagentoProductInfo {
	
	private Integer productid;
	private String sku;
	private String type;

	private String name;

	private BigDecimal weight;

	private BigDecimal price;

	private BigDecimal specialprice;

	
	private Date specialfromdate;
	private Date specialtodate;
	
	private String description;
	
	private String shortdescription;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortdescription() {
		return shortdescription;
	}

	public void setShortdescription(String shortdescription) {
		this.shortdescription = shortdescription;
	}

	private List<MagentoProductTierPrice> productTierPrices;

	public List<MagentoProductTierPrice> getProductTierPrices() {
		return productTierPrices;
	}

	public void setProductTierPrices(
			List<MagentoProductTierPrice> productTierPrices) {
		this.productTierPrices = productTierPrices;
	}

	public Integer getProductid() {
		return productid;
	}
	public void setProductid(Integer productid) {
		this.productid = productid;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku == null ? null : sku.trim();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type == null ? null : type.trim();
	}

	 
	public String getName() {
		return name;
	}

	 
	 
	 public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	
	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	
	public BigDecimal getSpecialprice() {
		return specialprice;
	}

	public void setSpecialprice(BigDecimal specialprice) {
		this.specialprice = specialprice;
	}

	public Date getSpecialfromdate() {
		return specialfromdate;
	}

	public void setSpecialfromdate(Date specialfromdate) {
		this.specialfromdate = specialfromdate;
	}

	public Date getSpecialtodate() {
		return specialtodate;
	}

	public void setSpecialtodate(Date specialtodate) {
		this.specialtodate = specialtodate;
	}

	
	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		MagentoProductInfo other = (MagentoProductInfo) that;
		return (this.getProductid() == null ? other.getProductid() == null
				: this.getProductid().equals(other.getProductid()))
				&& (this.getSku() == null ? other.getSku() == null : this
						.getSku().equals(other.getSku()))
				&& (this.getType() == null ? other.getType() == null : this
						.getType().equals(other.getType()))
				&& (this.getName() == null ? other.getName() == null : this
						.getName().equals(other.getName()))
				&& (this.getWeight() == null ? other.getWeight() == null : this
						.getWeight().equals(other.getWeight()))
				&& (this.getPrice() == null ? other.getPrice() == null : this
						.getPrice().equals(other.getPrice()))
				&& (this.getSpecialprice() == null ? other.getSpecialprice() == null
						: this.getSpecialprice()
								.equals(other.getSpecialprice()))
				&& (this.getSpecialfromdate() == null ? other
						.getSpecialfromdate() == null : this.getSpecialfromdate()
						.equals(other.getSpecialfromdate()))
				&& (this.getSpecialtodate() == null ? other.getSpecialtodate() == null
						: this.getSpecialtodate().equals(
								other.getSpecialtodate()));
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((getProductid() == null) ? 0 : getProductid().hashCode());
		result = prime * result
				+ ((getSku() == null) ? 0 : getSku().hashCode());
		result = prime * result
				+ ((getType() == null) ? 0 : getType().hashCode());
		result = prime * result
				+ ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result
				+ ((getWeight() == null) ? 0 : getWeight().hashCode());
		result = prime * result
				+ ((getPrice() == null) ? 0 : getPrice().hashCode());
		result = prime
				* result
				+ ((getSpecialprice() == null) ? 0 : getSpecialprice()
						.hashCode());
		result = prime
				* result
				+ ((getSpecialfromdate() == null) ? 0 : getSpecialfromdate()
						.hashCode());
		result = prime
				* result
				+ ((getSpecialtodate() == null) ? 0 : getSpecialtodate()
						.hashCode());
		return result;
	}

	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", productid=").append(productid);
		sb.append(", sku=").append(sku);
		sb.append(", type=").append(type);
		sb.append(", name=").append(name);
		sb.append(", weight=").append(weight);
		sb.append(", price=").append(price);
		sb.append(", specialprice=").append(specialprice);
		sb.append(", specialfromate=").append(specialfromdate);
		sb.append(", specialtodate=").append(specialtodate);
		sb.append("]");
		return sb.toString();
	}
}