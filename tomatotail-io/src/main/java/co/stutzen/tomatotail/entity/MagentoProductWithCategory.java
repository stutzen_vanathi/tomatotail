package co.stutzen.tomatotail.entity;

import java.util.List;

public class MagentoProductWithCategory {

	private int productid;
	private String shk;
	private String name;
	private String set;
	private String type;
	private List<String> categoryidlist;
	private List<String> websiteidlist;

	public int getProductid() {
		return productid;
	}

	public void setProductid(int productid) {
		this.productid = productid;
	}

	public String getShk() {
		return shk;
	}

	public void setShk(String shk) {
		this.shk = shk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSet() {
		return set;
	}

	public void setSet(String set) {
		this.set = set;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getCategoryidlist() {
		return categoryidlist;
	}

	public void setCategoryidlist(List<String> categoryidlist) {
		this.categoryidlist = categoryidlist;
	}

	public List<String> getWebsiteidlist() {
		return websiteidlist;
	}

	public void setWebsiteidlist(List<String> websiteidlist) {
		this.websiteidlist = websiteidlist;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((categoryidlist == null) ? 0 : categoryidlist.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + productid;
		result = prime * result + ((set == null) ? 0 : set.hashCode());
		result = prime * result + ((shk == null) ? 0 : shk.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result
				+ ((websiteidlist == null) ? 0 : websiteidlist.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MagentoProductWithCategory other = (MagentoProductWithCategory) obj;
		if (categoryidlist == null) {
			if (other.categoryidlist != null)
				return false;
		} else if (!categoryidlist.equals(other.categoryidlist))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (productid != other.productid)
			return false;
		if (set == null) {
			if (other.set != null)
				return false;
		} else if (!set.equals(other.set))
			return false;
		if (shk == null) {
			if (other.shk != null)
				return false;
		} else if (!shk.equals(other.shk))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (websiteidlist == null) {
			if (other.websiteidlist != null)
				return false;
		} else if (!websiteidlist.equals(other.websiteidlist))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MagentoProductWithCategory [productid=")
				.append(productid).append(", shk=").append(shk)
				.append(", name=").append(name).append(", set=").append(set)
				.append(", type=").append(type).append(", categoryidlist=")
				.append(categoryidlist).append(", websiteidlist=")
				.append(websiteidlist).append("]");
		return builder.toString();
	}

}
