package co.stutzen.tomatotail.entity;

import java.math.BigDecimal;
import java.util.Date;

public class OrderDetails {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.id
	 * @mbggenerated
	 */
	private Integer id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.order_id
	 * @mbggenerated
	 */
	private Integer orderId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.sku
	 * @mbggenerated
	 */
	private String sku;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.available_qty
	 * @mbggenerated
	 */
	private Integer availableQty;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.required_qty
	 * @mbggenerated
	 */
	private Integer requiredQty;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.MRP_price
	 * @mbggenerated
	 */
	private BigDecimal mrpPrice;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.current_price
	 * @mbggenerated
	 */
	private BigDecimal currentPrice;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.required_price
	 * @mbggenerated
	 */
	private BigDecimal requiredPrice;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.availability
	 * @mbggenerated
	 */
	private Integer availability;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.parent_sku
	 * @mbggenerated
	 */
	private String parentSku;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.total_price
	 * @mbggenerated
	 */
	private BigDecimal totalPrice;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.created_by
	 * @mbggenerated
	 */
	private Integer createdBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.modified_by
	 * @mbggenerated
	 */
	private Integer modifiedBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.created_on
	 * @mbggenerated
	 */
	private Date createdOn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.modified_on
	 * @mbggenerated
	 */
	private Date modifiedOn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_order_details.is_active
	 * @mbggenerated
	 */
	private Boolean isActive;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.id
	 * @return  the value of tbl_order_details.id
	 * @mbggenerated
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.id
	 * @param id  the value for tbl_order_details.id
	 * @mbggenerated
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.order_id
	 * @return  the value of tbl_order_details.order_id
	 * @mbggenerated
	 */
	public Integer getOrderId() {
		return orderId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.order_id
	 * @param orderId  the value for tbl_order_details.order_id
	 * @mbggenerated
	 */
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.sku
	 * @return  the value of tbl_order_details.sku
	 * @mbggenerated
	 */
	public String getSku() {
		return sku;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.sku
	 * @param sku  the value for tbl_order_details.sku
	 * @mbggenerated
	 */
	public void setSku(String sku) {
		this.sku = sku == null ? null : sku.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.available_qty
	 * @return  the value of tbl_order_details.available_qty
	 * @mbggenerated
	 */
	public Integer getAvailableQty() {
		return availableQty;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.available_qty
	 * @param availableQty  the value for tbl_order_details.available_qty
	 * @mbggenerated
	 */
	public void setAvailableQty(Integer availableQty) {
		this.availableQty = availableQty;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.required_qty
	 * @return  the value of tbl_order_details.required_qty
	 * @mbggenerated
	 */
	public Integer getRequiredQty() {
		return requiredQty;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.required_qty
	 * @param requiredQty  the value for tbl_order_details.required_qty
	 * @mbggenerated
	 */
	public void setRequiredQty(Integer requiredQty) {
		this.requiredQty = requiredQty;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.MRP_price
	 * @return  the value of tbl_order_details.MRP_price
	 * @mbggenerated
	 */
	public BigDecimal getMrpPrice() {
		return mrpPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.MRP_price
	 * @param mrpPrice  the value for tbl_order_details.MRP_price
	 * @mbggenerated
	 */
	public void setMrpPrice(BigDecimal mrpPrice) {
		this.mrpPrice = mrpPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.current_price
	 * @return  the value of tbl_order_details.current_price
	 * @mbggenerated
	 */
	public BigDecimal getCurrentPrice() {
		return currentPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.current_price
	 * @param currentPrice  the value for tbl_order_details.current_price
	 * @mbggenerated
	 */
	public void setCurrentPrice(BigDecimal currentPrice) {
		this.currentPrice = currentPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.required_price
	 * @return  the value of tbl_order_details.required_price
	 * @mbggenerated
	 */
	public BigDecimal getRequiredPrice() {
		return requiredPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.required_price
	 * @param requiredPrice  the value for tbl_order_details.required_price
	 * @mbggenerated
	 */
	public void setRequiredPrice(BigDecimal requiredPrice) {
		this.requiredPrice = requiredPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.availability
	 * @return  the value of tbl_order_details.availability
	 * @mbggenerated
	 */
	public Integer getAvailability() {
		return availability;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.availability
	 * @param availability  the value for tbl_order_details.availability
	 * @mbggenerated
	 */
	public void setAvailability(Integer availability) {
		this.availability = availability;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.parent_sku
	 * @return  the value of tbl_order_details.parent_sku
	 * @mbggenerated
	 */
	public String getParentSku() {
		return parentSku;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.parent_sku
	 * @param parentSku  the value for tbl_order_details.parent_sku
	 * @mbggenerated
	 */
	public void setParentSku(String parentSku) {
		this.parentSku = parentSku == null ? null : parentSku.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.total_price
	 * @return  the value of tbl_order_details.total_price
	 * @mbggenerated
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.total_price
	 * @param totalPrice  the value for tbl_order_details.total_price
	 * @mbggenerated
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.created_by
	 * @return  the value of tbl_order_details.created_by
	 * @mbggenerated
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.created_by
	 * @param createdBy  the value for tbl_order_details.created_by
	 * @mbggenerated
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.modified_by
	 * @return  the value of tbl_order_details.modified_by
	 * @mbggenerated
	 */
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.modified_by
	 * @param modifiedBy  the value for tbl_order_details.modified_by
	 * @mbggenerated
	 */
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.created_on
	 * @return  the value of tbl_order_details.created_on
	 * @mbggenerated
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.created_on
	 * @param createdOn  the value for tbl_order_details.created_on
	 * @mbggenerated
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.modified_on
	 * @return  the value of tbl_order_details.modified_on
	 * @mbggenerated
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.modified_on
	 * @param modifiedOn  the value for tbl_order_details.modified_on
	 * @mbggenerated
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_order_details.is_active
	 * @return  the value of tbl_order_details.is_active
	 * @mbggenerated
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_order_details.is_active
	 * @param isActive  the value for tbl_order_details.is_active
	 * @mbggenerated
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		OrderDetails other = (OrderDetails) that;
		return (this.getId() == null ? other.getId() == null : this.getId()
				.equals(other.getId()))
				&& (this.getOrderId() == null ? other.getOrderId() == null
						: this.getOrderId().equals(other.getOrderId()))
				&& (this.getSku() == null ? other.getSku() == null : this
						.getSku().equals(other.getSku()))
				&& (this.getAvailableQty() == null ? other.getAvailableQty() == null
						: this.getAvailableQty()
								.equals(other.getAvailableQty()))
				&& (this.getRequiredQty() == null ? other.getRequiredQty() == null
						: this.getRequiredQty().equals(other.getRequiredQty()))
				&& (this.getMrpPrice() == null ? other.getMrpPrice() == null
						: this.getMrpPrice().equals(other.getMrpPrice()))
				&& (this.getCurrentPrice() == null ? other.getCurrentPrice() == null
						: this.getCurrentPrice()
								.equals(other.getCurrentPrice()))
				&& (this.getRequiredPrice() == null ? other.getRequiredPrice() == null
						: this.getRequiredPrice().equals(
								other.getRequiredPrice()))
				&& (this.getAvailability() == null ? other.getAvailability() == null
						: this.getAvailability()
								.equals(other.getAvailability()))
				&& (this.getParentSku() == null ? other.getParentSku() == null
						: this.getParentSku().equals(other.getParentSku()))
				&& (this.getTotalPrice() == null ? other.getTotalPrice() == null
						: this.getTotalPrice().equals(other.getTotalPrice()))
				&& (this.getCreatedBy() == null ? other.getCreatedBy() == null
						: this.getCreatedBy().equals(other.getCreatedBy()))
				&& (this.getModifiedBy() == null ? other.getModifiedBy() == null
						: this.getModifiedBy().equals(other.getModifiedBy()))
				&& (this.getCreatedOn() == null ? other.getCreatedOn() == null
						: this.getCreatedOn().equals(other.getCreatedOn()))
				&& (this.getModifiedOn() == null ? other.getModifiedOn() == null
						: this.getModifiedOn().equals(other.getModifiedOn()))
				&& (this.getIsActive() == null ? other.getIsActive() == null
						: this.getIsActive().equals(other.getIsActive()));
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result
				+ ((getOrderId() == null) ? 0 : getOrderId().hashCode());
		result = prime * result
				+ ((getSku() == null) ? 0 : getSku().hashCode());
		result = prime
				* result
				+ ((getAvailableQty() == null) ? 0 : getAvailableQty()
						.hashCode());
		result = prime
				* result
				+ ((getRequiredQty() == null) ? 0 : getRequiredQty().hashCode());
		result = prime * result
				+ ((getMrpPrice() == null) ? 0 : getMrpPrice().hashCode());
		result = prime
				* result
				+ ((getCurrentPrice() == null) ? 0 : getCurrentPrice()
						.hashCode());
		result = prime
				* result
				+ ((getRequiredPrice() == null) ? 0 : getRequiredPrice()
						.hashCode());
		result = prime
				* result
				+ ((getAvailability() == null) ? 0 : getAvailability()
						.hashCode());
		result = prime * result
				+ ((getParentSku() == null) ? 0 : getParentSku().hashCode());
		result = prime * result
				+ ((getTotalPrice() == null) ? 0 : getTotalPrice().hashCode());
		result = prime * result
				+ ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
		result = prime * result
				+ ((getModifiedBy() == null) ? 0 : getModifiedBy().hashCode());
		result = prime * result
				+ ((getCreatedOn() == null) ? 0 : getCreatedOn().hashCode());
		result = prime * result
				+ ((getModifiedOn() == null) ? 0 : getModifiedOn().hashCode());
		result = prime * result
				+ ((getIsActive() == null) ? 0 : getIsActive().hashCode());
		return result;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", orderId=").append(orderId);
		sb.append(", sku=").append(sku);
		sb.append(", availableQty=").append(availableQty);
		sb.append(", requiredQty=").append(requiredQty);
		sb.append(", mrpPrice=").append(mrpPrice);
		sb.append(", currentPrice=").append(currentPrice);
		sb.append(", requiredPrice=").append(requiredPrice);
		sb.append(", availability=").append(availability);
		sb.append(", parentSku=").append(parentSku);
		sb.append(", totalPrice=").append(totalPrice);
		sb.append(", createdBy=").append(createdBy);
		sb.append(", modifiedBy=").append(modifiedBy);
		sb.append(", createdOn=").append(createdOn);
		sb.append(", modifiedOn=").append(modifiedOn);
		sb.append(", isActive=").append(isActive);
		sb.append("]");
		return sb.toString();
	}	
}