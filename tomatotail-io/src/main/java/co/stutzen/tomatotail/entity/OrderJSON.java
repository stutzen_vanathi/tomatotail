package co.stutzen.tomatotail.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderJSON {

private int id;

private Date dateTime;

private String sourceType;

private int customerId;

private String name;

private String mobile;

private int outletId;

private int addressId;

private String address;

private int noOfItems;

private String status;

	private String cash_type;

	public String getCash_type() {
		return cash_type;
	}

	public void setCash_type(String cash_type) {
		this.cash_type = cash_type;
	}

private List<OrderDetailJSON> orderDetails = new ArrayList<OrderDetailJSON>();

public List<OrderDetailJSON> getOrderDetails() {
	return orderDetails;
}

public void setOrderDetails(List<OrderDetailJSON> orderDetails) {
	this.orderDetails = orderDetails;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public Date getDateTime() {
	return dateTime;
}

public void setDateTime(Date dateTime) {
	this.dateTime = dateTime;
}

public String getSourceType() {
	return sourceType;
}

public void setSourceType(String sourceType) {
	this.sourceType = sourceType;
}

public int getCustomerId() {
	return customerId;
}

public void setCustomerId(int customerId) {
	this.customerId = customerId;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getMobile() {
	return mobile;
}

public void setMobile(String mobile) {
	this.mobile = mobile;
}

public int getOutletId() {
	return outletId;
}

public void setOutletId(int outletId) {
	this.outletId = outletId;
}

public int getAddressId() {
	return addressId;
}

public void setAddressId(int addressId) {
	this.addressId = addressId;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public int getNoOfItems() {
	return noOfItems;
}

public void setNoOfItems(int noOfItems) {
	this.noOfItems = noOfItems;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}
}
