package co.stutzen.tomatotail.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProductRequestExample {
    /**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	public ProductRequestExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value,
				String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1,
				Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(Integer value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(Integer value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(Integer value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(Integer value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(Integer value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<Integer> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<Integer> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(Integer value1, Integer value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(Integer value1, Integer value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andProductNameIsNull() {
			addCriterion("prod_name is null");
			return (Criteria) this;
		}

		public Criteria andProductNameIsNotNull() {
			addCriterion("prod_name is not null");
			return (Criteria) this;
		}

		public Criteria andProductNameEqualTo(String value) {
			addCriterion("prod_name =", value, "productName");
			return (Criteria) this;
		}

		public Criteria andProductNameNotEqualTo(String value) {
			addCriterion("prod_name <>", value, "productName");
			return (Criteria) this;
		}

		public Criteria andProductNameGreaterThan(String value) {
			addCriterion("prod_name >", value, "productName");
			return (Criteria) this;
		}

		public Criteria andProductNameGreaterThanOrEqualTo(String value) {
			addCriterion("prod_name >=", value, "productName");
			return (Criteria) this;
		}

		public Criteria andProductNameLessThan(String value) {
			addCriterion("prod_name <", value, "productName");
			return (Criteria) this;
		}

		public Criteria andProductNameLessThanOrEqualTo(String value) {
			addCriterion("prod_name <=", value, "productName");
			return (Criteria) this;
		}

		public Criteria andProductNameLike(String value) {
			addCriterion("prod_name like", value, "productName");
			return (Criteria) this;
		}

		public Criteria andProductNameNotLike(String value) {
			addCriterion("prod_name not like", value, "productName");
			return (Criteria) this;
		}

		public Criteria andProductNameIn(List<String> values) {
			addCriterion("prod_name in", values, "productName");
			return (Criteria) this;
		}

		public Criteria andProductNameNotIn(List<String> values) {
			addCriterion("prod_name not in", values, "productName");
			return (Criteria) this;
		}

		public Criteria andProductNameBetween(String value1, String value2) {
			addCriterion("prod_name between", value1, value2, "productName");
			return (Criteria) this;
		}

		public Criteria andProductNameNotBetween(String value1, String value2) {
			addCriterion("prod_name not between", value1, value2, "productName");
			return (Criteria) this;
		}

		public Criteria andSkuIsNull() {
			addCriterion("sku is null");
			return (Criteria) this;
		}

		public Criteria andSkuIsNotNull() {
			addCriterion("sku is not null");
			return (Criteria) this;
		}

		public Criteria andSkuEqualTo(String value) {
			addCriterion("sku =", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuNotEqualTo(String value) {
			addCriterion("sku <>", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuGreaterThan(String value) {
			addCriterion("sku >", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuGreaterThanOrEqualTo(String value) {
			addCriterion("sku >=", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuLessThan(String value) {
			addCriterion("sku <", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuLessThanOrEqualTo(String value) {
			addCriterion("sku <=", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuLike(String value) {
			addCriterion("sku like", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuNotLike(String value) {
			addCriterion("sku not like", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuIn(List<String> values) {
			addCriterion("sku in", values, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuNotIn(List<String> values) {
			addCriterion("sku not in", values, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuBetween(String value1, String value2) {
			addCriterion("sku between", value1, value2, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuNotBetween(String value1, String value2) {
			addCriterion("sku not between", value1, value2, "sku");
			return (Criteria) this;
		}

		public Criteria andCategoryIdIsNull() {
			addCriterion("category_id is null");
			return (Criteria) this;
		}

		public Criteria andCategoryIdIsNotNull() {
			addCriterion("category_id is not null");
			return (Criteria) this;
		}

		public Criteria andCategoryIdEqualTo(Integer value) {
			addCriterion("category_id =", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdNotEqualTo(Integer value) {
			addCriterion("category_id <>", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdGreaterThan(Integer value) {
			addCriterion("category_id >", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("category_id >=", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdLessThan(Integer value) {
			addCriterion("category_id <", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdLessThanOrEqualTo(Integer value) {
			addCriterion("category_id <=", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdIn(List<Integer> values) {
			addCriterion("category_id in", values, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdNotIn(List<Integer> values) {
			addCriterion("category_id not in", values, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdBetween(Integer value1, Integer value2) {
			addCriterion("category_id between", value1, value2, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdNotBetween(Integer value1, Integer value2) {
			addCriterion("category_id not between", value1, value2,
					"categoryId");
			return (Criteria) this;
		}

		public Criteria andBrandIdIsNull() {
			addCriterion("brand_id is null");
			return (Criteria) this;
		}

		public Criteria andBrandIdIsNotNull() {
			addCriterion("brand_id is not null");
			return (Criteria) this;
		}

		public Criteria andBrandIdEqualTo(Integer value) {
			addCriterion("brand_id =", value, "brandId");
			return (Criteria) this;
		}

		public Criteria andBrandIdNotEqualTo(Integer value) {
			addCriterion("brand_id <>", value, "brandId");
			return (Criteria) this;
		}

		public Criteria andBrandIdGreaterThan(Integer value) {
			addCriterion("brand_id >", value, "brandId");
			return (Criteria) this;
		}

		public Criteria andBrandIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("brand_id >=", value, "brandId");
			return (Criteria) this;
		}

		public Criteria andBrandIdLessThan(Integer value) {
			addCriterion("brand_id <", value, "brandId");
			return (Criteria) this;
		}

		public Criteria andBrandIdLessThanOrEqualTo(Integer value) {
			addCriterion("brand_id <=", value, "brandId");
			return (Criteria) this;
		}

		public Criteria andBrandIdIn(List<Integer> values) {
			addCriterion("brand_id in", values, "brandId");
			return (Criteria) this;
		}

		public Criteria andBrandIdNotIn(List<Integer> values) {
			addCriterion("brand_id not in", values, "brandId");
			return (Criteria) this;
		}

		public Criteria andBrandIdBetween(Integer value1, Integer value2) {
			addCriterion("brand_id between", value1, value2, "brandId");
			return (Criteria) this;
		}

		public Criteria andBrandIdNotBetween(Integer value1, Integer value2) {
			addCriterion("brand_id not between", value1, value2, "brandId");
			return (Criteria) this;
		}

		public Criteria andManuIdIsNull() {
			addCriterion("manu_id is null");
			return (Criteria) this;
		}

		public Criteria andManuIdIsNotNull() {
			addCriterion("manu_id is not null");
			return (Criteria) this;
		}

		public Criteria andManuIdEqualTo(Integer value) {
			addCriterion("manu_id =", value, "manuId");
			return (Criteria) this;
		}

		public Criteria andManuIdNotEqualTo(Integer value) {
			addCriterion("manu_id <>", value, "manuId");
			return (Criteria) this;
		}

		public Criteria andManuIdGreaterThan(Integer value) {
			addCriterion("manu_id >", value, "manuId");
			return (Criteria) this;
		}

		public Criteria andManuIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("manu_id >=", value, "manuId");
			return (Criteria) this;
		}

		public Criteria andManuIdLessThan(Integer value) {
			addCriterion("manu_id <", value, "manuId");
			return (Criteria) this;
		}

		public Criteria andManuIdLessThanOrEqualTo(Integer value) {
			addCriterion("manu_id <=", value, "manuId");
			return (Criteria) this;
		}

		public Criteria andManuIdIn(List<Integer> values) {
			addCriterion("manu_id in", values, "manuId");
			return (Criteria) this;
		}

		public Criteria andManuIdNotIn(List<Integer> values) {
			addCriterion("manu_id not in", values, "manuId");
			return (Criteria) this;
		}

		public Criteria andManuIdBetween(Integer value1, Integer value2) {
			addCriterion("manu_id between", value1, value2, "manuId");
			return (Criteria) this;
		}

		public Criteria andManuIdNotBetween(Integer value1, Integer value2) {
			addCriterion("manu_id not between", value1, value2, "manuId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdIsNull() {
			addCriterion("partner_id is null");
			return (Criteria) this;
		}

		public Criteria andPartnerIdIsNotNull() {
			addCriterion("partner_id is not null");
			return (Criteria) this;
		}

		public Criteria andPartnerIdEqualTo(Integer value) {
			addCriterion("partner_id =", value, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdNotEqualTo(Integer value) {
			addCriterion("partner_id <>", value, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdGreaterThan(Integer value) {
			addCriterion("partner_id >", value, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("partner_id >=", value, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdLessThan(Integer value) {
			addCriterion("partner_id <", value, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdLessThanOrEqualTo(Integer value) {
			addCriterion("partner_id <=", value, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdIn(List<Integer> values) {
			addCriterion("partner_id in", values, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdNotIn(List<Integer> values) {
			addCriterion("partner_id not in", values, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdBetween(Integer value1, Integer value2) {
			addCriterion("partner_id between", value1, value2, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdNotBetween(Integer value1, Integer value2) {
			addCriterion("partner_id not between", value1, value2, "partnerId");
			return (Criteria) this;
		}

		public Criteria andStatusIsNull() {
			addCriterion("status is null");
			return (Criteria) this;
		}

		public Criteria andStatusIsNotNull() {
			addCriterion("status is not null");
			return (Criteria) this;
		}

		public Criteria andStatusEqualTo(String value) {
			addCriterion("status =", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotEqualTo(String value) {
			addCriterion("status <>", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThan(String value) {
			addCriterion("status >", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThanOrEqualTo(String value) {
			addCriterion("status >=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThan(String value) {
			addCriterion("status <", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThanOrEqualTo(String value) {
			addCriterion("status <=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLike(String value) {
			addCriterion("status like", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotLike(String value) {
			addCriterion("status not like", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusIn(List<String> values) {
			addCriterion("status in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotIn(List<String> values) {
			addCriterion("status not in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusBetween(String value1, String value2) {
			addCriterion("status between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotBetween(String value1, String value2) {
			addCriterion("status not between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andMrpPriceIsNull() {
			addCriterion("mrp_price is null");
			return (Criteria) this;
		}

		public Criteria andMrpPriceIsNotNull() {
			addCriterion("mrp_price is not null");
			return (Criteria) this;
		}

		public Criteria andMrpPriceEqualTo(BigDecimal value) {
			addCriterion("mrp_price =", value, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceNotEqualTo(BigDecimal value) {
			addCriterion("mrp_price <>", value, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceGreaterThan(BigDecimal value) {
			addCriterion("mrp_price >", value, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("mrp_price >=", value, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceLessThan(BigDecimal value) {
			addCriterion("mrp_price <", value, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceLessThanOrEqualTo(BigDecimal value) {
			addCriterion("mrp_price <=", value, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceIn(List<BigDecimal> values) {
			addCriterion("mrp_price in", values, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceNotIn(List<BigDecimal> values) {
			addCriterion("mrp_price not in", values, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("mrp_price between", value1, value2, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceNotBetween(BigDecimal value1,
				BigDecimal value2) {
			addCriterion("mrp_price not between", value1, value2, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceIsNull() {
			addCriterion("selling_price is null");
			return (Criteria) this;
		}

		public Criteria andSellingPriceIsNotNull() {
			addCriterion("selling_price is not null");
			return (Criteria) this;
		}

		public Criteria andSellingPriceEqualTo(BigDecimal value) {
			addCriterion("selling_price =", value, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceNotEqualTo(BigDecimal value) {
			addCriterion("selling_price <>", value, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceGreaterThan(BigDecimal value) {
			addCriterion("selling_price >", value, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("selling_price >=", value, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceLessThan(BigDecimal value) {
			addCriterion("selling_price <", value, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceLessThanOrEqualTo(BigDecimal value) {
			addCriterion("selling_price <=", value, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceIn(List<BigDecimal> values) {
			addCriterion("selling_price in", values, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceNotIn(List<BigDecimal> values) {
			addCriterion("selling_price not in", values, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceBetween(BigDecimal value1,
				BigDecimal value2) {
			addCriterion("selling_price between", value1, value2,
					"sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceNotBetween(BigDecimal value1,
				BigDecimal value2) {
			addCriterion("selling_price not between", value1, value2,
					"sellingPrice");
			return (Criteria) this;
		}

		public Criteria andPurchasePriceIsNull() {
			addCriterion("purchase_price is null");
			return (Criteria) this;
		}

		public Criteria andPurchasePriceIsNotNull() {
			addCriterion("purchase_price is not null");
			return (Criteria) this;
		}

		public Criteria andPurchasePriceEqualTo(BigDecimal value) {
			addCriterion("purchase_price =", value, "purchasePrice");
			return (Criteria) this;
		}

		public Criteria andPurchasePriceNotEqualTo(BigDecimal value) {
			addCriterion("purchase_price <>", value, "purchasePrice");
			return (Criteria) this;
		}

		public Criteria andPurchasePriceGreaterThan(BigDecimal value) {
			addCriterion("purchase_price >", value, "purchasePrice");
			return (Criteria) this;
		}

		public Criteria andPurchasePriceGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("purchase_price >=", value, "purchasePrice");
			return (Criteria) this;
		}

		public Criteria andPurchasePriceLessThan(BigDecimal value) {
			addCriterion("purchase_price <", value, "purchasePrice");
			return (Criteria) this;
		}

		public Criteria andPurchasePriceLessThanOrEqualTo(BigDecimal value) {
			addCriterion("purchase_price <=", value, "purchasePrice");
			return (Criteria) this;
		}

		public Criteria andPurchasePriceIn(List<BigDecimal> values) {
			addCriterion("purchase_price in", values, "purchasePrice");
			return (Criteria) this;
		}

		public Criteria andPurchasePriceNotIn(List<BigDecimal> values) {
			addCriterion("purchase_price not in", values, "purchasePrice");
			return (Criteria) this;
		}

		public Criteria andPurchasePriceBetween(BigDecimal value1,
				BigDecimal value2) {
			addCriterion("purchase_price between", value1, value2,
					"purchasePrice");
			return (Criteria) this;
		}

		public Criteria andPurchasePriceNotBetween(BigDecimal value1,
				BigDecimal value2) {
			addCriterion("purchase_price not between", value1, value2,
					"purchasePrice");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNull() {
			addCriterion("created_by is null");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNotNull() {
			addCriterion("created_by is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedByEqualTo(Integer value) {
			addCriterion("created_by =", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotEqualTo(Integer value) {
			addCriterion("created_by <>", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThan(Integer value) {
			addCriterion("created_by >", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThanOrEqualTo(Integer value) {
			addCriterion("created_by >=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThan(Integer value) {
			addCriterion("created_by <", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThanOrEqualTo(Integer value) {
			addCriterion("created_by <=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByIn(List<Integer> values) {
			addCriterion("created_by in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotIn(List<Integer> values) {
			addCriterion("created_by not in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByBetween(Integer value1, Integer value2) {
			addCriterion("created_by between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotBetween(Integer value1, Integer value2) {
			addCriterion("created_by not between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByIsNull() {
			addCriterion("modified_by is null");
			return (Criteria) this;
		}

		public Criteria andModifiedByIsNotNull() {
			addCriterion("modified_by is not null");
			return (Criteria) this;
		}

		public Criteria andModifiedByEqualTo(Integer value) {
			addCriterion("modified_by =", value, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByNotEqualTo(Integer value) {
			addCriterion("modified_by <>", value, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByGreaterThan(Integer value) {
			addCriterion("modified_by >", value, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByGreaterThanOrEqualTo(Integer value) {
			addCriterion("modified_by >=", value, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByLessThan(Integer value) {
			addCriterion("modified_by <", value, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByLessThanOrEqualTo(Integer value) {
			addCriterion("modified_by <=", value, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByIn(List<Integer> values) {
			addCriterion("modified_by in", values, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByNotIn(List<Integer> values) {
			addCriterion("modified_by not in", values, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByBetween(Integer value1, Integer value2) {
			addCriterion("modified_by between", value1, value2, "modifiedBy");
			return (Criteria) this;
		}

		public Criteria andModifiedByNotBetween(Integer value1, Integer value2) {
			addCriterion("modified_by not between", value1, value2,
					"modifiedBy");
			return (Criteria) this;
		}

		public Criteria andCreatedOnIsNull() {
			addCriterion("created_on is null");
			return (Criteria) this;
		}

		public Criteria andCreatedOnIsNotNull() {
			addCriterion("created_on is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedOnEqualTo(Date value) {
			addCriterion("created_on =", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnNotEqualTo(Date value) {
			addCriterion("created_on <>", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnGreaterThan(Date value) {
			addCriterion("created_on >", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnGreaterThanOrEqualTo(Date value) {
			addCriterion("created_on >=", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnLessThan(Date value) {
			addCriterion("created_on <", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnLessThanOrEqualTo(Date value) {
			addCriterion("created_on <=", value, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnIn(List<Date> values) {
			addCriterion("created_on in", values, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnNotIn(List<Date> values) {
			addCriterion("created_on not in", values, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnBetween(Date value1, Date value2) {
			addCriterion("created_on between", value1, value2, "createdOn");
			return (Criteria) this;
		}

		public Criteria andCreatedOnNotBetween(Date value1, Date value2) {
			addCriterion("created_on not between", value1, value2, "createdOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnIsNull() {
			addCriterion("modified_on is null");
			return (Criteria) this;
		}

		public Criteria andModifiedOnIsNotNull() {
			addCriterion("modified_on is not null");
			return (Criteria) this;
		}

		public Criteria andModifiedOnEqualTo(Date value) {
			addCriterion("modified_on =", value, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnNotEqualTo(Date value) {
			addCriterion("modified_on <>", value, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnGreaterThan(Date value) {
			addCriterion("modified_on >", value, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnGreaterThanOrEqualTo(Date value) {
			addCriterion("modified_on >=", value, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnLessThan(Date value) {
			addCriterion("modified_on <", value, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnLessThanOrEqualTo(Date value) {
			addCriterion("modified_on <=", value, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnIn(List<Date> values) {
			addCriterion("modified_on in", values, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnNotIn(List<Date> values) {
			addCriterion("modified_on not in", values, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnBetween(Date value1, Date value2) {
			addCriterion("modified_on between", value1, value2, "modifiedOn");
			return (Criteria) this;
		}

		public Criteria andModifiedOnNotBetween(Date value1, Date value2) {
			addCriterion("modified_on not between", value1, value2,
					"modifiedOn");
			return (Criteria) this;
		}

		public Criteria andIsActiveIsNull() {
			addCriterion("is_active is null");
			return (Criteria) this;
		}

		public Criteria andIsActiveIsNotNull() {
			addCriterion("is_active is not null");
			return (Criteria) this;
		}

		public Criteria andIsActiveEqualTo(Boolean value) {
			addCriterion("is_active =", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveNotEqualTo(Boolean value) {
			addCriterion("is_active <>", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveGreaterThan(Boolean value) {
			addCriterion("is_active >", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveGreaterThanOrEqualTo(Boolean value) {
			addCriterion("is_active >=", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveLessThan(Boolean value) {
			addCriterion("is_active <", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveLessThanOrEqualTo(Boolean value) {
			addCriterion("is_active <=", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveIn(List<Boolean> values) {
			addCriterion("is_active in", values, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveNotIn(List<Boolean> values) {
			addCriterion("is_active not in", values, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveBetween(Boolean value1, Boolean value2) {
			addCriterion("is_active between", value1, value2, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveNotBetween(Boolean value1, Boolean value2) {
			addCriterion("is_active not between", value1, value2, "isActive");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table tbl_product_request
	 * @mbggenerated
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue,
				String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table tbl_product_request
     *
     * @mbggenerated do_not_delete_during_merge
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }
}