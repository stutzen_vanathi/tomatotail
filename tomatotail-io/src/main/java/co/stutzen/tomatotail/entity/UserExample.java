package co.stutzen.tomatotail.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserExample {
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	public UserExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	public void or(Criteria criteria) {
			oredCriteria.add(criteria);
		}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
		}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
		}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
			return criteria;
		}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
		}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to
	 * the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value,
				String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1,
				Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andUseridIsNull() {
			addCriterion("userid is null");
			return (Criteria) this;
		}

		public Criteria andUseridIsNotNull() {
			addCriterion("userid is not null");
			return (Criteria) this;
		}

		public Criteria andUseridEqualTo(Integer value) {
			addCriterion("userid =", value, "userid");
			return (Criteria) this;
		}

		public Criteria andUseridNotEqualTo(Integer value) {
			addCriterion("userid <>", value, "userid");
			return (Criteria) this;
		}

		public Criteria andUseridGreaterThan(Integer value) {
			addCriterion("userid >", value, "userid");
			return (Criteria) this;
		}

		public Criteria andUseridGreaterThanOrEqualTo(Integer value) {
			addCriterion("userid >=", value, "userid");
			return (Criteria) this;
		}

		public Criteria andUseridLessThan(Integer value) {
			addCriterion("userid <", value, "userid");
			return (Criteria) this;
		}

		public Criteria andUseridLessThanOrEqualTo(Integer value) {
			addCriterion("userid <=", value, "userid");
			return (Criteria) this;
		}

		public Criteria andUseridIn(List<Integer> values) {
			addCriterion("userid in", values, "userid");
			return (Criteria) this;
		}

		public Criteria andUseridNotIn(List<Integer> values) {
			addCriterion("userid not in", values, "userid");
			return (Criteria) this;
			}

		public Criteria andUseridBetween(Integer value1, Integer value2) {
			addCriterion("userid between", value1, value2, "userid");
			return (Criteria) this;
			}

		public Criteria andUseridNotBetween(Integer value1, Integer value2) {
			addCriterion("userid not between", value1, value2, "userid");
			return (Criteria) this;
			}

		public Criteria andUsernameIsNull() {
			addCriterion("username is null");
			return (Criteria) this;
		}

		public Criteria andUsernameIsNotNull() {
			addCriterion("username is not null");
			return (Criteria) this;
		}

		public Criteria andUsernameEqualTo(String value) {
			addCriterion("username =", value, "username");
			return (Criteria) this;
		}

		public Criteria andUsernameNotEqualTo(String value) {
			addCriterion("username <>", value, "username");
			return (Criteria) this;
		}

		public Criteria andUsernameGreaterThan(String value) {
			addCriterion("username >", value, "username");
			return (Criteria) this;
		}

		public Criteria andUsernameGreaterThanOrEqualTo(String value) {
			addCriterion("username >=", value, "username");
			return (Criteria) this;
		}

		public Criteria andUsernameLessThan(String value) {
			addCriterion("username <", value, "username");
			return (Criteria) this;
		}

		public Criteria andUsernameLessThanOrEqualTo(String value) {
			addCriterion("username <=", value, "username");
			return (Criteria) this;
		}

		public Criteria andUsernameLike(String value) {
			addCriterion("username like", value, "username");
			return (Criteria) this;
		}

		public Criteria andUsernameNotLike(String value) {
			addCriterion("username not like", value, "username");
			return (Criteria) this;
		}

		public Criteria andUsernameIn(List<String> values) {
			addCriterion("username in", values, "username");
			return (Criteria) this;
		}

		public Criteria andUsernameNotIn(List<String> values) {
			addCriterion("username not in", values, "username");
			return (Criteria) this;
		}

		public Criteria andUsernameBetween(String value1, String value2) {
			addCriterion("username between", value1, value2, "username");
			return (Criteria) this;
		}

		public Criteria andUsernameNotBetween(String value1, String value2) {
			addCriterion("username not between", value1, value2, "username");
			return (Criteria) this;
		}

		public Criteria andPasswordIsNull() {
			addCriterion("password is null");
			return (Criteria) this;
		}

		public Criteria andPasswordIsNotNull() {
			addCriterion("password is not null");
			return (Criteria) this;
		}

		public Criteria andPasswordEqualTo(String value) {
			addCriterion("password =", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordNotEqualTo(String value) {
			addCriterion("password <>", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordGreaterThan(String value) {
			addCriterion("password >", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordGreaterThanOrEqualTo(String value) {
			addCriterion("password >=", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordLessThan(String value) {
			addCriterion("password <", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordLessThanOrEqualTo(String value) {
			addCriterion("password <=", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordLike(String value) {
			addCriterion("password like", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordNotLike(String value) {
			addCriterion("password not like", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordIn(List<String> values) {
			addCriterion("password in", values, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordNotIn(List<String> values) {
			addCriterion("password not in", values, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordBetween(String value1, String value2) {
			addCriterion("password between", value1, value2, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordNotBetween(String value1, String value2) {
			addCriterion("password not between", value1, value2, "password");
			return (Criteria) this;
		}

		public Criteria andIsactiveIsNull() {
			addCriterion("isactive is null");
			return (Criteria) this;
		}

		public Criteria andIsactiveIsNotNull() {
			addCriterion("isactive is not null");
			return (Criteria) this;
		}

		public Criteria andIsactiveEqualTo(Byte value) {
			addCriterion("isactive =", value, "isactive");
			return (Criteria) this;
		}

		public Criteria andIsactiveNotEqualTo(Byte value) {
			addCriterion("isactive <>", value, "isactive");
			return (Criteria) this;
		}

		public Criteria andIsactiveGreaterThan(Byte value) {
			addCriterion("isactive >", value, "isactive");
			return (Criteria) this;
		}

		public Criteria andIsactiveGreaterThanOrEqualTo(Byte value) {
			addCriterion("isactive >=", value, "isactive");
			return (Criteria) this;
		}

		public Criteria andIsactiveLessThan(Byte value) {
			addCriterion("isactive <", value, "isactive");
			return (Criteria) this;
		}

		public Criteria andIsactiveLessThanOrEqualTo(Byte value) {
			addCriterion("isactive <=", value, "isactive");
			return (Criteria) this;
		}

		public Criteria andIsactiveIn(List<Byte> values) {
			addCriterion("isactive in", values, "isactive");
			return (Criteria) this;
		}

		public Criteria andIsactiveNotIn(List<Byte> values) {
			addCriterion("isactive not in", values, "isactive");
			return (Criteria) this;
		}

		public Criteria andIsactiveBetween(Byte value1, Byte value2) {
			addCriterion("isactive between", value1, value2, "isactive");
			return (Criteria) this;
		}

		public Criteria andIsactiveNotBetween(Byte value1, Byte value2) {
			addCriterion("isactive not between", value1, value2, "isactive");
			return (Criteria) this;
		}

		public Criteria andNameIsNull() {
			addCriterion("name is null");
			return (Criteria) this;
		}

		public Criteria andNameIsNotNull() {
			addCriterion("name is not null");
			return (Criteria) this;
		}

		public Criteria andNameEqualTo(String value) {
			addCriterion("name =", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotEqualTo(String value) {
			addCriterion("name <>", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThan(String value) {
			addCriterion("name >", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThanOrEqualTo(String value) {
			addCriterion("name >=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThan(String value) {
			addCriterion("name <", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThanOrEqualTo(String value) {
			addCriterion("name <=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLike(String value) {
			addCriterion("name like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotLike(String value) {
			addCriterion("name not like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameIn(List<String> values) {
			addCriterion("name in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotIn(List<String> values) {
			addCriterion("name not in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameBetween(String value1, String value2) {
			addCriterion("name between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotBetween(String value1, String value2) {
			addCriterion("name not between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andMobileNumberIsNull() {
			addCriterion("mobile_number is null");
			return (Criteria) this;
		}

		public Criteria andMobileNumberIsNotNull() {
			addCriterion("mobile_number is not null");
			return (Criteria) this;
		}

		public Criteria andMobileNumberEqualTo(String value) {
			addCriterion("mobile_number =", value, "mobileNumber");
			return (Criteria) this;
		}

		public Criteria andMobileNumberNotEqualTo(String value) {
			addCriterion("mobile_number <>", value, "mobileNumber");
			return (Criteria) this;
		}

		public Criteria andMobileNumberGreaterThan(String value) {
			addCriterion("mobile_number >", value, "mobileNumber");
			return (Criteria) this;
		}

		public Criteria andMobileNumberGreaterThanOrEqualTo(String value) {
			addCriterion("mobile_number >=", value, "mobileNumber");
			return (Criteria) this;
		}

		public Criteria andMobileNumberLessThan(String value) {
			addCriterion("mobile_number <", value, "mobileNumber");
			return (Criteria) this;
		}

		public Criteria andMobileNumberLessThanOrEqualTo(String value) {
			addCriterion("mobile_number <=", value, "mobileNumber");
			return (Criteria) this;
		}

		public Criteria andMobileNumberLike(String value) {
			addCriterion("mobile_number like", value, "mobileNumber");
			return (Criteria) this;
		}

		public Criteria andMobileNumberNotLike(String value) {
			addCriterion("mobile_number not like", value, "mobileNumber");
			return (Criteria) this;
		}

		public Criteria andMobileNumberIn(List<String> values) {
			addCriterion("mobile_number in", values, "mobileNumber");
			return (Criteria) this;
		}

		public Criteria andMobileNumberNotIn(List<String> values) {
			addCriterion("mobile_number not in", values, "mobileNumber");
			return (Criteria) this;
		}

		public Criteria andMobileNumberBetween(String value1, String value2) {
			addCriterion("mobile_number between", value1, value2,
					"mobileNumber");
			return (Criteria) this;
		}

		public Criteria andMobileNumberNotBetween(String value1, String value2) {
			addCriterion("mobile_number not between", value1, value2,
					"mobileNumber");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberIsNull() {
			addCriterion("phone_number is null");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberIsNotNull() {
			addCriterion("phone_number is not null");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberEqualTo(String value) {
			addCriterion("phone_number =", value, "phoneNumber");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberNotEqualTo(String value) {
			addCriterion("phone_number <>", value, "phoneNumber");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberGreaterThan(String value) {
			addCriterion("phone_number >", value, "phoneNumber");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberGreaterThanOrEqualTo(String value) {
			addCriterion("phone_number >=", value, "phoneNumber");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberLessThan(String value) {
			addCriterion("phone_number <", value, "phoneNumber");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberLessThanOrEqualTo(String value) {
			addCriterion("phone_number <=", value, "phoneNumber");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberLike(String value) {
			addCriterion("phone_number like", value, "phoneNumber");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberNotLike(String value) {
			addCriterion("phone_number not like", value, "phoneNumber");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberIn(List<String> values) {
			addCriterion("phone_number in", values, "phoneNumber");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberNotIn(List<String> values) {
			addCriterion("phone_number not in", values, "phoneNumber");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberBetween(String value1, String value2) {
			addCriterion("phone_number between", value1, value2, "phoneNumber");
			return (Criteria) this;
		}

		public Criteria andPhoneNumberNotBetween(String value1, String value2) {
			addCriterion("phone_number not between", value1, value2,
					"phoneNumber");
			return (Criteria) this;
		}

		public Criteria andEmailIsNull() {
			addCriterion("email is null");
			return (Criteria) this;
		}

		public Criteria andEmailIsNotNull() {
			addCriterion("email is not null");
			return (Criteria) this;
		}

		public Criteria andEmailEqualTo(String value) {
			addCriterion("email =", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailNotEqualTo(String value) {
			addCriterion("email <>", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailGreaterThan(String value) {
			addCriterion("email >", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailGreaterThanOrEqualTo(String value) {
			addCriterion("email >=", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailLessThan(String value) {
			addCriterion("email <", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailLessThanOrEqualTo(String value) {
			addCriterion("email <=", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailLike(String value) {
			addCriterion("email like", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailNotLike(String value) {
			addCriterion("email not like", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailIn(List<String> values) {
			addCriterion("email in", values, "email");
			return (Criteria) this;
		}

		public Criteria andEmailNotIn(List<String> values) {
			addCriterion("email not in", values, "email");
			return (Criteria) this;
		}

		public Criteria andEmailBetween(String value1, String value2) {
			addCriterion("email between", value1, value2, "email");
			return (Criteria) this;
		}

		public Criteria andEmailNotBetween(String value1, String value2) {
			addCriterion("email not between", value1, value2, "email");
			return (Criteria) this;
		}

		public Criteria andAddress1IsNull() {
			addCriterion("address1 is null");
			return (Criteria) this;
		}

		public Criteria andAddress1IsNotNull() {
			addCriterion("address1 is not null");
			return (Criteria) this;
		}

		public Criteria andAddress1EqualTo(String value) {
			addCriterion("address1 =", value, "address1");
			return (Criteria) this;
		}

		public Criteria andAddress1NotEqualTo(String value) {
			addCriterion("address1 <>", value, "address1");
			return (Criteria) this;
		}

		public Criteria andAddress1GreaterThan(String value) {
			addCriterion("address1 >", value, "address1");
			return (Criteria) this;
		}

		public Criteria andAddress1GreaterThanOrEqualTo(String value) {
			addCriterion("address1 >=", value, "address1");
			return (Criteria) this;
		}

		public Criteria andAddress1LessThan(String value) {
			addCriterion("address1 <", value, "address1");
			return (Criteria) this;
		}

		public Criteria andAddress1LessThanOrEqualTo(String value) {
			addCriterion("address1 <=", value, "address1");
			return (Criteria) this;
		}

		public Criteria andAddress1Like(String value) {
			addCriterion("address1 like", value, "address1");
			return (Criteria) this;
		}

		public Criteria andAddress1NotLike(String value) {
			addCriterion("address1 not like", value, "address1");
			return (Criteria) this;
		}

		public Criteria andAddress1In(List<String> values) {
			addCriterion("address1 in", values, "address1");
			return (Criteria) this;
		}

		public Criteria andAddress1NotIn(List<String> values) {
			addCriterion("address1 not in", values, "address1");
			return (Criteria) this;
		}

		public Criteria andAddress1Between(String value1, String value2) {
			addCriterion("address1 between", value1, value2, "address1");
			return (Criteria) this;
		}

		public Criteria andAddress1NotBetween(String value1, String value2) {
			addCriterion("address1 not between", value1, value2, "address1");
			return (Criteria) this;
		}

		public Criteria andAddress2IsNull() {
			addCriterion("address2 is null");
			return (Criteria) this;
		}

		public Criteria andAddress2IsNotNull() {
			addCriterion("address2 is not null");
			return (Criteria) this;
		}

		public Criteria andAddress2EqualTo(String value) {
			addCriterion("address2 =", value, "address2");
			return (Criteria) this;
		}

		public Criteria andAddress2NotEqualTo(String value) {
			addCriterion("address2 <>", value, "address2");
			return (Criteria) this;
		}

		public Criteria andAddress2GreaterThan(String value) {
			addCriterion("address2 >", value, "address2");
			return (Criteria) this;
		}

		public Criteria andAddress2GreaterThanOrEqualTo(String value) {
			addCriterion("address2 >=", value, "address2");
			return (Criteria) this;
		}

		public Criteria andAddress2LessThan(String value) {
			addCriterion("address2 <", value, "address2");
			return (Criteria) this;
		}

		public Criteria andAddress2LessThanOrEqualTo(String value) {
			addCriterion("address2 <=", value, "address2");
			return (Criteria) this;
		}

		public Criteria andAddress2Like(String value) {
			addCriterion("address2 like", value, "address2");
			return (Criteria) this;
		}

		public Criteria andAddress2NotLike(String value) {
			addCriterion("address2 not like", value, "address2");
			return (Criteria) this;
		}

		public Criteria andAddress2In(List<String> values) {
			addCriterion("address2 in", values, "address2");
			return (Criteria) this;
		}

		public Criteria andAddress2NotIn(List<String> values) {
			addCriterion("address2 not in", values, "address2");
			return (Criteria) this;
		}

		public Criteria andAddress2Between(String value1, String value2) {
			addCriterion("address2 between", value1, value2, "address2");
			return (Criteria) this;
		}

		public Criteria andAddress2NotBetween(String value1, String value2) {
			addCriterion("address2 not between", value1, value2, "address2");
			return (Criteria) this;
		}

		public Criteria andGenderIsNull() {
			addCriterion("gender is null");
			return (Criteria) this;
		}

		public Criteria andGenderIsNotNull() {
			addCriterion("gender is not null");
			return (Criteria) this;
		}

		public Criteria andGenderEqualTo(String value) {
			addCriterion("gender =", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderNotEqualTo(String value) {
			addCriterion("gender <>", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderGreaterThan(String value) {
			addCriterion("gender >", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderGreaterThanOrEqualTo(String value) {
			addCriterion("gender >=", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderLessThan(String value) {
			addCriterion("gender <", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderLessThanOrEqualTo(String value) {
			addCriterion("gender <=", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderLike(String value) {
			addCriterion("gender like", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderNotLike(String value) {
			addCriterion("gender not like", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderIn(List<String> values) {
			addCriterion("gender in", values, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderNotIn(List<String> values) {
			addCriterion("gender not in", values, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderBetween(String value1, String value2) {
			addCriterion("gender between", value1, value2, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderNotBetween(String value1, String value2) {
			addCriterion("gender not between", value1, value2, "gender");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNull() {
			addCriterion("created_by is null");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNotNull() {
			addCriterion("created_by is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedByEqualTo(Integer value) {
			addCriterion("created_by =", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotEqualTo(Integer value) {
			addCriterion("created_by <>", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThan(Integer value) {
			addCriterion("created_by >", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThanOrEqualTo(Integer value) {
			addCriterion("created_by >=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThan(Integer value) {
			addCriterion("created_by <", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThanOrEqualTo(Integer value) {
			addCriterion("created_by <=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByIn(List<Integer> values) {
			addCriterion("created_by in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotIn(List<Integer> values) {
			addCriterion("created_by not in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByBetween(Integer value1, Integer value2) {
			addCriterion("created_by between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotBetween(Integer value1, Integer value2) {
			addCriterion("created_by not between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andUpdatedByIsNull() {
			addCriterion("updated_by is null");
			return (Criteria) this;
		}

		public Criteria andUpdatedByIsNotNull() {
			addCriterion("updated_by is not null");
			return (Criteria) this;
		}

		public Criteria andUpdatedByEqualTo(Integer value) {
			addCriterion("updated_by =", value, "updatedBy");
			return (Criteria) this;
		}

		public Criteria andUpdatedByNotEqualTo(Integer value) {
			addCriterion("updated_by <>", value, "updatedBy");
			return (Criteria) this;
		}

		public Criteria andUpdatedByGreaterThan(Integer value) {
			addCriterion("updated_by >", value, "updatedBy");
			return (Criteria) this;
		}

		public Criteria andUpdatedByGreaterThanOrEqualTo(Integer value) {
			addCriterion("updated_by >=", value, "updatedBy");
			return (Criteria) this;
		}

		public Criteria andUpdatedByLessThan(Integer value) {
			addCriterion("updated_by <", value, "updatedBy");
			return (Criteria) this;
		}

		public Criteria andUpdatedByLessThanOrEqualTo(Integer value) {
			addCriterion("updated_by <=", value, "updatedBy");
			return (Criteria) this;
		}

		public Criteria andUpdatedByIn(List<Integer> values) {
			addCriterion("updated_by in", values, "updatedBy");
			return (Criteria) this;
		}

		public Criteria andUpdatedByNotIn(List<Integer> values) {
			addCriterion("updated_by not in", values, "updatedBy");
			return (Criteria) this;
		}

		public Criteria andUpdatedByBetween(Integer value1, Integer value2) {
			addCriterion("updated_by between", value1, value2, "updatedBy");
			return (Criteria) this;
		}

		public Criteria andUpdatedByNotBetween(Integer value1, Integer value2) {
			addCriterion("updated_by not between", value1, value2, "updatedBy");
			return (Criteria) this;
		}

		public Criteria andCreatedTimeIsNull() {
			addCriterion("created_time is null");
			return (Criteria) this;
		}

		public Criteria andCreatedTimeIsNotNull() {
			addCriterion("created_time is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedTimeEqualTo(Date value) {
			addCriterion("created_time =", value, "createdTime");
			return (Criteria) this;
		}

		public Criteria andCreatedTimeNotEqualTo(Date value) {
			addCriterion("created_time <>", value, "createdTime");
			return (Criteria) this;
		}

		public Criteria andCreatedTimeGreaterThan(Date value) {
			addCriterion("created_time >", value, "createdTime");
			return (Criteria) this;
		}

		public Criteria andCreatedTimeGreaterThanOrEqualTo(Date value) {
			addCriterion("created_time >=", value, "createdTime");
			return (Criteria) this;
		}

		public Criteria andCreatedTimeLessThan(Date value) {
			addCriterion("created_time <", value, "createdTime");
			return (Criteria) this;
		}

		public Criteria andCreatedTimeLessThanOrEqualTo(Date value) {
			addCriterion("created_time <=", value, "createdTime");
			return (Criteria) this;
		}

		public Criteria andCreatedTimeIn(List<Date> values) {
			addCriterion("created_time in", values, "createdTime");
			return (Criteria) this;
		}

		public Criteria andCreatedTimeNotIn(List<Date> values) {
			addCriterion("created_time not in", values, "createdTime");
			return (Criteria) this;
		}

		public Criteria andCreatedTimeBetween(Date value1, Date value2) {
			addCriterion("created_time between", value1, value2, "createdTime");
			return (Criteria) this;
		}

		public Criteria andCreatedTimeNotBetween(Date value1, Date value2) {
			addCriterion("created_time not between", value1, value2,
					"createdTime");
			return (Criteria) this;
		}

		public Criteria andUpdatedTimeIsNull() {
			addCriterion("updated_time is null");
			return (Criteria) this;
		}

		public Criteria andUpdatedTimeIsNotNull() {
			addCriterion("updated_time is not null");
			return (Criteria) this;
		}

		public Criteria andUpdatedTimeEqualTo(Date value) {
			addCriterion("updated_time =", value, "updatedTime");
			return (Criteria) this;
		}

		public Criteria andUpdatedTimeNotEqualTo(Date value) {
			addCriterion("updated_time <>", value, "updatedTime");
			return (Criteria) this;
		}

		public Criteria andUpdatedTimeGreaterThan(Date value) {
			addCriterion("updated_time >", value, "updatedTime");
			return (Criteria) this;
		}

		public Criteria andUpdatedTimeGreaterThanOrEqualTo(Date value) {
			addCriterion("updated_time >=", value, "updatedTime");
			return (Criteria) this;
		}

		public Criteria andUpdatedTimeLessThan(Date value) {
			addCriterion("updated_time <", value, "updatedTime");
			return (Criteria) this;
		}

		public Criteria andUpdatedTimeLessThanOrEqualTo(Date value) {
			addCriterion("updated_time <=", value, "updatedTime");
			return (Criteria) this;
		}

		public Criteria andUpdatedTimeIn(List<Date> values) {
			addCriterion("updated_time in", values, "updatedTime");
			return (Criteria) this;
		}

		public Criteria andUpdatedTimeNotIn(List<Date> values) {
			addCriterion("updated_time not in", values, "updatedTime");
			return (Criteria) this;
		}

		public Criteria andUpdatedTimeBetween(Date value1, Date value2) {
			addCriterion("updated_time between", value1, value2, "updatedTime");
			return (Criteria) this;
		}

		public Criteria andUpdatedTimeNotBetween(Date value1, Date value2) {
			addCriterion("updated_time not between", value1, value2,
					"updatedTime");
			return (Criteria) this;
		}

		public Criteria andPartnerIdIsNull() {
			addCriterion("partner_id is null");
			return (Criteria) this;
		}

		public Criteria andPartnerIdIsNotNull() {
			addCriterion("partner_id is not null");
			return (Criteria) this;
		}

		public Criteria andPartnerIdEqualTo(Integer value) {
			addCriterion("partner_id =", value, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdNotEqualTo(Integer value) {
			addCriterion("partner_id <>", value, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdGreaterThan(Integer value) {
			addCriterion("partner_id >", value, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("partner_id >=", value, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdLessThan(Integer value) {
			addCriterion("partner_id <", value, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdLessThanOrEqualTo(Integer value) {
			addCriterion("partner_id <=", value, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdIn(List<Integer> values) {
			addCriterion("partner_id in", values, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdNotIn(List<Integer> values) {
			addCriterion("partner_id not in", values, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdBetween(Integer value1, Integer value2) {
			addCriterion("partner_id between", value1, value2, "partnerId");
			return (Criteria) this;
		}

		public Criteria andPartnerIdNotBetween(Integer value1, Integer value2) {
			addCriterion("partner_id not between", value1, value2, "partnerId");
			return (Criteria) this;
		}
		}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to
	 * the database table tbl_user
	 * 
	 * @mbggenerated
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
			}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue,
				String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
		}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to
	 * the database table tbl_user
	 *
	 * @mbggenerated do_not_delete_during_merge
	 */
	public static class Criteria extends GeneratedCriteria {

		protected Criteria() {
			super();
		}
	}
}