package co.stutzen.tomatotail.mapper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import co.stutzen.tomatotail.entity.PartnerStoreMapping;
import co.stutzen.tomatotail.entity.PartnerStoreMappingExample.Criteria;
import co.stutzen.tomatotail.entity.PartnerStoreMappingExample.Criterion;
import co.stutzen.tomatotail.entity.PartnerStoreMappingExample;
import java.util.List;
import java.util.Map;

public class PartnerStoreMappingSqlProvider {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_partner_store_mapping
	 * @mbggenerated
	 */
	public String countByExample(PartnerStoreMappingExample example) {
		BEGIN();
		SELECT("count(*)");
		FROM("tbl_partner_store_mapping");
		applyWhere(example, false);
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_partner_store_mapping
	 * @mbggenerated
	 */
	public String deleteByExample(PartnerStoreMappingExample example) {
		BEGIN();
		DELETE_FROM("tbl_partner_store_mapping");
		applyWhere(example, false);
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_partner_store_mapping
	 * @mbggenerated
	 */
	public String insertSelective(PartnerStoreMapping record) {
		BEGIN();
		INSERT_INTO("tbl_partner_store_mapping");
		if (record.getStoreid() != null) {
			VALUES("store_id", "#{storeid,jdbcType=INTEGER}");
		}
		if (record.getPartnerid() != null) {
			VALUES("partner_id", "#{partnerid,jdbcType=INTEGER}");
		}
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_partner_store_mapping
	 * @mbggenerated
	 */
	public String selectByExample(PartnerStoreMappingExample example) {
		BEGIN();
		if (example != null && example.isDistinct()) {
			SELECT_DISTINCT("id");
		} else {
			SELECT("id");
		}
		SELECT("store_id");
		SELECT("partner_id");
		FROM("tbl_partner_store_mapping");
		applyWhere(example, false);
		if (example != null && example.getOrderByClause() != null) {
			ORDER_BY(example.getOrderByClause());
		}
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_partner_store_mapping
	 * @mbggenerated
	 */
	public String updateByExampleSelective(Map<String, Object> parameter) {
		PartnerStoreMapping record = (PartnerStoreMapping) parameter
				.get("record");
		PartnerStoreMappingExample example = (PartnerStoreMappingExample) parameter
				.get("example");
		BEGIN();
		UPDATE("tbl_partner_store_mapping");
		if (record.getId() != null) {
			SET("id = #{record.id,jdbcType=INTEGER}");
		}
		if (record.getStoreid() != null) {
			SET("store_id = #{record.storeid,jdbcType=INTEGER}");
		}
		if (record.getPartnerid() != null) {
			SET("partner_id = #{record.partnerid,jdbcType=INTEGER}");
		}
		applyWhere(example, true);
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_partner_store_mapping
	 * @mbggenerated
	 */
	public String updateByExample(Map<String, Object> parameter) {
		BEGIN();
		UPDATE("tbl_partner_store_mapping");
		SET("id = #{record.id,jdbcType=INTEGER}");
		SET("store_id = #{record.storeid,jdbcType=INTEGER}");
		SET("partner_id = #{record.partnerid,jdbcType=INTEGER}");
		PartnerStoreMappingExample example = (PartnerStoreMappingExample) parameter
				.get("example");
		applyWhere(example, true);
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_partner_store_mapping
	 * @mbggenerated
	 */
	public String updateByPrimaryKeySelective(PartnerStoreMapping record) {
		BEGIN();
		UPDATE("tbl_partner_store_mapping");
		if (record.getStoreid() != null) {
			SET("store_id = #{storeid,jdbcType=INTEGER}");
		}
		if (record.getPartnerid() != null) {
			SET("partner_id = #{partnerid,jdbcType=INTEGER}");
		}
		WHERE("id = #{id,jdbcType=INTEGER}");
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_partner_store_mapping
	 * @mbggenerated
	 */
	protected void applyWhere(PartnerStoreMappingExample example,
			boolean includeExamplePhrase) {
		if (example == null) {
			return;
		}
		String parmPhrase1;
		String parmPhrase1_th;
		String parmPhrase2;
		String parmPhrase2_th;
		String parmPhrase3;
		String parmPhrase3_th;
		if (includeExamplePhrase) {
			parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
			parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
			parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
			parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
			parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
			parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
		} else {
			parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
			parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
			parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
			parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
			parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
			parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
		}
		StringBuilder sb = new StringBuilder();
		List<Criteria> oredCriteria = example.getOredCriteria();
		boolean firstCriteria = true;
		for (int i = 0; i < oredCriteria.size(); i++) {
			Criteria criteria = oredCriteria.get(i);
			if (criteria.isValid()) {
				if (firstCriteria) {
					firstCriteria = false;
				} else {
					sb.append(" or ");
				}
				sb.append('(');
				List<Criterion> criterions = criteria.getAllCriteria();
				boolean firstCriterion = true;
				for (int j = 0; j < criterions.size(); j++) {
					Criterion criterion = criterions.get(j);
					if (firstCriterion) {
						firstCriterion = false;
					} else {
						sb.append(" and ");
					}
					if (criterion.isNoValue()) {
						sb.append(criterion.getCondition());
					} else if (criterion.isSingleValue()) {
						if (criterion.getTypeHandler() == null) {
							sb.append(String.format(parmPhrase1,
									criterion.getCondition(), i, j));
						} else {
							sb.append(String.format(parmPhrase1_th,
									criterion.getCondition(), i, j,
									criterion.getTypeHandler()));
						}
					} else if (criterion.isBetweenValue()) {
						if (criterion.getTypeHandler() == null) {
							sb.append(String.format(parmPhrase2,
									criterion.getCondition(), i, j, i, j));
						} else {
							sb.append(String.format(parmPhrase2_th,
									criterion.getCondition(), i, j,
									criterion.getTypeHandler(), i, j,
									criterion.getTypeHandler()));
						}
					} else if (criterion.isListValue()) {
						sb.append(criterion.getCondition());
						sb.append(" (");
						List<?> listItems = (List<?>) criterion.getValue();
						boolean comma = false;
						for (int k = 0; k < listItems.size(); k++) {
							if (comma) {
								sb.append(", ");
							} else {
								comma = true;
							}
							if (criterion.getTypeHandler() == null) {
								sb.append(String.format(parmPhrase3, i, j, k));
							} else {
								sb.append(String.format(parmPhrase3_th, i, j,
										k, criterion.getTypeHandler()));
							}
						}
						sb.append(')');
					}
				}
				sb.append(')');
			}
		}
		if (sb.length() > 0) {
			WHERE(sb.toString());
		}
	}
}