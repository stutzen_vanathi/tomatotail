package co.stutzen.tomatotail.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Bill;
import co.stutzen.tomatotail.entity.BillWithBLOBs;

public interface BillDetailService extends SWFService<Bill>{

	int updateStatus(int billId, String status, int userId);

	List<BillWithBLOBs> getBillList(String status, int outletid, int billid,
			String fromDate, String toDate, int begin, int end);

	void detailSave(BillWithBLOBs bill, int userId);
}
