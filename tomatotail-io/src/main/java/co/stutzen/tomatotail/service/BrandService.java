package co.stutzen.tomatotail.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Brand;

public interface BrandService extends SWFService<Brand> {

	List<Brand> listOfBrand(String brandName, String manufacturerName,
			int start, int limit);

	Brand selectDuplicateBrandWithManufacturer(String brandName,
			Integer manufacturerId);

	List<Brand> brandList(int start, int limit);

	Brand duplicateBrand(String brandName);

	

}
