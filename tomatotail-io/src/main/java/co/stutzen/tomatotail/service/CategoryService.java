package co.stutzen.tomatotail.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Category;

public interface CategoryService extends SWFService<Category>{
	
	List<Category> categoryList(String categoryName, int categoryId, int start, int limit);

	List<Category> listOfCategory();

	List<Category> getSubCategorysByParentId(String categoryId);

	List<Category> getSubCategoryList();

	Category duplicateCategoryName(String name);
	
}

