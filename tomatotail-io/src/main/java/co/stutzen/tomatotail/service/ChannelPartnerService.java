package co.stutzen.tomatotail.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.ChannelPartner;
import co.stutzen.tomatotail.entity.ChannelPartnerStoreMapping;
import co.stutzen.tomatotail.entity.ProductWithBLOBs;

public interface ChannelPartnerService extends SWFService<ChannelPartner> {

	boolean saveStorewithPartner(ChannelPartnerStoreMapping p);

	ChannelPartner channelPartnerById(int partnerId);

	List<ChannelPartner> channelPartnerList(int start, int limit);

	List<ChannelPartner> channelPartners();

	List<ProductWithBLOBs> skuDetailsForPartner(int id, String sku, int begin, int end);

	List<ChannelPartner> getActiveList(int id, String name, int begin, int end);
}
