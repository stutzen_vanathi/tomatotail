package co.stutzen.tomatotail.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.ChannelPriceMapping;
import co.stutzen.tomatotail.entity.Product;

public interface ChannelPriceMappingService extends
		SWFService<ChannelPriceMapping> {

	List<Product> listOfProductByName(String name, int start, int limit);

	List<Product> productPurchasePriceById(int partnerId, int start, int limit);

	ChannelPriceMapping duplicatePriceMapping(int partnerId,
			int productId);
}


