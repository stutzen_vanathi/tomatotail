package co.stutzen.tomatotail.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.CompetitorPrice;
import co.stutzen.tomatotail.entity.ProductWithBLOBs;

public interface CompetitorPriceService extends SWFService<CompetitorPrice> {

	List<ProductWithBLOBs> listOfCompetitorPriceBySearch(String manuName,
			String prodName, String brandName, int start, int limit);

}
