package co.stutzen.tomatotail.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Competitor;

public interface CompetitorService extends SWFService<Competitor> {

	int countOfCompetitor();

	List<Competitor> listOfCompetitorByName(String competitorName,int start,int limit);

	List<Competitor> competitorList(int start, int limit);

}
