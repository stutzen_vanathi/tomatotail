package co.stutzen.tomatotail.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Customer;

public interface CustomerService extends SWFService<Customer> {

	int saveCustomer(Customer customer);

	int updateCustomer(Customer customer);

	List<Customer> getCustomerList(String name, String mobile, int start, int limit);

}
