package co.stutzen.tomatotail.service;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.ExternalProductInfoWithBLOBs;

public interface ExternalProductInfoService extends SWFService<ExternalProductInfoWithBLOBs> {

	void insertProductInfoToLocalProductTbl(ExternalProductInfoWithBLOBs productInfo);
	

}
