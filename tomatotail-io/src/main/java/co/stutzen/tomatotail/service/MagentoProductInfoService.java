package co.stutzen.tomatotail.service;

import org.ksoap2.serialization.SoapObject;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.MagentoProductInfo;

public interface MagentoProductInfoService  extends SWFService<MagentoProductInfo>{

	MagentoProductInfo getProductInfo(SoapObject productInfo);
	

}
