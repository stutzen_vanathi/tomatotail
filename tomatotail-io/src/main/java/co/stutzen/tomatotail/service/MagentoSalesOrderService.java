package co.stutzen.tomatotail.service;

import java.util.Date;
import java.util.List;

import org.ksoap2.serialization.SoapObject;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.MagentoOrderItem;
import co.stutzen.tomatotail.entity.MagentoSalesOrder;
import co.stutzen.tomatotail.entity.ProductManufactureDate;

public interface MagentoSalesOrderService extends SWFService<MagentoSalesOrder> {

	List<MagentoSalesOrder> getSalesOrders();

	Date getRecentUpdatedDate();

	boolean updateByIncrementId(MagentoSalesOrder record);

	boolean updateByPrimaryKey(MagentoSalesOrder record);

	String getLocalSalesOrderIncrementId(String incrementId);

	List<MagentoSalesOrder> saveOrderList(SoapObject soapObj,
			int localSalesOrderCount);

	boolean saveOrUpdateOrderDetails(
			List<MagentoSalesOrder> magentoSalesOrders, boolean isSave);

	int count();

	List<MagentoSalesOrder> getOrderList(int start, int limit);

	List<MagentoSalesOrder> getOrdersBySearch(int storeId, String status,
			int start, int limit);

	MagentoSalesOrder getOrderDetailsByOrderId(int orderId);

	boolean updateOrderStatusById(MagentoSalesOrder salesOrder);

	void saveManufactureDate(ProductManufactureDate p);

	void saveOrderedItem(MagentoOrderItem orderItem, int productStatus);
}
