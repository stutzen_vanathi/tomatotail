package co.stutzen.tomatotail.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Manufacturer;

public interface ManufacturerService extends SWFService<Manufacturer> {

	List<Manufacturer> manufacturerByName(String name, int start, int limit);

	List<Manufacturer> manufacturerList(int start, int limit);

	int updateManufacturer(Manufacturer manufacturer);

	Manufacturer duplicateManufacturer(String name);

}
