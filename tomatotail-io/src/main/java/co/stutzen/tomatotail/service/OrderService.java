package co.stutzen.tomatotail.service;

import java.math.BigDecimal;
import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.Order;
import co.stutzen.tomatotail.entity.OrderJSON;

public interface OrderService extends SWFService<Order>{

	int saveOrder(Order order);

	List<Order> orderList(int id, String status, String date, String name,int outletId, String sourceType,
			int begin, int end);

	int updateOrder(Order order);

	BigDecimal getOrderTotalPrice(int orderId);

	int updateOrderStatus(int orderId, int modifiedBy, String status);

	List<OrderJSON> orderedItemList(int orderId, String status, String date, int outletId, int begin, int end);

}
