package co.stutzen.tomatotail.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.ProductRequestWithBLOBs;

public interface ProductRequestService extends SWFService<ProductRequestWithBLOBs>{

	List<ProductRequestWithBLOBs> listAll(String prodReqId, String partnerId, String partnerCity, String prodName, String status, int start, int limit);

	int modify(ProductRequestWithBLOBs prodRequest);

	int save(ProductRequestWithBLOBs prodRequest);

	int updateStatus(int prodReqId, String status, int userId);

}
