package co.stutzen.tomatotail.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.ProductWithBLOBs;

public interface ProductService extends SWFService<ProductWithBLOBs> {

	List<ProductWithBLOBs> listOfProduct(String productName, String brandName,
			String manufacturerName, int start, int limit);

	List<ProductWithBLOBs> duplicateProductList(String productName,
			Integer manufacturerId);

	ProductWithBLOBs productWithCompetitorPrices(int productId);

	String duplicateProductSku(String sku);

	List<ProductWithBLOBs> productList(String productName, int partnerId,
			String sku, int start, int limit);

	List<ProductWithBLOBs> productListAll(int start, int limit);

	int saveProduct(ProductWithBLOBs product);

	int updateProduct(ProductWithBLOBs product);

}
