package co.stutzen.tomatotail.service;

import java.util.List;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.UnProcessedOrder;

public interface UnProcessedOrderService extends SWFService<UnProcessedOrder> {

	List<UnProcessedOrder> getUnProcessedOrderList(int id, String date,
			String status, String name, String mobile, int begin, int end);

}
