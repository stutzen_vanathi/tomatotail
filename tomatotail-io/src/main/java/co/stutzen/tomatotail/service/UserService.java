package co.stutzen.tomatotail.service;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.tomatotail.entity.User;
import co.stutzen.tomatotail.entity.UserRole;

public interface UserService extends SWFService<User>{

	User getPasswordByUserName(String name);

	void updatePassword(User userObjFromDB);

	User duplicateEmail(String email);

	User duplicateUser(String username);

	void saveRole(UserRole role);

	void updateRole(User user, String oldUserName);

	String oldUserName(Integer userid);

	void updateUser(User user);

	User getUserInfo(int id);
}
