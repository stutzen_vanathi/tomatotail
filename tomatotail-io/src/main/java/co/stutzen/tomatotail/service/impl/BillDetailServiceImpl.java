package co.stutzen.tomatotail.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.Bill;
import co.stutzen.tomatotail.entity.BillDetailsWithBLOBs;
import co.stutzen.tomatotail.entity.BillExample;
import co.stutzen.tomatotail.entity.BillWithBLOBs;
import co.stutzen.tomatotail.mapper.BillDetailsMapper;
import co.stutzen.tomatotail.mapper.BillMapper;
import co.stutzen.tomatotail.mapper.OrderMapper;
import co.stutzen.tomatotail.service.BillDetailService;

@Service
public class BillDetailServiceImpl extends SWFServiceImpl<Bill> implements BillDetailService{

	@Autowired
	private BillMapper billMapper;

	@Autowired
	private BillDetailsMapper billdetailMapper;

	@Autowired private OrderMapper orderMapper;

	@Override
	public int updateStatus(int billId, String status, int userId) {
		int updateCount = billMapper.updateStatus(billId, status, userId);
		return updateCount;
	}

	@Override
	public List<BillWithBLOBs> getBillList(String status, int outletId,
			int billId, String fromDate, String toDate, int begin, int end) {
		List<BillWithBLOBs> list = new ArrayList<BillWithBLOBs>();
		if(begin ==0 && end ==0)
			list = billMapper.getBillListCount(status, outletId, billId,
					fromDate, toDate);
		else
			list = billMapper.getBillListWithLimit(status, outletId, billId,
					fromDate, toDate, new BillExample(), new RowBounds(begin,
							end));
		return list;
	}

	@Override
	public void detailSave(BillWithBLOBs bill, int userId) {
		if (bill.getBillDetailList() != null
				&& bill.getBillDetailList().size() > 0)
			for (BillDetailsWithBLOBs bd : bill.getBillDetailList()) {
				bd.setBillId(bill.getId());
				bd.setCreatedBy(userId);
				bd.setModifiedBy(userId);
				bd.setIsActive(true);
				billdetailMapper.insert(bd);
			}
	}

}
