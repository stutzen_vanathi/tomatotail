package co.stutzen.tomatotail.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.Brand;
import co.stutzen.tomatotail.entity.BrandExample;
import co.stutzen.tomatotail.mapper.BrandMapper;
import co.stutzen.tomatotail.service.BrandService;

@Service
public class BrandServiceImpl extends SWFServiceImpl<Brand> implements
		BrandService {

	@Autowired
	BrandMapper brandMapper;

	@Override
	public List<Brand> listOfBrand(String brandName, String manuName,
			int start, int limit) {
		List<Brand> list = null;
		if (start == 0 && limit == 0) {
			list = brandMapper.listOfBrandByManufacturerAndBrandNames(brandName,
					manuName);
		} else {
			list = brandMapper.listOfBrandByManufacturerAndBrandNameWithLimit(
					brandName, manuName, new BrandExample(), new RowBounds(
							start, limit));
		}
		return list;

	}

	@Override
	public Brand selectDuplicateBrandWithManufacturer(String brandName,
			Integer manufacturerId) {
		return brandMapper.selectDuplicateBrandWithManufacturer(brandName,
				manufacturerId);
	}

	@Override
	public List<Brand> brandList(int start, int limit) {
		return brandMapper.selectByExampleWithRowbounds(new BrandExample(),
				new RowBounds(start, limit));
	}

	@Override
	public Brand duplicateBrand(String brandName) {
		return brandMapper.duplicateBrand(brandName);
	}
}
