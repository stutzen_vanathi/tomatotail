package co.stutzen.tomatotail.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.Category;
import co.stutzen.tomatotail.entity.CategoryExample;
import co.stutzen.tomatotail.mapper.CategoryMapper;
import co.stutzen.tomatotail.service.CategoryService;
import co.stutzen.tomatotail.util.CommonConstants;

@Service
public class CategoryServiceImpl extends SWFServiceImpl<Category> implements
		CategoryService {

	@Autowired
	CategoryMapper categoryMapper;

	@Override
	public List<Category> categoryList(String categoryName, int categoryId,
			int start, int limit) {
		List<Category> list = null;
		if (start == 0 && limit == 0) {
			list = categoryMapper.categoryListCount(categoryName, categoryId);
		} else {
			list = categoryMapper.categoryListWithLimit(categoryName,
					categoryId, new CategoryExample(), new RowBounds(start,
							limit));
		}
		return list;
	}

	@Override
	public List<Category> listOfCategory() {
		return categoryMapper.parentCategoryList(CommonConstants.IS_PARENT);
	}

	@Override
	public List<Category> getSubCategorysByParentId(String categoryId) {
		return categoryMapper.subCategoryListByParentId(categoryId);
	}

	@Override
	public List<Category> getSubCategoryList() {
		return categoryMapper.subCategoryList();
	}

	@Override
	public Category duplicateCategoryName(String name) {
		return categoryMapper.duplicateCategoryName(name);
	}

}
