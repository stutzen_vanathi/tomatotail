package co.stutzen.tomatotail.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.ChannelPartner;
import co.stutzen.tomatotail.entity.ChannelPartnerExample;
import co.stutzen.tomatotail.entity.ChannelPartnerStoreMapping;
import co.stutzen.tomatotail.entity.ProductExample;
import co.stutzen.tomatotail.entity.ProductWithBLOBs;
import co.stutzen.tomatotail.entity.Store;
import co.stutzen.tomatotail.mapper.ChannelPartnerMapper;
import co.stutzen.tomatotail.mapper.ChannelPartnerStoreMappingMapper;
import co.stutzen.tomatotail.mapper.ProductMapper;
import co.stutzen.tomatotail.service.ChannelPartnerService;

@Service
public class ChannelPartnerServiceImpl extends SWFServiceImpl<ChannelPartner>
		implements ChannelPartnerService {

	@Autowired
	private ChannelPartnerStoreMappingMapper storeMappingMapper;

	@Autowired
	private ChannelPartnerMapper partnerMapper;

	@Autowired
	private ProductMapper productMapper;

	@Override
	public boolean saveStorewithPartner(ChannelPartnerStoreMapping p) {
		boolean isSuccess = false;
		boolean isStoreExist = false;
		/*
		 * Find storeId for particular partner
		 */
		List<ChannelPartnerStoreMapping> listOfStore = storeMappingMapper.getStoreListById(p.getPartnerid());
		int size = listOfStore.size();
		/*
		 * Getting storeId one by one
		 */
		for (Store s : p.getStoreList()) {
			p.setStoreid(s.getId());
			isStoreExist = false;
			for (ChannelPartnerStoreMapping mapping : listOfStore) {
				if (mapping.getStoreid() == s.getId()) {
					isStoreExist = true;
					isSuccess = true;
					break;
				}
			}
			if (size == 0 || isStoreExist == false) {
				storeMappingMapper.insert(p);
				isSuccess = true;
			}
		}
		return isSuccess;
	}

	@Override
	public ChannelPartner channelPartnerById(int partnerId) {
		return partnerMapper.channelPartnerById(partnerId);
	}

	@Override
	public List<ChannelPartner> channelPartnerList(int start, int limit) {
		return partnerMapper.selectByExampleWithRowbounds(new ChannelPartnerExample(), new RowBounds(start, limit));
	}

	@Override
	public List<ChannelPartner> channelPartners() {
		return partnerMapper.getChannelPartners();
	}

	@Override
	public List<ProductWithBLOBs> skuDetailsForPartner(int id, String sku, int begin,
			int end) {
		List<ProductWithBLOBs> list = new ArrayList<ProductWithBLOBs>();
		if (begin == 0 && end == 0) {
			list = productMapper.productListByPartnerId(id, sku);
		} else
			list = productMapper.productListForPartner(id, sku, new ProductExample(), new RowBounds(begin, end));
		return list;
	}

	@Override
	public List<ChannelPartner> getActiveList(int id, String name, int begin,
			int end) {
		List<ChannelPartner> list = new ArrayList<ChannelPartner>();
		if (begin == 0 && end == 0) {
			list = partnerMapper.getactivelistcount(id, name);
		} else {
			list = partnerMapper.getactivelist(id, name,
					new ChannelPartnerExample(), new RowBounds(begin, end));
		}
		return list;
	}
}
