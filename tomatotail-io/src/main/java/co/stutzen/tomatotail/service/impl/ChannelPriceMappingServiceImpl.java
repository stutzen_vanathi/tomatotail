package co.stutzen.tomatotail.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.ChannelPriceMapping;
import co.stutzen.tomatotail.entity.ChannelPriceMappingExample;
import co.stutzen.tomatotail.entity.Product;
import co.stutzen.tomatotail.mapper.ChannelPriceMappingMapper;
import co.stutzen.tomatotail.mapper.ProductMapper;
import co.stutzen.tomatotail.service.ChannelPriceMappingService;

@Service
public class ChannelPriceMappingServiceImpl extends
		SWFServiceImpl<ChannelPriceMapping> implements
		ChannelPriceMappingService {

	@Autowired
	private ProductMapper productMapper;

	@Autowired
	private ChannelPriceMappingMapper priceMappingMapper;

	@Override
	public List<Product> listOfProductByName(String name, int start, int limit) {

		List<Product> list = null;
		if (!("".equals("name"))) {
			if (start == 0 && limit == 0) {
				list = productMapper.productListByName(name);

			} else {
				list = productMapper.listOfProductByName(name,
						new ChannelPriceMappingExample(), new RowBounds(start,
								limit));
			}
		}
		return list;
	}


	@Override
	public List<Product> productPurchasePriceById(int partnerId, int start,
			int limit) {
		List<Product> priceMappingList = null;
		if (start == 0 && limit == 0) {
			priceMappingList = priceMappingMapper
					.productPurchasePrice(partnerId);
		} else {
			priceMappingList = priceMappingMapper.productPurchasePriceById(
					partnerId, new ChannelPriceMappingExample(), new RowBounds(
							start, limit));
		}

		return priceMappingList;
	}

	@Override
	public ChannelPriceMapping duplicatePriceMapping(int partnerId,
			int productId) {
		return priceMappingMapper.duplicatePriceMapping(partnerId,productId);
	}

}
