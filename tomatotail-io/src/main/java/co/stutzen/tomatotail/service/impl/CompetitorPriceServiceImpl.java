package co.stutzen.tomatotail.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.CompetitorPrice;
import co.stutzen.tomatotail.entity.ProductExample;
import co.stutzen.tomatotail.entity.ProductWithBLOBs;
import co.stutzen.tomatotail.mapper.CompetitorPriceMapper;
import co.stutzen.tomatotail.mapper.ProductMapper;
import co.stutzen.tomatotail.service.CompetitorPriceService;

@Service
public class CompetitorPriceServiceImpl extends SWFServiceImpl<CompetitorPrice>
		implements CompetitorPriceService {

	@Autowired
	CompetitorPriceMapper competitorPriceMapper;

	@Autowired
	ProductMapper productMapper;

	@Override
	public List<ProductWithBLOBs> listOfCompetitorPriceBySearch(String manuName,
			String productName, String brandName, int start, int limit) {
		List<ProductWithBLOBs> list = null;
		if (start == 0 && limit == 0) {
			list = productMapper.productListBySearch(productName+"%", brandName+"%",
					manuName+"%");
		} else {
			list = productMapper
					.selectProductBySearch(productName+"%", brandName+"%", manuName+"%",
							new ProductExample(), new RowBounds(start, limit));
		}
		return list;
	}

}
