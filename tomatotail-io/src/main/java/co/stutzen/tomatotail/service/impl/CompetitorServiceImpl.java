package co.stutzen.tomatotail.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.Competitor;
import co.stutzen.tomatotail.entity.CompetitorExample;
import co.stutzen.tomatotail.mapper.CompetitorMapper;
import co.stutzen.tomatotail.service.CompetitorService;

@Service
public class CompetitorServiceImpl extends SWFServiceImpl<Competitor> implements
		CompetitorService {

	@Autowired
	public CompetitorMapper competitorMapper;

	@Override
	public List<Competitor> listOfCompetitorByName(String competitorName,
			int start, int limit) {
		List<Competitor> competitorList = null;
		if (start == 0 && limit == 0) {
			competitorList = competitorMapper.competitorListByName(competitorName);
		} else {
			competitorList = competitorMapper.listOfCompetitorByName(
					competitorName, new CompetitorExample(), new RowBounds(
							start, limit));
		}
		return competitorList;
	}

	@Override
	public int countOfCompetitor() {
		return competitorMapper.countByExample(new CompetitorExample());
	}

	@Override
	public List<Competitor> competitorList(int start, int limit) {
		return competitorMapper.selectByExampleWithRowbounds(
				new CompetitorExample(), new RowBounds(start, limit));
	}

}
