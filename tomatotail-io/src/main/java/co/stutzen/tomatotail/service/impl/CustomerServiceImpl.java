package co.stutzen.tomatotail.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.Customer;
import co.stutzen.tomatotail.entity.CustomerAddress;
import co.stutzen.tomatotail.entity.CustomerExample;
import co.stutzen.tomatotail.mapper.CustomerAddressMapper;
import co.stutzen.tomatotail.mapper.CustomerMapper;
import co.stutzen.tomatotail.service.CustomerService;
import co.stutzen.tomatotail.util.CommonConstants;

@Service
public class CustomerServiceImpl extends SWFServiceImpl<Customer> implements
		CustomerService {

	@Autowired
	private CustomerMapper customerMapper;

	@Autowired
	private CustomerAddressMapper addressMapper;

	@Override
	public int saveCustomer(Customer customer) {
		customer.setSourceType(CommonConstants.INTERNAL_SYSTEM);
		customer.setRefCustomerId(CommonConstants.CUSTOMER_REF_ID);
		customerMapper.insert(customer);
		for (CustomerAddress address : customer.getAddress()) {
			address.setCustomerId(customer.getId());
			if (address.getIsActive() == null)
				address.setIsActive(true);
			addressMapper.insert(address);
		}
		return customer.getId();
	}

	@Override
	public int updateCustomer(Customer customer) {
		customer.setSourceType(CommonConstants.INTERNAL_SYSTEM);
		customer.setRefCustomerId(CommonConstants.CUSTOMER_REF_ID);
		customerMapper.updateByPrimaryKeySelective(customer);
		for (CustomerAddress address : customer.getAddress()) {
			address.setCustomerId(customer.getId());
			if (address.getIsActive() == null)
				address.setIsActive(true);
			/* insert if new address */
			if (address.getId() == 0) {
				address.setCreatedBy(address.getModifiedBy());
				addressMapper.insert(address);
			} else if (address.getId() != 0)
				addressMapper.updateByPrimaryKeySelective(address);
		}
		return customer.getId();
	}

	@Override
	public List<Customer> getCustomerList(String name, String mobile,
			int start, int limit) {
		List<Customer> list = new ArrayList<Customer>();
		if (start == 0 && limit == 0) {
			list = customerMapper.getCustomerListCount(name, mobile);
		} else {
			list = customerMapper.getCustomerListWithLimit(name, mobile,
					new CustomerExample(), new RowBounds(start, limit));
		}
		return list;
	}
}
