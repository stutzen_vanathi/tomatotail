package co.stutzen.tomatotail.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.Brand;
import co.stutzen.tomatotail.entity.Category;
import co.stutzen.tomatotail.entity.ChannelPriceMapping;
import co.stutzen.tomatotail.entity.ExternalProductInfoWithBLOBs;
import co.stutzen.tomatotail.entity.Manufacturer;
import co.stutzen.tomatotail.entity.PartnerStoreMapping;
import co.stutzen.tomatotail.entity.Product;
import co.stutzen.tomatotail.entity.ProductWithBLOBs;
import co.stutzen.tomatotail.entity.Store;
import co.stutzen.tomatotail.mapper.BrandMapper;
import co.stutzen.tomatotail.mapper.CategoryMapper;
import co.stutzen.tomatotail.mapper.ChannelPriceMappingMapper;
import co.stutzen.tomatotail.mapper.ExternalProductInfoMapper;
import co.stutzen.tomatotail.mapper.ManufacturerMapper;
import co.stutzen.tomatotail.mapper.PartnerStoreMappingMapper;
import co.stutzen.tomatotail.mapper.ProductMapper;
import co.stutzen.tomatotail.mapper.StoreMapper;
import co.stutzen.tomatotail.service.ExternalProductInfoService;
import co.stutzen.tomatotail.util.CUtil;

@Service
public class ExternalProductInfoServiceImpl extends SWFServiceImpl<ExternalProductInfoWithBLOBs> implements ExternalProductInfoService {

	@Autowired
	private ProductMapper productMapper;

	@Autowired
	private ExternalProductInfoMapper externalProductInfoMapper;

	@Autowired
	private ManufacturerMapper manuMapper;

	@Autowired
	private StoreMapper storeMapper;

	@Autowired
	private BrandMapper brandMapper;

	@Autowired
	private ChannelPriceMappingMapper priceMapper;

	@Autowired
	private CategoryMapper categoryMapper;

	@Autowired
	private PartnerStoreMappingMapper partnerStoreMappingMapper;

	@Override
	public void insertProductInfoToLocalProductTbl(ExternalProductInfoWithBLOBs productInfo) {

		if (productInfo != null) {
			int manuId = manuSave(productInfo);
			int brandId = brandSave(productInfo, manuId);
			storeSave(productInfo);
			int productId = productSave(productInfo, manuId, brandId);
			partnerPriceSave(productInfo, productId);
		}
	}

	/**
	 * Manufacturer Save
	 */
	private int manuSave(ExternalProductInfoWithBLOBs productInfo) {
		Manufacturer m = new Manufacturer();
		productInfo = null;
		m.setName(productInfo.getManufacturer());
		m.setAddress(productInfo.getStore());
		m.setIsactive((byte) 1);
		Manufacturer manu = manuMapper.duplicateManufacturer(CUtil
				.checkNull(productInfo.getManufacturer()));
		if (manu == null) {
			manuMapper.insert(m);
			manu = m;
		}
		return manu.getManufacturerid();
	}

	/**
	 * Brand Save
	 */
	private int brandSave(ExternalProductInfoWithBLOBs productInfo, int manuId) {
		Brand b = new Brand();
		b.setName(productInfo.getBrand());
		b.setComments(productInfo.getDescription());
		b.setManufacturerid(manuId);
		b.setIsactive((byte) 1);
		Brand brand = brandMapper.duplicateBrand(productInfo.getBrand());
		if (brand == null) {
			brandMapper.insert(b);
			brand = b;
		}
		return brand.getBrandid();
	}

	/**
	 * Product Save
	 */
	private int productSave(ExternalProductInfoWithBLOBs productInfo,
			int manuId, int brandId) {
		ProductWithBLOBs p = new ProductWithBLOBs();
		p.setSKU(productInfo.getSku());
		p.setName(productInfo.getName());
		p.setBrandid(brandId);
		p.setManufacturerId(manuId);
		p.setWeight(CUtil.checkNullForStringToBigDecimal(productInfo
				.getWeight()));
		p.setCategoryid(1);
		/*
		 * p.setCategoryid(CUtil.checkNullForStringToInt(productInfo
		 * .getCategory_ids()));
		 */
		p.setIsactive((byte) 1);
		Product product = productMapper
				.selectProductBySku(productInfo.getSku());
		if (product == null) {
			productMapper.insert(p);
			product = p;
		}
		return product.getProductid();
	}

	/**
	 * Partner Store Save
	 */
	private int storeSave(ExternalProductInfoWithBLOBs productInfo) {
		Store s = new Store();
		s.setName(productInfo.getStore());
		s.setStoreId(productInfo.getStore_id());
		Store store = storeMapper.getDuplicateStoreName(productInfo.getStore());
		if (store == null) {
			storeMapper.insert(s);
			store = s;
		}
		return store.getId();
	}

	/**
	 * Partner Price save
	 */
	private void partnerPriceSave(ExternalProductInfoWithBLOBs productInfo,
			int productId) {
		ChannelPriceMapping priceMapping = new ChannelPriceMapping();
		Store store = storeMapper.getStoreByName(productInfo.getStore());
		if (store != null) {
			PartnerStoreMapping partnerWithStore = partnerStoreMappingMapper
					.getPartnerId(CUtil.checkNullForInteger(store.getId()));
			if (partnerWithStore != null) {
				ChannelPriceMapping skuPrice = priceMapper
						.duplicatePriceMapping(partnerWithStore.getPartnerid(),
								productId);
				if (skuPrice == null) {
					priceMapping.setPartnerid(partnerWithStore.getPartnerid());
					priceMapping.setPurchaseprice(CUtil
							.checkNullForStringToBigDecimal(
									productInfo.getPrice()).doubleValue());
					priceMapping.setSellingprice(CUtil
							.checkNullForStringToBigDecimal(productInfo
									.getSpecial_price()));
					priceMapping.setMrp_Price(CUtil
							.checkNullForStringToBigDecimal(productInfo
									.getMsrp()));
					priceMapping.setProductid(productId);
					priceMapping.setIsactive((byte) 1);
					priceMapper.insert(priceMapping);
				}
			}
		}
	}

}
