package co.stutzen.tomatotail.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.ksoap2.serialization.SoapObject;
import org.springframework.stereotype.Service;

import co.stutzen.tomatotail.entity.MagentoProductInfo;
import co.stutzen.tomatotail.entity.MagentoProductTierPrice;
import co.stutzen.tomatotail.util.NumberParserUtil;

@Service
public class MagentoProductInfoServiceImpl {


	public MagentoProductInfo getProductInfo(SoapObject productInfoRes) {

		MagentoProductInfo productInfo = new MagentoProductInfo();
		productInfo.setProductid(NumberParserUtil.getIntegerSafely(productInfoRes
				.getPropertySafely("product_id").toString()));
		productInfo
				.setSku((productInfoRes.getPropertySafely("sku").toString()));
		productInfo.setType((productInfoRes.getPropertySafely("type")
				.toString()));
		productInfo.setName((productInfoRes.getPropertySafely("name")
				.toString()));
		productInfo.setDescription((productInfoRes
				.getPropertySafely("description").toString()));
		productInfo.setShortdescription((productInfoRes
				.getPropertySafely("short_description").toString()));
		productInfo.setWeight(NumberParserUtil.getBigDecimalSafely(productInfoRes
				.getPropertySafely("product_id").toString()));
		productInfo.setPrice(NumberParserUtil.getBigDecimalSafely(productInfoRes
				.getPropertySafely("price").toString()));
		productInfo.setSpecialprice(NumberParserUtil
				.getBigDecimalSafely(productInfoRes.getPropertySafely(
						"special_price").toString()));
		productInfo.setSpecialfromdate(NumberParserUtil.getDateSafely(productInfoRes
				.getPropertySafely("special_from_date").toString()));
		productInfo.setSpecialtodate(NumberParserUtil.getDateSafely(productInfoRes
				.getPropertySafely("special_to_date").toString()));

		SoapObject tierPriceObject = (SoapObject) (productInfoRes
				.getProperty("tier_price"));
		productInfo.setProductTierPrices(getTierPriceList(tierPriceObject,
				productInfo.getProductid()));
		return productInfo;

	}

	public List<MagentoProductTierPrice> getTierPriceList(
			SoapObject tierPriceObject, Integer productId) {
		MagentoProductTierPrice tierPrice = new MagentoProductTierPrice();
		List<MagentoProductTierPrice> tierPriceList = new ArrayList<MagentoProductTierPrice>();
		tierPrice.setProductid(productId);
		tierPrice.setQty(NumberParserUtil.getIntegerSafely(tierPriceObject
				.getPropertySafely("qty").toString()));
		tierPrice.setCustomergroupid(NumberParserUtil
				.getIntegerSafely(tierPriceObject
						.getPropertySafely("customer_group_id").toString()));
		tierPrice.setPrice(NumberParserUtil.getBigDecimalSafely(tierPriceObject
				.getPropertySafely("price").toString()));
		tierPrice.setWebsite((tierPriceObject
				.getPropertySafely("website").toString()));
		tierPriceList.add(tierPrice);
		return tierPriceList;

	}
}
