package co.stutzen.tomatotail.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.aspectj.apache.bcel.classfile.Constant;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;
import org.xmlpull.v1.XmlPullParserException;

import co.stutzen.tomatotail.entity.MagentoOrderAddress;
import co.stutzen.tomatotail.entity.MagentoOrderItem;
import co.stutzen.tomatotail.entity.MagentoOrderPayment;
import co.stutzen.tomatotail.entity.MagentoOrderStatus;
import co.stutzen.tomatotail.entity.MagentoSalesOrder;
import co.stutzen.tomatotail.entity.MagentoSalesOrderExample;
import co.stutzen.tomatotail.entity.Order;
import co.stutzen.tomatotail.entity.OrderDetails;
import co.stutzen.tomatotail.entity.OrderDetailsWithBLOBs;
import co.stutzen.tomatotail.entity.ProductManufactureDate;
import co.stutzen.tomatotail.mapper.MagentoOrderAddressMapper;
import co.stutzen.tomatotail.mapper.MagentoOrderItemMapper;
import co.stutzen.tomatotail.mapper.MagentoOrderPaymentMapper;
import co.stutzen.tomatotail.mapper.MagentoOrderStatusMapper;
import co.stutzen.tomatotail.mapper.MagentoSalesOrderMapper;
import co.stutzen.tomatotail.mapper.OrderDetailsMapper;
import co.stutzen.tomatotail.mapper.OrderMapper;
import co.stutzen.tomatotail.mapper.PartnerStoreMappingMapper;
import co.stutzen.tomatotail.mapper.ProductManufactureDateMapper;
import co.stutzen.tomatotail.service.MagentoSalesOrderService;
import co.stutzen.tomatotail.util.CUtil;
import co.stutzen.tomatotail.util.CommonConstants;
import co.stutzen.tomatotail.util.CommonWSUtil;
import co.stutzen.tomatotail.util.KSoapUtil;
import co.stutzen.tomatotail.util.NumberParserUtil;

@Service
public class MagentoSalesOrderServiceImpl extends
		SWFServiceImpl<MagentoSalesOrder> implements MagentoSalesOrderService {

	@Autowired
	private CommonWSUtil wsUtil;

	@Autowired
	private MagentoSalesOrderMapper magentoOrderMapper;

	@Autowired
	private MagentoOrderAddressMapper addressMapper;

	@Autowired
	private MagentoOrderPaymentMapper paymentMapper;

	@Autowired
	private MagentoOrderStatusMapper statusMapper;

	@Autowired
	private MagentoOrderItemMapper itemMapper;

	@Autowired
	private PartnerStoreMappingMapper storeMappingMapper;

	@Autowired
	private ProductManufactureDateMapper manufactureDateMapper;

	@Autowired
	private OrderMapper orderMapper;

	@Autowired
	private OrderDetailsMapper orderDetailsMapper;

	@Override
	public List<MagentoSalesOrder> getSalesOrders() {
		return magentoOrderMapper.getSalesOrder();
	}

	@Override
	public Date getRecentUpdatedDate() {
		return magentoOrderMapper.getRecentUpdatedDate();
	}

	@Override
	public boolean updateByIncrementId(MagentoSalesOrder record) {
		boolean isSuccess = false;
		try {
			/*
			 * Find partner id for particular store
			 */
			/* int storeId = record.getStoreid(); */
			int storeId = 0;
			/*
			 * int partnerId =
			 * storeMappingMapper.getPartnerIdByStoreId(storeId);
			 */

			/* record.setPartnerId(partnerId); */
			magentoOrderMapper.updateByIncrementId(record);
			for (MagentoOrderAddress orderAddress : record
					.getOrderAddressList()) {
				addressMapper.insert(orderAddress);
			}
			for (MagentoOrderItem orderItem : record.getOrderItemList()) {
				orderItem.setIsActive(Byte.parseByte("1"));
				itemMapper.insert(orderItem);
			}
			paymentMapper.insert(record.getOrderPayment());
			for (MagentoOrderStatus orderStatus : record.getOrderStatusList()) {
				orderStatus.setIsmagento((byte) 1);
				statusMapper.insert(orderStatus);
			}
			isSuccess = true;

		} catch (Exception e) {
			e.printStackTrace();
			isSuccess = false;
		}
		return isSuccess;
	}

	@Override
	public boolean updateByPrimaryKey(MagentoSalesOrder record) {
		boolean isSuccess = false;
		List<MagentoOrderStatus> orderStatusList = statusMapper
				.getStatusHistoryByOrderId(record.getOrderid());
		try {
			magentoOrderMapper.updateByOrderId(record);
			for (MagentoOrderAddress orderAddress : record
					.getOrderAddressList()) {
				addressMapper.updateByOrderId(orderAddress);

				for (MagentoOrderItem orderItem : record.getOrderItemList()) {
					itemMapper.updateByOrderId(orderItem);
				}
				paymentMapper.updateByOrderId(record.getOrderPayment());
				for (MagentoOrderStatus orderStatus : record
						.getOrderStatusList()) {
					orderStatus.setIsmagento((byte) 1);
					isSuccess = saveOrUpdateOrderStatus(orderStatus,
							orderStatusList);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			isSuccess = false;
		}
		return isSuccess;
	}

	@Override
	public String getLocalSalesOrderIncrementId(String incrementId) {
		return magentoOrderMapper.getLocalSalesOrderIncrementId(incrementId);
	}

	@Override
	public int count() {
		return magentoOrderMapper.count();
	}

	@Override
	public List<MagentoSalesOrder> getOrderList(int start, int limit) {
		List<MagentoSalesOrder> list = magentoOrderMapper.getOrderList(
				new MagentoSalesOrderExample(), new RowBounds(start, limit));
		List<MagentoSalesOrder> orderList = new ArrayList<MagentoSalesOrder>();
		if (list.size() > 0) {
			for (MagentoSalesOrder order : list) {
				if (order.getOrderItemList() != null
						&& order.getOrderItemList().size() > 0) {
					orderList.add(order);
				}
			}
		}
		return orderList;
	}

	@Override
	public List<MagentoSalesOrder> getOrdersBySearch(int storeId,
			String status, int start, int limit) {
		List<MagentoSalesOrder> list = null;
		if (start == 0 && limit == 0) {
			list = magentoOrderMapper.orderListBySearch(storeId, status);
		} else {
			list = magentoOrderMapper
					.getOrdersBySearch(storeId, status,
							new MagentoSalesOrderExample(), new RowBounds(
									start, limit));
		}
		List<MagentoSalesOrder> orderList = new ArrayList<MagentoSalesOrder>();
		if (list.size() > 0) {
			for (MagentoSalesOrder order : list) {
				if (order.getOrderItemList() != null
						&& order.getOrderItemList().size() > 0) {
					orderList.add(order);
				}
			}
		}
		return orderList;
	}

	@Override
	public MagentoSalesOrder getOrderDetailsByOrderId(int orderId) {
		MagentoSalesOrder order = magentoOrderMapper
				.getOrderDetailsByOrderId(orderId);
		if (order.getOrderItemList().isEmpty()) {
			order = null;
		}
		return order;
	}

	@Override
	public boolean updateOrderStatusById(MagentoSalesOrder salesOrder) {
		boolean isSuccess = false;
		MagentoOrderStatus orderStatus = new MagentoOrderStatus();
		orderStatus.setOrder_status(salesOrder.getOrderstatus());
		orderStatus.setOrderid(salesOrder.getOrderid());
		orderStatus.setIsmagento((byte) 0);
		orderStatus.setIsactive((byte) 1);
		salesOrder.setOrderstatus(salesOrder.getOrderstatus());
		salesOrder.setCreatedat(salesOrder.getCreatedat());
		salesOrder.setUpdatedat(salesOrder.getUpdatedat());
		magentoOrderMapper.updateOrderStatusById(salesOrder);
		statusMapper.insert(orderStatus);
		isSuccess = true;
		return isSuccess;
	}

	public boolean saveOrUpdateOrderStatus(MagentoOrderStatus orderStatus,
			List<MagentoOrderStatus> orderStatusList) {
		boolean isInsert = false;
		boolean isUpdate = false;
		boolean isSuccess = false;
		int size = orderStatusList.size();
		if (size > 0) {
			for (MagentoOrderStatus status : orderStatusList) {
				if (status.getCreatedat().compareTo(orderStatus.getCreatedat()) == 0
						&& status.getOrder_status().equals(
								orderStatus.getOrder_status())) {
					isUpdate = true;
					break;
				} else if (orderStatus != null
						&& status.getCreatedat().compareTo(
								orderStatus.getCreatedat()) != 0
						&& (!(status.getOrder_status()
								.equalsIgnoreCase(orderStatus.getOrder_status())) || (status
								.getOrder_status().equalsIgnoreCase(orderStatus
								.getOrder_status())))) {
					isInsert = true;
					break;
				}
			}
		}
		if (isInsert) {
			statusMapper.insert(orderStatus);
			isSuccess = true;
		}
		if (isUpdate) {
			statusMapper.updateByOrderId(orderStatus);
			isSuccess = true;
		}

		if (isUpdate && isInsert) {
			statusMapper.insert(orderStatus);
			isSuccess = true;
		}
		return isSuccess;
	}

	public List<MagentoSalesOrder> saveOrderList(SoapObject soapObj,
			int localSalesOrderCount) {
		MagentoSalesOrder magentoSalesOrder = null;
		int count = 0;
		if (soapObj != null) {
			count = soapObj.getPropertyCount();
		}
		List<MagentoSalesOrder> orders = new ArrayList<MagentoSalesOrder>();
		List<MagentoSalesOrder> existingOrderInLocal = new ArrayList<MagentoSalesOrder>();
		for (int i = 0; i < count; i++) {
			SoapObject object = (SoapObject) (soapObj.getProperty(i));
			magentoSalesOrder = new MagentoSalesOrder();
			String incrementId = object.getPropertySafely("increment_id")
					.toString();
			magentoSalesOrder.setIncrementid(object.getPropertySafely(
					"increment_id").toString());
			magentoSalesOrder.setOrderid(Integer.parseInt(object
					.getPropertySafely("order_id").toString()));

			// if incrementId is exist
			if (localSalesOrderCount > 0) {
				String localSalesOrderIncrementId = magentoOrderMapper
						.getLocalSalesOrderIncrementId(incrementId);
				if (localSalesOrderIncrementId != null
						&& localSalesOrderIncrementId.equals(incrementId)) {
					existingOrderInLocal.add(magentoSalesOrder);
				} else {
					orders.add(magentoSalesOrder);
					magentoOrderMapper.insert(magentoSalesOrder);
				}

			} else {
				orders.add(magentoSalesOrder);
				magentoOrderMapper.insert(magentoSalesOrder);
			}
		}

		// update child & parent tbl
		if (existingOrderInLocal.size() > 0) {
			saveOrUpdateOrderDetails(existingOrderInLocal, false);
		}

		return orders;
	}

	// Save or update Order details
	public boolean saveOrUpdateOrderDetails(
			List<MagentoSalesOrder> magentoSalesOrders, boolean isSave) {
		boolean isSuccess = false;
		SoapObject orderResp = null;
		SoapSerializationEnvelope env = KSoapUtil.getEnvelope();

		String sessionId = wsUtil.getSessionId();
		SoapObject orderRequest = null;
		for (int i = 0; i < magentoSalesOrders.size(); i++) {
			orderRequest = new SoapObject(wsUtil.nameSpace(), "salesOrderInfo");
			orderRequest.addProperty("sessionId", sessionId);
			orderRequest.addProperty("orderIncrementId", magentoSalesOrders
					.get(i).getIncrementid());
			env.setOutputSoapObject(orderRequest);
			try {
				// Webservice call
				wsUtil.getHttpTransport().call("", env);

				// Retrieval of resp
				orderResp = (SoapObject) env.getResponse();

			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			// get child & order details
			MagentoSalesOrder magentoSalesOrder = getSalesOrderDetails(orderResp);
			System.out.println("ORDER ---->" + magentoSalesOrder);
			MagentoSalesOrder order = magentoOrderMapper
					.getOrderId(magentoSalesOrder.getOrderid());
			// insert if new record
			if (isSave) {
				isSuccess = updateByIncrementId(magentoSalesOrder);
				int orderId = 0;
				if (order != null)
					orderId = order.getId();
				saveOrUpdateExternalSystemOrders(orderId, CommonConstants.IS_SAVE);
			} else {
				// update
				isSuccess = updateByPrimaryKey(magentoSalesOrder);
				if (order != null)
					saveOrUpdateExternalSystemOrders(order.getId(),
							CommonConstants.IS_UPDATE);
			}
			orderRequest = null;
		}
		return isSuccess;
	}

	private List<MagentoSalesOrder> saveOrUpdateExternalSystemOrders(
			int orderId, boolean saveOrUpdate) {
		Order orderFromLocalDb = orderMapper.selectIdByCriteria(orderId,
				CommonConstants.EXTERNAL_SYSTEM);
		MagentoSalesOrder externalSystemOrder = magentoOrderMapper
				.getOrdersByOrderId(orderId);
		Order order = new Order();
		order.setOutletId(CUtil.checkNullForInteger(externalSystemOrder.getPartnerId()));
		order.setDateTime(externalSystemOrder.getCreatedat());
		order.setSourceType(CommonConstants.EXTERNAL_SYSTEM);
		order.setRefOrderId(CUtil.checkNullForInteger(externalSystemOrder.getId()));
		order.setCustomerId(CUtil.checkNullForInteger(externalSystemOrder
				.getCustomerid()));
		order.setStatus(CommonConstants.PROCESSED);
		order.setIsActive(true);
		order.setComments(CommonConstants.EXTERNAL_SYSTEM_ORDER);
		for (MagentoOrderAddress address : externalSystemOrder
				.getOrderAddressList()) {
			if (address != null
					&& address.getAddresstype().equalsIgnoreCase(
							CommonConstants.SHIPPING_ADDRESS)) {
				order.setName(address.getFirstname());
				order.setMobile(address.getTelephone());
				order.setAddressId(address.getId());
				order.setAddress(getAddress(address));
			}
		}
		if (saveOrUpdate == CommonConstants.IS_SAVE)
			orderMapper.insert(order);
		else if (saveOrUpdate == CommonConstants.IS_UPDATE) {
			order.setRefOrderId(orderId);
			order.setId(orderFromLocalDb.getId());
			orderMapper.updateOrderByCriteria(order);
		}
		int itemCount = 0;
		for (MagentoOrderItem item : externalSystemOrder.getOrderItemList()) {
			OrderDetailsWithBLOBs details = new OrderDetailsWithBLOBs();
			if (item != null) {
				details.setOrderId(order.getId());
				details.setAvailability(CommonConstants.AVAILABILITY);
				details.setAvailableQty(CUtil.checkNull(item.getQtyordered())
						.intValue());
				details.setRequiredQty(CUtil.checkNull(item.getQtyordered())
						.intValue());
				details.setMrpPrice(CUtil.checkNull(item.getPrice()));
				details.setCurrentPrice(CUtil.checkNull(item.getPrice()));
				details.setRequiredPrice(CUtil.checkNull(item.getPrice()));
				details.setSku(CUtil.checkNull(item.getSku()));
				details.setProductName(CUtil.checkNull(item.getName()));
				details.setParentSku(CUtil.checkNull(item.getSku()));
				BigDecimal totalPrice = CUtil.checkNull(item.getPrice())
						.multiply(CUtil.checkNull(item.getQtyordered()));
				details.setTotalPrice(totalPrice.setScale(2, RoundingMode.UP));
				details.setIsActive(true);
				details.setComments(CommonConstants.EXTERNAL_SYSTEM_ORDER);
				if (saveOrUpdate == CommonConstants.IS_SAVE)
					orderDetailsMapper.insert(details);
				else if (saveOrUpdate == CommonConstants.IS_UPDATE) {
					OrderDetails orderDetails = orderDetailsMapper
							.getOrderDetailsId(order.getId(), details.getSku());
					details.setId(orderDetails.getId());
					orderDetailsMapper.updateOrderDetails(details);
				}
				itemCount++;
			}
			orderMapper.updateItemCount(order.getId(), itemCount);
		}
		return null;
	}

	private String getAddress(MagentoOrderAddress address) {
		StringBuilder sb = new StringBuilder();
		if (address != null)
			sb.append(CUtil.checkNull(address.getStreet())).append(",")
					.append(CUtil.checkNull(address.getCity())).append(",")
					.append(CUtil.checkNull(address.getPostcode())).append(",")
					.append(CUtil.checkNull(address.getRegion())).append(",")
					.append(address.getCountryid());
		else
			sb.append(" ");
		return sb.toString();
	}

	// get order details
	private MagentoSalesOrder getSalesOrderDetails(SoapObject soapObj) {

		MagentoSalesOrder salesOrder = new MagentoSalesOrder();
		salesOrder.setOrderid(Integer.parseInt((soapObj
				.getPropertySafely("order_id").toString())));
		salesOrder.setIncrementid(soapObj.getPropertySafely("increment_id")
				.toString());

		/* salesOrder.setUpdateBy(updateBy); */

		salesOrder.setStoreid(NumberParserUtil.getIntegerSafely((soapObj
				.getPropertySafely("store_id").toString())));
		salesOrder.setCreatedat(NumberParserUtil.getDateSafely(soapObj
				.getPropertySafely("created_at").toString()));
		salesOrder.setUpdatedat(NumberParserUtil.getDateSafely(soapObj
				.getPropertySafely("updated_at").toString()));
		salesOrder.setCustomerid(NumberParserUtil.getIntegerSafely(soapObj
				.getPropertySafely("customer_id").toString()));
		salesOrder.setTaxamount(NumberParserUtil.getBigDecimalSafely((soapObj
				.getPropertySafely("tax_amount").toString())));
		salesOrder.setShippingamount(NumberParserUtil
				.getBigDecimalSafely((soapObj
						.getPropertySafely("shipping_amount").toString())));
		salesOrder.setDiscountamount(NumberParserUtil
				.getBigDecimalSafely((soapObj
						.getPropertySafely("discount_amount").toString())));
		salesOrder.setSubtotal(NumberParserUtil.getBigDecimalSafely((soapObj
				.getPropertySafely("subtotal").toString())));
		salesOrder.setGrandtotal(NumberParserUtil.getBigDecimalSafely((soapObj
				.getPropertySafely("grand_total").toString())));
		salesOrder.setTotalqtyordered(NumberParserUtil
				.getBigDecimalSafely((soapObj
						.getPropertySafely("total_qty_ordered").toString())));
		salesOrder.setTotalpaid(NumberParserUtil.getBigDecimalSafely((soapObj
				.getPropertySafely("total_paid").toString())));
		salesOrder.setTotalrefunded(NumberParserUtil
				.getBigDecimalSafely(soapObj
						.getPropertySafely("total_refunded").toString()));
		salesOrder.setTotalcanceled(NumberParserUtil
				.getBigDecimalSafely(soapObj
						.getPropertySafely("total_canceled").toString()));
		salesOrder.setTotalinvoiced(NumberParserUtil
				.getBigDecimalSafely(soapObj
						.getPropertySafely("total_invoiced").toString()));
		salesOrder.setTotalonlinerefunded(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"total_online_refunded").toString()));
		salesOrder.setTotalinvoiced(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"total_offline_refunded").toString()));
		salesOrder.setBasetaxamount(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_tax_amount").toString()));
		salesOrder.setBaseshippingamount(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_shipping_amount").toString()));
		salesOrder.setDiscountamount(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_discount_amount").toString()));
		salesOrder.setBasesubtotal(NumberParserUtil.getBigDecimalSafely(soapObj
				.getPropertySafely("base_subtotal").toString()));
		salesOrder.setBasegrandtotal(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_grand_total").toString()));
		salesOrder.setBasetotalpaid(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_total_paid").toString()));
		salesOrder.setBasetotalrefunded(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_total_refunded").toString()));
		salesOrder.setBasetotalqtyordered(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_total_qty_ordered").toString()));
		salesOrder.setBasetotalcanceled(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_total_canceled").toString()));
		salesOrder.setBasetotalinvoiced(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_total_invoiced").toString()));
		salesOrder.setBasetotalonlinerefunded(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_total_online_refunded").toString()));
		salesOrder.setBasetotalofflinerefunded(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_total_offline_refunded").toString()));
		salesOrder.setBillingaddressid(NumberParserUtil
				.getIntegerSafely(soapObj.getPropertySafely(
						"billing_address_id").toString()));
		salesOrder.setShippingaddressid(NumberParserUtil
				.getIntegerSafely(soapObj.getPropertySafely(
						"shipping_address_id").toString()));
		salesOrder.setStoretobaserate(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"store_to_base_rate").toString()));
		salesOrder.setStoretoorderrate(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"store_to_order_rate").toString()));
		salesOrder.setBasetoglobalrate(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_to_global_rate").toString()));
		salesOrder.setBasetoorderrate(NumberParserUtil
				.getBigDecimalSafely(soapObj.getPropertySafely(
						"base_to_order_rate").toString()));
		salesOrder.setStorename(soapObj.getPropertySafely("store_name")
				.toString());
		salesOrder.setWeight(NumberParserUtil.getBigDecimalSafely(soapObj
				.getPropertySafely("weight").toString()));
		salesOrder.setRemoteip((soapObj.getPropertySafely("remote_ip")
				.toString()));
		salesOrder.setAppliedruleids(soapObj.getPropertySafely(
				"applied_rule_ids").toString());
		salesOrder.setGlobalcurrencycode((soapObj
				.getPropertySafely("global_currency_code").toString()));
		salesOrder.setBasecurrencycode((soapObj
				.getPropertySafely("base_currency_code").toString()));
		salesOrder.setStorecurrencycode((soapObj
				.getPropertySafely("store_currency_code").toString()));
		salesOrder.setOrdercurrencycode((soapObj
				.getPropertySafely("order_currency_code").toString()));
		salesOrder.setShippingmethod((soapObj
				.getPropertySafely("shipping_method").toString()));
		salesOrder.setShippingdescription((soapObj
				.getPropertySafely("shipping_description").toString()));
		salesOrder.setCustomeremail((soapObj
				.getPropertySafely("customer_email").toString()));
		salesOrder.setCustomerfirstname((soapObj
				.getPropertySafely("customer_firstname").toString()));
		salesOrder.setCustomerlastname((soapObj
				.getPropertySafely("customer_lastname").toString()));
		salesOrder.setQuoteid(NumberParserUtil.getIntegerSafely(soapObj
				.getPropertySafely("quote_id").toString()));
		salesOrder.setIsvirtual(NumberParserUtil.getIntegerSafely(soapObj
				.getPropertySafely("is_virtual").toString()));
		salesOrder.setCustomergroupid(NumberParserUtil.getIntegerSafely(soapObj
				.getPropertySafely("customer_group_id").toString()));
		salesOrder.setCustomernotenotify(NumberParserUtil
				.getIntegerSafely(soapObj.getPropertySafely(
						"customer_note_notify").toString()));
		salesOrder.setCustomerisguest(NumberParserUtil.getIntegerSafely(soapObj
				.getPropertySafely("customer_is_guest").toString()));
		salesOrder.setEmailsent(NumberParserUtil.getIntegerSafely((soapObj
				.getPropertySafely("email_sent").toString())));
		salesOrder.setOrderstatus((soapObj.getPropertySafely("status")
				.toString()));
		salesOrder.setState((soapObj.getPropertySafely("state").toString()));

		// Get child entities details

		List<MagentoOrderAddress> orderAddressList = new ArrayList<MagentoOrderAddress>();
		SoapObject shippingAddress = (SoapObject) (soapObj
				.getProperty("shipping_address"));
		SoapObject billingAddress = (SoapObject) (soapObj
				.getProperty("billing_address"));
		orderAddressList.add(getOrderAddress(shippingAddress,
				salesOrder.getOrderid()));
		orderAddressList.add(getOrderAddress(billingAddress,
				salesOrder.getOrderid()));
		salesOrder.setOrderAddressList(orderAddressList);
		SoapObject statusObj = (SoapObject) (soapObj
				.getProperty("status_history"));
		List<MagentoOrderStatus> orderStatusList = getOrderStatus(statusObj,
				salesOrder.getOrderid());
		salesOrder.setOrderStatusList(orderStatusList);
		SoapObject paymentObj = (SoapObject) (soapObj.getProperty("payment"));
		MagentoOrderPayment magentoOrderPayment = getOrderPayment(paymentObj,
				salesOrder.getOrderid());
		salesOrder.setOrderPayment(magentoOrderPayment);
		SoapObject orderItem = (SoapObject) (soapObj.getProperty("items"));
		List<MagentoOrderItem> orderItemList = getOrderItems(orderItem,
				salesOrder.getOrderid());
		salesOrder.setOrderItemList((orderItemList));
		return salesOrder;
	}

	// get order address
	private MagentoOrderAddress getOrderAddress(SoapObject addressObj,
			Integer orderId) {

		MagentoOrderAddress orderAddress = new MagentoOrderAddress();
		orderAddress.setOrderid(orderId);
		orderAddress.setAddresstype((addressObj
				.getPropertySafely("address_type").toString()));
		orderAddress.setCreatedat(NumberParserUtil.getDateSafely(addressObj
				.getPropertySafely("created_at").toString()));
		orderAddress.setUpdatedat(NumberParserUtil.getDateSafely(addressObj
				.getPropertySafely("updated_at").toString()));

		Byte isActive = NumberParserUtil.getByteSafely((addressObj
				.getPropertySafely("is_active").toString()));
		if (isActive == null) {
			isActive = (byte) 1;
			orderAddress.setIsactive(isActive);
		} else {
			orderAddress.setIsactive(isActive);
		}
		orderAddress.setFirstname(addressObj.getPropertySafely("firstname")
				.toString());
		orderAddress.setLastname(addressObj.getPropertySafely("lastname")
				.toString());
		orderAddress.setCompany(addressObj.getPropertySafely("company")
				.toString());

		orderAddress.setStreet(addressObj.getPropertySafely("street")
				.toString());

		orderAddress.setCity(addressObj.getPropertySafely("city").toString());
		orderAddress.setRegion(addressObj.getPropertySafely("region")
				.toString());
		orderAddress.setPostcode(addressObj.getPropertySafely("postcode")
				.toString());
		orderAddress.setCountryid((addressObj.getPropertySafely("country_id")
				.toString()));
		orderAddress.setTelephone(addressObj.getPropertySafely("telephone")
				.toString());
		orderAddress.setFax(addressObj.getPropertySafely("fax").toString());
		orderAddress.setRegion_id(NumberParserUtil.getIntegerSafely(addressObj
				.getPropertySafely("region_id").toString()));
		orderAddress.setAddress_id(NumberParserUtil.getIntegerSafely(addressObj
				.getPropertySafely("address_id").toString()));
		return orderAddress;
	}

	// get orderstatus
	private List<MagentoOrderStatus> getOrderStatus(SoapObject statusObj,
			Integer orderId) {

		MagentoOrderStatus orderStatus = new MagentoOrderStatus();
		List<MagentoOrderStatus> orderStatusList = new ArrayList<MagentoOrderStatus>();
		for (int i = 0; i < statusObj.getPropertyCount(); i++) {
			orderStatus.setOrderid(orderId);
			SoapObject object = (SoapObject) (statusObj.getProperty(i));

			/* salesOrder.setUpdateBy(updateBy); */

			/*
			 * orderStatus.setComments(statusObj.getPropertySafely("comments")
			 * .toString());
			 */

			orderStatus.setCreatedat(NumberParserUtil.getDateSafely(object
					.getPropertySafely("created_at").toString()));
			orderStatus.setUpdatedat(NumberParserUtil.getDateSafely(object
					.getPropertySafely("created_at").toString()));
			String s = statusObj.getPropertySafely("created_at").toString();
			orderStatus.setOrder_status(object.getPropertySafely("status")
					.toString());

			Byte isActive = NumberParserUtil.getByteSafely((object
					.getPropertySafely("is_active").toString()));
			if (isActive == null) {
				isActive = (byte) 1;
				orderStatus.setIsactive(isActive);
			} else {
				orderStatus.setIsactive(isActive);
			}
			orderStatus.setIscustomernotified(NumberParserUtil
					.getByteSafely(object.getPropertySafely(
							"is_customer_notified").toString()));
			orderStatus.setUpdatedby(object.getPropertySafely("updateby")
					.toString());

			orderStatusList.add(orderStatus);
		}
		return orderStatusList;
	}

	// get order payment
	private MagentoOrderPayment getOrderPayment(SoapObject paymentObj,
			Integer orderId) {

		MagentoOrderPayment orderPayment = new MagentoOrderPayment();

		orderPayment.setOrderid(orderId);
		orderPayment.setCreatedat(NumberParserUtil.getDateSafely(paymentObj
				.getPropertySafely("created_at").toString()));
		orderPayment.setUpdatedat(NumberParserUtil.getDateSafely(paymentObj
				.getPropertySafely("updated_at").toString()));
		orderPayment.setAmountordered(NumberParserUtil
				.getBigDecimalSafely(paymentObj.getPropertySafely(
						"amount_ordered").toString()));
		orderPayment.setShippingamount(NumberParserUtil
				.getBigDecimalSafely(paymentObj.getPropertySafely(
						"shipping_amount").toString()));
		orderPayment.setBaseamountordered(NumberParserUtil
				.getBigDecimalSafely(paymentObj.getPropertySafely(
						"base_amount_ordered").toString()));
		orderPayment.setBaseshippingamount(NumberParserUtil
				.getBigDecimalSafely(paymentObj.getPropertySafely(
						"base_shipping_amount").toString()));
		orderPayment.setMethod(paymentObj.getPropertySafely("method")
				.toString());
		orderPayment.setPo_number(paymentObj.getPropertySafely("po_number")
				.toString());
		orderPayment.setCctype(paymentObj.getPropertySafely("cc_type")
				.toString());
		orderPayment.setCcnumberenc(paymentObj.getPropertySafely(
				"cc_number_enc").toString());
		orderPayment.setCclast4(paymentObj.getPropertySafely("cc_last4")
				.toString());
		orderPayment.setCcowner(paymentObj.getPropertySafely("cc_owner")
				.toString());
		orderPayment.setCcexpmonth(paymentObj.getPropertySafely("cc_exp_month")
				.toString());
		orderPayment.setCcexpyear(paymentObj.getPropertySafely("cc_exp_year")
				.toString());
		orderPayment.setCcssstartmonth(paymentObj.getPropertySafely(
				"cc_ss_start_month").toString());
		orderPayment.setCcssstartyear(paymentObj.getPropertySafely(
				"cc_ss_start_year").toString());
		orderPayment.setPaymentid(NumberParserUtil.getIntegerSafely(paymentObj
				.getPropertySafely("payment_id").toString()));

		return orderPayment;
	}

	// get order items
	List<MagentoOrderItem> getOrderItems(SoapObject orderItemObj,
			Integer orderId) {

		MagentoOrderItem orderItem = new MagentoOrderItem();

		List<MagentoOrderItem> itemList = new ArrayList<MagentoOrderItem>();
		for (int i = 0; i < orderItemObj.getPropertyCount(); i++) {
			SoapObject object = (SoapObject) (orderItemObj.getProperty(i));
			orderItem.setOrderid(orderId);
			orderItem.setItemid(Integer.parseInt((String) (object
					.getPropertySafely("item_id"))));

			orderItem.setCreatedat(NumberParserUtil.getDateSafely(orderItemObj
					.getPropertySafely("updated_at").toString()));
			orderItem.setUpdatedat(NumberParserUtil.getDateSafely(orderItemObj
					.getPropertySafely("updated_at").toString()));
			orderItem.setQuoteitemid(Integer.parseInt((object
					.getPropertySafely("quote_item_id")).toString()));
			orderItem.setProductid(Integer.parseInt((object
					.getPropertySafely("product_id")).toString()));
			orderItem.setProducttype((object.getPropertySafely("product_type"))
					.toString());

			/*
			 * orderItem.setProductoptions((object
			 * .getPropertySafely("product_options")).toString());
			 */

			orderItem.setWeight(NumberParserUtil.getBigDecimalSafely((object
					.getPropertySafely("weight")).toString()));
			orderItem.setIsvirtual(NumberParserUtil.getIntegerSafely((object
					.getPropertySafely("is_virtual")).toString()));
			orderItem.setSku((object.getPropertySafely("sku")).toString());
			orderItem.setName((object.getPropertySafely("name")).toString());
			orderItem.setAppliedruleids((object
					.getPropertySafely("applied_rule_ids")).toString());
			orderItem.setFreeshipping(NumberParserUtil.getIntegerSafely((object
					.getPropertySafely("free_shippingl")).toString()));
			orderItem.setIsqtydecimal(NumberParserUtil.getIntegerSafely((object
					.getPropertySafely("is_qty_decimal")).toString()));
			orderItem.setNodiscount(NumberParserUtil.getIntegerSafely((object
					.getPropertySafely("no_discount")).toString()));
			orderItem.setQtycanceled(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("qty_canceled")).toString()));
			orderItem.setQtyinvoiced(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("qty_invoiced")).toString()));
			orderItem.setQtyordered(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("qty_ordered")).toString()));
			orderItem.setQtyrefunded(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("qty_refunded")).toString()));
			orderItem.setQtyshipped(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("qty_shipped")).toString()));
			orderItem.setCost(NumberParserUtil.getBigDecimalSafely((object
					.getPropertySafely("cost")).toString()));
			orderItem.setPrice(NumberParserUtil.getBigDecimalSafely((object
					.getPropertySafely("price")).toString()));
			orderItem.setBaseprice(NumberParserUtil.getBigDecimalSafely((object
					.getPropertySafely("base_price")).toString()));
			orderItem.setOriginalprice(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("original_price")).toString()));
			orderItem.setBaseoriginalprice(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("base_original_price"))
							.toString()));
			orderItem.setTaxpercent(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("tax_percent")).toString()));
			orderItem.setTaxamount(NumberParserUtil.getBigDecimalSafely((object
					.getPropertySafely("tax_amount")).toString()));
			orderItem.setBasetaxamount(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("base_tax_amount")).toString()));
			orderItem.setTaxinvoiced(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("tax_invoiced")).toString()));
			orderItem
					.setBasetaxinvoiced(NumberParserUtil
							.getBigDecimalSafely((object
									.getPropertySafely("base_tax_invoiced"))
									.toString()));
			orderItem
					.setDiscountpercent(NumberParserUtil
							.getBigDecimalSafely((object
									.getPropertySafely("discount_percent"))
									.toString()));
			orderItem.setDiscountamount(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("discount_amount")).toString()));
			orderItem.setBasediscountamount(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("base_discount_amount"))
							.toString()));
			orderItem
					.setDiscountinvoiced(NumberParserUtil
							.getBigDecimalSafely((object
									.getPropertySafely("discount_invoiced"))
									.toString()));
			orderItem.setBasediscountinvoiced(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("base_discount_invoiced"))
							.toString()));
			orderItem.setRowtotal(NumberParserUtil.getBigDecimalSafely((object
					.getPropertySafely("row_total")).toString()));
			orderItem.setBaserowtotal(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("base_row_total")).toString()));
			orderItem.setAmountrefunded(NumberParserUtil
					.getBigDecimalSafely(object.getPropertySafely(
							"amount_refunded").toString()));
			orderItem.setRowinvoiced(NumberParserUtil
					.getBigDecimalSafely(object.getPropertySafely(
							"row_invoiced").toString()));
			orderItem.setBaseamountrefunded(NumberParserUtil
					.getBigDecimalSafely((object
							.getPropertySafely("base_amount_refunded"))
							.toString()));
			orderItem.setGiftmessageid(NumberParserUtil.getIntegerSafely(object
					.getPropertySafely("gift_message_id").toString()));
			orderItem.setGiftmessage(object.getPropertySafely("gift_message")
					.toString());
			orderItem.setGiftmessageavailable(NumberParserUtil
					.getIntegerSafely(object.getPropertySafely(
							"gift_message_available").toString()));
			orderItem.setBaseprice(NumberParserUtil.getBigDecimalSafely(object
					.getPropertySafely("base_tax_before_discount").toString()));
			orderItem.setBaseprice(NumberParserUtil.getBigDecimalSafely(object
					.getPropertySafely("tax_before_discount").toString()));
			/*
			 * orderItem.setBaseprice((object
			 * .getPropertySafely("weee_tax_applied").toString()));
			 */
			orderItem.setBaseprice(NumberParserUtil.getBigDecimalSafely(object
					.getPropertySafely("weee_tax_applied_amount").toString()));
			orderItem.setBaseprice(NumberParserUtil.getBigDecimalSafely(object
					.getPropertySafely("weee_tax_applied_row_amount")
					.toString()));
			orderItem.setBaseprice(NumberParserUtil.getBigDecimalSafely(object
					.getPropertySafely("base_weee_tax_applied_amount")
					.toString()));
			orderItem.setBaseweeetaxappliedrowamount(NumberParserUtil
					.getBigDecimalSafely(object.getPropertySafely(
							"base_weee_tax_applied_row_amount").toString()));
			orderItem.setWeeetaxdisposition(NumberParserUtil
					.getBigDecimalSafely(object.getPropertySafely(
							"weee_tax_disposition").toString()));
			orderItem.setWeeetaxrowdisposition(NumberParserUtil
					.getBigDecimalSafely(object.getPropertySafely(
							"weee_tax_row_disposition").toString()));
			orderItem.setBaseweeetaxdisposition(NumberParserUtil
					.getBigDecimalSafely(object.getPropertySafely(
							"base_weee_tax_disposition").toString()));
			orderItem.setBaseweeetaxrowdisposition(NumberParserUtil
					.getBigDecimalSafely(object.getPropertySafely(
							"base_weee_tax_row_disposition").toString()));
			orderItem
					.setBaserowinvoiced(NumberParserUtil
							.getBigDecimalSafely((object
									.getPropertySafely("base_row_invoiced"))
									.toString()));
			orderItem.setRowweight(NumberParserUtil.getBigDecimalSafely((object
					.getPropertySafely("row_weight")).toString()));
			itemList.add(orderItem);
		}
		return itemList;

	}

	@Override
	public void saveManufactureDate(ProductManufactureDate p) {
		manufactureDateMapper.insert(p);
	}

	@Override
	public void saveOrderedItem(MagentoOrderItem orderItem, int productStatus) {
		/*
		 * If productStatus is "0" then add the item to particular order. If
		 * productStatus is "1" then disable the item for particular order
		 */
		if (productStatus == 0)
			itemMapper.insert(orderItem);
		if (productStatus == 1) {
			orderItem.setIsActive(Byte.parseByte("0"));
			itemMapper.updateItem(orderItem);
		}
	}
}
