package co.stutzen.tomatotail.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.Manufacturer;
import co.stutzen.tomatotail.entity.ManufacturerExample;
import co.stutzen.tomatotail.mapper.ManufacturerMapper;
import co.stutzen.tomatotail.service.ManufacturerService;

@Service
public class ManufacturerServiceImpl extends SWFServiceImpl<Manufacturer>
		implements ManufacturerService {

	@Autowired
	ManufacturerMapper manufacturerMapper;

	@Override
	public List<Manufacturer> manufacturerByName(String name, int start,
			int limit) {
		List<Manufacturer> list = null;
		if(start == 0 && limit ==0){
			list = manufacturerMapper.manufactureDetailsByName(name);
		}
		else{
			list = manufacturerMapper.manufactureByName(name, new ManufacturerExample(), new RowBounds(start, limit));
		}
		return list;
	}

	@Override
	public List<Manufacturer> manufacturerList(int start, int limit) {
		return manufacturerMapper.selectByExampleWithRowbounds(
				new ManufacturerExample(), new RowBounds(start, limit));
	}

	@Override
	public int updateManufacturer(Manufacturer manufacturer) {
		return manufacturerMapper.updateManufacturer(manufacturer);
	}

	@Override
	public Manufacturer duplicateManufacturer(String name) {
		return manufacturerMapper.duplicateManufacturer(name);
	}

}
