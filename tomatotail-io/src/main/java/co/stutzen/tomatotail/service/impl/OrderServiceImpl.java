package co.stutzen.tomatotail.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.CustomerAddress;
import co.stutzen.tomatotail.entity.Order;
import co.stutzen.tomatotail.entity.OrderDetails;
import co.stutzen.tomatotail.entity.OrderDetailsWithBLOBs;
import co.stutzen.tomatotail.entity.OrderExample;
import co.stutzen.tomatotail.entity.OrderHistory;
import co.stutzen.tomatotail.entity.OrderJSON;
import co.stutzen.tomatotail.entity.Product;
import co.stutzen.tomatotail.mapper.ChannelPriceMappingMapper;
import co.stutzen.tomatotail.mapper.CustomerAddressMapper;
import co.stutzen.tomatotail.mapper.OrderDetailsMapper;
import co.stutzen.tomatotail.mapper.OrderHistoryMapper;
import co.stutzen.tomatotail.mapper.OrderMapper;
import co.stutzen.tomatotail.mapper.ProductMapper;
import co.stutzen.tomatotail.mapper.UnProcessedOrderMapper;
import co.stutzen.tomatotail.service.OrderService;
import co.stutzen.tomatotail.util.CUtil;
import co.stutzen.tomatotail.util.CommonConstants;

@Service
public class OrderServiceImpl extends SWFServiceImpl<Order> implements OrderService {

	@Autowired
	private OrderMapper orderMapper;

	@Autowired
	private OrderDetailsMapper orderDetailsMapper;

	@Autowired
	private CustomerAddressMapper addressMapper;

	@Autowired
	private OrderHistoryMapper historyMapper;

	@Autowired
	private UnProcessedOrderMapper unProcessedOrderMapper;

	@Autowired
	private ChannelPriceMappingMapper priceMapping;

	@Autowired
	private ProductMapper productMapper;

	@Override
	public int saveOrder(Order order) {
		order.setSourceType(CommonConstants.INTERNAL_SYSTEM);
		order.setStatus(CommonConstants.PROCESSED);
		order.setAddress(addressFormat(addressMapper.selectByPrimaryKey(order.getAddressId())));
		orderMapper.insert(order);
		for (OrderDetailsWithBLOBs details : order.getOrderDetails()) {
			if (details != null) {
				if (details.getIsActive() == null)
					details.setIsActive(true);
				details.setCreatedBy(order.getCreatedBy());
				details.setModifiedBy(order.getModifiedBy());
				details.setOrderId(order.getId());
				details.setAvailableQty(details.getRequiredQty());
				details.setCurrentPrice(details.getRequiredPrice());
				details.setAvailability(CommonConstants.AVAILABILITY);
				details.setParentSku(details.getSku());
				BigDecimal totalPrice = CUtil.checkNull(details.getCurrentPrice()).multiply(
						new BigDecimal(CUtil.checkNullForInteger(details.getRequiredQty())));
				details.setTotalPrice(totalPrice.setScale(2, RoundingMode.UP));
				/*
				 * During product revision in outlet, product price may change. So update Channel
				 * Partner's product price (MRP & Selling Price).
				 */
				productPriceUpdate(details, order.getOutletId(), order.getModifiedBy());
				
				orderDetailsMapper.insert(details);
			}
		}
		/* Save Order History */
		if (order.getId() > 0) {
			saveOrderHistory(order, CommonConstants.PROCESSED);
		}
		/* Update Unprocess Order status from 'Initiated' to 'Processing' */
		updateUnProcessedOrderStatus(order.getRefOrderId(),
				order.getModifiedBy(), CommonConstants.PROCESSING);
		return order.getId();
	}

	private void updateUnProcessedOrderStatus(int unProcessedOrderId,
			int modifiedBy, String status) {
		unProcessedOrderMapper.updateStatusById(unProcessedOrderId, status, modifiedBy);
	}

	private void saveOrderHistory(Order order, String status) {
		OrderHistory orderHistory = new OrderHistory();
		orderHistory.setOrderId(order.getId());
		orderHistory.setComment(order.getComments());
		orderHistory.setCreatedBy(order.getModifiedBy());
		orderHistory.setModifiedBy(order.getModifiedBy());
		orderHistory.setIsActive(true);
		orderHistory.setStatus(status);
		historyMapper.insert(orderHistory);
	}

	private String addressFormat(CustomerAddress address) {
		StringBuilder sb = new StringBuilder();
		if (address != null)
			sb.append(CUtil.checkNull(address.getAddress())).append(",")
					.append(CUtil.checkNull(address.getCity())).append(",")
					.append(CUtil.checkNull(address.getEmail())).append(",")
					.append(CUtil.checkNull(address.getPincode())).append(",")
					.append(CUtil.checkNull(address.getState()));
		else
			sb.append(" ");
		return sb.toString();
	}

	@Override
	public List<Order> orderList(int orderId, String status, String date,
			String name, int outletId, String sourceType, int start, int limit) {
		List<Order> list = new ArrayList<Order>();
		if (start == 0 && limit == 0)
			list = orderMapper.orderListCount(orderId, status, date, name, outletId, sourceType);
		else
			list = orderMapper.orderListWithLimit(orderId, status, date, name, outletId, sourceType, new OrderExample(), new RowBounds(start, limit));
		return list;
	}

	@Override
	public int updateOrder(Order order) {
		order.setAddress(addressFormat(addressMapper.selectByPrimaryKey(order.getAddressId())));
		orderMapper.updateByOrderId(order);
		for (OrderDetailsWithBLOBs details : order.getOrderDetails()) {
			details.setOrderId(order.getId());
			/*
			 * During product revision in outlet, product price may change. So update Channel
			 * Partner's product price (MRP & Selling Price).
			 */
			productPriceUpdate(details, order.getOutletId(), order.getModifiedBy());
				
			if (details.getId() > 0) {
				BigDecimal totalPrice = CUtil.checkNull(details.getCurrentPrice()).multiply(new BigDecimal(CUtil.checkNullForInteger(details.getRequiredQty())));
				details.setTotalPrice(totalPrice.setScale(2, RoundingMode.UP));
				details.setModifiedBy(order.getModifiedBy());
				orderDetailsMapper.updateOrderDetails(details);
			}
			if (details.getId() == 0) {
				details.setCreatedBy(order.getModifiedBy());
				details.setModifiedBy(order.getModifiedBy());
				details.setOrderId(order.getId());
				details.setAvailability(CommonConstants.AVAILABILITY);
				BigDecimal totalPrice = new BigDecimal(CUtil.checkNull(details.getCurrentPrice()).doubleValue()
						* CUtil.checkNullForInteger(details.getRequiredQty()).doubleValue());
				details.setTotalPrice(totalPrice.setScale(2, RoundingMode.UP));
				orderDetailsMapper.insert(details);
			}
		}

		if (order.getStatus().equalsIgnoreCase(CommonConstants.REVISED)) {
			/* Save order history */
			if (order.getId() > 0) {
				saveOrderHistory(order, CommonConstants.REVISED);
			}
			/* Update Unprocess Order status from 'Initiated' to 'Processing' */
			updateUnProcessedOrderStatus(order.getRefOrderId(),order.getModifiedBy(), CommonConstants.REVISED);
			return order.getId();
		}
		return order.getId();
	}

	@Override
	public BigDecimal getOrderTotalPrice(int orderId) {
		return orderDetailsMapper.orderTotalPrice(orderId);
	}

	@Override
	public int updateOrderStatus(int orderId, int modifiedBy, String status) {
		Order order = orderMapper.selectByPrimaryKey(orderId);
		if (order != null) {
			order.setModifiedBy(modifiedBy);
			order.setStatus(status);
			orderMapper.updateByOrderId(order);
		}
		/* Save Order History */
		if (order.getId() > 0) {
			saveOrderHistory(order, status);
		}
		/* Update Unprocess Order status */
		if (order.getSourceType().equalsIgnoreCase(
				CommonConstants.INTERNAL_SYSTEM)) {
			updateUnProcessedOrderStatus(order.getRefOrderId(),
					order.getModifiedBy(), status);
		}
		return orderId;
	}

	private void productPriceUpdate(OrderDetails details, int outletId, int modifiedBy){
		Product p = productMapper.selectProductBySku(CUtil.checkNull(details.getSku()));
		if (p != null) {
			priceMapping.updatePartnerPrice(details.getMrpPrice(), details.getCurrentPrice(), p.getProductid(), outletId, modifiedBy);
		}
	}

	@Override
	public List<OrderJSON> orderedItemList(int orderId, String status, String date, int outletId, int begin, int end) {
		List<OrderJSON> list = new ArrayList<OrderJSON>();
		if(begin ==0 && end ==0)
			list = orderMapper.getOrderedItemCount(orderId, status, date, outletId);
		else
			list = orderMapper.getOrderedItemList(orderId, status, date, outletId, new OrderExample(), new RowBounds(begin, end));
		return list;
	}

	
}
