package co.stutzen.tomatotail.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.ProductRequestExample;
import co.stutzen.tomatotail.entity.ProductRequestWithBLOBs;
import co.stutzen.tomatotail.mapper.ProductRequestMapper;
import co.stutzen.tomatotail.service.ProductRequestService;

@Service
public class ProductRequestServiceImpl extends SWFServiceImpl<ProductRequestWithBLOBs> implements ProductRequestService{

	@Autowired
	private ProductRequestMapper reqMapper;
	
	@Override
	public List<ProductRequestWithBLOBs> listAll(String prodReqId, String partnerId, String partnerCity, String prodName, String status,int start, int limit) {
		List<ProductRequestWithBLOBs> list = null;
		if(start == 0 && limit == 0)
			list = reqMapper.listAllCount(prodReqId, partnerId, partnerCity, prodName, status);
		else
			list = reqMapper.listAllWithLimit(prodReqId, partnerId,  partnerCity, prodName, status, new ProductRequestExample(), new RowBounds(start, limit));
		return list;
	}

	@Override
	public int modify(ProductRequestWithBLOBs prodRequest) {
		return reqMapper.updateById(prodRequest);
	}

	@Override
	public int save(ProductRequestWithBLOBs prodRequest) {
		return reqMapper.insert(prodRequest);
	}

	@Override
	public int updateStatus(int prodReqId, String status, int updatedBy) {
		return reqMapper.updateStatus(prodReqId, status, updatedBy);
	}

}
