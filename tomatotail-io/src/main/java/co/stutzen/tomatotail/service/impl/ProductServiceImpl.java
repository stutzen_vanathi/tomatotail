package co.stutzen.tomatotail.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.ProductExample;
import co.stutzen.tomatotail.entity.ProductWithBLOBs;
import co.stutzen.tomatotail.mapper.ProductMapper;
import co.stutzen.tomatotail.service.ProductService;

@Service
public class ProductServiceImpl extends SWFServiceImpl<ProductWithBLOBs> implements
		ProductService {

	@Autowired
	ProductMapper productmapper;

	@Override
	public List<ProductWithBLOBs> listOfProduct(String productName, String brandName,
			String manuName, int start, int limit) {
		List<ProductWithBLOBs> list = null;
		if(start ==0 && limit ==0){
			list = productmapper.productListBySearch(productName+"%", brandName+"%",
					manuName+"%");
		}else{

			list = productmapper.selectProductBySearch(productName+"%", brandName+"%",
					manuName+"%", new ProductExample(), new RowBounds(start, limit));
		}
		return list;
	}

	@Override
	public List<ProductWithBLOBs> duplicateProductList(String productName,
			Integer manufacturerId) {
		return productmapper.duplicateProductList(productName, manufacturerId);
	}

	@Override
	public ProductWithBLOBs productWithCompetitorPrices(int productId) {
		return productmapper
				.selectProductByPrimaryKeyWithCompetitorPrices(productId);
	}

	@Override
	public String duplicateProductSku(String sku) {
		return productmapper.duplicateProductSku(sku);
	}

	@Override
	public List<ProductWithBLOBs> productList(String productName,
			int partnerId, String sku, int start, int limit) {
		List<ProductWithBLOBs> list = null;
		if(start ==0 && limit ==0){
			list = productmapper.productListCount(productName, partnerId, sku);
		}
		else{
			list = productmapper.productListWithLimit(productName, partnerId, sku, new ProductExample(), new RowBounds(start, limit)); 
		}
		return list;
	}

	@Override
	public List<ProductWithBLOBs> productListAll(int start, int limit) {
		List<ProductWithBLOBs> list = null;
		if(start ==0 && limit ==0){
			list = productmapper.selectByExampleWithBLOBs(new ProductExample());
		}else{
			list = productmapper.selectByExampleWithBLOBsWithRowbounds(new ProductExample(), new RowBounds(start, limit));
		}
		return list;
	}

	@Override
	public int saveProduct(ProductWithBLOBs product) {
		return productmapper.insert(product);
	}

	@Override
	public int updateProduct(ProductWithBLOBs product) {
		return productmapper.updateByPrimaryKeyWithBLOBs(product);
	}

}
