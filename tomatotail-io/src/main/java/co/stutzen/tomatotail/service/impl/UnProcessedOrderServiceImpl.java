package co.stutzen.tomatotail.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.UnProcessedOrder;
import co.stutzen.tomatotail.entity.UnProcessedOrderExample;
import co.stutzen.tomatotail.mapper.UnProcessedOrderMapper;
import co.stutzen.tomatotail.service.UnProcessedOrderService;

@Service
public class UnProcessedOrderServiceImpl extends
		SWFServiceImpl<UnProcessedOrder> implements UnProcessedOrderService {

	@Autowired
	private UnProcessedOrderMapper unProcessedOrderMapper;

	@Override
	public List<UnProcessedOrder> getUnProcessedOrderList(int id,String date,
			String status,String name,String mobile, int begin, int end) {
		List<UnProcessedOrder> list = null;
		if (begin == 0 && end == 0) {
			list = unProcessedOrderMapper.getUnProcessedOrderListCount(id,date, status,name,mobile);
		} else {
			list = unProcessedOrderMapper.getUnProcessedOrderListWithLimit(id,date, status,name,mobile,
					new UnProcessedOrderExample(), new RowBounds(begin, end));
		}
		return list;
	}

}
