package co.stutzen.tomatotail.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.tomatotail.entity.User;
import co.stutzen.tomatotail.entity.UserRole;
import co.stutzen.tomatotail.mapper.UserMapper;
import co.stutzen.tomatotail.mapper.UserRoleMapper;
import co.stutzen.tomatotail.service.UserService;

@Service
public class UserServiceImpl extends SWFServiceImpl<User> implements UserService{

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private UserRoleMapper roleMapper;

	@Override
	public User getPasswordByUserName(String name) {
		return userMapper.getPasswordByUserName(name);
	}

	@Override
	public void updatePassword(User user) {
		userMapper.updatePassword(user);
	}

	@Override
	public User duplicateEmail(String email) {
		return userMapper.duplicateEmail(email);
	}

	@Override
	public User duplicateUser(String userName) {
		return userMapper.duplicateUser(userName);
	}

	@Override
	public void saveRole(UserRole role) {
		roleMapper.insert(role);
	}

	@Override
	public void updateRole(User user, String oldUserName) {
		user.getRole().setUsername(user.getUsername());
		roleMapper.updateByUserName(user.getRole().getUsername(),user.getRole().getRolename(),oldUserName);
	}

	@Override
	public String oldUserName(Integer userId) {
		return userMapper.getuserName(userId);
	}

	@Override
	public void updateUser(User user) {
		userMapper.updateUser(user);
	}

	@Override
	public User getUserInfo(int userId) {
		return userMapper.getuserInfo(userId);
	}
}
