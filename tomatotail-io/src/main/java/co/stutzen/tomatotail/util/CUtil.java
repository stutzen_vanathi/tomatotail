package co.stutzen.tomatotail.util;

import java.math.BigDecimal;

public class CUtil {

	public static String checkNull(String param1) {
		String output = " ";
		if (param1 != null && !param1.isEmpty())
			output = param1;
		return output;
	}
	
	public static Integer checkNullForInteger(Integer param1) {
		Integer output =  new Integer(0);
		if (param1 != null)
			output = param1;
		return output;
	}
	
	public static BigDecimal checkNull(BigDecimal param1) {
		BigDecimal output = BigDecimal.ZERO;
		if (param1 != null)
			output = param1;
		return output;
	}
	
	public static BigDecimal checkNullForStringToBigDecimal(String param1) {
		BigDecimal output = BigDecimal.ZERO;
		if (param1 != null && !param1.isEmpty())
			output = new BigDecimal(param1);
		return output;
	}
	
	public static int checkNullForStringToInt(String param1) {
		int output = 0;
		if (param1 != null && !param1.isEmpty())
			output = Integer.parseInt(param1);
		return output;
	}
}
