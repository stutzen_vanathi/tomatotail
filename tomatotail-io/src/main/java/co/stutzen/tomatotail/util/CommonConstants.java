package co.stutzen.tomatotail.util;

public class CommonConstants {

	public static final String INTERNAL_SYSTEM = "Internal";
	public static final String EXTERNAL_SYSTEM = "External";
	public static final int CUSTOMER_REF_ID = 0;
	public static final String INITIATED = "Initiated";
	public static final String PROCESSED = "Processed";
	public static final String REVISED = "Revised";
	public static final String APPROVED = "Approved";
	public static final String REJECTED = "Rejected";
	public static final String COMPLETED = "Completed";
	public static final String ACCEPTED = "Accepted";
	public static final int AVAILABILITY = 1;
	public static final String PROCESSING = "Processing";
	public static final String SHIPPING_ADDRESS = "Shipping";
	public static final String EXTERNAL_SYSTEM_ORDER = "External System Order";
	public static final boolean IS_SAVE = true;
	public static final boolean IS_UPDATE = false;
	public static final String SENT_FOR_APPROVAL = "Sent for Approval";
	public static final int ACCEPT_MODE = 1;
	public static final int REJECT_MODE = 2;
	public static final int REVISED_MODE = 3;
	public static final boolean IS_PARENT = true;
}
