package co.stutzen.tomatotail.util;

import javax.annotation.PostConstruct;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.stutzen.webframework.logger.LogFormater;
import org.stutzen.webframework.logger.LoggerManager;

@Configuration
public class CommonWSUtil {

	@Autowired
	private Environment env;
	private HttpTransportSE httpTransport;
	String url = null;
	String nameSpace = null;

	@PostConstruct
	public void init() {
		httpTransport = new HttpTransportSE(env.getProperty("magento.url"));
	}

	public String nameSpace() {
		nameSpace = env.getProperty("magento.namespace");
		return nameSpace;
	}

	public String url() {
		url = env.getProperty("magento.url");
		return url;
	}

	public SoapObject getMagentoLoginDetails() {
		SoapObject request = new SoapObject(env.getProperty("magento.namespace"), "consumerLogin");
		request.addProperty("username", env.getProperty("magento.username"));
		request.addProperty("password", env.getProperty("magento.password"));
		request.addProperty("consumerKey", env.getProperty("magento.consumerKey"));
		request.addProperty("consumerSecret", env.getProperty("magento.consumerSecret"));
		return request;
	}

	public String getSessionId() {
		SoapSerializationEnvelope envolpe = KSoapUtil.getEnvelope();
		SoapObject request = getMagentoLoginDetails();
		String sessionId = null;
		try {
			envolpe.setOutputSoapObject(request);
			httpTransport.call("", envolpe);
			Object loginRes = envolpe.getResponse();
			/*sessionId = loginRes.toString();*/
			
			String[] loginReponse = loginRes.toString().split(";");
			for(int i = 0; i < loginReponse.length ; i++){
				String customerData = loginReponse[i];
				String[] temp = null;
				temp = customerData.split("=");
				String s = temp[0];
				if(s.equalsIgnoreCase(" token")){
					sessionId = temp[1];
				}
				if(s.equalsIgnoreCase(" secret")){
					sessionId = sessionId+"|"+temp[1];
				}
				System.out.println(sessionId);
			}
			
		} catch (Exception e) {
			LoggerManager.error(LogFormater.getErrorLog(getClass(),
					"getSession",
					"Error occurred while connecting with Magento", e));
		}
		return sessionId;
	}

	public HttpTransportSE getHttpTransport() {
		return httpTransport;
	}

	public SoapObject getComplexFilter(String column, String operator,
			String value) {

		// associativeEntity level
		SoapObject associativeEntity = new SoapObject(
				env.getProperty("magento.namespace"), "associativeEntity");

		associativeEntity.addProperty("key", operator);
		associativeEntity.addProperty("value", value);

		// complexFilter level
		SoapObject complexFilter = new SoapObject(
				env.getProperty("magento.namespace"), "complexFilter");
		complexFilter.addProperty("key", column);
		complexFilter.addProperty("value", associativeEntity);

		return complexFilter;
	}
	
	public SoapObject getSimpleFilter(String column, String value) {
		// associativeEntity level
		SoapObject associativeEntity = new SoapObject(env.getProperty("magento.namespace"),
				"associativeEntity");
		associativeEntity.addProperty("key", column);
		associativeEntity.addProperty("value", value);

		return associativeEntity;
	}
}
