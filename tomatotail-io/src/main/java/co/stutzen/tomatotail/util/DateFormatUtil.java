package co.stutzen.tomatotail.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatUtil {

	private static SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public static Date getDateTimeFormat(String dateTime)
			throws java.text.ParseException {
		Date dateTimeFormat = sdf.parse(dateTime);
		return dateTimeFormat;
	}
	
	public static String getDateTimeStringObj(Date dateTime)
			throws java.text.ParseException {
		return sdf.format(dateTime);
	}
}
