package co.stutzen.tomatotail.util;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapSerializationEnvelope;

public class KSoapUtil {
	
	public static SoapSerializationEnvelope getEnvelope() {
		
		SoapSerializationEnvelope envolpe = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envolpe.dotNet = false;
		envolpe.xsd = SoapSerializationEnvelope.XSD;
		envolpe.enc = SoapSerializationEnvelope.ENC;
		return envolpe;
	}
}
