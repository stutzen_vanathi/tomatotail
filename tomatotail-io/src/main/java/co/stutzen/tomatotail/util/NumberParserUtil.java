package co.stutzen.tomatotail.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NumberParserUtil {

	private static SimpleDateFormat sqlDateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public static BigDecimal getBigDecimalSafely(String input) {

		BigDecimal bigDecimal = null;
		if (input != null) {
			bigDecimal = new BigDecimal(input);
		}
		return bigDecimal;

	}

	public static Integer getIntegerSafely(String input) {

		Integer integer = null;
		if (input != null) {
			integer = new Integer(input);
		}
		return integer;
	}

	public static Date getDateSafely(String input) {

		Date date = null;
		try {
			if (input != null) {
				date = sqlDateFormat.parse(input);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return date;
	}

	public static Byte getByteSafely(String input) {
		Byte b = null;

		if (input != null) {
			b = new Byte(input);
		}

		return b;
	}

	public static Boolean getBooleanSafely(String input) {
		Boolean b = null;

		if (input != null) {
			b = new Boolean(input);
		}
		return b;
	}

}
