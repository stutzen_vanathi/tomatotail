<html>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" href="css/print.css" type="text/css" media="print" />
<script src="jquery.min.js"></script>
<script type="text/javascript">

function requestList(){
var orderid="<%=request.getParameter("rid")%>";
	$.ajax({
		type:'GET',
		url : 'order/orderById?orderId='+orderid,
		success:function(response){
		var files= JSON.parse(response);
			  var table = $('<table id="orders" />').appendTo($('#tablediv'));
			  $('<tr/>').appendTo(table).append($('<th />').text("S.No")).append($('<th />').text("Product Name")).append($('<th />').text("SKU")).append($('<th>').text("Price")).append($('<th />').text("Quantity")).append($('<th />').text("Sub Total"));
              $(files.data.order.orderItemList).each(function(i, file) {
	                         $('<tr />').appendTo(table)
	                         .append($('<td />').text(""))
	                         .append($('<td />').text(file.name))
							  .append($('<td />').text(file.sku))
							  .append($('<td />').text(file.price))
							  .append($('<td />').text(file.qtyordered))
	                         .append($('<td />').text(file.price * file.qtyordered));

	          });

			  $(files.data.order).each(function(i, file) {
	                 $('#orderIddiv').html("Order Id : " + "#"+file.orderid);
	          });

			  $(files.data.order).each(function(i, file) {
				   var MyDate = "/Date("+file.createdat+")/"
				   console.log("MyDate"+MyDate);
			  var value = new Date
						(
                 parseInt(MyDate.replace(/(^.*\()|([+-].*$)/g, ''))
						);
			var dat = value.getMonth() +
                         1 +
                       "/" +
           value.getDate() +
                       "/" +
       value.getFullYear();
	                 $('#orderdate').html("Order Date : " +dat);
	          });

			   $(files.data.order.orderAddressList).each(function(i, file) {
	                 $('#customername').html(file.firstname+" "+file.lastname+",");
	          });

			    $(files.data.order.orderAddressList).each(function(i, file) {
	                 $('#shippingaddress').html(file.street+",<br/>"+file.city+",<br/>"+file.postcode);
	          });

			  /* $(files.data.order.orderAddressList).each(function(i, file) {
	                 $('#customeraddress').html(file.street+",<br/>"+file.city+",<br/>"+file.postcode);
	          }); */

			   $(files.data.order.orderPayment).each(function(i, file) {
	                 $('#paymentType').html(file.method);
	          });

			   $(files.data.order).each(function(i, file) {
	                 $('#shippingamount').html("Shipping : &#8377;"+file.shippingamount +" + Total : &#8377;"+file.subtotal);
	          });

			    $(files.data.order).each(function(i, file) {
	                 $('#grandtotal').html("Total Amount : &#8377;"+file.grandtotal);
	          });

			   $(files.data.order).each(function(i, file) {
	                 $('#totalquantity').html("Total Product Quantity :<b>" +file.totalqtyordered+"</b>");
	          });

			}
	});

	$.ajax({
		type:'GET',
		url : 'channelPartner/channelPartnerById?partnerId='+1,
		success:function(response){
		var files= JSON.parse(response);

			   $(files.data.partnerDetails).each(function(i, file) {
	                 $('#customeraddress').html("Invoice by,<br />" +file.name+"<br />"+file.address);
	          });
		}
	});
}
</script>
</head>
<body onload="javascript:requestList();">
<div class="main"><div width="100%" class="header" align="left" ><div class="header_content title1"><img src="../images/logo.png" /></div>
<div class="header_content title2">Invoice</div>
<div class="header_content" id="date"></div> </div>
<div id="section" class="strikethrough"></div><div class="details" >
<div class="customer" id="customername"></div><br>
<div class="details1"><div id="orderIddiv"></div><div class="order-date" id="orderdate"></div><div>Shipping Address :</div><div class="shipping-address" id="shippingaddress"></div></div><div class="details2"><div id="channelPartner"></div><div class="customer-address" id="customeraddress"></div></div></div><div class="quantity" id="totalquantity"></div>
<script type="text/javascript">

  var myDate = new Date();

  var month=new Array();
  month[0]="JAN";
  month[1]="FEB";
  month[2]="MAR";
  month[3]="APR";
  month[4]="MAY";
  month[5]="JUN";
  month[6]="JUL";
  month[7]="AUG";
  month[8]="SEP";
  month[9]="OCT";
  month[10]="NOV";
  month[11]="DEC";
  var hours = myDate.getHours();
  var minutes = myDate.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ampm;
 document.getElementById("date").innerHTML = myDate.getDate()+" "+month[myDate.getMonth()];
</script>
<div id="tablediv" align="center"></div><div class="summary" id="shippingamount"></div><div class="total" ><span class="total-span" id="grandtotal"></span></div><div class="footer_section"></div><div class="buttons"><button class="print-btn" onclick="javascript:window.print();">Print</button></div><div class="footer-image"><img src="../images/clipart.png"></img></div><br /><div align="center" class="footer">Thank you for shopping with us...! Visit again.</div><br /></div>

</body>
</html>