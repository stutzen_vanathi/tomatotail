/**
 * This class is the base class for all entities in the application.
 */
Ext.define('TomTail.model.Base', {
    extend: 'Ext.data.Model',

    fields: [{
        name: 'id',
        type: 'int'
    }],

    schema: {
        namespace: 'TomTail.model',
        proxy: {
        	type:'ajax',
            url: '{prefix}/{entityName:uncapitalize}',
           reader:{
        	   type:'json'
           }
        }
    }
});
