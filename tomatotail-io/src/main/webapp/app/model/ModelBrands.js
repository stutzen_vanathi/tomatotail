Ext.define('TomTail.model.ModelBrands', {
    extend: 'Ext.data.Model',

    fields:["brandid","name","manufacturerid","manufacturerName","isactive","comments"],
});
