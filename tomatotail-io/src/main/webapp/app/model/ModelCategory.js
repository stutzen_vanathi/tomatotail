Ext.define('TomTail.model.ModelCategory', {
    extend: 'Ext.data.Model',
    
    fields:["categoryid","name","comments","isactive"],
});
