Ext.define('TomTail.model.ModelCompetitorAnalysis',
		{
	extend:'TomTail.model.Base',
	
	fields: [{
                    name: 'competitorId',
					//mapping: 'competitors.CompetitorName'
                }, {
					name: 'competitorprice',
					//mapping: 'competitors.CompetitorPrice'
                }, {
                    name: 'discountprice',
					//mapping: 'competitors.CompetitorDiscountPrice'
                }, {
					name: 'name',
					mapping: 'competitor.name'
                }, {
                    name: 'address',
					mapping: 'competitor.address'
                }],
				belongsTo: 'TomTail.model.ModelProductAnalysis'
				
		});
		