Ext.define('TomTail.model.ModelCompetitors', {
    extend: 'Ext.data.Model',
    
   fields:["competitorid","name","comments","address","isactive"],
});
