Ext.define('TomTail.model.ModelManufacturer', {
    extend: 'Ext.data.Model',

    fields:["manufacturerid","name","address","isactive","comments"],
});
