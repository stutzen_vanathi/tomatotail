Ext.define('TomTail.model.ModelOrderAddress',
		{
	extend:'TomTail.model.Base',
	
	fields: [{
                    name: 'firstname',
                }, {
					name: 'lastname',
                }, {
                    name: 'street',
                }, {
					name: 'company',
                }, {
                    name: 'city',
                },{
                    name: 'postcode',
                },{
                    name: 'telephone',
                }],
				belongsTo: 'TomTail.model.ModelOrders'
				
		});
		