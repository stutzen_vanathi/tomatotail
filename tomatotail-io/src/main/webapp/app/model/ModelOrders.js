Ext.define('TomTail.model.ModelOrders', {
    extend:'TomTail.model.Base',
    requires: [
	 'TomTail.model.ModelOrderAddress'
	 ],
    fields: [
        { name: 'orderid', type: 'int' },
        { name: 'createdat',  type: 'date', convert:function(v,record){return Ext.Date.format(new Date(v), 'Y-m-d');}},
        { name: 'orderstatus'},
        { name: 'totalqtyordered'},
		{name:'shippingamount'},
    ],
	 hasMany: [{
        model: 'TomTail.model.ModelOrderAddress',
        name: 'orderAddressList'
    }],
});
