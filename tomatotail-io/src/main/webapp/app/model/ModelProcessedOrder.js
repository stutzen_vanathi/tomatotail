Ext.define('TomTail.model.ModelProcessedOrder',
		{
	extend:'TomTail.model.Base',
	
	 fields: [{
			name:'id',
		},{
			name:'orderId',
		},{
            name: 'productName',
			//mapping:'orderDetails.productName'
        }, {
            name: 'sku',
			//mapping:'orderDetails.sku'
        }, {
            name: 'currentPrice',
			//mapping:'orderDetails.currentPrice'
        },  {
            name: 'requiredPrice',
			//mapping:'orderDetails.requiredPrice'
        }, {
			name:'requiredQty',
			////mapping:'orderDetails.requiredQty'
		}],
				belongsTo: 'TomTail.model.ModelProcessedOrderDetails'
				
		});
		