Ext.define('TomTail.model.ModelProcessedOrderDetails',
		{
	extend:'TomTail.model.Base',
	/* requires: [
	 'TomTail.model.ModelProcessedOrder'
	 ], */
	 fields: [{
			name:'id',
		},{
			name:'orderDetails',
		},{
			name:'unProcessedOrder',
		},{
			name:'totalPrice',
		},{
			name:'dateTime',
		},{
            name: 'sourceType',
        },{
            name: 'refOrderId',
        },{
            name: 'customerId',
        },{
            name: 'mobile',
        },{
            name: 'outletId',
        },{
            name: 'addressId',
        },{
            name: 'address',
        },{
            name: 'noOfItems',
        },{
            name: 'status',
        },{
            name: 'isActive',
        },{
            name: 'comments',
        },{
			name:'orderId',
		},{
            name: 'productName',
			//mapping:'orderDetails.productName'
        }, {
            name: 'sku',
			//mapping:'orderDetails.sku'
        }, {
            name: 'currentPrice',
			//mapping:'orderDetails.currentPrice'
        },  {
            name: 'requiredPrice',
			//mapping:'orderDetails.requiredPrice'
        }, {
			name:'requiredQty',
			////mapping:'orderDetails.requiredQty'
		}],
/* 			 hasMany: [{
        model: 'TomTail.model.ModelProcessedOrder',
        name: 'orderDetails'
    }], */
		});
		