Ext.define('TomTail.model.ModelProductAnalysis',
		{
	extend:'TomTail.model.Base',
	requires: [
	 'TomTail.model.ModelCompetitorAnalysis'
	 ],
	fields: [{
                    name: 'productid',
                }, {
                    name: 'name'
                }, {
                    name: 'weight'
                }, {
                    name: 'sku'
                }, {
                    name: 'categoryid'
                }, {
                    name: 'brandid'
                }],
	        idProperty:'productanalysisid',
			 hasMany: [{
        model: 'TomTail.model.ModelCompetitorAnalysis',
        name: 'competitorPrices'
    }],
		});
		