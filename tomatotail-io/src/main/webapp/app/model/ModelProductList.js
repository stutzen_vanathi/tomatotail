Ext.define('TomTail.model.ModelProductList', {
    extend: 'Ext.data.Model',
    
    fields: [{
            name: 'name'
        }, {
            name: 'sku'
        }, {
            name: 'price'
        }, {
            name: 'qtyordered'
        }, {
            name: 'total',
            type: 'int',
            convert: function(val, row) {
                return row.data.CompetitorPrice * row.data.productQty;
            }
        },{
			name:'shipping'
		}],
});
