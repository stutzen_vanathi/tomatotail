Ext.define('TomTail.model.ModelProducts', {
    extend: 'Ext.data.Model',
    
  fields:["productid","name","brandid","SKU","weight","categoryid","comments","isactive"],
});
