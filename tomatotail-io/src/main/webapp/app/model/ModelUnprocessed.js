Ext.define('TomTail.model.ModelUnprocessed', {
    extend: 'Ext.data.Model',
    
  fields: [{
            name: 'id'
        },{
            name: 'customerId'
        },{
            name: 'date'
        },{
            name: 'dateTime',
			 type: 'date', 
			 convert:function(v,record){return Ext.Date.format(new Date(v), 'Y-m-d H:i:s');}
        }, {
            name: 'addressId'
        }, {
            name: 'name',
			mapping:'customer.name',
        }, {
            name: 'mobile',
			mapping:'customer.mobile',
        },{
            name: 'city',
			mapping:'address.city',
        }, {
            name: 'pincode',
			mapping:'address.pincode',
        },{
            name: 'state',
			mapping:'address.state',
        }, {
            name: 'address',
        },{
            name: 'address',
			mapping:'address.address',
        }, {
            name: 'comments',
			//mapping:'customer.comments',
        }, {
            name: 'isActive',
			//mapping:'customer.isActive',
        },{
            name: 'sourceType',
			mapping:'customer.sourceType',
        }],
});
