Ext.define('ChannelPartner.store.Navigation', {
    extend: 'Ext.data.TreeStore',
    alias: 'store.navigation',
	storeId:'navigation',
    constructor: function(config) {
        var me = this,
            queryParams = Ext.Object.fromQueryString(location.search),
            charts = ('charts' in queryParams) && !/0|false|no/i.test(queryParams.charts);

        me.callParent([Ext.apply({
            root: {
                text: 'All',
                id: 'all',
                expanded: true,
                children: charts ? me.getChartNavItems() : me.getNavItems()
            }
        }, config)]);
    },

    
    getNavItems: function() {
        return [{
                    text: "Home",
                    id: 'home',
                    children: [{
                        text: "Billy",
                        id: 'billy',
                       expanded: true,
						children: [{
                        text: "Puppy",
                        id: 'puppy',
                        leaf: true
                    },{
                        text: "History",
                        id: 'history',
                        leaf: true,
						
                    }],
                    },{
                        text: "Product",
                        id: 'productPanel',
                        leaf: true
                    },{
                        text: "Feedback",
                        id: 'feedback',
                        leaf: true
                    }]
                }
        ];
    }
});
