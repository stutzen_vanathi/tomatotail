Ext.define('TomTail.store.NavigationTree', {
    extend: 'Ext.data.TreeStore',

    storeId: 'NavigationTree',
    root: {
        expanded: true,
        children: [
            {
                text:   'Dashboard',
                //view:   'dashboard.Dashboard',
                leaf:   true,
                iconCls: 'right-icon new-icon x-fa fa-desktop',
                routeId: 'dashboard'
            },
            {
                text:   'Competitor Analysis',
                view:   'config.competitorsAnalysis.CompetitorsAnalysisWorkPanel',
                iconCls: 'right-icon x-fa fa-send ',
                leaf:   true,
                routeId: 'competitorsAnalysis'

            },
            {
                text: 'Orders',
                expanded: false,
                selectable: false,
                iconCls: 'x-fa fa-leanpub',
                routeId : 'orders-parent',
                id:       'orders-parent',
                children: [
                    {
                        text: 'Unprocessed Orders',
                        view: 'orders.Orders',
                        leaf: true,
                        iconCls: 'x-fa fa-file-o',
                        routeId:'orders.unprocess'
                    },

                    {
                        text: 'Processed Orders',
                        view: 'orders.ProcessedOrdersPanel',
                        leaf: true,
                        iconCls: 'x-fa fa-exclamation-triangle',
                        routeId:'orders.process'
                    },
                    {
                        text: 'Revised Orders',
                        view: 'config.orderrevision.OrderrevisionWorkPanel',
                        leaf: true,
                        iconCls: 'x-fa fa-times-circle',
                        routeId:'orders.revised'
                    },
                ]
            },
/*             {
                text:   'Search results',
                view:   'search.Results',
                leaf:   true,
                iconCls: 'x-fa fa-search',
                routeId:'search'
            },
            {
                text: 'FAQ',
                view: 'pages.FAQ',
                leaf: true,
                iconCls: 'x-fa fa-question',
                routeId:'faq'
            }, */
            {
                text: 'Configuration',
                expanded: false,
                selectable: false,
                iconCls: 'x-fa fa-leanpub',
                routeId : 'pages-parent',
                id:       'pages-parent',
                children: [
                    {
                        text: 'Manufacturer',
                        view: 'config.manufacturer.ManufacturerWorkPanel',
                        leaf: true,
                        iconCls: 'x-fa fa-file-o',
                        routeId:'config.manufacturer'
                    },

                    {
                        text: 'Brands',
                         view: 'config.brands.BrandWorkPanel',
                        leaf: true,
                        iconCls: 'x-fa fa-exclamation-triangle',
                        routeId:'config.brands'
                    },
                    {
                        text: 'Category',
                         view: 'config.category.CategoryWorkPanel',
                        leaf: true,
                        iconCls: 'x-fa fa-times-circle',
                        routeId:'config.category'
                    },
                    {
                        text: 'Competitors',
                         view: 'config.competitors.CompetitorsWorkPanel',
                        leaf: true,
                        iconCls: 'x-fa fa-lock',
                        routeId:'config.competitors'
                    },

                    {
                        text: 'Products',
                        view: 'config.products.ProductWorkPanel',
                        leaf: true,
                        iconCls: 'x-fa fa-check',
                        routeId:'config.products'
                    },
                    {
                        text: 'Users',
                        view: 'pages.BlankPage',
                        leaf: true,
						iconCls: 'x-fa fa-user',
                        //iconCls: 'x-fa fa-pencil-square-o',
                        routeId:'authentication.register'
                    },
                    {
                        text: 'Password Reset',
                        view: 'authentication.PasswordReset',
                        leaf: true,
                        iconCls: 'x-fa fa-lightbulb-o',
                        routeId:'authentication.passwordreset'
                    }
                ]
            },
            {
                text:   'Outlet Products',
                view:   'widgets.Widgets',
                leaf:   true,
                iconCls: 'x-fa fa-flask',
                routeId:'widgets'
            },
            /* {
                text:   'Feedback',
                view:   'forms.Wizards',
                leaf:   true,
                iconCls: 'x-fa fa-edit',
                routeId:'forms'
            },
			 {
                text:   'Widgets',
                view:   'widgets.Widgets',
                leaf:   true,
                iconCls: 'x-fa fa-flask',
                routeId:'widgets'
            },
             {
                text: 'Charts',
                view: 'charts.Charts',
                iconCls: 'x-fa fa-pie-chart',
                leaf:   true,
                routeId:'charts'
            },
			{
                text:   'Profile',
                view:   'profile.UserProfile',
                leaf:   true,
                iconCls: 'x-fa fa-user',
                routeId:'profile'
            },
            {
                text:   'Search results',
                view:   'search.Results',
                leaf:   true,
                iconCls: 'x-fa fa-search',
                routeId:'search'
            },
            {
                text: 'FAQ',
                view: 'pages.FAQ',
                leaf: true,
                iconCls: 'x-fa fa-question',
                routeId:'faq'
            }, */
        ]
    },
    fields: [
        {
            name: 'text'
        }
    ]
});
