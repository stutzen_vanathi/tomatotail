Ext.define('TomTail.view.config.billy.history.HistoryPanel', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.historyPanel',
	xtype: 'history-panels',
    requires: [
        'TomTail.view.config.billy.history.HistoryController'
    ],
    controller: 'main',
    viewModel: 'history',

	html:'Sample History',
	bodyStyle:'background:url(../../../../images/bg_page.png) !important',
});