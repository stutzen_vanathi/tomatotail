Ext.define('TomTail.view.config.billy.puppy.PuppyPanel', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.puppyPanel',
	xtype: 'puppy-panels',
    requires: [
        'TomTail.view.config.billy.puppy.PuppyController'
    ],
	id:'PuppyPanel',
    controller: 'puppy',
    viewModel: 'puppy',

    layout: 'fit',
	html:'Sample page',

	bodyStyle:'background:url(../../../../images/bg_page.png) !important',

});