Ext.define('TomTail.view.config.brands.BrandAddWindow', {
    extend: 'Ext.window.Window',
	alias:'widget.brand',

    //modal: true,
    title: 'Brand',
    itemId:'brandWindow',
	width:580,
	items:[{
	xtype:'form',
	 layout: {
                type: 'table',
                columns: 2,
                tdAttrs: {
                    style: 'padding: 5px;'
                }
            },
	bodyPadding:10,
    defaults: {
        xtype: 'textfield',
    },

    items: [{
        fieldLabel: 'Name',
		allowBlank:false,
        name: 'name'
    },{
        fieldLabel: 'Id',
		readOnly:true,
		tabIndex:1,
		style:'margin-left:80px;',
		labelWidth:20,
		width:80,
        name: 'brandid'
    },{
		xtype:'fieldset',
		 border: false,
		 colspan:2,
            frame: false,
            padding: 0,
            layout: {
                type: 'table',
                columns: 2,
                tdAttrs: {
                    style: 'padding: 0px;'
                }
            },
		items:[{
		xtype:'combobox',
        fieldLabel: 'Manufacturer',
		allowBlank:false,
		editable:false,
        name: 'manufacturerid',
		emptyText: 'Please select...',
						forceSelection: true,
                        store:new Ext.data.Store({
                     		 storeId:'manufacturernamestore',
                              fields: ['manufacturerid', 'name'],
							  autoLoad:true,
                               proxy : {
                           		type : 'ajax',
                           		url:   "manufacturer/list",
                           		reader : {
                           			type : 'json',
                           			rootProperty:'data.list',
                           		},

                               },
                           }),
                        queryMode: 'remote',
                        displayField: 'name',
                        valueField: 'manufacturerid',
                        //value:1,
    }, {
				xtype : 'button',
				icon:'images/add1.png',
				tooltip:'Add Manufacturer',
				style:'background:white; borderColor:white;',
				margin : '10px',
				handler:function(){
					Ext.widget('manufacturer').show();
				}
			}]
	},{
		xtype:'textareafield',
		colspan:2,
		width:530,
        fieldLabel: 'Comments',
        name: 'comments'
    },{
        xtype: 'checkbox',
        fieldLabel: 'Enable',
        name: 'isactive',
        checked: true,
        value: true,
		inputValue:1,
		uncheckedValue:0
    }],
}],
    buttons: [{
        text: 'Save',
		handler:function(){

			var win = this.up('window');
			console.log("wndow"+win);
			var form = win.down('form');
			console.log("form"+form);
			if (form.isValid()) {
			var formData = Ext.encode(form.getValues());
			var url='brand/modify';
			if(form.getForm().findField('brandid').getValue()==null || form.getForm().findField('brandid').getValue()==''){
				url='brand/save';
			}
			Ext.Ajax.request({
				url : url,
				method : 'POST',
				jsonData : formData,
				success: function ( result, request ) {
	    				  Ext.MessageBox.show({
                                    title:'Success',
                                    msg: 'Successfully saved...',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.INFO
                                })
						  if (win) {
				win.close();
				Ext.getCmp('brandgrid').store.load();
			}
				},
				 failure: function ( result, request ) {
	    				   Ext.MessageBox.show({
                                    title:'Failure',
                                    msg: 'Invalid Data',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.ERROR
                                })
	    			  }
			});
			
			}
		}
    }, {
        text: 'Cancel',
		handler:function(){
			Ext.ComponentQuery.query('#brandWindow')[0].close();
		}
    },{
        text: 'Delete',
		itemId:'DeleteButton',
		hidden:true,
		handler:function(){

			var win = this.up('window');
			console.log("wndow"+win);
			var form = win.down('form');
			console.log("form"+form);
			var formData = Ext.encode(form.getValues());
			var url='brand/delete';
			var name = form.getForm().findField('name').getValue();
			if(form.getForm().findField('brandid').getValue()==null || form.getForm().findField('brandid').getValue()==''){
				url='brand/delete';
			}
			Ext.Ajax.request({
				url : url,
				method : 'POST',
				jsonData : formData,
				success: function ( result, request ) {
	    				  Ext.Msg.alert('Success','Brand ' + name + " was Deleted.");
						  if (win) {
				win.close();
				Ext.getCmp('brandgrid').store.load();
			}
				},
				 failure: function ( result, request ) {
	    				  console.log('failure');
	    			  }
			});
			

		}
    }],
	listeners:{
		close:function(){
			Ext.ComponentQuery.query('#brandPanel')[0].enable();
			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
			Ext.ComponentQuery.query('#header')[0].enable(); 
		}
	},


});