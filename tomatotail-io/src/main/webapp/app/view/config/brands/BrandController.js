Ext.define('TomTail.view.config.brands.BrandController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.brands-brand',
    init: function() {
        var me = this;
        this.control({
            'brandgrid dataview': {
                itemdblclick: function(view, rec, elm, inx, evt, obj) {
                    me.loadInfo(rec);
                }
            },
			'brandsearchpanel button[action=searchBrand]':{
                click:'searchConfig'
            },
			'brandsearchpanel button[action=ClearBrand]':{
                click:'clearConfig'
            },
        });
    },

    loadInfo: function(rec) {
        var win = Ext.widget('brand');
		var formvalue=win.down('form').getForm();
    	    formvalue.loadRecord(rec);
        win.show();
		//Ext.ComponentQuery.query('#DeleteButton')[0].disable();
		Ext.ComponentQuery.query('#brandPanel')[0].disable();
		Ext.ComponentQuery.query('#tabbar')[0].disable();
		Ext.ComponentQuery.query('#header')[0].disable();

    },
	 searchConfig:function(){
    	var store=this.getViewModel().getStore('brands');
    	var brandName =this.lookupReference('brandName').getValue();
		var manuName = this.lookupReference('manufacturerName').getValue();
    	console.log('brandName'+brandName);
    	 if(brandName!='' || manuName!=''){
url="brand/searchByName?brandName="+brandName+"&manuName="+manuName ;
}else{
    		console.log('else');
    		url="brand/brandList";
    	}
    	console.log('url'+url);
    	store.proxy.url=url;
    	store.load();
    },
	 clearConfig:function(){
    	this.lookupReference('brandName').reset();
		this.lookupReference('manufacturerName').reset();
    }
});
