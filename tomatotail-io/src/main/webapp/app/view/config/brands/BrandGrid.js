Ext.define('TomTail.view.config.brands.BrandGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.brandgrid',
	requires:[
	         'TomTail.view.config.brands.BrandAddWindow',
			 'Ext.data.identifier.Negative'
	          ],
	border : false,
	title : 'Search Result',
	height : 288,
	header: {
        xtype: 'header',
        titlePosition: 0,
        defaults: {
            margin: '0 10px'
        },
        items: [
           
            {
                xtype: 'button',
                text: "Add New",
                handler: Ext.bind(function() {
                	Ext.widget('brand').show();
					Ext.ComponentQuery.query('#brandPanel')[0].disable(); 
					Ext.ComponentQuery.query('#tabbar')[0].disable(); 
					Ext.ComponentQuery.query('#header')[0].disable(); 
                }, this)
            }
        ]
    },
	width : '100%',
	bind: {
        store: '{brands}'
    },
	id:'brandgrid',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			autoScroll : true,
			
			columns : [ {
				text : 'Id',
				dataIndex : 'brandid',
				sortable:false,
			}, {
				text : 'Name',
				dataIndex : 'name',
				sortable:false,
				flex : 1
			},{
				text : 'Manufacturer Name',
				dataIndex : 'manufacturerid',
				renderer:function(value,me,recs){
    	 var record = Ext.getStore('manufacturernamestore').findRecord('manufacturerid', value);
    	 var returnname=(record === null ? '' : record.data.name );
    	 return returnname;
    },
				sortable:false,
				flex : 1
			},{
				text : 'Enable',
				dataIndex : 'isactive',
				sortable:false,
				flex : 1,
				renderer: function(value) {
			if (value == 1) {
				value = "True";
			}else{
				value = "False";
			}
			return value;
		   }
			}],
			cls:'paging',
			dockedItems : [ {
				xtype : 'pagingtoolbar',
				dock : 'bottom',
				bind: {
        store: '{brands}'
    },
				displayInfo : true,
				displayMsg : 'Displaying Brands {0} - {1} of {2}'
			}]
		});
		this.callParent(arguments);
	}
});