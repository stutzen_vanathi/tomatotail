Ext.define('TomTail.view.config.brands.BrandModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.brands-brand',
	 requires: [
               'TomTail.model.ModelBrands'
           ],
    data: {
        name: 'TomTail'
    },

	stores:{
		 brands:{
				storeId: 'brandStore',
				model : 'TomTail.model.ModelBrands',
				autoLoad: true,
				 pageSize:20,
				 proxy: {
            type: 'ajax',
            url: 'brand/brandList',
            reader: {
                rootProperty: 'data.list',
                totalProperty:'data.total'
            },
            listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }

        },
			},
			manufacturer:{
				storeId: 'manufacturerStore',
				model :'TomTail.model.ModelManufacturer',
				autoLoad: true,
				proxy: {
            type: 'ajax',
            url: 'manufacturer/list',
            reader: {
                rootProperty: 'data.list'
            },
            listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }

        },
			},
	},

});
