Ext.define('TomTail.view.config.brands.BrandSearchPanel', {
	extend : 'Ext.form.Panel',
	alias : 'widget.brandsearchpanel',
	requires:[
'Ext.form.field.Hidden'

	          ],
	title : 'Search brands ',

	layout : 'hbox',
	height:'50px',

	reference:'brandform',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			padding:7,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [{
				fieldLabel : 'Manufacturer Name',
				name : 'manufacturerName',
				reference: 'manufacturerName',
				labelWidth:'n',
				xtype : 'textfield',
				margin : '10px'
			}, {
				fieldLabel : 'Brand Name',
				name : 'name',
				xtype : 'textfield',
				margin : '10px',
				reference: 'brandName',
			}, {
				xtype : 'button',
				text : 'Clear',
				width : 70,
				margin : '10px',
				action:'ClearBrand'
			},{
				xtype : 'button',
				text : 'Search',
				width : 70,
				margin : '10px',
				action:'searchBrand'
			}]
		});
		this.callParent(arguments);
	}
});