Ext.define("TomTail.view.config.brands.BrandWorkPanel",{
    extend: "Ext.panel.Panel",
	alias: 'widget.brandconfigpanel',
    requires:[
    'TomTail.view.config.brands.BrandSearchPanel',
    'TomTail.view.config.brands.BrandGrid',
	'TomTail.view.config.brands.BrandController',
	'TomTail.view.config.brands.BrandModel'
    ],
    controller: "brands-brand",
    viewModel: {
        type: "brands-brand"
    },
     layout: {
        type: 'vbox',
        align: 'center'
    },
 
    bodyStyle:'background:url(../../../../images/bg_page.png) !important',
    items:[{
        xtype: 'brandsearchpanel',
        //collapsible:true,
        height:102,
        width:'100%'
        	 },{
        xtype: 'brandgrid',
        //collapsible:true,
        flex:2,
        width:'100%'
        
    }],
    initComponent : function() {
    this.callParent(arguments);
    }
});
