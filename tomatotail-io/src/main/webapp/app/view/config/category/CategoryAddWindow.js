Ext.define('TomTail.view.config.category.CategoryAddWindow', {
    extend: 'Ext.window.Window',

    alias: 'widget.category',

   controller: "category-category",
    viewModel: {
        type: "category-category"
    },
	id:'categoryWindow',
   // modal: true,
    title: 'Category',
    width: 500,
	items:[{
	xtype:'form',
	bodyPadding:10,
    defaults: {
        xtype: 'textfield',
    },
	/* layout: {
            type: 'table',
            columns: 2,
             tdAttrs: { style: 'padding: 10px; vertical-align: top;' }
    }, */
        
    items: [ {
		xtype:'fieldset',
		 border: false,
		 colspan:2,
            frame: false,
            style:'padding:0px; margin-left:-1px; margin-bottom:5px;',
            layout: {
                type: 'hbox',
            }, 
		items:[{
			xtype:'textfield',
        fieldLabel: 'Name',
		allowBlank:false,
        name: 'name'
    },{
		xtype:'textfield',
		readOnly:true,
		tabIndex:1,
        fieldLabel: 'Id',
		style:'margin-left:100px;',
		labelWidth:20,
		width:80,
        name: 'categoryid'
    }]
	}, {
        xtype: 'checkbox',
        fieldLabel: 'Parent',
        name: 'isParent',
		itemId:'parentcat',
		colspan:2,
		style:'margin-left:-1px;',
        //value: true,
		checked:true,
		inputValue:1,
		uncheckedValue:0,
		listeners:{
			change:function(cb, checked){
				if(checked){
					Ext.ComponentQuery.query("#parentcategory")[0].setDisabled(true);
				}else{
					Ext.ComponentQuery.query("#parentcategory")[0].setDisabled(false);
				}
			}
		}
    },/*{
        xtype: 'checkbox',
        fieldLabel: 'Sub Category',
        name: '',
		itemId:'subcat',
        //value: true,
		inputValue:1,
		uncheckedValue:0,
		listeners:{
			change:function(cb, checked){
				if(checked){
				Ext.ComponentQuery.query("#parentcategory")[0].setDisabled(false);
				Ext.ComponentQuery.query("#parentcat")[0].setDisabled(true);
				}else{
				Ext.ComponentQuery.query("#parentcategory")[0].setDisabled(true);	
				Ext.ComponentQuery.query("#parentcat")[0].setDisabled(false);
				}
			}
		}
    },*/{
            xtype: 'combobox',
			editable:false,
            fieldLabel: 'Parent',
			disabled:true,
			itemId:'parentcategory',
            name: 'parentId',
            emptyText: 'Please select...',
            forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'',
                fields: ['categoryid', 'name'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "category/parentCategorys",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'categoryid',
            //value:1,
        }, {
		xtype:'textareafield',
        fieldLabel: 'Comments',
        name: 'comments'
    },{
        xtype: 'checkbox',
        fieldLabel: 'Enable',
        name: 'isactive',
        checked: true,
        value: true,
		inputValue:1,
		uncheckedValue:0
    }],
}],
    buttons: [{
        text: 'Save',
		handler:function(){
			var win = this.up('window');
			console.log("wndow"+win);
			var form = win.down('form');
			console.log("form"+form);
			if (form.isValid()) {
			var formData = Ext.encode(form.getValues());
			var url='category/modify';
			if(form.getForm().findField('categoryid').getValue()==null || form.getForm().findField('categoryid').getValue()==''){
				url='category/save';
			}
			Ext.Ajax.request({
				url : url,
				method : 'POST',
				jsonData : formData,
				success: function ( result, request ) {
					var jsondata = Ext.util.JSON.decode(result.responseText);
                	Ext.MessageBox.show({
                        title: 'Success',
                        msg: jsondata.message,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    })
						 if (win) {
				win.close();
				Ext.getCmp('categorygrid').store.load();
			}
				},
				 failure: function ( result, request ) {
	    				  Ext.MessageBox.show({
                                    title:'Failure',
                                    msg: 'Invalid Data',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.ERROR
                                })
	    			  }
			});
			
			}
		}
    }, {
        text: 'Cancel',
		handler:function(){
			Ext.getCmp('categoryWindow').close();
		}
    }],
	
	listeners:{
		close:function(){
			Ext.ComponentQuery.query('#categoryPanel')[0].enable();
			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
		Ext.ComponentQuery.query('#header')[0].enable(); 
		}
	},

});