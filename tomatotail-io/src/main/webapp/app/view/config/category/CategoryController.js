Ext.define('TomTail.view.config.category.CategoryController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.category-category',
    init: function() {
        var me = this;
        this.control({
            'categorygrid dataview': {
                itemdblclick: function(view, rec, elm, inx, evt, obj) {
                    me.loadInfo(rec);
                }
            },
			'categorysearchpanel button[action=searchCategory]':{
                click:'searchConfig'
            },
			'categorysearchpanel button[action=ClearCategory]':{
                click:'clearConfig'
            },
        });
    },

    loadInfo: function(rec) {
        var win = Ext.widget('category');
		var formvalue=win.down('form').getForm();
    	    formvalue.loadRecord(rec);
        win.show();
		formvalue.findField('comments').setValue(rec.data.comments);
		console.log("comments"+rec.data.comments);
		//Ext.ComponentQuery.query('#DeleteButton')[0].setVisible(true);
		Ext.ComponentQuery.query('#categoryPanel')[0].disable();
		Ext.ComponentQuery.query('#tabbar')[0].disable();
		Ext.ComponentQuery.query('#header')[0].disable();

    },

	 searchConfig:function(){
    	var store=this.getViewModel().getStore('category');
    	var categoryName =this.lookupReference('categoryName').getValue();
    	console.log('categoryName'+categoryName);
    	if(categoryName!=null){
    		console.log('if');
    		url="category/searchByName?categoryName="+categoryName;
    	}else{
    		console.log('else');
    		url="category/list";
    	}
    	console.log('url'+url);
    	store.proxy.url=url;
    	store.load();
    },
	clearConfig:function(){
		var category = this.lookupReference('categoryName');
		//this.lookupReference('subcategory').reset();
		category.reset();
	},
});
