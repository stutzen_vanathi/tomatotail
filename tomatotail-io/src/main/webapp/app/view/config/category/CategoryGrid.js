Ext.define('TomTail.view.config.category.CategoryGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.categorygrid',
	requires:[
	         'TomTail.view.config.category.CategoryAddWindow'
	          ],
	border : false,
	title : 'Search Result',
	header: {
        xtype: 'header',
        titlePosition: 0,
        defaults: {
            margin: '0 10px'
        },
        items: [
           
            {
                xtype: 'button',
                text: "Add New",
                handler: Ext.bind(function() {
                	Ext.widget('category').show();
					Ext.ComponentQuery.query('#categoryPanel')[0].disable(); 
					Ext.ComponentQuery.query('#tabbar')[0].disable(); 
					Ext.ComponentQuery.query('#header')[0].disable(); 
                }, this)
            }
        ]
    },
	height : 288,
	width : '100%',
	bind: {
        store: '{category}'
    },
	id:'categorygrid',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			autoScroll : true,
			
			columns : [ {
				text : 'Id',
				dataIndex : 'categoryid',
				sortable:false,
			}, {
				text : 'Name',
				dataIndex : 'name',
				sortable:false,
				flex : 1
			},{
				text : 'Enable',
				dataIndex : 'isactive',
				sortable:false,
				flex : 1,
				renderer: function(value) {
			if (value == 1) {
				value = "True";
			}else{
				value = "False";
			}
			return value;
		   }
			},{
				hidden:true,
				dataIndex : 'comments',
				sortable:false,
				flex : 1
			}],
			cls:'paging',
			dockedItems : [ {
				xtype : 'pagingtoolbar',
				dock : 'bottom',
				bind: {
        store: '{category}'
    },
				displayInfo : true,
				displayMsg : 'Displaying Category {0} - {1} of {2}'
			}]
		});
		this.callParent(arguments);
	}
});