Ext.define('TomTail.view.config.category.CategoryModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.category-category',
	requires: [
               'TomTail.model.ModelCategory'
           ],
    data: {
        name: 'TomTail'
    },
	stores:{
		category:{
				storeId: 'categoryStore',
				model:'TomTail.model.ModelCategory',
				autoLoad: true,
				pageSize:20,
				proxy: {
            type: 'ajax',
            url: 'category/categoryList',
            reader: {
                rootProperty: 'data.list',
				totalProperty: 'data.total',
            },
            listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }

        },
			},
	},

});
