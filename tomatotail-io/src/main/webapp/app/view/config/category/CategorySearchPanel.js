Ext.define('TomTail.view.config.category.CategorySearchPanel', {
	extend : 'Ext.form.Panel',
	alias : 'widget.categorysearchpanel',
	requires:[
'Ext.form.field.Hidden'
	          ],
	title : 'Search Category ',
	layout : 'hbox',
	height:'50px',
	reference:'Categoryform',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			padding:7,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [ {
				fieldLabel : 'Category Name',
				name : 'categoryName',
				reference: 'categoryName',
				xtype : 'textfield',
				margin : '10px',
				listeners:{
					change:function(field, newValue, oldValue, options){
						 var grid = Ext.getCmp('categorygrid');
						grid.store.clearFilter();

						if (newValue) {
							var category = new RegExp(Ext.String.escapeRegex(newValue), "i");
							grid.store.filter({
								filterFn: function(record) {
								return category.test(record.get('categoryid')) ||
								category.test(record.get('name'));
							}
					});
				}
					}
				}
			},/* {
            xtype: 'combobox',
			editable:false,
			margin : '10px',
			reference:'categoryName',
            fieldLabel: 'Category',
            name: 'categoryid',
            emptyText: 'Please select...',
            forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'manufacturerstore',
                fields: ['categoryid', 'name'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "category/list",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'name',
            //value:1,
        }, *//* {
            xtype: 'combobox',
			editable:false,
			margin : '10px',
			reference:'subcategory',
            fieldLabel: 'Sub Category',
            name: 'categoryid',
            emptyText: 'Please select...',
            forceSelection: true,
             store: new Ext.data.Store({
                // storeId:'manufacturerstore',
                fields: ['subcategoryid', 'name'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "category/list",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }), 
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'name',
            //value:1,
        }, */{
				xtype : 'button',
				text : 'Clear',
				width : 70,
				margin : '10px',
				action:'ClearCategory',
			},/* {
				xtype : 'button',
				text : 'Search',
				width : 70,
				margin : '10px',
				action:'searchCategory',
			} */]
		});
		this.callParent(arguments);
	}
});