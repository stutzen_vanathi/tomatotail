
Ext.define("TomTail.view.config.category.CategoryWorkPanel",{
    extend: "Ext.panel.Panel",
	alias: 'widget.categoryconfigpanel',
    requires:[
    'TomTail.view.config.category.CategorySearchPanel',
    'TomTail.view.config.category.CategoryGrid',
	'TomTail.view.config.category.CategoryController',
    ],
    controller: "category-category",
    viewModel: {
        type: "category-category"
    },
    layout: {
        type: 'vbox',
        align: 'center'
    },
    bodyStyle:'background:url(../../../../images/bg_page.png) !important',
    items:[{
        xtype: 'categorysearchpanel',
        //collapsible:true,
        height:102,
        width:'100%'
        	 },{
        xtype: 'categorygrid',
        //collapsible:true,
        flex:2,
        width:'100%'
        
    }],
    initComponent : function() {
    this.callParent(arguments);
    }
});
