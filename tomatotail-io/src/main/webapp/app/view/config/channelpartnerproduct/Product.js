
Ext.define("TomTail.view.config.channelpartnerproduct.Product",{
    extend: "Ext.panel.Panel",
	alias: 'widget.partnerproductPanel',
    requires:[
	'TomTail.view.config.channelpartnerproduct.ProductController',
    'TomTail.view.config.channelpartnerproduct.ProductSearchPanel',
    'TomTail.view.config.channelpartnerproduct.ProductGrid',
	'TomTail.view.config.channelpartnerproduct.ProductModel',
	'TomTail.view.config.channelpartnerproduct.PurchasePriceWindow'
    ],
    controller: "channelpartnerproduct-product",
    viewModel: {
        type: "channelpartnerproduct-product"
    },

     layout: {
        type: 'vbox',
        align: 'center'
    },
    items:[{
        xtype: 'chproductsearchpanel',
        height: 102,
        width:'100%'
        	 },{
        xtype: 'partnerproductgrid',
        flex:2,
        width:'100%'
    }],
    initComponent : function() {
    this.callParent(arguments);
    }
});
