Ext.define('TomTail.view.config.channelpartnerproduct.ProductController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.channelpartnerproduct-product',
    init: function() {
        var me = this;
        this.control({
            'partnerproductgrid dataview': {
                itemdblclick: function(view, rec, elm, inx, evt, obj) {
                    me.loadInfo(rec);
                }
            },
            'chproductsearchpanel #chproductname': { 
    	        specialkey: function(field, e) { 
    	            if(e.getKey() == e.ENTER) { 
    	                      this.searchConfig() 
    	            } 
    	        } 
            },
            'chproductsearchpanel #chsku': { 
    	        specialkey: function(field, e) { 
    	            if(e.getKey() == e.ENTER) { 
    	                      this.searchConfig() 
    	            } 
    	        } 
            },
            'chproductsearchpanel button[action=searchProduct]':{
                click:'searchConfig'
            },
			'chproductsearchpanel button[action=ClearProduct]':{
                click:'clearConfig'
            },
        });
    },

    loadInfo: function(rec) {
        var win = Ext.widget('purchsepricewindow');
		var formvalue=win.down('form').getForm();
    	    formvalue.loadRecord(rec);
        win.show();

    },
    
    searchConfig:function(){
    	var store=this.getViewModel().getStore('productpurchaseprice');
    	var productName = this.lookupReference('productName').getValue();
		var sku = this.lookupReference('sku').getValue();
		var partnerId = this.lookupReference('partnerId').getValue();
		if(partnerId==null){
			partnerId='';
		}
		if(partnerId!=''){
    	if(productName!='' ||  sku!='' || partnerId!=''){
    		url="product/productList?productName="+productName+"&partnerId="+partnerId+"&sku="+sku;
    	}else{
    		console.log('else');
    		url="product/productList?partnerId="+partnerId;
    	}
    	console.log('url'+url);
    	store.proxy.url=url;
    	store.load();
		}else{
			Ext.MessageBox.show({
                title:'Error',
                msg: 'Choose a Outlet',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            })
		}
    	
    },
    
   
    
	clearConfig:function(){
		this.lookupReference('productName').reset();
		this.lookupReference('sku').reset();
		var store=this.getViewModel().getStore('productpurchaseprice');
    	var partnerId = this.lookupReference('partnerId').getValue();
    	if(partnerId==null){
    		partnerId=''
    	}
    	store.proxy.url='product/productList?partnerId='+partnerId;
    	store.load();
		//this.lookupReference('partnerId').reset();
	}
});
