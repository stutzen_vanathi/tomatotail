Ext.define('TomTail.view.config.channelpartnerproduct.ProductGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.partnerproductgrid',
	
	border : false,
	title : 'Product Price',
	height : 288,
	width : '100%',
	bind: {
        store: '{productpurchaseprice}'
    },
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			autoScroll : true,
			
			columns : [{
				hidden:true,
				dataIndex : 'id',
			},{
				text : 'Product Id',
				dataIndex : 'productid',
				sortable:false,
			}, {
				text : 'Product Name',
				dataIndex : 'name',
				sortable:false,
				flex : 1
			},{
				text : 'Purchase Price',
				dataIndex : 'purchaseprice',
				sortable:false,
				editor: {
					xtype:'numberfield',
					minValue:0,
                    allowBlank: false
                },
				flex : 1
			},{
				text : 'Selling Price',
				dataIndex : 'sellingprice',
				sortable:false,
				flex : 1,
				editor: {
					xtype:'numberfield',
					minValue:0,
                    allowBlank: false
                },
			}],
			 selModel: 'cellmodel',
    plugins: {
        ptype: 'cellediting',
        clicksToEdit: 1,
    },
    cls:'paging',
			dockedItems : [{
				xtype : 'pagingtoolbar',
				dock : 'bottom',
				bind: {
					store: '{productpurchaseprice}'
				},
				displayInfo : true,
				displayMsg : 'Displaying products {0} - {1} of {2}'
			}],
			buttons:[{
				 text:'Save',
				 scope:this,
				 handler:function(grid){
					 var partnerId = Ext.ComponentQuery.query('#purchasePriceOutlet')[0].getValue();
					 console.log(partnerId);
						var store_grid = Ext.ComponentQuery.query('partnerproductgrid')[0].store;
						console.log("store_grid"+store_grid);
		               var selectionne = store_grid.getRange();
						console.log("selectionne"+selectionne);
		                var data_grid = new Array();
		                Ext.each(selectionne, function(record) {
							console.log("record"+record);
							var idval=""+record.data.id;

		                    if (record.data.sellingprice != null && record.data.purchaseprice != null) {
		                    	if(idval.indexOf("-")>-1){
		                    		idval=0;
		                    	}else{
		                    		idval=record.data.id;
		                    	}
		                        data_grid.push({
		                        	id: idval,
		                        	partnerid: partnerId,
									productid: record.data.productid,
									sellingprice: record.data.sellingprice,
									purchaseprice: record.data.purchaseprice,
									mrp_Price: record.data.purchaseprice,
								});
		                    }
		                });
						console.log("data_grid"+data_grid);
		                var gridData =  Ext.encode(data_grid);
						console.log("gridData"+gridData);
						if(partnerId!=null){
						Ext.Ajax.request({
							url : 'purchasePrice/modify',
							method : 'POST',
							jsonData : gridData,
							success: function ( result, request ) {
				    				 Ext.MessageBox.show({
			                                    title:'Success',
			                                    msg: 'Successfully saved...',
			                                    buttons: Ext.MessageBox.OK,
			                                    icon: Ext.MessageBox.INFO
			                                })
								var store = Ext.getStore('partnerpurchasePriceStore');
			                         store.proxy.url="product/productList?partnerId="+partnerId;
			                         store.load();
				    				 //Ext.getStore('partnerpurchasePriceStore').load();
						
							},
							 failure: function ( result, request ) {
				    				   Ext.MessageBox.show({
			                                    title:'Failure',
			                                    msg: 'Invalid Data',
			                                    buttons: Ext.MessageBox.OK,
			                                    icon: Ext.MessageBox.ERROR
			                                })
				    			  }
						});
						}else{
							Ext.MessageBox.show({
                                title:'Error',
                                msg: 'Choose a Outlet',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            })
						}
						

					}
			 },{
                    minWidth: 80,
                    text: 'Cancel',
                    scope:this,
                    handler:function(){
                    	this.store.load();
                    }

                }]
		});
		this.callParent(arguments);
	}
});