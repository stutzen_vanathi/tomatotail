Ext.define('TomTail.view.config.channelpartnerproduct.ProductModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.channelpartnerproduct-product',
	requires: ['Ext.data.TreeStore'],
    data: {
        name: 'TomTail'
    },
	stores:{
		productpurchaseprice:{
				storeId: 'partnerpurchasePriceStore',
				fields:[{name:"id",mapping:'priceMapping.id'},"productid","name",{name:"sellingprice",mapping:'priceMapping.sellingprice'},{name:"purchaseprice",mapping:'priceMapping.purchaseprice'},{name:"mrp_Price",mapping:'priceMapping.mrp_Price'}],
				autoLoad: false,
				pageSize:20,
				proxy: {
            type: 'ajax',
            url: 'product/productList?partnerId=',
            reader: {
                rootProperty: 'data.list',
                totalProperty:'data.total',
            },
            listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }

        },
			},
			
	},

});
