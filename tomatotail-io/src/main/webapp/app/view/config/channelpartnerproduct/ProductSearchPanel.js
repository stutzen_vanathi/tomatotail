Ext.define('TomTail.view.config.channelpartnerproduct.ProductSearchPanel', {
	extend : 'Ext.form.Panel',
	alias : 'widget.chproductsearchpanel',
	requires:[
'Ext.form.field.Hidden'
	          ],
	title : 'Search Products',

	layout : 'hbox',
	height:'50px',
	
	reference:'productform',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [{
				fieldLabel : 'Product Name',
				name : 'productName',
				xtype : 'textfield',
				margin : '10px',
				itemId:'chproductname',
				labelWidth:'n',
				reference:'productName'
			},{
				fieldLabel : 'SKU',
				name : 'sku',
				labelWidth:'n',
				itemId:'chsku',
				xtype : 'textfield',
				margin : '10px',
				reference:'sku'
			},{
	            xtype: 'combobox',
	            editable: false,
	            allowBlank:false,
	            margin : '10px',
	            reference:'partnerId',
	            fieldLabel: 'Outlet',
	            name: 'outletId',
	            itemId:'purchasePriceOutlet',
	            emptyText: 'Please select...',
	            //forceSelection: true,
	            store: new Ext.data.Store({
	                // storeId:'',
	                fields: [ 'name','partnerId','city'],
	                autoLoad: true,
	                proxy: {
	                    type: 'ajax',
	                    url: "channelPartner/listAll",
	                    reader: {
	                        type: 'json',
	                        rootProperty: 'data.list',
	                    },

	                },
	            }),
	            queryMode: 'remote',
	            displayField: 'name',
	            valueField: 'partnerId',
	            listeners:{
	         	   select: function(combo, selection) {
	         		  var partnerProduct = Ext.getStore('partnerpurchasePriceStore');
	         		partnerProduct.proxy.url="product/productList?partnerId="+combo.getValue();
	         		 partnerProduct.load();
	         		
	                    }
	                }
	            
	        
	       
	        },{
				xtype : 'button',
				text : 'Clear',
				width : 70,
				margin : '10px',
				action:'ClearProduct'
			},{
				xtype : 'button',
				text : 'Search',
				width : 70,
				margin : '10px',
				action:'searchProduct'
			}]
		});
		this.callParent(arguments);
	}
});