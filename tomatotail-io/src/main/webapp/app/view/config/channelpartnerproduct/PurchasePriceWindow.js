Ext.define('TomTail.view.config.channelpartnerproduct.PurchasePriceWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.purchsepricewindow',
    modal: true,
	controller: "channelpartnerproduct-product",
    viewModel: {
        type: "channelpartnerproduct-product"
    },
    title: 'Purchase Price',
	itemId:'priceWindow',
	width:450,
	layout:'fit',
	bodyPadding:20,
	items:[{
	xtype:'form',
	
	defaults:{
		xtype:'displayfield',
		
	},
	items: [{
    	//xtype:'textfield',
    	//readOnly:true,
        fieldLabel: 'Product Id',
        name: 'productid'
    },{
        fieldLabel: 'Product',
        name: 'name'
    },{
        fieldLabel: 'SKU',
        name: 'sku'
    },{
    	//xtype:'numberfield',
        fieldLabel: 'MRP',
        name: 'mrp_Price'
    },{
    	//xtype:'numberfield',
        fieldLabel: 'Purchase Price',
        name: 'purchaseprice'
    },{
    	//xtype:'numberfield',
        fieldLabel: 'Selling Price',
        name: 'sellingprice'
    }/*,{
        xtype: 'checkbox',
        fieldLabel: 'Enable',
        name: 'isactive',
        checked: true,
        value: true,
    }*/],
  
		}],
   
		 buttons: [{
		        text: 'Close',
				handler:function(){
					Ext.ComponentQuery.query('#priceWindow')[0].close();
				}
		    }],

});