Ext.define('TomTail.view.config.competitors.CompetitorsAddWindow', {
    extend: 'Ext.window.Window',

    alias: 'widget.competitors',

    
   // modal: true,
	id:'competitorsWindow',
    title: 'Competitors',
    width: 500,
	items:[{
	xtype:'form',
	bodyPadding:10,
    defaults: {
        xtype: 'textfield',
    },

    items: [{ 
	xtype:'fieldset',
		 border: false,
            frame: false,
            style:'padding:0px; padding-bottom:5px;',
            layout: {
                type: 'hbox',
            }, 
		items:[{
			xtype:'textfield',
        fieldLabel: 'Name',
		
        name: 'name'
    },{
		xtype:'textfield',
		readOnly:true,
		tabIndex:1,
        fieldLabel: 'Id',
		style:'margin-left:110px;',
		labelWidth:20,
		width:80,
        name: 'competitorid'
    }]
	},{
		xtype:'textareafield',
		
        fieldLabel: 'Address',
        name: 'address'
    },{
		xtype:'textareafield',
        fieldLabel: 'Comments',
        name: 'comments'
    },{
        xtype: 'checkbox',
        fieldLabel: 'Enable',
        name: 'isactive',
        checked: true,
        value: true,
		inputValue:1,
		uncheckedValue:0
    }],
}],
    buttons: [{
        text: 'Save',
		handler:function(){
			var win = this.up('window');
			console.log("wndow"+win);
			var form = win.down('form');
			console.log("form"+form);
			if (form.isValid()) {
			var formData = Ext.encode(form.getValues());
			var url='competitor/update';
			if(form.getForm().findField('competitorid').getValue()==null || form.getForm().findField('competitorid').getValue()==''){
				url='competitor/save';
			}
			Ext.Ajax.request({
				url : url,
				method : 'POST',
				jsonData : formData,
				success: function ( result, request ) {
	    				  Ext.MessageBox.show({
                                    title:'Success',
                                    msg: 'Successfully saved...',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.INFO
                                })
						  if (win) {
				win.close();
				Ext.getCmp('competitorsgrid').store.load();
			}
				},
				 failure: function ( result, request ) {
	    				   Ext.MessageBox.show({
                                    title:'Failure',
                                    msg: 'Invalid Data',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.ERROR
                                })
	    			  }
			});
			}
			
		}
    }, {
        text: 'Cancel',
		handler:function(){
			Ext.getCmp('competitorsWindow').close();
		}
    },{
        text: 'Delete',
		itemId:'DeleteButton',
		hidden:true,
		handler:function(){

			var win = this.up('window');
			console.log("wndow"+win);
			var form = win.down('form');
			console.log("form"+form);
			var formData = Ext.encode(form.getValues());
			var url='competitor/delete';
			var name = form.getForm().findField('name').getValue();
			if(form.getForm().findField('competitorid').getValue()==null || form.getForm().findField('competitorid').getValue()==''){
				url='competitor/delete';
			}
			Ext.Ajax.request({
				url : url,
				method : 'POST',
				jsonData : formData,
				success: function ( result, request ) {
	    				  Ext.Msg.alert('Success','Competitor ' + name + " was Deleted.");
						  if (win) {
				win.close();
				Ext.getCmp('competitorsgrid').store.load();
			}
				},
				 failure: function ( result, request ) {
	    				  console.log('failure');
	    			  }
			});
			

		}
    }],
	
	listeners:{
		close:function(){
			Ext.ComponentQuery.query('#competitorsPanel')[0].enable();
			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
		Ext.ComponentQuery.query('#header')[0].enable(); 
		}
	},


});