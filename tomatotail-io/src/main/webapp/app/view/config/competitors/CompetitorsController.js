Ext.define('TomTail.view.config.competitors.CompetitorsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.competitors-competitors',
    init: function() {
        var me = this;
        this.control({
            'competitorsgrid dataview': {
                itemdblclick: function(view, rec, elm, inx, evt, obj) {
                    me.loadInfo(rec);
                }
            },
			'competitorssearchpanel button[action=searchCompetitors]':{
                click:'searchConfig'
            },
			'competitorssearchpanel button[action=ClearCompetitor]':{
                click:'clearConfig'
            },
        });
    },

    loadInfo: function(rec) {
        var win = Ext.widget('competitors');
		var formvalue=win.down('form').getForm();
    	    formvalue.loadRecord(rec);
        win.show();
		//Ext.ComponentQuery.query('#DeleteButton')[0].setVisible(true);
		Ext.ComponentQuery.query('#competitorsPanel')[0].disable();
		Ext.ComponentQuery.query('#tabbar')[0].disable();
		Ext.ComponentQuery.query('#header')[0].disable();

    },

	searchConfig:function(){
    	var store=this.getViewModel().getStore('competitors');
    	var competitorsName =this.lookupReference('competitorsName').getValue();
    	console.log('competitorsName'+competitorsName);
    	if(competitorsName!=''){
    		console.log('if');
    		url="competitor/find?competitorName="+competitorsName;
    	}else{
    		console.log('else');
    		url="competitor/competitorList";
    	}
    	console.log('url'+url);
    	store.proxy.url=url;
    	store.load();
    },
	clearConfig:function(){
		var comp = this.lookupReference('competitorsName');
		comp.reset();
	},
});
