Ext.define('TomTail.view.config.competitors.CompetitorsGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.competitorsgrid',
	requires:[
	         'TomTail.view.config.competitors.CompetitorsAddWindow'
	          ],
	border : false,
	title : 'Search Result',
	header: {
        xtype: 'header',
        titlePosition: 0,
        defaults: {
            margin: '0 10px'
        },
        items: [
           
            {
                xtype: 'button',
                text: "Add New",
                handler: Ext.bind(function() {
                	Ext.widget('competitors').show();
					Ext.ComponentQuery.query('#competitorsPanel')[0].disable(); 
					Ext.ComponentQuery.query('#tabbar')[0].disable(); 
					Ext.ComponentQuery.query('#header')[0].disable(); 
                }, this)
            }
        ]
    },
	height : 288,
	width : '100%',
	bind: {
        store: '{competitors}'
    },
	id:'competitorsgrid',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			autoScroll : true,
			columns : [ {
				text : 'Id',
				dataIndex : 'competitorid',
				sortable:false,
			}, {
				text : 'Name',
				dataIndex : 'name',
				sortable:false,
				flex : 1
			},{
				text : 'Enable',
				dataIndex : 'isactive',
				sortable:false,
				flex : 1,
				renderer: function(value) {
			if (value == 1) {
				value = "True";
			}else{
				value = "False";
			}
			return value;
		   }
			}],
			cls:'paging',
			dockedItems : [ {
				xtype : 'pagingtoolbar',
				dock : 'bottom',
				bind: {
        store: '{competitors}'
    },
				displayInfo : true,
				displayMsg : 'Displaying Competitors {0} - {1} of {2}'
			}]
		});
		this.callParent(arguments);
	}
});