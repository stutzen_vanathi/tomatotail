Ext.define('TomTail.view.config.competitors.CompetitorsModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.competitors-competitors',
	requires: [
               'TomTail.model.ModelCompetitors'
           ],
    data: {
        name: 'TomTail'
    },
	stores:{
		competitors:{
				storeId: 'competitorStore',
				model:'TomTail.model.ModelCompetitors',
				autoLoad: true,
				pageSize:20,
				proxy: {
            type: 'ajax',
            url: 'competitor/competitorList',
            reader: {
                rootProperty: 'data.list',
				totalProperty: 'data.total',
            },
            listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }

        },
			},
	},

});
