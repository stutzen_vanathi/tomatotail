Ext.define('TomTail.view.config.competitors.CompetitorsSearchPanel', {
	extend : 'Ext.form.Panel',
	alias : 'widget.competitorssearchpanel',
	requires:[
'Ext.form.field.Hidden'
	          ],
	title : 'Search Competitors ',

	layout : 'hbox',
	height:'50px',

	reference:'Competitorsform',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			padding:7,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [ {
				fieldLabel : 'Competitors Name',
				name : 'competitorsName',
				reference:'competitorsName',
				labelWidth:'n',
				xtype : 'textfield',
				margin : '10px',
				listeners:{
					change:function(field, newValue, oldValue, options){
						 var grid = Ext.getCmp('competitorsgrid');
						grid.store.clearFilter();

						if (newValue) {
							var category = new RegExp(Ext.String.escapeRegex(newValue), "i");
							grid.store.filter({
								filterFn: function(record) {
								return category.test(record.get('competitorid')) ||
								category.test(record.get('name'));
							}
					});
				}
					}
				}
			},{
				xtype : 'button',
				text : 'Clear',
				width : 70,
				margin : '10px',
				action:'ClearCompetitor',
			}]
		});
		this.callParent(arguments);
	}
});