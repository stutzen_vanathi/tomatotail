
Ext.define("TomTail.view.config.competitors.CompetitorsWorkPanel",{
    extend: "Ext.panel.Panel",
	alias: 'widget.competitorsconfigpanel',
    requires:[
    'TomTail.view.config.competitors.CompetitorsSearchPanel',
    'TomTail.view.config.competitors.CompetitorsGrid',
	'TomTail.view.config.competitors.CompetitorsController'
    ],
    controller: "competitors-competitors",
    viewModel: {
        type: "competitors-competitors"
    },
    layout: {
        type: 'vbox',
        align: 'center'
    },
    bodyStyle:'background:url(../../../../images/bg_page.png) !important',
    items:[{
        xtype: 'competitorssearchpanel',
        //collapsible:true,
        height:102,
        width:'100%'
        	 },{
        xtype: 'competitorsgrid',
        //collapsible:true,
        flex:2,
        width:'100%'
        
    }],
    initComponent : function() {
    this.callParent(arguments);
    }
});
