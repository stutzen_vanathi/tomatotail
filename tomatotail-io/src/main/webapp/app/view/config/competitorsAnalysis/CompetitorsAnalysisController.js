Ext.define('TomTail.view.config.competitorsAnalysis.CompetitorsAnalysisController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.competitorsanalysis-competitorsanalysis',

	init: function() {
        var me = this;
        this.control({

			'competitorsanalysissearchpanel button[action=SearchComp]':{
                click:'searchConfig'
            },
			'competitorsanalysissearchpanel button[action=ClearConfig]':{
                click:'clearConfig'
            },
        });
    },

	searchConfig:function(){
    	var store = this.getViewModel().getStore('productAnalysis');
		var manuName = this.lookupReference('manufacturerName').getValue();
		var brandName = this.lookupReference('brandName').getValue();
		var productName =this.lookupReference('productName').getValue();
    	console.log('productName'+productName);
    	store.proxy.url="competitorPrice/searchByName?brandName="+brandName+"&manuName="+manuName+"&productName="+productName;
    	store.load();
    },
	clearConfig:function(){
		this.lookupReference('productName').reset();
		this.lookupReference('brandName').reset();
		this.lookupReference('manufacturerName').reset();
	}
});
