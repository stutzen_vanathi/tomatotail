Ext.define('TomTail.view.config.competitorsAnalysis.CompetitorsAnalysisGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.competitorsanalysisGrid',

    width: '100%',
    title: 'Search Result',
    autoScroll: true,
	height:410,
    autoDestroy: true,
	layout:'fit',
    bind:{
		store:'{productAnalysis}',
	},
	itemId:'competitorsanalysisGrid',
     
    columns: [{
        text: 'Competitor Name',
		//dataIndex:'competitorId',
		 //locked: true,
        renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
            values = [];
            record.competitorPrices().each(function(competitors) {
                values.push(competitors.get('name'));
            });
            return values.join('<br\><br\>');
        },
		//width:300,
        flex: 1
    }, {
        text: 'Price',
		//dataIndex:'competitorprice',
        flex: 1,
		renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
            values = [];
            record.competitorPrices().each(function(competitors) {
                values.push(competitors.get('competitorprice'));
            });
            return values.join('<br\><br\>');
        },
    }, {
        text: 'Discount Price',
		//dataIndex:'discountprice',
        flex: 1,
        renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
            values = [];
            record.competitorPrices().each(function(competitors) {
                values.push(competitors.get('discountprice'));
            });
            return values.join('<br\><br\>');
        },
    }],
    features: [{
        ftype: 'grouping',
        groupHeaderTpl: '{[values.rows[0].data.name]} - {[values.rows[0].data.brand.name]} - {[values.rows[0].data.manu.name]}',
    }],
    cls:'paging',
	dockedItems: [{
        xtype: 'pagingtoolbar',
        dock: 'bottom',
		bind: {
        store: '{productAnalysis}'
		},
        displayInfo: true,
        displayMsg: 'Displaying Competitor {0} - {1} of {2}',
    }],
	
});