Ext.define('TomTail.view.config.competitorsAnalysis.CompetitorsAnalysisModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.competitorsanalysis-competitorsanalysis',
	requires:['TomTail.model.ModelProductAnalysis'],
    data: {
        name: 'TomTail'
    },
	stores: {
		productAnalysis:{
        storeId: 'productAnalysisStore',
		model:'TomTail.model.ModelProductAnalysis',
		pageSize: 20,
		groupField: 'productid',
		autoLoad:false,
			proxy:{
				type:'ajax',
				url:'competitorPrice/searchByName?brandName=&manuName=&productName=',
				reader:{
					type:'json',
					rootProperty:'data.list',
					totalProperty:'data.total',
				}
			}
		},
    },

});
