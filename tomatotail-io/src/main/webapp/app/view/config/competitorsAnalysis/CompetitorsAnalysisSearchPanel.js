Ext.define('TomTail.view.config.competitorsAnalysis.CompetitorsAnalysisSearchPanel', {
	extend : 'Ext.form.Panel',
	alias : 'widget.competitorsanalysissearchpanel',

	requires:[
'Ext.form.field.Hidden'
	          ],
	title : 'Search Competitors ',

	layout : 'hbox',
	height:'50px',

	reference:'Competitorsform',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			padding:7,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [ {
		xtype:'combobox',
        fieldLabel: 'Manufacturer',
		value: '',
        name: 'manufacturerid',
		editable:false,
		margin : '10px',
		reference:'manufacturerName',
		emptyText: 'Please select...',
						//forceSelection: true,
                        store:new Ext.data.Store({
                     		 // storeId:'manufacturerstore',
                              fields: ['manufacturerid', 'name'],
                               proxy : {
                           		type : 'ajax',
                           		url:   "manufacturer/list",
                           		reader : {
                           			type : 'json',
                           			rootProperty:'data.list',
                           		},

                               },
                           }),
                        queryMode: 'remote',
                        displayField: 'name',
                        valueField: 'name',
						
                        //value:1,
    },{
		xtype:'combobox',
        fieldLabel: 'Brand',
        name: 'brandid',
		value:'',
		editable:false,
		reference:'brandName',
		margin : '10px',
		emptyText: 'Please select...',
						//forceSelection: true,
                        store:new Ext.data.Store({
                     		 // storeId:'',
                              fields: ['brandid', 'name'],
                               proxy : {
                           		type : 'ajax',
                           		url:   "brand/brandList",
                           		reader : {
                           			type : 'json',
                           			rootProperty:'data.list',
                           		},

                               },
                           }),
                        queryMode: 'remote',
                        displayField: 'name',
                        valueField: 'name',
						
    },{
				fieldLabel : 'Product Name',
				name : 'name',
				xtype : 'textfield',
				margin : '10px',
				reference:'productName',
				value: ''
			},{
				xtype : 'button',
				text : 'Clear',
				width : 70,
				margin : '10px',
				action:'ClearConfig',
			},{
				xtype : 'button',
				text : 'Search',
				width : 70,
				margin : '10px',
				action:'SearchComp',
			}]
		});
		this.callParent(arguments);
	}
});