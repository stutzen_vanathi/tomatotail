
Ext.define("TomTail.view.config.competitorsAnalysis.CompetitorsAnalysisWorkPanel",{
    extend: "Ext.panel.Panel",
	alias: 'widget.competitorsanalysisconfigpanel',
    requires:[
    'TomTail.view.config.competitorsAnalysis.CompetitorsAnalysisGrid',
	'TomTail.view.config.competitorsAnalysis.CompetitorsAnalysisSearchPanel'
    ],
    controller: "competitorsanalysis-competitorsanalysis",
    viewModel: {
        type: "competitorsanalysis-competitorsanalysis"
    },

     layout: {
        type: 'vbox',
        align: 'center'
    },
    bodyStyle:'background:url(../../../../images/bg_page.png) !important',
    items:[{
        xtype: 'competitorsanalysissearchpanel',
        height:102,
        width:'100%'
    },{
        xtype: 'competitorsanalysisGrid',
        width:'100%'
    }],
    initComponent : function() {
    this.callParent(arguments);
    }
});
