Ext.define('TomTail.view.config.competitorsAnalysis.ProductAnalysisGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.productAnalysisGrid',
	
	controller: "competitorsanalysis-competitorsanalysis",
    viewModel: {
        type: "competitorsanalysis-competitorsanalysis"
    },
	
    width: '100%',
    title: 'Search Result',
    autoScroll: true,
	height:470,
    autoDestroy: true,
	layout:'fit',
    bind: {
        store: '{productAnalysis}'
    },
     
    columns: [{
        text: 'Competitor Name',
		//dataIndex:'CompetitorName',
		 //locked: true,
        renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
            values = [];
            record.competitorPrices().each(function(competitor) {
                values.push(competitor.get('competitorId'));
            });
            return values.join('<br\><br\>');
        },
		//width:300,
        flex: 1
    }, {
        text: 'Price',
		//dataIndex:'CompetitorPrice',
        flex: 1,
        renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
            values = [];
            record.competitorPrices().each(function(competitor) {
                values.push(competitor.get('competitorprice'));
            });
            return values.join('<br\><br\>');
        },
    }, {
        text: 'Discount Price',
		//dataIndex:'CompetitorDiscountPrice',
        flex: 1,
        renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
            values = [];
            record.competitorPrices().each(function(competitor) {
                values.push(competitor.get('discountprice'));
            });
            return values.join('<br\><br\>');
        },
    }],
    features: [{
        ftype: 'grouping',
        groupHeaderTpl: '{[values.rows[0].data.productid]} - {[values.rows[0].data.brandid]} {[values.rows[0].data.name]}  {[values.rows[0].data.weight]}',
    }],

});