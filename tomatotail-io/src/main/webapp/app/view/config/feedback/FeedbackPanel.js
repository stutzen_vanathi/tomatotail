Ext.define('TomTail.view.config.feedback.FeedbackPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.feedbackPanel',
    requires: [
       'TomTail.view.config.feedback.FeedbackController'
    ],
    controller: 'feedback',
    viewModel: 'feedback',

	html:'Sample Feedback',

	bodyStyle:'background:url(../../../../images/bg_page.png) !important',

});