Ext.define('TomTail.view.config.manufacturer.ManufacturerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.manufacturer-manufacturer',
    init: function() {
        var me = this;
        this.control({
            'manufacturergrid dataview': {
                itemdblclick: function(view, rec, elm, inx, evt, obj) {
                    me.loadInfo(rec);
                }
            },
			'manufacturersearchpanel button[action=searchconfig]':{
                click:'searchConfig'
            },
			'manufacturersearchpanel button[action=ManuReset]':{
                click:'clearConfig'
            },
        });
    },

    loadInfo: function(rec) {
        var win = Ext.widget('manufacturer');
		var formvalue = win.down('form').getForm(); 
    	    formvalue.loadRecord(rec);
        win.show();
		//Ext.ComponentQuery.query('#DeleteButton')[0].setVisible(true);
		Ext.ComponentQuery.query('#manufacturerPanel')[0].disable();
		Ext.ComponentQuery.query('#tabbar')[0].disable(); 
		Ext.ComponentQuery.query('#header')[0].disable(); 
    },
	
	 searchConfig:function(){
    	var store=this.getViewModel().getStore('manufacturer');
    	var manufacturerName=this.lookupReference('manufacturerName').getValue();
    	console.log('manufacturerName'+manufacturerName);
    	if(manufacturerName!=''){
    		console.log('if');
    		url="manufacturer/find?manufacturerName="+manufacturerName;
    	}else{
    		console.log('else');
    		url="manufacturer/list";
    	}
    	console.log('url'+url);
    	store.proxy.url=url;
    	store.load();
    },
	clearConfig:function(){
	var manu = this.lookupReference('manufacturerName');
					manu.reset();
	}
});
