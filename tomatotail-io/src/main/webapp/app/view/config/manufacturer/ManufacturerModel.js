Ext.define('TomTail.view.config.manufacturer.ManufacturerModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.manufacturer-manufacturer',
	requires: [
               'TomTail.model.ModelManufacturer'
           ],
    data: {
        name: 'TomTail'
    },
	stores:{
		manufacturer:{
				storeId: 'manufacturerStore',
				model :'TomTail.model.ModelManufacturer',
				autoLoad: true,
				pageSize:20,
				proxy: {
            type: 'ajax',
            url: 'manufacturer/manufacturerList',
            reader: {
                rootProperty: 'data.list',
				totalProperty:'data.total',
            },
            listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }

        },
			},
	},

});
