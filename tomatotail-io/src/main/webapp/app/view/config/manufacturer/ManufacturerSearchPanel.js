Ext.define('TomTail.view.config.manufacturer.ManufacturerSearchPanel', {
	extend : 'Ext.form.Panel',
	alias : 'widget.manufacturersearchpanel',
	requires:[
'Ext.form.field.Hidden'

	          ],
	title : 'Search Manufacturer ',

	layout : 'hbox',
	height:'50px',

	reference:'manufacturersearchform',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			padding:7,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [ {
				fieldLabel : 'Manufacturer',
				name : 'manufacturerName',
				reference: 'manufacturerName',
				xtype : 'textfield',
				margin : '10px',
				listeners:{
					change:function(field, newValue, oldValue, options){
						 var grid = Ext.getCmp('manufacturergrid');
						grid.store.clearFilter();

						if (newValue) {
							var category = new RegExp(Ext.String.escapeRegex(newValue), "i");
							grid.store.filter({
								filterFn: function(record) {
								return category.test(record.get('manufacturerid')) ||
								category.test(record.get('name')) ||
								category.test(record.get('address'));
							}
					});
				}
					}
				}
			},{
				xtype : 'button',
				text : 'Clear',
				width : 70,
				margin : '10px',
				action:'ManuReset',
			},/* {
				xtype : 'button',
				text : 'Search',
				width : 70,
				margin : '10px',
				action:'searchconfig',
			}*/]
		});
		this.callParent(arguments);
	}
});