
Ext.define("TomTail.view.config.manufacturer.ManufacturerWorkPanel",{
    extend: "Ext.panel.Panel",
	alias: 'widget.manufacturerconfigpanel',
    requires:[
    'TomTail.view.config.manufacturer.ManufacturerSearchPanel',
    'TomTail.view.config.manufacturer.ManufacturerGrid',
	'TomTail.view.config.manufacturer.ManufacturerController',
	'TomTail.view.config.manufacturer.ManufacturerModel'
    ],
    controller: "manufacturer-manufacturer",
    viewModel: {
        type: "manufacturer-manufacturer"
    },

    layout: {
        type: 'vbox',
        align: 'center'
    },
    bodyStyle:'background:url(../../../../images/bg_page.png) !important',
    items:[{
        xtype: 'manufacturersearchpanel',
       // collapsible:true,
        height:102,
        width:'100%'
        	 },{
        xtype: 'manufacturergrid',
        //collapsible:true,
        flex:2,
        width:'100%'
        
    }],
    initComponent : function() {
    this.callParent(arguments);
    }
});
