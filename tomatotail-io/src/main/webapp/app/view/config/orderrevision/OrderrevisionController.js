Ext.define('TomTail.view.config.orderrevision.OrderrevisionController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.orderrevision',
    init: function() {
        var me = this;
        this.control({
        	
        	'orderrevisionsearchpanel button[action=searchOrderrevision]':{
                click:'searchProcessedOrder'
            },
			
			'orderrevisionsearchpanel button[action=clearOrderrevision]':{
               click:'clearprocessed'
            },
            'ordersrevisedwindow #viewunprocessed':{
                click:'viewUnprocessedOrder'
             },
            'orderrevisiongrid dataview': {
                itemdblclick: function(view, rec, elm, inx, evt, obj) {
                    me.loadRevisedOrder(rec);
                }
            },
        });
    },
    
    searchProcessedOrder:function(){
    	var store=this.getViewModel().getStore('orderrevision');
		var orderdate = this.lookupReference('processedDate').getValue();
		
		if (orderdate != null){
    	var date =Ext.Date.format(new Date(orderdate), 'Y-m-d H:i:s');
		}else{
			console.log(orderdate);
		}
		var orderStatus =this.lookupReference('processorderStatus').getValue();
		if(date == null || date == undefined){
			date = '';
		}
		var name =this.lookupReference('name').getValue();
		
		var sourceType =this.lookupReference('sourceType').getValue();
		if(sourceType == null){
			sourceType = '';
		}
		
    	if(date!='' || orderStatus !='' || name!='' || sourceType!=''){
    		console.log('if');
    		url="order/listAll?status="+orderStatus+"&date="+date+"&name="+name+"&sourceType="+sourceType;
    	}else{
    		console.log('else');
    		url="order/listAll";
    	}
    	console.log('url'+url);
    	store.proxy.url=url;
    	store.load();
    },
    
    clearprocessed:function(){
		this.lookupReference('processedDate').reset();
		this.lookupReference('processorderStatus').reset();
		this.lookupReference('name').reset();
		this.lookupReference('sourceType').reset();
		var store=this.getViewModel().getStore('orderrevision');
		store.proxy.url="order/listAll";
    	store.load();
	},
    
	loadRevisedOrder:function(rec){
		 var win = Ext.widget('ordersrevisedwindow');
		var formvalue=win.down('form').getForm();
   	    formvalue.loadRecord(rec);
			var datetime = Ext.Date.format(new Date(rec.data.dateTime), 'Y-m-d H:i:s');
			formvalue.findField('date').setValue(datetime);
			formvalue.findField('city').setValue(rec.data.partner.city);
			formvalue.findField('sourceType').setValue(rec.data.sourceType);
			formvalue.findField('refOrderId').setValue(rec.data.refOrderId);
			//formvalue.findField('addressId').setValue(rec.data.addressId);
			//formvalue.findField('noOfItems').setValue(rec.data.orderDetails.length);
			Ext.ComponentQuery.query('#revisednoOfItems')[0].setValue(rec.data.orderDetails.length);
			var order = new Array();
				 Ext.each(rec.data.orderDetails, function(record) {
					order.push(record);
				});
				
				Ext.ComponentQuery.query('revisedproductitemgrid')[0].store.loadData(order, false);
				//Ext.ComponentQuery.query('#gridaddBtn')[0].setDisabled(true);
				var partnerProduct = Ext.ComponentQuery.query('#outletProduct')[0];
         		 partnerProduct.clearValue();
         		partnerProduct.store.proxy.url="channelPartner/skuList?partnerId="+rec.data.outletId;
         		partnerProduct.store.load();	
				 Ext.Ajax.request({
			url : 'order/listAll?orderId='+rec.get('id'),
			method : 'GET',
			success : function(response, options) {
				var jsonresp = response.responseText;
				var jsondata = Ext.util.JSON.decode(jsonresp);
				var total = parseFloat(jsondata.data.totalPrice).toFixed(2);
				//Ext.ComponentQuery.query('#revisedtotal')[0].setVisible(true);
				Ext.ComponentQuery.query('#revisedtotal')[0].setValue(total);
				
			},
			failure : function(response, options) {
				Ext.MessageBox.alert('FAILED', 'Error Occured!');
			},
				 });
				 
				 var store = Ext.getStore('revisedItemStore');
				 var totalItems = 0;
				 store.each(function(record){
	    	           
	    	          totalItems += record.get('requiredQty');
	    	         Ext.ComponentQuery.query('#revisedtotalItems')[0].setValue(totalItems);
	       		 });
				 
				 if(rec.data.status == 'Revised'){
					 Ext.ComponentQuery.query('#revisedacceptBtn')[0].setVisible(false);
						Ext.ComponentQuery.query('#revisedrejectBtn')[0].setVisible(false);
						Ext.ComponentQuery.query('#revisedprintBtn')[0].setVisible(false);
						//Ext.ComponentQuery.query('#revisedsubmitBtn')[0].setVisible(false);
					}else if(rec.data.status == 'Processed'){
						//Ext.ComponentQuery.query('#revisedacceptBtn')[0].setVisible(false);
						//Ext.ComponentQuery.query('#revisedsubmitBtn')[0].setDisabled(true);
						Ext.ComponentQuery.query('#revisedrejectBtn')[0].setVisible(false);
						Ext.ComponentQuery.query('#revisedprintBtn')[0].setVisible(false);
					}else if(rec.data.status == 'Approved'){
						
						Ext.ComponentQuery.query('#revisedacceptBtn')[0].setVisible(false);
						Ext.ComponentQuery.query('#revisedrejectBtn')[0].setVisible(false);
						Ext.ComponentQuery.query('#revisedsubmitBtn')[0].setVisible(false);
					}else if(rec.data.status == 'Rejected'){
						Ext.ComponentQuery.query('#revisedacceptBtn')[0].setVisible(false);
						Ext.ComponentQuery.query('#revisedrejectBtn')[0].setVisible(false);
						Ext.ComponentQuery.query('#revisedsubmitBtn')[0].setVisible(false);
						Ext.ComponentQuery.query('#revisedprintBtn')[0].setVisible(false);
					}
				win.show();
				Ext.ComponentQuery.query('#orderrevisionPanel')[0].disable();
				Ext.ComponentQuery.query('#tabbar')[0].disable();
			Ext.ComponentQuery.query('#header')[0].disable();
	},
	
	viewUnprocessedOrder:function(){
		var win = Ext.widget('viewunprocessedwindow');
		var form = win.down('form').getForm();
		var grid = Ext.ComponentQuery.query('orderrevisiongrid')[0];
    	var cust = grid.getSelectionModel();
    	var record;
    	
    	console.log(cust.getSelection());
    	console.log(cust.getSelection().length);
		for(var i=0;i<cust.getSelection().length;i++){
			record=cust.getSelection()[i];

			 Ext.Ajax.request({
					url : 'unProcessedOrder/listAll?unProcessedOrderId='+record.data.refOrderId,
					method : 'GET',
					success : function(response, options) {
						var jsonresp = response.responseText;
						var jsondata = Ext.util.JSON.decode(jsonresp);
						form.loadRecord(record);
							form.findField('orderDetails').setValue(jsondata.data.list[0].orderDetails);
							form.findField('comments').setValue(jsondata.data.list[0].comments);
					},
					failure : function(response, options) {
						Ext.MessageBox.alert('FAILED', 'Error Occured!');
					},
						 });
		}
		
		
					win.show();
	},
});
