Ext.define('TomTail.view.config.orderrevision.OrderrevisionGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.orderrevisiongrid',
	requires:[
	         //'TomTail.view.config.orderrevision.OrderrevisionAddWindow'
	          ],
	border : false,
	title : 'Search Result',
	height : 288,
	width : '100%',
	 bind: {
        store: '{orderrevision}'
    },
	id:'orderrevisiongrid',
	viewConfig: {
		style: { overflow: 'auto', overflowX: 'hidden' },
        getRowClass: function(record, index) {
            var status = record.get('status');
            if(status == 'Approved'){
            	return 'processed';
            }else if(status == 'Rejected'){
            	return 'rejected'
            }
        },
        loadMask:false
    },
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			autoScroll : true,
			
			columns: [{
		        text: 'Id',
		        dataIndex: 'id',
		        sortable: false,
		        menuDisabled:true,
		    }, {
		        text: 'Name',
		        dataIndex: 'name',
		        sortable: false,
		        flex: 1,
		        menuDisabled:true,

		    }, {
		        text: 'Status',
		        dataIndex: 'status',
		        sortable: false,
		        flex: 1,
		        menuDisabled:true,
		    }, {
		        text: 'Source Type',
		        dataIndex: 'sourceType',
		        sortable: false,
		        flex: 1,
				//renderer:Ext.util.Format.dateRenderer('m/d/Y g:i A')
		        menuDisabled:true,
		    }],
		    cls:'paging',
			dockedItems : [{
				xtype : 'pagingtoolbar',
				dock : 'bottom',
				bind: {
        store: '{orderrevision}'
    },
				displayInfo : true,
				displayMsg : 'Displaying Orders {0} - {1} of {2}'
			},/* {
				xtype : 'toolbar',
				dock : 'top',
				items : [ {
					text : 'Add New',
					cls : 'x-btn-text-icon',
					handler : function() {
						//Ext.widget('orderrevision').show();
						Ext.ComponentQuery.query('#orderrevisionPanel')[0].disable();
						Ext.ComponentQuery.query('#tabbar')[0].disable(); 
						Ext.ComponentQuery.query('#header')[0].disable(); 
					}
				} ]
			}*/ ]
		});
		this.callParent(arguments);
	}
});