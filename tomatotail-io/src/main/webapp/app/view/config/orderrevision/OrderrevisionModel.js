Ext.define('TomTail.view.config.orderrevision.OrderrevisionModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.orderrevision',
    data: {
        name: 'TomTail'
    },
    
    stores: {
    orderrevision:{
		   storeId:'OrdersrevisionStore',
		   model:'TomTail.model.ModelProcessedOrderDetails',
		    autoLoad:true,
			pageSize:13,
 	   proxy : {
 		   type : 'ajax',
 		   url : 'order/listAll',
			reader : {
				type : 'json',
				rootProperty:'data.list',
				totalProperty:'data.total'
			},
			 listeners: {
             exception: function(proxy, response, operation, eOpts) {
                 alert('Records not found!');
             }
         }
 	   },
 	  
    },
    }

});
