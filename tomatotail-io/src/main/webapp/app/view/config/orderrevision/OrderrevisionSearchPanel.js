Ext.define('TomTail.view.config.orderrevision.OrderrevisionSearchPanel', {
	extend : 'Ext.form.Panel',
	alias : 'widget.orderrevisionsearchpanel',
	requires:[
'Ext.form.field.Hidden'

	          ],
	title : 'Search Orderrevision ',

	layout : 'vbox',
	height:'50px',
	//cls:'bg-logo',
	reference:'orderrevisionsearchform',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			padding:7,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [{
				xtype:'fieldset',
				layout:'hbox',
				border:0,
				margin:0,
				padding:0,
				items:[ {
					xtype:'datefield',
					margin : '10px',
					labelWidth:'n',
			        fieldLabel: 'Date',
			        name: 'date',
					reference:'processedDate',
					format:'Y-m-d TH:i:s.u',
					altFormats: 'Y-m-d TH:i:s.u',
			    }, {
					xtype:'combobox',
					margin : '10px',
					editable:false,
					allowBlank:false,
			        fieldLabel: 'Order Status',
					labelWidth:'n',
			        name: 'status',
					reference:'processorderStatus',
					emptyText: 'Please select...',
					value:'',
									//forceSelection: true,
			                       store:new Ext.data.Store({
			                     		 // storeId:'',
			                             fields: ['status'],
			                                data: [{
			                                    "status": "Processing",
			                                },{
												"status": "Processed",
											},{
												"status": "Revised",
											}],

			                           }),
			                        queryMode: 'local',
			                        displayField: 'status',
			                        valueField: 'status',
			    },{
					xtype:'combobox',
					margin : '10px',
					editable:false,
					allowBlank:false,
			        fieldLabel: 'Name',
					labelWidth:'n',
			        name: 'name',
					reference:'name',
					emptyText: 'Please select...',
					value:'',
									//forceSelection: true,
			                       store:new Ext.data.Store({
			                     		fields: [{
			            name: 'id'
			        },{
			            name: 'name'
			        }, {
			            name: 'address',
						mapping:'address[0]',
			        },{
			            name: 'city',
						mapping:'address[0].city',
			        },{
			            name: 'state'
			        },{
			            name: 'pincode'
			        } ,{
			            name: 'mobile'
			        },{
			            name: 'comments'
			        },{
						name:'isActive',
					},],
			                               proxy: {
			                            type: 'ajax',
			                            url: "customer/listAll",
			                            reader: {
			                                type: 'json',
			                                rootProperty: 'data.list',
			                            },

			                        },

			                           }),
			                        queryMode: 'remote',
			                        displayField: 'name',
			                        valueField: 'name',
			    },{
					xtype:'combobox',
					margin : '10px',
					editable:false,
					allowBlank:false,
			        fieldLabel: 'SourceType',
					labelWidth:'n',
			        name: 'name',
					reference:'sourceType',
					emptyText: 'Please select...',
									//forceSelection: true,
			                        store:new Ext.data.Store({
			                     		 // storeId:'',
			                             fields: ['sourceType'],
			                                data: [{
			                                    "sourceType": "Internal",
			                                },{
												"sourceType": "External",
											}],

			                           }),
			                        queryMode: 'remote',
			                        displayField: 'sourceType',
			                        valueField: 'sourceType',
			    }]
			},{
				xtype:'fieldset',
				layout:'hbox',
				border:0,
				margin:0,
				//padding:0,
				style:'padding-left: 19.9%;',
				items:[{
				xtype : 'button',
				text : 'Clear',
				width : 70,
				margin : '10px',
				action:'clearOrderrevision',
			},{
				xtype : 'button',
				text : 'Search',
				width : 70,
				margin : '10px',
				action:'searchOrderrevision',
			}]
			}]
		});
		this.callParent(arguments);
	}
});