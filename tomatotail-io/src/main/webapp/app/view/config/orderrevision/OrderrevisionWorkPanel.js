
Ext.define("TomTail.view.config.orderrevision.OrderrevisionWorkPanel",{
    extend: "Ext.panel.Panel",
    alias:'widget.orderrevisionworkpanel',
    requires: [
        "TomTail.view.config.orderrevision.OrderrevisionController",
        "TomTail.view.config.orderrevision.OrderrevisionModel",
        'TomTail.view.config.orderrevision.OrderrevisionGrid',
        'TomTail.view.config.orderrevision.OrderrevisionSearchPanel',
        'TomTail.view.config.orderrevision.OrdersRevisedWindow',
        'TomTail.view.config.orderrevision.ViewUnprocessedWindow'
    ],

    controller: "orderrevision",
    viewModel: {
        type: "orderrevision"
    },

    bodyStyle:'background:url(../../../../images/bg_page.png) !important',
    layout: {
        type: 'vbox',
        align: 'center'
    },
    items:[{
        xtype: 'orderrevisionsearchpanel',
        height: 142,
        width:'100%'
        	 },{
        xtype: 'orderrevisiongrid',
       // collapsible:true,
        flex:2,
        width:'100%'
    }],
});
