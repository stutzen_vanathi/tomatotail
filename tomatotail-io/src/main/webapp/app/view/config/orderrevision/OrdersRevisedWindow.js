Ext.define('TomTail.view.config.orderrevision.OrdersRevisedWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.ordersrevisedwindow',
    requires: [
               'TomTail.view.config.orderrevision.RevisedProductItemGrid',
               ],
    height: 560,
    width: 911,
    title: 'Order Revision',
    bodyStyle: 'padding:10px;',
    
    controller: "orderrevision",
    viewModel: {
        type: "orderrevision"
    },
    itemId:'orderrevisionWin',
    items: [{
        xtype: 'form',
        height: 470,
        width: 900,

        layout: {
            type: 'table',
            columns: 3,
            tdAttrs: {
                style: 'padding:10px; vertical-align: top;'
            }
        },
        items: [{
        	xtype:'hidden',
        	name:'sourceType'
        },{
            xtype: 'textfield',
            name: 'id',
            fieldLabel: 'Order Id',
            readOnly:true,
        },{
            xtype: 'textfield',
            name: 'city',
            readOnly:true,
            fieldLabel: 'City',
        }, {
            xtype: 'combobox',
            editable: false,
            allowBlank:false,
            colspan:1,
            itemId:'revisedOutletCombo',
            fieldLabel: 'Outlet',
            style: 'margin-left:5px;',
            name: 'outletId',
            emptyText: 'Please select...',
            //forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'',
                fields: [ 'name','partnerId','city'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "channelPartner/listAll",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'partnerId',
            listeners:{
         	   select: function(combo, selection) {
         		  var partnerProd = Ext.ComponentQuery.query('#outletProduct')[0];
         		 partnerProd.clearValue();
         		partnerProd.store.proxy.url="channelPartner/skuList?partnerId="+combo.getValue();
         		partnerProd.store.load();
         		
                    }
                }
            
        
       
        },{
        	xtype:'button',
        	text:'View Ordererd Items',
        	//action:'viewunprocessed',
        	itemId:'viewunprocessed',
        	colspan:3,
        	style:'float:right;'
        },{
            xtype: 'revisedproductitemgrid',
            colspan:3,
            title: '',
            height:362,
        },{
            xtype: 'hidden',
            
            name: 'refOrderId',
           
        },  {
                    xtype: 'hidden',
                    name: 'name',
                },  {
                    xtype: 'textfield',
                    name: 'mobile',
                    width: 277,
					
                    style: 'margin-bottom:7px;margin-left:5px;',
                    fieldLabel: '',
					hidden:true,
                    labelSeparator: '',
                },  {
                    xtype: 'datefield',
                    name: 'date',
					width:277,
					value:new Date(),
                    style: 'margin-bottom:7px;margin-left:5px;',
                    itemId: 'date',
					hidden:true,
                   format: 'Y-m-d H:i:s',
                }, {
                    xtype: 'hidden',
					name:'customerId',
                },  {
                    xtype: 'hidden',
                    name: 'addressId',
                }, {
                    xtype: 'textareafield',
                    style: 'margin-bottom:7px;margin-left:5px;',
                    name: 'address',
					width:277,
					hidden:true,
                },  {
                    xtype: 'numberfield',
                    name: 'noOfItems',
					width:277,
					hidden:true,
					
                    style: 'margin-bottom:7px;margin-left:5px;',
                    minValue: 1,
                },  
                  
                {
                    xtype: 'textareafield',
                    name: 'comments',
                    colspan: 2,
					hidden:true,
                    style: 'margin-bottom:7px;margin-left:5px;',
                    width: 277
                }, {
                    xtype: 'checkbox',
                    fieldLabel: 'Enable',
                    name: 'isActive',
                    checked: true,
					hidden:true,
                    inputValue: 1,
                    uncheckedValue: 0
                }],
    }],

    buttons: [{
    	xtype: 'displayfield',
        fieldLabel: 'Unique Items',
		labelWidth:'n',
		itemId:'revisednoOfItems',
		fieldStyle:'font-weight:bold;font-size:20px;color: #666;',
        labelStyle: 'font-size:18px;',
        name: 'revisednoOfItems',
    },'||',{
    	xtype: 'displayfield',
        fieldLabel: 'Total Items',
		labelWidth:'n',
		itemId:'revisedtotalItems',
		fieldStyle:'font-weight:bold;font-size:20px;color: #666;',
        labelStyle: 'font-size:18px;',
        name: 'revisedtotalItems',
    },'||',{
    	xtype: 'displayfield',
        fieldLabel: 'Total: <span style="color:rgb(238, 47, 37);">&#8377;</span>',
		labelWidth:'n',
		itemId:'revisedtotal',
        labelSeparator: '',
		fieldStyle:'font-weight:bold;font-size:20px;color:rgb(238, 47, 37);',
        labelStyle: 'font-weight:bold;font-size:20px;',
        readOnly: true,
        name: 'totalPrice',
    },'->',{
    	text: 'Accept',
		itemId:'revisedacceptBtn',
		handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            console.log("form" + form);
            if (form.isValid()) {

               /* var url = 'order/update';
                if (form.getForm().findField('id').getValue() == null || form.getForm().findField('id').getValue() == '') {
                    url = 'order/save';
                }*/
                console.log("url" + form.getForm().findField('isActive').getValue());
                var store_grid = Ext.ComponentQuery.query('revisedproductitemgrid')[0].store;
                console.log("store_grid" + store_grid);
                var selectionne = store_grid.getRange();
                console.log("selectionne" + selectionne);
                var data_grid = new Array();
                Ext.each(selectionne, function(record) {
                    console.log("record" + record);
                    var idval = "" + record.data.id;
                    var altval = record.data.availability;
                    if (altval == 3){
                    	altval = 2;
                    }else{
                    	altval = record.data.availability;
                    }
                    
                    var parentsku = record.data.parentSku;
                    if(parentsku == ""){
                    	parentsku = record.data.sku
                    }else{
                    	parentsku = record.data.parentSku
                    }
                    
                    if (record.data.sku != null && record.data.productName != null) {
                        if (idval.indexOf("-") > -1) {
                            idval = 0;
                        } else {
                            idval = record.data.id;
                        }
                        data_grid.push({
                        	id:idval,
                            sku: record.data.sku,
                            availability:altval,
                            productName: record.data.productName,
                            requiredQty: record.data.requiredQty,
                            availableQty:record.data.availableQty,
                            mrpPrice: record.data.mrpPrice,
                            requiredPrice: record.data.requiredPrice,
                            currentPrice: record.data.currentPrice,
                            isActive: 1,
                            parentSku: parentsku,
                            comments: record.data.comments,
                        });
                    }
                });
                console.log("data_grid" + data_grid);
              /*   var gridData = Ext.encode(data_grid);
                console.log("gridData" + gridData);
                form.getForm().findField('orderDetails').setValue(gridData);

                var formData = Ext.encode(form.getValues());
                console.log("formData" + formData); */
                var orderid = form.getForm().findField('id').getValue();
                if (store_grid.getCount() > 0) {
                    Ext.Ajax.request({
                        url: 'order/updateStatus',
                        method: 'POST',
                        jsonData: {
							"id":form.getForm().findField('id').getValue(),
                            "date": Ext.Date.format(new Date(form.getForm().findField('date').getValue()), 'Y-m-d H:i:s'),
                            "sourceType":form.getForm().findField('sourceType').getValue(),
                            "refOrderId": form.getForm().findField('refOrderId').getValue(),
                            "customerId": form.getForm().findField('customerId').getValue(),
                            "name": form.getForm().findField('name').getValue(),
                            "mobile": form.getForm().findField('mobile').getValue(),
                            "outletId": form.getForm().findField('outletId').getValue(),
                            "addressId": form.getForm().findField('addressId').getValue(),
                            "noOfItems": form.getForm().findField('noOfItems').getValue(),
                            "isActive": form.getForm().findField('isActive').getValue(),
                            "comments": form.getForm().findField('comments').getValue(),
                            "orderDetails": data_grid
                        },
                        params: {
                        	"orderId":orderid,
                            "statusMode": 1
                        },
                        success: function(result, request) {
                        	var jsondata = Ext.util.JSON.decode(result.responseText);
                        	Ext.MessageBox.show({
                                title: 'Success',
                                msg: jsondata.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            })
                            Ext.ComponentQuery.query('#orderrevisionPanel')[0].enable();
                			Ext.ComponentQuery.query('#tabbar')[0].enable();
                		Ext.ComponentQuery.query('#header')[0].enable();
                            if (win) {
                                win.close();
								Ext.ComponentQuery.query('orderrevisiongrid')[0].store.load();
                            }

                        },
                        failure: function(response) {
                        	var jsondata = Ext.util.JSON.decode(result.responseText);
                            Ext.MessageBox.show({
                                title: 'Failure',
                                msg: jsondata.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            })
                        }
                    });

                } else {
                    Ext.MessageBox.show({
                        title: 'Failure',
                        msg: 'Please add the products',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    })
                }
            }
        }
    },{
    	text:'Reject',
    	itemId:'revisedrejectBtn',
    	handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            console.log("form" + form);
            if (form.isValid()) {

               /* var url = 'order/update';
                if (form.getForm().findField('id').getValue() == null || form.getForm().findField('id').getValue() == '') {
                    url = 'order/save';
                }*/
                console.log("url" + form.getForm().findField('isActive').getValue());
                var store_grid = Ext.ComponentQuery.query('revisedproductitemgrid')[0].store;
                console.log("store_grid" + store_grid);
                var selectionne = store_grid.getRange();
                console.log("selectionne" + selectionne);
                var data_grid = new Array();
                Ext.each(selectionne, function(record) {
                    console.log("record" + record);
                    var idval = "" + record.data.id;
                    var altval = record.data.availability;
                    if (altval == 3){
                    	altval = 2;
                    }else{
                    	altval = record.data.availability;
                    }
                    
                    var parentsku = record.data.parentSku;
                    if(parentsku == ""){
                    	parentsku = record.data.sku
                    }else{
                    	parentsku = record.data.parentSku
                    }
                    
                    if (record.data.sku != null && record.data.productName != null) {
                        if (idval.indexOf("-") > -1) {
                            idval = 0;
                        } else {
                            idval = record.data.id;
                        }
                        data_grid.push({
                        	id:idval,
                            sku: record.data.sku,
                            availability:altval,
                            productName: record.data.productName,
                            requiredQty: record.data.requiredQty,
                            availableQty:record.data.availableQty,
                            mrpPrice: record.data.mrpPrice,
                            requiredPrice: record.data.requiredPrice,
                            currentPrice: record.data.currentPrice,
                            isActive: 1,
                            parentSku: parentsku,
                            comments: record.data.comments,
                        });
                    }
                });
                console.log("data_grid" + data_grid);
              /*   var gridData = Ext.encode(data_grid);
                console.log("gridData" + gridData);
                form.getForm().findField('orderDetails').setValue(gridData);

                var formData = Ext.encode(form.getValues());
                console.log("formData" + formData); */
                var orderid = form.getForm().findField('id').getValue();
                if (store_grid.getCount() > 0) {
                    Ext.Ajax.request({
                        url: 'order/updateStatus',
                        method: 'POST',
                        jsonData: {
							"id":form.getForm().findField('id').getValue(),
                            "date": Ext.Date.format(new Date(form.getForm().findField('date').getValue()), 'Y-m-d H:i:s'),
                            "sourceType":form.getForm().findField('sourceType').getValue(),
                            "refOrderId": form.getForm().findField('refOrderId').getValue(),
                            "customerId": form.getForm().findField('customerId').getValue(),
                            "name": form.getForm().findField('name').getValue(),
                            "mobile": form.getForm().findField('mobile').getValue(),
                            "outletId": form.getForm().findField('outletId').getValue(),
                            "addressId": form.getForm().findField('addressId').getValue(),
                            "noOfItems": form.getForm().findField('noOfItems').getValue(),
                            "isActive": form.getForm().findField('isActive').getValue(),
                            "comments": form.getForm().findField('comments').getValue(),
                            "orderDetails": data_grid
                        },
                        params: {
                        	"orderId":orderid,
                            "statusMode": 2
                        },

                        success: function(result, request) {
                        	var jsondata = Ext.util.JSON.decode(result.responseText);
                        	Ext.MessageBox.show({
                                title: 'Success',
                                msg: jsondata.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            })
                            Ext.ComponentQuery.query('#orderrevisionPanel')[0].enable();
                			Ext.ComponentQuery.query('#tabbar')[0].enable();
                		Ext.ComponentQuery.query('#header')[0].enable();
                            if (win) {
                                win.close();
								Ext.ComponentQuery.query('orderrevisiongrid')[0].store.load();
                            }

                        },
                        failure: function(response) {
                        	var jsondata = Ext.util.JSON.decode(result.responseText);
                            Ext.MessageBox.show({
                                title: 'Failure',
                                msg: jsondata.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            })
                        }
                    });

                } else {
                    Ext.MessageBox.show({
                        title: 'Failure',
                        msg: 'Please add the products',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    })
                }
            }
        }
    },{
    	text:'Print',
    	itemId:'revisedprintBtn',
    },{
        text: 'Place Order',
		itemId:'revisedsubmitBtn',
        handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            console.log("form" + form);
            if (form.isValid()) {

               /* var url = 'order/update';
                if (form.getForm().findField('id').getValue() == null || form.getForm().findField('id').getValue() == '') {
                    url = 'order/save';
                }*/

                var store_grid = Ext.ComponentQuery.query('revisedproductitemgrid')[0].store;
                var selectionne = store_grid.getRange();
                var data_grid = new Array();
                Ext.each(selectionne, function(record) {
                    console.log("record" + record);
                    var idval = "" + record.data.id;
                    var altval = record.data.availability;
                    if (altval == 3){
                    	altval = 2;
                    }else{
                    	altval = record.data.availability;
                    }
                    
                    var parentsku = record.data.parentSku;
                    if(parentsku == ""){
                    	parentsku = record.data.sku
                    }else{
                    	parentsku = record.data.parentSku
                    }
                    if (record.data.sku != null && record.data.productName != null) {
                        if (idval.indexOf("-") > -1) {
                            idval = 0;
                        } else {
                            idval = record.data.id;
                        }
                        data_grid.push({
                        	id:idval,
                            sku: record.data.sku,
                            availability:altval,
                            productName: record.data.productName,
                            requiredQty: record.data.requiredQty,
                            availableQty:record.data.availableQty,
                            mrpPrice: record.data.mrpPrice,
                            requiredPrice: record.data.requiredPrice,
                            currentPrice: record.data.currentPrice,
                            isActive: 1,
                            parentSku: parentsku,
                            comments: record.data.comments,
                        });
                    }
                });
                console.log("data_grid" + data_grid);
              /*   var gridData = Ext.encode(data_grid);
                console.log("gridData" + gridData);
                form.getForm().findField('orderDetails').setValue(gridData);

                var formData = Ext.encode(form.getValues());
                console.log("formData" + formData); */
                if (store_grid.getCount() > 0) {
                    Ext.Ajax.request({
                        url: 'order/updateOrder',
                        method: 'POST',
                        jsonData: {
							"id":form.getForm().findField('id').getValue(),
                            "date": Ext.Date.format(new Date(form.getForm().findField('date').getValue()), 'Y-m-d H:i:s'),
                            "sourceType":form.getForm().findField('sourceType').getValue(),
                            "refOrderId": form.getForm().findField('refOrderId').getValue(),
                            "customerId": form.getForm().findField('customerId').getValue(),
                            "name": form.getForm().findField('name').getValue(),
                            "mobile": form.getForm().findField('mobile').getValue(),
                            "outletId": form.getForm().findField('outletId').getValue(),
                            "addressId": form.getForm().findField('addressId').getValue(),
                            "noOfItems": Ext.ComponentQuery.query('#revisedtotalItems')[0].getValue(),
                            "isActive": 1,//form.getForm().findField('isActive').getValue(),
                            "comments": form.getForm().findField('comments').getValue(),
                            "orderDetails": data_grid
                        },
                        
                        params: {
                            "statusMode": 3
                        },

                        success: function(result, request) {
                        	var jsondata = Ext.util.JSON.decode(result.responseText);
                        	Ext.MessageBox.show({
                                title: 'Success',
                                msg: jsondata.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            })
                            Ext.ComponentQuery.query('#orderrevisionPanel')[0].enable();
                			Ext.ComponentQuery.query('#tabbar')[0].enable();
                		Ext.ComponentQuery.query('#header')[0].enable();
                           
                                win.destroy();
								Ext.ComponentQuery.query('orderrevisiongrid')[0].store.load();
                            

                        },
                        failure: function(response) {
                        	var jsondata = Ext.util.JSON.decode(result.responseText);
                            Ext.MessageBox.show({
                                title: 'Failure',
                                msg: jsondata.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            })
                        }
                    });

                } else {
                    Ext.MessageBox.show({
                        title: 'Failure',
                        msg: 'Please add the products',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    })
                }
            }
        }
    }],
    
    listeners:{
		close:function(){
			
			Ext.ComponentQuery.query('#orderrevisionPanel')[0].enable();
			Ext.ComponentQuery.query('#tabbar')[0].enable();
		Ext.ComponentQuery.query('#header')[0].enable();
		},
		beforeclose:function(){
			Ext.ComponentQuery.query('#revisionGrid')[0].destroy();
			console.log(Ext.ComponentQuery.query('#revisionGrid')[0])
		}
	},
	
});