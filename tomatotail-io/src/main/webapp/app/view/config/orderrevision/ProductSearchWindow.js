Ext.define('TomTail.view.config.orderrevision.ProductSearchWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.productsearchfield',
	width: 550,
	layout:'vbox',
	modal:true,
	title: 'Product Search',
	bodyStyle:'padding:10px;',
	items: [{
		xtype:'displayfield',
		name:'parentSku',
		fieldLabel:'Parent Sku',
		itemId:'ParentSku',
	},{
		xtype:'hidden',
		itemId:'altpartnerId',
	},{
        xtype: 'combobox',
        //editable:false,
        width: '80%',
       // allowBlank: false,
        itemId:'altproductSearch',
        autoSelect: true,
        listConfig: {
            loadingText: 'Searching...',
            emptyText: 'No matching posts found.',

            // Custom rendering template for each item
            getInnerTpl: function() {
                return '<div class="search-item" style="border-bottom:1px solid;">' +
                    '<span><strong>Product Name : {name}</strong> || <strong>SKU:</strong> {sku}<br/><strong>MRP:</strong> {mrp_Price}, <strong>TTP:</strong> {sellingprice}</span>' +
                    '</div>';
            }
        },
        //pageSize: 5,
        //typeAhead: true,
        name: 'productSrch',
        emptyText: 'Enter product name or sku...',
        //forceSelection: true,
        selectOnFocus: true,
        store: new Ext.data.Store({
            // storeId:'',
        	fields: [{
                name: 'productid'
            },{
                name: 'name'
            },{
                name: 'id',
    			mapping:'priceMapping.id',
            }, {
                name: 'sellingprice',
    			mapping:'priceMapping.sellingprice',
            },{
                name: 'purchaseprice',
                mapping:'priceMapping.purchaseprice',
            },{
                name: 'mrp_Price',
                mapping:'priceMapping.mrp_Price',
            }],
            //pageSize: 5,
            proxy: {
                type: 'ajax',
                url: "channelPartner/skuList?partnerId=",
                reader: {
                    type: 'json',
                    rootProperty: 'data.list',
                    totalProperty:'data.total'
                },

            },
        }),
        queryMode: 'remote',
        displayField: 'sku',
        valueField: 'sku',
        listeners: {
           // buffer: 50,
            change: function() {
                var store = this.store;
                //store.suspendEvents();
                store.clearFilter();
                //store.resumeEvents();
                store.filter({
                    property: 'sku',
                    anyMatch: true,
                    value: this.getValue()
                });
            },
            select: function(combo, selection) {
                var post = selection[0];
                if (post) {
					var win = Ext.widget('productcartwin');
					var formvalue=win.down('form').getForm();
					formvalue.loadRecord(post);
					formvalue.findField('name').setValue(post.get('name'));
					formvalue.findField('sku').setValue(post.get('sku'));
					formvalue.findField('mrp_price').setValue(post.get('mrp_Price'));
					formvalue.findField('price').setValue(post.get('sellingprice'));
					win.show();
					Ext.ComponentQuery.query('#prntsku')[0].setVisible(true);
					Ext.ComponentQuery.query('#prntsku')[0].setValue(Ext.ComponentQuery.query('#ParentSku')[0].getValue());
				}
				combo.reset();
				this.up('window').close();
            }
        }

    }]
});