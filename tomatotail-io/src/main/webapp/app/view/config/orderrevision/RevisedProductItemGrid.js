Ext.define('TomTail.view.config.orderrevision.RevisedProductItemGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.revisedproductitemgrid',
    border: true,
	frame:true,
    height: 230,
    requires:['TomTail.view.config.orderrevision.ProductSearchWindow'],
	autoScroll:true,
    width: '100%',
    itemId:'revisionGrid',
    store: Ext.create('Ext.data.Store', {
        storeId: 'revisedItemStore',
        fields: [{
            name: 'id',

        }, {
            name: 'productName',
        }, {
            name: 'sku',
        },{
            name: 'parentSku',
        }, {
            name: 'mrpPrice',
        },{
            name: 'requiredPrice',
        }, {
            name: 'currentPrice',
        }, {
            name: 'requiredQty',
        },  {
            name: 'total',
            type: 'int',
            convert: function(val, row) {
                return row.data.currentPrice * row.data.requiredQty;
            }
        }],

    }),
    columns: [{
    	hidden:true,
    	dataIndex:'id',
    },{
        text: 'SKU',
        dataIndex: 'sku',
        menuDisabled:true,
        sortable: false,
		editor: {
            xtype: 'combobox',
            editable:false,
            allowBlank: false,
            typeAhead: true,
            name: 'name',
            emptyText: 'Please select...',
            value: '',
            //forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'',
                fields: ['name', 'sku'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: 'product/listAll',
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'sku',
            valueField: 'sku',
            listeners: {
                buffer: 50,
                change: function() {
                    var store = this.store;
                    //store.suspendEvents();
                    store.clearFilter();
                    //store.resumeEvents();
                    store.filter({
                        property: 'sku',
                        anyMatch: true,
                        value: this.getValue()
                    });
                }
            }

        },
		
    }, {
    	text:'Parent SKU',
    	dataIndex:'parentSku',
    	hidden:true,
    	
    },{
        text: 'Product',
        dataIndex: 'productName',
        sortable: false,
        menuDisabled:true,
		editor: {
            xtype: 'combobox',
            editable:false,
            allowBlank: false,
            typeAhead: true,
            name: 'name',
            emptyText: 'Please select...',
            value: '',
            //forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'',
                fields: ['name', 'sku'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: 'product/listAll',
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'name',
            listeners: {
                buffer: 50,
                change: function() {
                    var store = this.store;
                    //store.suspendEvents();
                    store.clearFilter();
                    //store.resumeEvents();
                    store.filter({
                        property: 'name',
                        anyMatch: true,
                        value: this.getValue()
                    });
                }
            }

        },
        flex: 1,
		//renderer:Ext.util.Format.dateRenderer('m/d/Y g:i A')
    },{
    	text:'Availability',
    	dataIndex:'availability',
    	menuDisabled:true,
    	editor: {
            xtype: 'combobox',
            //editable:false,
            name: 'name',
            //hideActive: true,
            emptyText: 'Please select...',
            //forceSelection: true,
            selectOnFocus:true,
            itemId:'availCombo',
            queryMode: 'remote',
            displayField: 'availabilityType',
            valueField: 'availabilityId',
            store: new Ext.data.Store({
                 storeId:'availabilityStore',
            	fields: ['availabilityId','availabilityType'],
            	autoLoad: true,
                data: [{
                    "availabilityType": "N/A",
                    "availabilityId": 0,
                },{
					"availabilityType": "Available",
					 "availabilityId": 1,
				},{
					"availabilityType": "Alt",
					 "availabilityId": 2,
				}],
            }),
            listeners : {
                select : function(combo){
                	var val = combo.getValue();
                	if(val == 2){
                		Ext.widget('productsearchfield').show();
                 		var altProduct = Ext.ComponentQuery.query('#altproductSearch')[0];
                		altProduct.clearValue();
                		altProduct.store.proxy.url="channelPartner/skuList?partnerId="+Ext.ComponentQuery.query('#revisedOutletCombo')[0].getValue();
                		altProduct.store.load();
                	}
                },
                change:function(){
                	Ext.ComponentQuery.query('#revisedacceptBtn')[0].setDisabled(true);
                }
            }
            
            },
            renderer:function(val,me,recs,record){
            	if(val=="0"){
            		return "N/A"
            	}else if(val == 1){
            		return "Available"
            	}else if(val == 2){
            		return "Alt"
            	}
                
            },
            
           

    }, {
        text: 'Required Qty',
        dataIndex: 'requiredQty',
        menuDisabled:true,
        sortable: false,
        flex: 1.3,
		editor:{
			xtype:'numberfield',
			minValue:0,
			 name: 'requiredQty',
			 listeners:{
				 change:function(){
					 Ext.ComponentQuery.query('#revisedacceptBtn')[0].setDisabled(true);
				 }
			 }
		}
    }, {
        text: 'Available Qty',
        menuDisabled:true,
        dataIndex: 'availableQty',
        sortable: false,
        flex: 1.3,
		editor:{
			xtype:'numberfield',
			minValue:0,
			 name: 'availableQty',
			 listeners:{
				 change:function(){
					 Ext.ComponentQuery.query('#revisedacceptBtn')[0].setDisabled(true);
				 }
			 }
		}
    }, {
        text: 'MRP',
        menuDisabled:true,
        dataIndex: 'mrpPrice',
        sortable: false,
        flex: 1,
		editor:{
			xtype:'numberfield',
			minValue:0,
			 name: 'mrpPrice',
			 listeners:{
				 change:function(){
					 Ext.ComponentQuery.query('#revisedacceptBtn')[0].setDisabled(true);
				 }
			 }
		}
    },{
        text: 'Required Price',
        dataIndex: 'requiredPrice',
        menuDisabled:true,
        sortable: false,
        flex: 1,
		editor:{
			xtype:'numberfield',
			minValue:0,
			 name: 'requiredPrice',
			 listeners:{
				 change:function(){
					 Ext.ComponentQuery.query('#revisedacceptBtn')[0].setDisabled(true);
				 }
			 }
		}
    },{
        text: 'TTPrice',
        dataIndex: 'currentPrice',
        menuDisabled:true,
        sortable: false,
        flex: 1,
		
		editor:{
			xtype:'numberfield',
			minValue:0,
			 name: 'currentPrice',
			 listeners:{
				 change:function(){
					 Ext.ComponentQuery.query('#revisedacceptBtn')[0].setDisabled(true);
				 }
			 }
		},
		/*renderer: function(value, metaData, record, row, col, store, gridView){
		    console.log(record.get('availability'))
		  }*/
    },{
        text: 'Sub Total',
        dataIndex: 'total',
        sortable: false,
        menuDisabled:true,
    },/* {
        text: 'Sub Total',
        dataIndex: 'total',
		//summaryType:'sum',
		hidden:true,
		itemId:'summary',
		 summaryType:function( records ) {
			 var i = 0,
                length = records.length,
				 sum = 0,
				 shipping=0,
                record;
         for (i; i < length; ++i) {
            var record = records[ i ];
            sum += record.get('currentPrice') * record.get('requiredQty');
			//shipping +=record.get('cost');
         }
         grandtot= Math.floor( sum+shipping );
         return '<strong>'+' Total :<span style="padding-left:62px;"></span>'+ sum +'</strong>';
		 }, 
        flex: 1,
		
    }, */{
        dataIndex: 'isActive',
        hidden: true,
        flex: 1,
		
    },{
        dataIndex: 'comments',
        hidden: true,
        flex: 1,
    },{
        xtype: 'actioncolumn',
        width: 30,
        sortable: false,
        menuDisabled: true,
        itemId:'revisionDeleteBtn',
        items: [{
            icon: 'images/icon16_error.png',
            tooltip: 'Delete Product',
            style: 'cursor:pointer;',
            scope: this,
            handler: function(grid, rowIndex) {
                grid.getStore().removeAt(rowIndex);
                Ext.ComponentQuery.query('#revisedtotal')[0].reset();
                Ext.ComponentQuery.query('#revisedtotalItems')[0].reset();
                Ext.ComponentQuery.query('#revisednoOfItems')[0].reset();
                var store = Ext.getStore('revisedItemStore');
           	 var storecount=store.getCount();
                var sum = 0;
				 var totalItems = 0;
       		 store.each(function(record){
    	            sum += record.get('currentPrice') * record.get('requiredQty');
    	           grandtot= parseFloat(sum).toFixed(2);
    	           
    	           Ext.ComponentQuery.query('#revisedtotal')[0].setValue(grandtot);
    	          totalItems += record.get('requiredQty');
    	         Ext.ComponentQuery.query('#revisedtotalItems')[0].setValue(totalItems);
    	         Ext.ComponentQuery.query('#revisednoOfItems')[0].setValue(storecount);
       		 });
       		Ext.ComponentQuery.query('#revisedacceptBtn')[0].setDisabled(true);
       		
            }
        }],
        renderer: function(val, metadata, record) {
            metadata.style = 'cursor: pointer;';
            return val;
        }
    }],
	/* features : [{
				ftype : 'summary',
			}], */
	selModel: 'cellmodel',
    plugins: {
        ptype: 'cellediting',
        clicksToEdit: 1,
    },
    listeners : {
        edit : function( editor, e, eOpts ){

        	
        	var store = Ext.getStore('revisedItemStore');
       	 var storecount=store.getCount();
       	 var rec = e.record;

            var rowtotal = rec.get('currentPrice') * rec.get('requiredQty');
           
            rec.set('total',rowtotal);
            
				var sum = 0;
				 var totalItems = 0;
				 var altValue = rec.get('availability');

				
				 
				 if(altValue == 2){
					 Ext.ComponentQuery.query('#ParentSku')[0].setValue(rec.get('sku'));
				 }
       		 store.each(function(record){
    	            sum += record.get('total');
    	           grandtot= parseFloat(sum).toFixed(2);
    	           
    	           Ext.ComponentQuery.query('#revisedtotal')[0].setValue(grandtot);
    	          totalItems += record.get('requiredQty');
    	         Ext.ComponentQuery.query('#revisedtotalItems')[0].setValue(totalItems);
       		 });
       		 
       		 if(altValue == 2 || altValue == 0){
				 var totQty =  Ext.ComponentQuery.query('#revisedtotalItems')[0].getValue();
				 Ext.ComponentQuery.query('#revisedtotalItems')[0].setValue(totQty-rec.get('requiredQty'));
				 var tot = Ext.ComponentQuery.query('#revisedtotal')[0].getValue();
				 var gtot =  parseFloat(tot-rec.get('currentPrice')).toFixed(2);
				 Ext.ComponentQuery.query('#revisedtotal')[0].setValue(gtot);
			 }
                    },
                    beforeedit: function(plugin, edit) {
    					var grid = Ext.ComponentQuery.query('orderrevisiongrid')[0];
    			    	var cust = grid.getSelectionModel();
    			    	var record;
    					for(var i=0;i<cust.getSelection().length;i++){
    						record=cust.getSelection()[i];
    		                if (record.data.status=="Approved" || record.data.status=="Rejected") {
    		                    return false;
    		                }
    					}
    					
    	            },
    	            afterrender:function(){
    	            	var grid = Ext.ComponentQuery.query('orderrevisiongrid')[0];
    			    	var cust = grid.getSelectionModel();
    			    	var record;
    					for(var i=0;i<cust.getSelection().length;i++){
    						record=cust.getSelection()[i];
    		                if (record.data.status=="Approved" || record.data.status=="Rejected") {
    		                	Ext.ComponentQuery.query('#gridaddBtn')[0].setVisible(false);
    		                	console.log(Ext.ComponentQuery.query('#gridaddBtn')[0])
    		                	Ext.ComponentQuery.query('#revisionProductAdd')[0].setVisible(false);
    		                	Ext.ComponentQuery.query('#revisionDeleteBtn')[0].setVisible(false);
    		                    return false;
    		                }
    					}
    	            }
                    
        },
  
		dockedItems : [{
				xtype : 'toolbar',
				dock : 'top',
				itemId:'gridaddBtn',
				items : [{
		            xtype: 'combobox',
		            //editable:false,
		            width: '80%',
		            itemId:'outletProduct',
		           // allowBlank: false,
		            autoSelect: true,
		            listConfig: {
		                loadingText: 'Searching...',
		                emptyText: 'No matching products found.',

		                // Custom rendering template for each item
		                getInnerTpl: function() {
		                    return '<div class="search-item" style="border-bottom:1px solid;">' +
		                        '<span><strong>Product Name : {name}</strong> || <strong>SKU:</strong> {sku}<br/><strong>MRP:</strong> {mrp_Price}, <strong>TTP:</strong> {sellingprice}</span>' +
		                        '</div>';
		                }
		            },
		            //pageSize: 5,
		            //typeAhead: true,
		            name: 'productSearch',
		            emptyText: 'Enter a product sku...',
		            //forceSelection: true,
		            selectOnFocus: true,
		            store: new Ext.data.Store({
		                 storeId:'outletStore',
		            	fields: [{
		                    name: 'productid'
		                },{
		                    name: 'name'
		                },{
		                    name: 'id',
		        			mapping:'priceMapping.id',
		                }, {
		                    name: 'sellingprice',
		        			mapping:'priceMapping.sellingprice',
		                },{
		                    name: 'purchaseprice',
		                    mapping:'priceMapping.purchaseprice',
		                },{
		                    name: 'mrp_Price',
		                    mapping:'priceMapping.mrp_Price',
		                }],
		                //pageSize: 5,
		                proxy: {
		                    type: 'ajax',
		                    url: "product/productList?partnerId=",
		                    reader: {
		                        type: 'json',
		                        rootProperty: 'data.list',
		                    },

		                },
		            }),
		            queryMode: 'remote',
		            displayField: 'sku',
		            valueField: 'sku',
		            listeners: {
		               // buffer: 50,
		                change: function() {
		                    var store = this.store;
		                    //store.suspendEvents();
		                    store.clearFilter();
		                    //store.resumeEvents();
		                    store.filter({
		                        property: 'sku',
		                        anyMatch: true,
		                        value: this.getValue()
		                    });
		                },
		                select: function(combo, selection) {
		                    var post = selection[0];
		                    if (post) {
								var win = Ext.widget('productcartwin');
								var formvalue=win.down('form').getForm();
								formvalue.loadRecord(post);
								formvalue.findField('name').setValue(post.get('name'));
								formvalue.findField('sku').setValue(post.get('sku'));
								formvalue.findField('mrp_price').setValue(post.get('mrp_Price'));
								formvalue.findField('price').setValue(post.get('sellingprice'));
								win.show();
								
							}
							combo.reset();
		                },
		           
		            }

		        }, {
					text : 'Add new product',
					//icon:'images/drop-add.png',
					itemId:'revisionProductAdd',
					cls : 'x-btn-text-icon',
					handler : function(selection) {
						var cart = Ext.widget('productcartwin');
						Ext.ComponentQuery.query('#name')[0].setReadOnly(false);
						Ext.ComponentQuery.query('#sku')[0].setReadOnly(false);
						cart.show();
					}
				}]
			}],
	 
});