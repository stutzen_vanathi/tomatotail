Ext.define('TomTail.view.config.orderrevision.ViewUnprocessedWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.viewunprocessedwindow',
	width: 600,
	layout:'vbox',
	modal:true,
	title: 'Unprocessed Info',
	
	bodyStyle:'padding:10px;',
	items: [{
		xtype:'form',
		items:[{
			xtype:'component',
			html:'Product Items :',
			style:'margin-bottom:8px;'
		},{
                xtype: 'textareafield',
                fieldLabel: '',
                labelSeparator:'',
                name: 'orderDetails',
                style:'margin-bottom:8px;',
                itemId: 'orderdetails',
                readOnly:true,
                height: 350,
                width: 570
            },{
    			xtype:'component',
    			html:'Comments :',
    			style:'margin-bottom:8px;'
    		}, {
                xtype: 'textareafield',
                readOnly:true,
                fieldLabel: '',
                labelSeparator:'',
                name: 'comments',
                width: 570
            }]
	}]
});