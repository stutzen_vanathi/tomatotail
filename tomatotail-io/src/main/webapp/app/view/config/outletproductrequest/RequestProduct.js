
Ext.define("TomTail.view.config.outletproductrequest.RequestProduct",{
    extend: "Ext.panel.Panel",
	alias: 'widget.requestproductPanel',
    requires:[
	'TomTail.view.config.outletproductrequest.RequestProductController',
    'TomTail.view.config.outletproductrequest.RequestProductSearch',
    'TomTail.view.config.outletproductrequest.RequestProductGrid',
	'TomTail.view.config.outletproductrequest.RequestProductModel',
	'TomTail.view.config.outletproductrequest.RequestProductWindow'
    ],
    controller: "outletproductrequest-product",
    viewModel: {
        type: "outletproductrequest-product"
    },
    itemId:'RequestProduct',
     layout: {
        type: 'vbox',
        align: 'center'
    },
    items:[{
        xtype: 'requestproductsearch',
        height: 102,
        width:'100%'
        	 },{
        xtype: 'requestproductgrid',
        flex:2,
        width:'100%'
    }],
    initComponent : function() {
    this.callParent(arguments);
    }
});
