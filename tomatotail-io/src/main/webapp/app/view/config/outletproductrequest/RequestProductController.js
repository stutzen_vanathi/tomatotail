Ext.define('TomTail.view.config.outletproductrequest.RequestProductController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.outletproductrequest-product',
    init: function() {
        var me = this;
        this.control({
            'requestproductgrid dataview': {
                itemdblclick: function(view, rec, elm, inx, evt, obj) {
                    me.loadInfo(rec);
                }
            },
            'requestproductsearch #productsearch': { 
    	        specialkey: function(field, e) { 
    	            if(e.getKey() == e.ENTER) { 
    	                      this.searchConfig() 
    	            } 
    	        } 
            },
            'requestproductsearch button[action=searchProduct]':{
                click:'searchConfig'
            },
			'requestproductsearch button[action=ClearProduct]':{
                click:'clearConfig'
            },
            
            'requestproductwindow button[action=saveprodRequest]':{
                click:'saveproductRequest'
            },
        });
    },

    loadInfo: function(rec) {
        var win = Ext.widget('requestproductwindow');
		var formvalue=win.down('form').getForm();
    	    formvalue.loadRecord(rec);
    	    if(rec.data.status != 'Initiated' ){
    	    	Ext.ComponentQuery.query('#requestsavebtn')[0].setVisible(false);
    	    	Ext.ComponentQuery.query('#requestsaveaddnewbtn')[0].setVisible(false);
    	    }
        win.show();
    },
    
    searchConfig:function(){
    	var store=this.getViewModel().getStore('productrequest');
    	var status = this.lookupReference('status').getValue();
    	var productName = this.lookupReference('productName').getValue();
    	if(status== null){
			status=''
		}
    	if(productName== null){
    		productName=''
		}

    	if(status!='' || productName!=''){
    		url="prodRequest/listAll?status="+status+"&prodName="+productName;
    	}else{
    		console.log('else');
    		url="prodRequest/listAll";
    	}
    	console.log('url'+url);
    	store.proxy.url=url;
    	store.load();
    	
    },
	clearConfig:function(){
		this.lookupReference('status').reset();
		this.lookupReference('productName').reset();
		var store=this.getViewModel().getStore('productrequest');
    	store.proxy.url="prodRequest/listAll";
    	store.load();
	},
	
	saveproductRequest:function(){
		 var formPanel = this.lookupReference('requestprodform');
	       form = formPanel.getForm();
	 	  var win=Ext.ComponentQuery.query('#requestproductwin')[0];

		 if (form.isValid()) {
   	       form.submit({
   	    	   		url : 'prodRequest/saveOrUpdate',
   					success: function(result, request) {
                       	var jsonresp = Ext.util.JSON.decode(result.responseText);
                          	Ext.MessageBox.show({
                               title: 'Success',
                               msg: jsonresp.message,
                               buttons: Ext.MessageBox.OK,
                               icon: Ext.MessageBox.INFO
                           })
                           Ext.ComponentQuery.query('#requestproductgrid')[0].store.load();
							 if (win) {
                               win.close();
                               
                           }

                       },
                       failure: function(response) {
                           Ext.MessageBox.show({
                               title: 'Failure',
                               msg: 'Invalid Data',
                               buttons: Ext.MessageBox.OK,
                               icon: Ext.MessageBox.ERROR
                           })
                       }
   				});
   	   }
	}
});
