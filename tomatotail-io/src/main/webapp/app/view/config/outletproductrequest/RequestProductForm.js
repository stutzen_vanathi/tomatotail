Ext.define('TomTail.view.config.outletproductrequest.RequestProductForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.requestproductform',

	controller: "outletproductrequest-product",
    viewModel: {
        type: "outletproductrequest-product"
    },
    
    reference:'requestprodform',
   
        width: 630,
        //cls:'pricegrid',
        bodyPadding: 10,
        defaults: {
            xtype: 'textfield',
            padding: 10
        },
        layout: {
            type: 'table',
            columns: 2,
            tdAttrs: {
                style: 'padding: 0px;'
            }
        },

        items: [{
			xtype:'fieldset',
			 border: false,
			 colspan:2,
	            frame: false,
	            padding: 5.5,
	            layout: {
	                type: 'hbox',
	              
	            },
				items:[{
					xtype:'textfield',
		            fieldLabel: 'Product Name',
		            name: 'productName'
		        },{
				xtype:'textfield',
				tabIndex:1,
				readOnly:true,
				style: 'margin-left:230px;',
				width:80,
	            fieldLabel: 'Id',
				labelWidth:20,
	            name: 'id'
	        }]
			},{
            xtype: 'combobox',
            editable: false,
            allowBlank:false,
            fieldLabel: 'Outlet',
            name: 'partnerId',
            emptyText: 'Please select...',
            //forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'',
                fields: [ 'name','partnerId','city'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "channelPartner/listAll",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'partnerId',
        },{
            xtype:'textfield',
			//hideTrigger:true,
            fieldLabel: 'SKU',
            name: 'sku'
        }, /*{
			xtype:'numberfield',
			hideTrigger:true,
            fieldLabel: 'Weight',
            
            name: 'weight'
        },*/ {
            xtype: 'combobox',
			editable:false,
            fieldLabel: 'Category',
            name: 'categoryId',
            emptyText: 'Please select...',
            forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'manufacturerstore',
                fields: ['categoryid', 'name'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "category/list",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'categoryid',
            //value:1,
        },  {
            xtype: 'combobox',
			editable:false,
            fieldLabel: 'Manufacturer',
            name: 'manuId',
            emptyText: 'Please select...',
            forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'manufacturerstore',
                fields: ['manufacturerid', 'name'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "manufacturer/list",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'manufacturerid',
            //value:1,
        },  {
            xtype: 'combobox',
			editable:false,
            fieldLabel: 'Brand',
            name: 'brandId',
            emptyText: 'Please select...',
            //forceSelection: true,
            store: new Ext.data.Store({
                storeId:'brandnamestore',
                fields: ['brandid', 'name'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "brand/brandList",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'brandid',
            //value:1,
        },{
        	xtype:'hidden',
        	name:'status',
        	value:'Intiated'
        }, {
            xtype:'numberfield',
			hideTrigger:true,
            fieldLabel: 'MRP',
            name: 'mrpPrice'
        }, {
            xtype:'numberfield',
			hideTrigger:true,
            fieldLabel: 'Selling Price',
            
            name: 'sellingPrice'
        }, {
            xtype:'numberfield',
			hideTrigger:true,
            fieldLabel: 'Purchase Price',
            name: 'purchasePrice'
        },   {
            xtype: 'textareafield',
            colspan:2,
            width:595,
            fieldLabel: 'Short Desc',
            name: 'shortDesc'
        },    {
            xtype: 'textareafield',
            colspan:2,
            width:595,
            fieldLabel: 'Long Desc',
            name: 'longDesc'
        }, {
            xtype: 'textareafield',
            colspan:2,
            width:595,
            fieldLabel: 'Comments',
            name: 'comments'
        },{
            xtype: 'checkbox',
            //colspan: 2,
            fieldLabel: 'Is Active',
            name: 'isActive',
            checked: true,
            value: true,
			colspan:2,
            inputValue: 1,
            uncheckedValue: 0
        }],
    });