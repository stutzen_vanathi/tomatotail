Ext.define('TomTail.view.config.outletproductrequest.RequestProductGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.requestproductgrid',
	
	border : false,
	title : 'Product Request',
	height : 288,
	width : '100%',
	header: {
        xtype: 'header',
        titlePosition: 0,
        defaults: {
            margin: '0 10px'
        },
        items: [
           
            {
                xtype: 'button',
                text: "Add New",
                handler: Ext.bind(function() {
                	Ext.widget('requestproductwindow').show();
					Ext.ComponentQuery.query('#RequestProduct')[0].disable();
					Ext.ComponentQuery.query('#tabbar')[0].disable(); 
	Ext.ComponentQuery.query('#header')[0].disable(); 
                }, this)
            }
        ]
    },
    viewConfig: {
		style: { overflow: 'auto', overflowX: 'hidden' },
        getRowClass: function(record, index) {
            var status = record.get('status');
            if(status == 'Rejected'){
            	return 'rejected';
            }
        },
        loadMask:false
    },
	bind: {
        store: '{productrequest}'
    },
    itemId:'requestproductgrid',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			autoScroll : true,
			
			columns : [{
				hidden:true,
				dataIndex : 'id',
			},{
				text : 'Product Id',
				dataIndex : 'id',
				sortable:false,
			}, {
				text : 'Product Name',
				dataIndex : 'productName',
				sortable:false,
				flex : 1
			},{
				text : 'Purchase Price',
				dataIndex : 'purchasePrice',
				sortable:false,
				
				flex : 1
			},{
				text : 'Selling Price',
				dataIndex : 'sellingPrice',
				sortable:false,
				flex : 1,
				
			},{
				text:'Status',
				dataIndex:'status',
				flex:1
			}],
			
    cls:'paging',
			dockedItems : [{
				xtype : 'pagingtoolbar',
				dock : 'bottom',
				bind: {
					store: '{productrequest}'
				},
				displayInfo : true,
				displayMsg : 'Displaying products {0} - {1} of {2}'
			}],
			
		});
		this.callParent(arguments);
	}
});