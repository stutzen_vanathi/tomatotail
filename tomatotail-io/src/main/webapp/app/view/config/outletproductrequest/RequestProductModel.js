Ext.define('TomTail.view.config.outletproductrequest.RequestProductModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.outletproductrequest-product',
	requires: ['Ext.data.TreeStore'],
    data: {
        name: 'TomTail'
    },
	stores:{
		productrequest:{
				storeId: 'requestproductStore',
				fields:[{name:"id"},{name:"productName"},{name:"sku"},{name:"sellingprice"},{name:"purchaseprice"},],
				autoLoad: true,
				pageSize:20,
				proxy: {
            type: 'ajax',
            url: 'prodRequest/listAll',
            reader: {
                rootProperty: 'data.list',
                totalProperty:'data.total',
            },
            listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }

        },
			},
			
	},

});
