Ext.define('TomTail.view.config.outletproductrequest.RequestProductSearch', {
	extend : 'Ext.form.Panel',
	alias : 'widget.requestproductsearch',
	requires:[
'Ext.form.field.Hidden'
	          ],
	title : 'Search Products',

	layout : 'hbox',
	height:'50px',

	reference:'productform',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [{
				xtype:'textfield',
				fieldLabel:'Product Name',
				itemId:'productsearch',
				margin:'10px',
				name:'prodName',
				reference:'productName'
			},{
	            xtype: 'combobox',
	            fieldLabel: 'Status',
	            name: 'status',
	            margin : '10px',
	            reference:'status',
	            emptyText: 'Please select...',
	            //forceSelection: true,
	            store: new Ext.data.Store({
	                // storeId:'',
	                fields: ['status'],
                    data: [{
                        "status": "Initiated",
                    },{
                        "status": "Processing",
                    },{
						"status": "Processed",
					},{
						"status": "Accepted",
					},{
						"status": "Rejected",
					}],
	            }),
	            queryMode: 'local',
	            displayField: 'status',
                valueField: 'status',
	        },{
				xtype : 'button',
				text : 'Clear',
				width : 70,
				margin : '10px',
				action:'ClearProduct'
			},{
				xtype : 'button',
				text : 'Search',
				width : 70,
				margin : '10px',
				action:'searchProduct'
			}]
		});
		this.callParent(arguments);
	}
});