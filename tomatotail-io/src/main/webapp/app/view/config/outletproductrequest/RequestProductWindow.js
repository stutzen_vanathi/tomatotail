Ext.define('TomTail.view.config.outletproductrequest.RequestProductWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.requestproductwindow',

	controller: "outletproductrequest-product",
    viewModel: {
        type: "outletproductrequest-product"
    },
    
    title: 'Request New Product',
    itemId:'requestproductwin',
    
    width: 650,
    autoScroll:true,
    height: 527,
    //cls:'pricegrid',
    items: [{
        xtype: 'form',
        width: 630,
        //cls:'pricegrid',
        bodyPadding: 10,
        defaults: {
            xtype: 'textfield',
            padding: 10
        },
        layout: {
            type: 'table',
            columns: 2,
            tdAttrs: {
                style: 'padding: 0px;'
            }
        },

        items: [{
			xtype:'fieldset',
			 border: false,
			 colspan:2,
	            frame: false,
	            padding: 5.5,
	            layout: {
	                type: 'hbox',
	              
	            },
				items:[{
					xtype:'textfield',
		            fieldLabel: 'Product Name',
		            allowBlank:false,
		            name: 'productName'
		        },{
				xtype:'textfield',
				tabIndex:1,
				readOnly:true,
				style: 'margin-left:230px;',
				width:80,
	            fieldLabel: 'Id',
				labelWidth:20,
	            name: 'id'
	        }]
			},{
            xtype: 'combobox',
            editable: false,
            allowBlank:false,
            fieldLabel: 'Outlet',
            name: 'partnerId',
            emptyText: 'Please select...',
            //forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'',
                fields: [ 'name','partnerId','city'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "channelPartner/listAll",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'partnerId',
        },{
            xtype:'textfield',
			//hideTrigger:true,
            fieldLabel: 'SKU',
            allowBlank:false,
            name: 'sku'
        },  {
            xtype: 'combobox',
			editable:false,
			allowBlank:false,
            fieldLabel: 'Category',
            name: 'categoryId',
            emptyText: 'Please select...',
            forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'manufacturerstore',
                fields: ['categoryid', 'name'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "category/list",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'categoryid',
            //value:1,
        },  {
            xtype: 'combobox',
			editable:false,
			allowBlank:false,
            fieldLabel: 'Manufacturer',
            name: 'manuId',
            emptyText: 'Please select...',
            forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'manufacturerstore',
                fields: ['manufacturerid', 'name'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "manufacturer/list",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'manufacturerid',
            //value:1,
        },  {
            xtype: 'combobox',
			editable:false,
			allowBlank:false,
            fieldLabel: 'Brand',
            name: 'brandId',
            emptyText: 'Please select...',
            //forceSelection: true,
            store: new Ext.data.Store({
                storeId:'brandnamestore',
                fields: ['brandid', 'name'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "brand/brandList",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'brandid',
            //value:1,
        },{
        	xtype:'hidden',
        	name:'status',
        	value:'Initiated'
        }, {
            xtype:'numberfield',
			hideTrigger:true,
            fieldLabel: 'MRP',
            allowBlank:false,
            name: 'mrpPrice'
        }, {
            xtype:'numberfield',
			hideTrigger:true,
            fieldLabel: 'Selling Price',
            allowBlank:false,
            name: 'sellingPrice'
        }, {
            xtype:'numberfield',
			hideTrigger:true,
			allowBlank:false,
            fieldLabel: 'Purchase Price',
            name: 'purchasePrice'
        },   {
            xtype: 'textareafield',
            colspan:2,
            width:595,
            fieldLabel: 'Short Desc',
            name: 'shortDesc'
        },    {
            xtype: 'textareafield',
            colspan:2,
            width:595,
            fieldLabel: 'Long Desc',
            name: 'longDesc'
        }, {
            xtype: 'textareafield',
            colspan:2,
            width:595,
            fieldLabel: 'Comments',
            name: 'comments'
        },{
            xtype: 'checkbox',
            //colspan: 2,
            fieldLabel: 'Is Active',
            name: 'isActive',
            checked: true,
            value: true,
			colspan:2,
            inputValue: 1,
            uncheckedValue: 0
        }],
    }],
    buttons: [{
        text: 'Save',
        itemId:'requestsavebtn',
        handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            console.log("form" + form);
            var i;
            if (form.isValid()) {
                var formData = Ext.encode(form.getValues());
                
                if (form.getForm().findField('id').getValue() == null || form.getForm().findField('id').getValue() == '') {
                	form.getForm().findField('id').setValue(0);
                	
                }

                console.log(formData);
				var form_data = [];
				
				form_data.push({
				    "id":form.getForm().findField('id').getValue(), 
				  "shortDesc":form.getForm().findField('shortDesc').getValue(),
				  "longDesc":form.getForm().findField('longDesc').getValue(),
				  "comments":form.getForm().findField('comments').getValue(),
				  "productName":form.getForm().findField('productName').getValue(),
				  "sku":form.getForm().findField('sku').getValue(),
				  "categoryId":form.getForm().findField('categoryId').getValue(),
				  "brandId":form.getForm().findField('brandId').getValue(),
				  "partnerId":form.getForm().findField('partnerId').getValue(),
				  "isActive":form.getForm().findField('isActive').getValue(),
				  "status":form.getForm().findField('status').getValue(), 
				  "manuId":form.getForm().findField('manuId').getValue(),
				  "mrpPrice":form.getForm().findField('mrpPrice').getValue(),
				  "sellingPrice":form.getForm().findField('sellingPrice').getValue(),
				  "purchasePrice":form.getForm().findField('purchasePrice').getValue()
				});

				 var requestData =  Ext.encode(form_data);
					console.log("requestData"+requestData);
                    Ext.Ajax.request({
                        url: 'prodRequest/saveOrUpdate',
                        method: 'POST',
                        jsonData:requestData,
                        success: function(result, request) {
                        	var jsonresp = Ext.util.JSON.decode(result.responseText);
                           	Ext.MessageBox.show({
                                title: 'Success',
                                msg: jsonresp.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            })
                            Ext.ComponentQuery.query('#requestproductgrid')[0].store.load();
							 if (win) {
                                win.close();
                                
                            }
							 
                        },
                        failure: function(response) {
                            Ext.MessageBox.show({
                                title: 'Failure',
                                msg: 'Invalid Data',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            })
                        }
                    });

                 }
        }
    }, {
        text: 'Save & Add New',
        itemId:'requestsaveaddnewbtn',
        handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            console.log("form" + form);
            var i;
            if (form.isValid()) {
                var formData = Ext.encode(form.getValues());
                
                if (form.getForm().findField('id').getValue() == null || form.getForm().findField('id').getValue() == '') {
                	form.getForm().findField('id').setValue(0);
                	
                }

                console.log(formData);
				var form_data = [];
				
				form_data.push({
				    "id":form.getForm().findField('id').getValue(), 
				  "shortDesc":form.getForm().findField('shortDesc').getValue(),
				  "longDesc":form.getForm().findField('longDesc').getValue(),
				  "comments":form.getForm().findField('comments').getValue(),
				  "productName":form.getForm().findField('productName').getValue(),
				  "sku":form.getForm().findField('sku').getValue(),
				  "categoryId":form.getForm().findField('categoryId').getValue(),
				  "brandId":form.getForm().findField('brandId').getValue(),
				  "partnerId":form.getForm().findField('partnerId').getValue(),
				  "isActive":form.getForm().findField('isActive').getValue(),
				  "status":form.getForm().findField('status').getValue(), 
				  "manuId":form.getForm().findField('manuId').getValue(),
				  "mrpPrice":form.getForm().findField('mrpPrice').getValue(),
				  "sellingPrice":form.getForm().findField('sellingPrice').getValue(),
				  "purchasePrice":form.getForm().findField('purchasePrice').getValue()
				});

				 var requestData =  Ext.encode(form_data);
					console.log("requestData"+requestData);
                    Ext.Ajax.request({
                        url: 'prodRequest/saveOrUpdate',
                        method: 'POST',
                        jsonData:requestData,
                        success: function(result, request) {
                        	var jsonresp = Ext.util.JSON.decode(result.responseText);
                           	Ext.MessageBox.show({
                                title: 'Success',
                                msg: jsonresp.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            })
                            Ext.ComponentQuery.query('#requestproductgrid')[0].store.load();
							 if (win) {
                                win.close();
                                
                            }
							 Ext.widget('requestproductwindow').show();
                        },
                        failure: function(response) {
                            Ext.MessageBox.show({
                                title: 'Failure',
                                msg: 'Invalid Data',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            })
                        }
                    });

                 }
        }
    },{
        text: 'Cancel',
        handler: function() {
            Ext.ComponentQuery.query('#requestproductwin')[0].close();
        }
    }],
		    listeners: {
		        close: function() {
		            Ext.ComponentQuery.query('#RequestProduct')[0].enable();
		            Ext.ComponentQuery.query('#tabbar')[0].enable();
		            Ext.ComponentQuery.query('#header')[0].enable();
		        }
		    },
});