Ext.define('TomTail.view.config.products.ProductAddWindow', {
    extend: 'Ext.window.Window',

    alias: 'widget.products',

    requires: ['TomTail.view.config.products.ProductPriceGrid'],

    controller: "products-product",
    viewModel: {
        type: "products-product"
    },
    //modal: true,
    autoScroll: true,
    title: 'Products',
    itemId: 'productWindow',
    width: 650,
    height: 500,
    //cls:'pricegrid',
    items: [{
        xtype: 'form',
        width: 630,
        //cls:'pricegrid',
        bodyPadding: 10,
        defaults: {
            xtype: 'textfield',
            padding: 10
        },
        layout: {
            type: 'table',
            columns: 2,
            tdAttrs: {
                style: 'padding: 0px;'
            }
        },

        items: [{
			xtype:'fieldset',
		 border: false,
		 colspan:2,
            frame: false,
            padding: 5.5,
            layout: {
                type: 'hbox',
              /*  columns: 2,
                tdAttrs: {
                    style: 'padding:0px;'
                }*/
            },
			items:[{
			xtype:'textfield',
            fieldLabel: 'Product Name',
            
            name: 'name'
        },{
			xtype:'textfield',
			tabIndex:1,
			readOnly:true,
			style: 'margin-left:230px;',
			width:80,
            fieldLabel: 'Id',
			labelWidth:20,
            name: 'productid'
        }]
		},{
            xtype:'textfield',
			//hideTrigger:true,
            fieldLabel: 'SKU',
            
            name: 'sku'
        }, {
			xtype:'numberfield',
			hideTrigger:true,
            fieldLabel: 'Weight',
            
            name: 'weight'
        }, {
            xtype: 'combobox',
			editable:false,
            fieldLabel: 'Category',
            name: 'categoryid',
            emptyText: 'Please select...',
            //forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'manufacturerstore',
                fields: ['categoryid', 'name'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "category/list",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'categoryid',
            //value:1,
        },  {
            xtype: 'combobox',
			editable:false,
            fieldLabel: 'Manufacturer',
            name: 'manufacturerId',
            emptyText: 'Please select...',
            //forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'manufacturerstore',
                fields: ['manufacturerid', 'name'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "manufacturer/list",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'manufacturerid',
            //value:1,
        },  {
            xtype: 'combobox',
			editable:false,
            fieldLabel: 'Brand',
            colspan:2,
            name: 'brandid',
            emptyText: 'Please select...',
            //forceSelection: true,
            store: new Ext.data.Store({
                storeId:'brandnamestore',
                fields: ['brandid', 'name'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "brand/brandList",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'brandid',
            //value:1,
        },   {
            xtype: 'textareafield',
            colspan:2,
            width:595,
            fieldLabel: 'Short Desc',
            name: 'shortDesc'
        },    {
            xtype: 'textareafield',
            colspan:2,
            width:595,
            fieldLabel: 'Long Desc',
            name: 'longDesc'
        }, {
            xtype: 'checkbox',
            //colspan: 2,
            fieldLabel: 'Enable',
            name: 'isactive',
            checked: true,
            value: true,
			colspan:2,
            inputValue: 1,
            uncheckedValue: 0
        }, {
            xtype: 'productpriceGrid',
            colspan: 2,
        }],
    }],
    buttons: [{
        text: 'Save',
        handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            console.log("form" + form);
            if (form.isValid()) {
                var formData = Ext.encode(form.getValues());
                var url = 'product/updatedata';
                if (form.getForm().findField('productid').getValue() == null || form.getForm().findField('productid').getValue() == '') {
                    url = 'product/save';
                }
				console.log("url"+url);
				var productId = form.getForm().findField('productid').getValue();
                var store_grid = Ext.ComponentQuery.query('productpriceGrid')[0].store;
				console.log("store_grid"+store_grid);
               var selectionne = store_grid.getRange();
				console.log("selectionne"+selectionne);
                var data_grid = new Array();
                Ext.each(selectionne, function(record) {
					console.log("record"+record);
					var idval=""+record.data.id;

                    if (record.data.competitorprice != null && record.data.discountprice != null) {
                    	if(idval.indexOf("-")>-1){
                    		idval=null;
                    	}else{
                    		idval=record.data.id;
                    	}
                        data_grid.push({
							competitorId: record.data.competitorId,
							competitorprice: record.data.competitorprice,
							discountprice: record.data.discountprice,
							id: idval,
						});
                    }
                });
				console.log("data_grid"+data_grid);
                var gridData =  Ext.encode(data_grid);
				console.log("gridData"+gridData);
					/*store_grid.proxy.url="competitorPrice/save"
		            store_grid.proxy.extraParams={"productId":productId},
		            store_grid.sync();*/
                if (store_grid.getCount() > 0) {
                    Ext.Ajax.request({
                        url: url,
                        method: 'POST',
                        jsonData: formData,
                        success: function(result, request) {
                           var jsonresp = Ext.util.JSON.decode(result.responseText);
                            var productid = jsonresp.data.productId;
                            Ext.Ajax.request({
                                url: 'competitorPrice/save',
                                method: 'POST',
                                jsonData: gridData,
                                params: {
                                    "productId": productid
                                },
                                success: function(result, request) {
                                    //var jsonresp = Ext.util.JSON.decode(result.responseText);
                                    //console.log("jsonresp4" + jsonresp.data);
                                	var jsondata = Ext.util.JSON.decode(result.responseText);
                                	Ext.MessageBox.show({
                                        title: 'Success',
                                        msg: jsondata.message,
                                        buttons: Ext.MessageBox.OK,
                                        icon: Ext.MessageBox.INFO
                                    })
                                    Ext.ComponentQuery.query('#productgrid')[0].store.load();
                                },
                                failure: function(result, request) {
                                    Ext.MessageBox.show({
                                        title: 'Failure',
                                        msg: 'Error Occured',
                                        buttons: Ext.MessageBox.OK,
                                        icon: Ext.MessageBox.ERROR
                                    })
                                }
                            });
                           
                        	Ext.MessageBox.show({
                                title: 'Success',
                                msg: jsonresp.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            })
                            Ext.ComponentQuery.query('#productgrid')[0].store.load();
							 if (win) {
                                win.close();
                                
                            }

                        },
                        failure: function(response) {
                            Ext.MessageBox.show({
                                title: 'Failure',
                                msg: 'Invalid Data',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            })
                        }
                    });

                } else {
                    Ext.MessageBox.show({
                        title: 'Failure',
                        msg: 'Please add a product price',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    })
                }
            }
        }
    }, {
        text: 'Cancel',
        handler: function() {
            Ext.ComponentQuery.query('#productWindow')[0].close();
        }
    }, ],

    listeners: {
        close: function() {
        	if(Ext.ComponentQuery.query('#productPanel')[0] != undefined){
            Ext.ComponentQuery.query('#productPanel')[0].enable();
            Ext.ComponentQuery.query('#tabbar')[0].enable();
            Ext.ComponentQuery.query('#header')[0].enable();
        	}
        }
    },

}); 