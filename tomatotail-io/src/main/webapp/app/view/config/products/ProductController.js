Ext.define('TomTail.view.config.products.ProductController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.products-product',
	init: function() {
        var me = this;
        this.control({
            'productgrid dataview': {
                itemdblclick: function(view, rec, elm, inx, evt, obj) {
                    me.loadInfo(rec);
                }
            },

			'productsearchpanel button[action=searchProduct]':{
                click:'searchConfig'
            },
			'productsearchpanel button[action=ClearProduct]':{
                click:'clearConfig'
            },
        });
    },

    loadInfo: function(rec) {
        var win = Ext.widget('products');
		var formvalue=win.down('form').getForm();
		//formvalue.findField('productSku').setValue(rec.data.sku);
    	    formvalue.loadRecord(rec);
		/*var store = Ext.getStore('productPriceStore');
		console.log("store"+store);
		var productId = formvalue.findField('productid').getValue();
		console.log("productid"+productId);
		store.getProxy().url="product/productWithCompetitor?productId="+productId;
    	store.load();
		console.log("Success");*/
		//var store = new Ext.create('TomTail.view.config.products.ProductPriceGrid');
		//store.getProxy().setExtraParam('url', "./getFilteredReports.html"); //
		
        win.show();

		Ext.Ajax.request({
			url : 'product/productWithCompetitor?productId=' + rec.get('productid'),
			method : 'GET',
			success : function(response, options) {
				var jsonresp = response.responseText;
				var jsondata = Ext.util.JSON.decode(jsonresp);
				var list = new Array();
				 Ext.each(jsondata.data.list.competitorPrices, function(record) {
					list.push(record);
				});
				Ext.ComponentQuery.query('productpriceGrid')[0].store.loadData(list, false);
			},
			failure : function(response, options) {
				Ext.MessageBox.alert('FAILED', 'Unable to SAVE');
			}
		});
	
		Ext.ComponentQuery.query('#productPanel')[0].disable();
		Ext.ComponentQuery.query('#tabbar')[0].disable();
		Ext.ComponentQuery.query('#header')[0].disable();

    },


	searchConfig:function(){
    	var store=this.getViewModel().getStore('products');
    	var productName = this.lookupReference('productName').getValue();
		var brandName = this.lookupReference('brandName').getValue();
		var manuName = this.lookupReference('manufacturerName').getValue();
    	console.log('productName'+productName);
    	if(manuName!='' || productName!='' ||  brandName!=''){
url="product/searchByName?manuName="+manuName+"&productName="+productName+"&brandName="+brandName;
}else{
    		console.log('else');
    		url="product/listAll";
    	}
    	console.log('url'+url);
    	store.proxy.url=url;
    	store.load();
    },
	clearConfig:function(){
		this.lookupReference('productName').reset();
		this.lookupReference('brandName').reset();
		this.lookupReference('manufacturerName').reset();
	}

});
