Ext.define('TomTail.view.config.products.ProductGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.productgrid',
	requires:[
	         'TomTail.view.config.products.ProductAddWindow'
	          ],
	border : false,
	title : 'Search Result',
	height : 288,
	header: {
        xtype: 'header',
        titlePosition: 0,
        defaults: {
            margin: '0 10px'
        },
        items: [
           
            {
                xtype: 'button',
                text: "Add New",
                handler: Ext.bind(function() {
                	Ext.widget('products').show();
					Ext.ComponentQuery.query('#productPanel')[0].disable();
					Ext.ComponentQuery.query('#tabbar')[0].disable(); 
	Ext.ComponentQuery.query('#header')[0].disable(); 
	Ext.ComponentQuery.query('productpriceGrid')[0].store.loadData(false);
	console.log("productpriceGrid Grid Cleared");
                }, this)
            }
        ]
    },
	width : '100%',
	bind: {
        store: '{products}'
    },
	itemId:'productgrid',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			autoScroll : true,
			
			columns : [{
				text : 'Id',
				dataIndex : 'productid',
				sortable:false,
			}, {
				text : 'Name',
				dataIndex : 'name',
				sortable:false,
				flex : 1
			},{
				text : 'Brand',
				dataIndex : 'brandid',
				sortable:false,
				flex : 1,
				renderer:function(value,me,recs){
    	 var record = Ext.getStore('brandnamestore').findRecord('brandid', value);
    	 var returnname=(record === null ? '' : record.data.name );
    	 return returnname;
    }
			},{
				text : 'SKU',
				dataIndex : 'sku',
				sortable:false,
				flex : 1
			}],
			cls:'paging',
			dockedItems : [{
				xtype : 'pagingtoolbar',
				dock : 'bottom',
				pageSize:10,
				bind: {
        store: '{products}'
    },
				displayInfo : true,
				displayMsg : 'Displaying Product {0} - {1} of {2}'
			}]
		});
		this.callParent(arguments);
	}
});