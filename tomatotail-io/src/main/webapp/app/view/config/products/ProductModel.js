Ext.define('TomTail.view.config.products.ProductModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.products-product',
	requires: [
               'TomTail.model.ModelProducts'
           ],
    data: {
        name: 'TomTail'
    },
	stores:{
		products:{
				storeId: 'productStore',
				model:'TomTail.model.ModelProducts',
				autoLoad: false,
				pageSize:20,
				proxy: {
            type: 'ajax',
            url: 'product/listAll',
            reader: {
                rootProperty: 'data.list',
				totalProperty: 'data.total',
            },
            listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }

			},
			},
	
	},

});
