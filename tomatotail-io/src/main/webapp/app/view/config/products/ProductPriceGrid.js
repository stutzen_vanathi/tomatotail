Ext.define('TomTail.view.config.products.ProductPriceGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.productpriceGrid',
	controller: "products-product",
    viewModel: {
        type: "products-product"
    },
	requires:['Ext.data.identifier.Negative'],
    width: '100%',
    autoScroll: true,
	border:true,
		frame:true,
		itemId:'productpricegrid',
		
	store : Ext.create('Ext.data.Store',{
        storeId: 'productPriceStore',
		fields: [{
			name:'id',

		},{
                    name: 'competitorId',
                }, {
                    name: 'competitorprice',
                }, {
                    name: 'discountprice',
                }],

    }),

        columns: [{
        	  dataIndex: 'id',editor: 'textfield',hidden:true
        },{
            text: 'Competitor Name',
			dataIndex:'competitorId',
			reference:'competitor',
            flex:1,
			editor:{
		xtype:'combobox',
        name: 'competitorId',
		id:'competitorName',
		editable:false,
		emptyText: 'Please select...',
						//forceSelection: true,
                        store:new Ext.data.Store({
                     		  storeId:'comppricestore',
                              fields: ['competitorid', 'name'],
							  autoLoad:true,
                               proxy : {
                           		type : 'ajax',
                           		url:   "competitor/list",
                           		reader : {
                           			type : 'json',
                           			rootProperty:'data.list',
                           		},

                               },
                           }),
                        queryMode: 'remote',
                        displayField: 'name',
                        valueField: 'competitorid',
                        //value:1,
						listeners: {
							select:function (combo,selection) {
                	var store = Ext.getStore('productPriceStore');
                	store.each(function(record){
                		if(record.get('competitorId') == combo.getValue()){
                			Ext.MessageBox.show({
                                title: 'Failure',
                                msg: 'Competitor already exist...',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR,
                                
                            })
                            combo.reset();
                			
                		}
                	});
	                    /*if (me.oldrec !== null){
						
						me.oldrec = recs;
						me.store.remove(recs);
						//me.store.add(me.oldrec);
					}*/
                },
				
            }
			
    },
	
    renderer:function(value,me,recs) {
    	 var record = Ext.getStore('comppricestore').findRecord('competitorid', value);
    	 var returnname=(record === null ? '' : record.data.name );
    	 return returnname;
    },
	
        },{
            text: 'Price',
			dataIndex: 'competitorprice',
			flex:1,
			editor: {
					xtype:'numberfield',
					hideTrigger:true,
                    allowBlank: false,
                },
			renderer:function(value){
					if(value==null){
						return 0;
					}else if (value==""){
						return 0;
					}else{
						return value;
					}
			}
        },{
            text: 'Discount Price',
			dataIndex: 'discountprice',
			flex:1,
			editor: {
				xtype:'numberfield',
					hideTrigger:true,
                    allowBlank: false,
                },
			renderer:function(value){
					if(value==null){
						return 0;
					}else if (value==""){
						return 0;
					}else{
						return value;
					}
			}
        },{
                xtype: 'actioncolumn',
                width: 30,
                sortable: false,
                menuDisabled: true,
                items: [{
                    icon: 'images/icon16_error.png',
                    tooltip: 'Delete Competitor',
					style:'cursor:pointer;',
                    scope: this,
                    handler: function(grid, rowIndex){
							grid.getStore().removeAt(rowIndex);
					}
                }],
				renderer: function (val, metadata, record) {
            metadata.style = 'cursor: pointer;'; 
        return val;
    }
		}],
		
		selModel: 'cellmodel',
    plugins: {
        ptype: 'cellediting',
        clicksToEdit: 1,
    },

		dockedItems : [{
				xtype : 'toolbar',
				dock : 'top',
				items : [ {
					text : 'Add Competitor',
					icon:'images/drop-add.png',
					cls : 'x-btn-text-icon',
					handler : function() {
						 var rec = new TomTail.view.config.products.ProductPriceGrid({
            competitorId: '',
            competitorprice: 0,
            discountprice: 0,

        });
		var store = Ext.getStore('productPriceStore');
		store.insert(0, rec);
					}
				}]
			}],
			
});
