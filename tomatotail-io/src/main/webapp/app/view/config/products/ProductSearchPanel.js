Ext.define('TomTail.view.config.products.ProductSearchPanel', {
	extend : 'Ext.form.Panel',
	alias : 'widget.productsearchpanel',
	requires:[
'Ext.form.field.Hidden'

	          ],
	title : 'Search Products',

	layout : 'hbox',
	height:'50px',

	reference:'productform',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			padding:7,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [{
				fieldLabel : 'Brand Name',
				name : 'brandName',
				reference:'brandName',
				xtype : 'textfield',
				margin : '10px'
			},{
				fieldLabel : 'Manufacturer Name',
				name : 'ManufacturerName',
				reference:'manufacturerName',
				labelWidth:'n',
				xtype : 'textfield',
				margin : '10px'
			},{
				fieldLabel : 'Product Name',
				name : 'productName',
				xtype : 'textfield',
				margin : '10px',
				reference:'productName',
			},{
				xtype : 'button',
				text : 'Clear',
				width : 70,
				margin : '10px',
				action:'ClearProduct',
			},{
				xtype : 'button',
				text : 'Search',
				width : 70,
				margin : '10px',
				action:'searchProduct',
			}]
		});
		this.callParent(arguments);
	}
});