Ext.define('ChannelPartner.view.products.ProductViewForm', {
    extend: 'Ext.form.Panel',

    alias: 'widget.productsView',
	controller: "products-product",
    viewModel: {
        type: "products-product"
    },
	autoScroll:true,
  
	id:'productForm',
	bodyPadding:10,
    defaults: {
        xtype: 'displayfield',
		labelWidth:150,
		labelStyle: 'white-space: nowrap;',
    },

    items: [{
        fieldLabel: 'Id',
        name: 'productId'
    },{
        fieldLabel: 'Manufacturer Name',
        name: 'manufacturerName',
    }, {
        fieldLabel: 'Brand',
        name: 'brandName',
    }, {
        fieldLabel: 'Category',
        name: 'categoryName',
    },{
        fieldLabel: 'Product',
        name: 'productName'
    },{
        fieldLabel: 'Weight',
        name: 'weight'
    },{
        fieldLabel: 'SKU',
        name: 'sku'
    },{
        fieldLabel: 'Purchase Price',
        name: 'price'
    },{
        fieldLabel: 'Selling Price',
        name: 'sellingPrice'
    },/*{
        xtype: 'checkbox',
        fieldLabel: 'Enable',
        name: 'isActive',
        checked: true,
        value: true,
    }*/],
   buttons: [{
        text: 'Close',
		handler:function(){
			Ext.getCmp('productWindow').close();
		}
    }],

});