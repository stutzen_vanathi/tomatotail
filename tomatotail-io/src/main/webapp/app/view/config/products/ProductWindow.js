Ext.define('ChannelPartner.view.products.ProductWindow', {
    extend: 'Ext.window.Window',
	requires:['ChannelPartner.view.products.ProductViewForm'],
    alias: 'widget.productWindow',
    modal: true,
	controller: "products-product",
    viewModel: {
        type: "products-product"
    },
    title: 'Products',
	id:'productWindow',
	width:450,
	items:[{
	xtype:'productsView',
		}],
   

});