Ext.define("TomTail.view.config.products.ProductWorkPanel",{
    extend: "Ext.panel.Panel",
	alias: 'widget.productconfigpanel',
    requires:[
	'TomTail.view.config.products.ProductController',
    'TomTail.view.config.products.ProductSearchPanel',
    'TomTail.view.config.products.ProductGrid',
    ],
    controller: "products-product",
    viewModel: {
        type: "products-product"
    },
    layout: {
        type: 'vbox',
        align: 'center'
    },
    bodyStyle:'background:url(../../../../images/bg_page.png) !important',
    items:[{
        xtype: 'productsearchpanel',
        //collapsible:true,
        height:102,
        width:'100%'
        	 },{
        xtype: 'productgrid',
        //collapsible:true,
        flex:2,
        width:'100%'
        
    }],
    initComponent : function() {
    this.callParent(arguments);
    }
});
