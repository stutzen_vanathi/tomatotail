Ext.define('TomTail.view.config.profile.ChangePasswordForm', {
    extend: 'Ext.form.Panel',

    alias: 'widget.passwordform',

   
    width: 500,
    controller: "profile-profile",
    viewModel: {
        type: "profile-profile"
    },

   autoScroll:true,
	bodyPadding:10,
    defaults: {
        xtype: 'textfield',
		labelStyle: 'white-space: nowrap;',
		labelWidth:150
    },

    items: [{
        fieldLabel: 'Current Password',
		allowBlank:false,
        name: 'currentPassword',
		inputType: 'password',
        emptyText: 'Current Password',
    }, {
        fieldLabel: 'New Password',
		allowBlank:false,
        name: 'newPassword',
		inputType: 'password',
        emptyText: 'New Password',
    }, {
        fieldLabel: 'Confirm Password',
		allowBlank:false,
        name: 'confirmPassword',
		inputType: 'password',
        emptyText: 'Confirm Password',
    }],

    buttons: [{
        text: 'Save',
    }],

});