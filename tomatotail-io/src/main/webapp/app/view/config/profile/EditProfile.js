Ext.define("TomTail.view.config.profile.EditProfile",{
    extend: "Ext.tab.Panel",
	
	alias:'widget.editprofile',
	requires:[
	'TomTail.view.config.profile.ProfileController',
	'TomTail.view.config.profile.ProfileModel',
	'TomTail.view.config.profile.EditProfileForm',
	'TomTail.view.config.profile.PasswordWindow'
	],
    controller: "profile-profile",
    viewModel: {
        type: "profile-profile"
    },

    itemId:'profilePanel',
	
	layout: {
       type: 'fit'
   },
   
			tabWidth: 200,
			tabPosition: 'left',
			tabRotation:0,
	
	items:[{
		title:'Personal Info',
		xtype:'editprofileform',
	},{
		title:'Change Password',
		xtype:'passwordform',
	}],
	/*listeners:{
	afterrender: function(panel) {
            var bar = panel.tabBar;
            bar.insert(2, [{
                xtype: 'component',
                //width: 200,
                //flex: 1
            }, {
                xtype: 'button',
				text:'Change Password',
				cls:'password',
                overCls: 'my-over',
                pressedCls: 'pressed',

				//width:200,
                icon: '',
                listeners: {
                    click: function() {
                        Ext.widget('passwordwindow').show();
						Ext.ComponentQuery.query('#passwordTab')[0].show();
						Ext.ComponentQuery.query('#profilePanel')[0].disable();
						Ext.ComponentQuery.query('#tabbar')[0].disable(); 
						Ext.ComponentQuery.query('#header')[0].disable(); 
                    }
                },
            }])
        }
	}*/
});
