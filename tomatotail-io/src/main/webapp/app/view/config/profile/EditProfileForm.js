Ext.define('TomTail.view.config.profile.EditProfileForm', {
    extend: 'Ext.form.Panel',

    alias: 'widget.editprofileform',

   
    width: 500,
    controller: "profile-profile",
    viewModel: {
        type: "profile-profile"
    },

   autoScroll:true,
	bodyPadding:10,
	layout:{
		type:'table',
		columns:2,
		tdAttrs: {
            style: 'padding: 10px; vertical-align: top;'
        }
	},
    defaults: {
        xtype: 'textfield',
		labelStyle: 'white-space: nowrap;',
		labelWidth:150
    },

    items: [{
            xtype: 'combobox',
			editable:false,
            fieldLabel: 'Role',
            name: 'roleid',
            emptyText: 'Please select...',
            //forceSelection: true,
            store: new Ext.data.Store({
                fields: ['roleid', 'user'],
            data: [{
                "user": "Admin"
            }, {
                "user": "Channel Partner"
            }, {
                "user": "User"
            }],
            }),
            queryMode: 'local',
            displayField: 'user',
            valueField: 'roleid',
            //value:1,
        },{
        fieldLabel: 'User Id',
		readOnly:true,
        name: 'userName'
    },{
        fieldLabel: 'User Name',
        name: 'userName'
    },{
		xtype:'textareafield',
        fieldLabel: 'Address',
		
        name: 'useraddress'
    }, {
        fieldLabel: 'Mobile Number',
		
        name: 'mobilenumber',
		maxLength:10,
    }, {
        fieldLabel: 'LandLine',
        name: 'landLine',
		maxLength:12,
    }, {
        fieldLabel: 'Email',
		
        name: 'email'
    }, {
        fieldLabel: 'City',
		
        name: 'city'
    }],

    buttons: [{
        text: 'Save',
    }],

});