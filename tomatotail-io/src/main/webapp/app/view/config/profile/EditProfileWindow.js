Ext.define('TomTail.view.config.profile.EditProfileWindow', {
    extend: 'Ext.window.Window',

    alias: 'widget.profile',
	requires:[
	'TomTail.view.config.profile.EditProfileForm'
	],
   
    width: 500,
	itemId:'profileWindow',
   controller: "profile-profile",
    viewModel: {
        type: "profile-profile"
    },

    //modal: true,
    title: 'Profile Details',
	items:[{
		xtype:'editprofileForm',
    }],
	listeners:{
		close:function(){
			Ext.ComponentQuery.query('#profilePanel')[0].enable();
		Ext.ComponentQuery.query('#tabbar')[0].enable(); 
		Ext.ComponentQuery.query('#header')[0].enable(); 
		}
	},

});