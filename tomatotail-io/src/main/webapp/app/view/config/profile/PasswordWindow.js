Ext.define('TomTail.view.config.profile.PasswordWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.passwordwindow',
	requires : [ 'TomTail.view.config.profile.ChangePasswordForm' ],
	width : 400,
	title : 'Change Password',
	layout : 'fit',
	//modal:true,
	reference:'passwordWindow',
	
	items : [{
				xtype : 'passwordform'
	}],
	
	listeners:{
		close:function(){
			Ext.ComponentQuery.query('#profilePanel')[0].enable();
		Ext.ComponentQuery.query('#tabbar')[0].enable(); 
		Ext.ComponentQuery.query('#header')[0].enable(); 
		}
	},

});