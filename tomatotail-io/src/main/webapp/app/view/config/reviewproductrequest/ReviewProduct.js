
Ext.define("TomTail.view.config.reviewproductrequest.ReviewProduct",{
    extend: "Ext.panel.Panel",
	alias: 'widget.reviewproductPanel',
    requires:[
	'TomTail.view.config.reviewproductrequest.ReviewProductController',
    'TomTail.view.config.reviewproductrequest.ReviewProductSearch',
    'TomTail.view.config.reviewproductrequest.ReviewProductGrid',
	'TomTail.view.config.reviewproductrequest.ReviewProductModel',
	'TomTail.view.config.reviewproductrequest.ReviewProductWindow'
    ],
    controller: "reviewproductrequest-product",
    viewModel: {
        type: "reviewproductrequest-product"
    },
    itemId:'ReviewProduct',
     layout: {
        type: 'vbox',
        align: 'center'
    },
    items:[{
        xtype: 'reviewproductsearch',
        height: 102,
        width:'100%'
        	 },{
        xtype: 'reviewproductgrid',
        flex:2,
        width:'100%'
    }],
    initComponent : function() {
    this.callParent(arguments);
    }
});
