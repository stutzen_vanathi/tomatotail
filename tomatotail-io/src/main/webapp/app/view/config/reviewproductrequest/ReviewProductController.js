Ext.define('TomTail.view.config.reviewproductrequest.ReviewProductController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.reviewproductrequest-product',
    init: function() {
        var me = this;
        this.control({
            'reviewproductgrid dataview': {
                itemdblclick: function(view, rec, elm, inx, evt, obj) {
                    me.loadInfo(rec);
                }
            },
            'reviewproductsearch button[action=searchProduct]':{
                click:'searchConfig'
            },
			'reviewproductsearch button[action=ClearProduct]':{
                click:'clearConfig'
            },
            
            'reviewproductwindow button[action=saveprodReview]':{
                click:'saveproductReview'
            },
        });
    },

    loadInfo: function(rec) {
        var win = Ext.widget('reviewproductwindow');
		var formvalue=win.down('form').getForm();
    	    formvalue.loadRecord(rec);
    	    formvalue.findField('categoryId').setValue(rec.data.category.name);
    	    formvalue.findField('manuId').setValue(rec.data.manufacturer.name);
    	    formvalue.findField('brandId').setValue(rec.data.brand.name);
    	    //Ext.ComponentQuery.query('#status')[0].setValue(rec.data.status);
    	    
        win.show();
    },
    
    searchConfig:function(){
    	var store=this.getViewModel().getStore('productreview');
    	var partnerId = this.lookupReference('partnerId').getValue();
		var status = this.lookupReference('status').getValue();
		var partnerCity = this.lookupReference('partnerCity').getValue();
		if(partnerId== null){
			partnerId=''
		}
		if(status== null){
			status=''
		}
		if(partnerCity== null){
			partnerCity=''
		}
    	if(partnerId!='' ||  status!='' ||  partnerCity!=''){
    		url="prodRequest/listAll?partnerId="+partnerId+"&status="+status+"&partnerCity="+partnerCity;
    	}else{
    		console.log('else');
    		url="prodRequest/listAll";
    	}
    	console.log('url'+url);
    	store.proxy.url=url;
    	store.load();
    	
    },
	clearConfig:function(){
		this.lookupReference('partnerId').reset();
		this.lookupReference('status').reset();
		this.lookupReference('partnerCity').reset();
		var store=this.getViewModel().getStore('productreview');
    	store.proxy.url="prodRequest/listAll";
    	store.load();
	},
	
	
});
