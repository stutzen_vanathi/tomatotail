Ext.define('TomTail.view.config.reviewproductrequest.ReviewProductGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.reviewproductgrid',
	
	border : false,
	title : 'Product Review',
	height : 288,
	width : '100%',
	
	viewConfig: {
		style: { overflow: 'auto', overflowX: 'hidden' },
        getRowClass: function(record, index) {
            var status = record.get('status');
            if(status == 'Rejected'){
            	return 'rejected';
            }
        },
        loadMask:false
    },
	bind: {
        store: '{productreview}'
    },
    itemId:'reviewproductgrid',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			autoScroll : true,
			
			columns : [{
				hidden:true,
				dataIndex : 'id',
			},{
				text : 'Product Id',
				dataIndex : 'id',
				sortable:false,
			}, {
				text : 'Product Name',
				dataIndex : 'productName',
				sortable:false,
				flex : 1
			},{
				text : 'Purchase Price',
				dataIndex : 'purchasePrice',
				sortable:false,
				
				flex : 1
			},{
				text : 'Selling Price',
				dataIndex : 'sellingPrice',
				sortable:false,
				flex : 1,
				
			},{
				text:'Status',
				dataIndex:'status',
				flex:1
			}],
			 
    cls:'paging',
			dockedItems : [{
				xtype : 'pagingtoolbar',
				dock : 'bottom',
				bind: {
					store: '{productreview}'
				},
				displayInfo : true,
				displayMsg : 'Displaying products {0} - {1} of {2}'
			}],
			
		});
		this.callParent(arguments);
	}
});