Ext.define('TomTail.view.config.reviewproductrequest.ReviewProductModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.reviewproductrequest-product',
	requires: ['Ext.data.TreeStore'],
    data: {
        name: 'TomTail'
    },
	stores:{
		productreview:{
				storeId: 'reviewproductStore',
				fields:[{name:"id"},{name:"productName"},{name:"sku"},{name:"sellingprice"},{name:"purchaseprice"},{name:"status"}],
				autoLoad: true,
				pageSize:20,
				proxy: {
            type: 'ajax',
            url: 'prodRequest/listAll',
            reader: {
                rootProperty: 'data.list',
                totalProperty:'data.total',
            },
            listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }

        },
			},
			
	},

});
