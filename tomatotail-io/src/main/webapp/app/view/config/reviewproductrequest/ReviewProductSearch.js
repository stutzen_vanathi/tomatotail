Ext.define('TomTail.view.config.reviewproductrequest.ReviewProductSearch', {
	extend : 'Ext.form.Panel',
	alias : 'widget.reviewproductsearch',
	requires:[
'Ext.form.field.Hidden'
	          ],
	title : 'Search Products',

	layout : 'hbox',
	height:'50px',

	reference:'productform',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [{
	            xtype: 'combobox',
	            fieldLabel: 'Partner City',
	            name: 'partnerCity',
	            margin : '10px',
	            reference:'partnerCity',
	            emptyText: 'Please select...',
	            //forceSelection: true,
	            store: new Ext.data.Store({
	                // storeId:'',
	                fields: [ 'name','partnerId','city'],
	                autoLoad: true,
	                proxy: {
	                    type: 'ajax',
	                    url: "channelPartner/listAll",
	                    reader: {
	                        type: 'json',
	                        rootProperty: 'data.list',
	                    },

	                },
	            }),
	            queryMode: 'remote',
	            displayField: 'city',
	            valueField: 'city',
	        },{
	            xtype: 'combobox',
	            reference:'partnerId',
	            fieldLabel: 'Outlet',
	            margin : '10px',
	            name: 'partnerId',
	            emptyText: 'Please select...',
	            //forceSelection: true,
	            store: new Ext.data.Store({
	                // storeId:'',
	                fields: [ 'name','partnerId','city'],
	                autoLoad: true,
	                proxy: {
	                    type: 'ajax',
	                    url: "channelPartner/listAll",
	                    reader: {
	                        type: 'json',
	                        rootProperty: 'data.list',
	                    },

	                },
	            }),
	            queryMode: 'remote',
	            displayField: 'name',
	            valueField: 'partnerId',
	        },{
	            xtype: 'combobox',
	            fieldLabel: 'Status',
	            name: 'status',
	            margin : '10px',
	            reference:'status',
	            emptyText: 'Please select...',
	            //forceSelection: true,
	            store: new Ext.data.Store({
	                // storeId:'',
	                fields: ['status'],
                    data: [{
                        "status": "Initiated",
                    },{
                        "status": "Processing",
                    },{
						"status": "Processed",
					},{
                        "status": "Accepted",
                    },{
						"status": "Rejected",
					}],
	            }),
	            queryMode: 'local',
	            displayField: 'status',
                valueField: 'status',
	        },{
				xtype : 'button',
				text : 'Clear',
				width : 70,
				margin : '10px',
				action:'ClearProduct'
			},{
				xtype : 'button',
				text : 'Search',
				width : 70,
				margin : '10px',
				action:'searchProduct'
			}]
		});
		this.callParent(arguments);
	}
});