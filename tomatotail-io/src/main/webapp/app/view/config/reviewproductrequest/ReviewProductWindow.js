Ext.define('TomTail.view.config.reviewproductrequest.ReviewProductWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.reviewproductwindow',
    requires:['TomTail.view.config.products.ProductAddWindow'],
	controller: "reviewproductrequest-product",
    viewModel: {
        type: "reviewproductrequest-product"
    },
    
    title: 'Review Requested Product',
    itemId:'reviewproductwin',
    modal:true,
    width: 650,
    autoScroll:true,
    height: 527,
    //cls:'pricegrid',
    items: [{
        xtype: 'form',
        width: 630,
        //cls:'pricegrid',
        bodyPadding: 10,
        defaults: {
            xtype: 'displayfield',
            padding: 10
        },
        layout: {
            type: 'table',
            columns: 2,
            tdAttrs: {
                style: 'padding: 0px;'
            }
        },

        items: [{
			xtype:'displayfield',
            fieldLabel: 'Product Name',
            name: 'productName'
        },{
		xtype:'textfield',
		tabIndex:1,
		readOnly:true,
		style: 'margin-left:230px;',
		width:80,
        fieldLabel: 'Id',
		labelWidth:20,
        name: 'id'
    },{
            fieldLabel: 'Outlet',
            name: 'partnerId',
            
        },{

            fieldLabel: 'SKU',
            name: 'sku'
        },{
            fieldLabel: 'Category',
            name: 'categoryId',
           
        },  {

            fieldLabel: 'Manufacturer',
            name: 'manuId',
            //value:1,
        },  {
			
            fieldLabel: 'Brand',
            name: 'brandId',
            
        }, {
            //xtype:'numberfield',
			hideTrigger:true,
            fieldLabel: 'MRP',
            name: 'mrpPrice'
        }, {
           // xtype:'numberfield',
			hideTrigger:true,
            fieldLabel: 'Selling Price',
            
            name: 'sellingPrice'
        }, {
            //xtype:'numberfield',
            fieldLabel: 'Purchase Price',
            name: 'purchasePrice'
        },  {
            //xtype: 'textareafield',
            colspan:2,
            width:595,
            fieldLabel: 'Short Desc',
            name: 'shortDesc'
        },    {
            //xtype: 'textareafield',
            colspan:2,
            width:595,
            fieldLabel: 'Long Desc',
            name: 'longDesc'
        }, {
            //xtype: 'textareafield',
            colspan:2,
            width:595,
            fieldLabel: 'Comments',
            name: 'comments'
        },{
	            xtype: 'combobox',
	            fieldLabel: 'Status',
	            editable:false,
	            name: 'status',
				itemId:'status',
	            margin : '10px',
	            emptyText: 'Please select...',
	            //forceSelection: true,
	            store: new Ext.data.Store({
	                // storeId:'',
	                fields: ['status'],
                    data: [{
                        "status": "Initiated",
                    },{
                        "status": "Processing",
                    },{
						"status": "Processed",
					},{
                        "status": "Accepted",
                    },{
						"status": "Rejected",
					}],
	            }),
	            queryMode: 'local',
	            displayField: 'status',
                valueField: 'status',
	        }, /*{
           // xtype: 'checkbox',
            //colspan: 2,
            fieldLabel: 'Is Active',
            name: 'isActive',
            checked: true,
            value: true,
			colspan:2,
            inputValue: 1,
            uncheckedValue: 0
        }*/],
    }],
    buttons: [{
        text: 'Save',
        itemId:'productreviewacceptbtn',
        handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            if (form.isValid()) {
            	 
					var productStatus = form.getForm().findField('status').getValue();
					console.log(productStatus);
                    Ext.Ajax.request({
                        url: 'prodRequest/statusUpdate',
                        method: 'POST',
                        params:{
                        	"prodReqId":form.getForm().findField('id').getValue(),
                        	"status": productStatus,
                        },
                        //jsonData:requestData,
                        success: function(result, request) {
                        	var jsonresp = Ext.util.JSON.decode(result.responseText);
                           	Ext.MessageBox.show({
                                title: 'Success',
                                msg: jsonresp.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            })
                             
                            Ext.ComponentQuery.query('#reviewproductgrid')[0].store.load();
							 if (win) {
                                win.close();
                                
                            }
							
							
							

                        },
                        failure: function(response) {
                            Ext.MessageBox.show({
                                title: 'Failure',
                                msg: 'Invalid Data',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            })
                        }
                    });

                 }
        }
    }, {
        text: 'Cancel',
        handler: function() {
            Ext.ComponentQuery.query('#reviewproductwin')[0].close();
        }
    }],
		    listeners: {
		        close: function() {
		            Ext.ComponentQuery.query('#ReviewProduct')[0].enable();
		            Ext.ComponentQuery.query('#tabbar')[0].enable();
		            Ext.ComponentQuery.query('#header')[0].enable();
		        }
		    },
});