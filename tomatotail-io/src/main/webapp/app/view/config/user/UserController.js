/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('TomTail.view.config.user.UserController', {
    extend: 'Ext.app.ViewController',


    //requires:[﻿ 'TomTail.model.*'],

    alias: 'controller.user',
    init:function(){
        var me = this;
        this.control({

        	'usersearchpanel button[action=searchuser]':{
                click:'searchConfig'
                },
            'usersearchpanel button[action=clearuser]':{
                    click:'clearConfig'
                },
				  'usergrid dataview': {
                itemdblclick: function(view, rec, elm, inx, evt, obj) {
                    me.loadInfo(rec);
                }
            },
        });
    },

    searchConfig:function(){
    	var store=this.getViewModel().getStore('users');
    	var userName=this.lookupReference('usernameref').getValue();
    	var url="";
    	if(userName==""){
    		url="user/list";
    	}else{
    		url="user/list/s/"+userName;
    	}
    	store.proxy.url=url;
    	store.load();
    },
    clearConfig:function(){
    	var usf=this.lookupReference('usersearchform');
    	usf.reset();
    },
	 loadInfo: function(rec) {
        var win = Ext.widget('usereditpanel');
        win.show();

    },

});
