Ext.define('TomTail.view.config.user.UserEditPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.usereditpanel',
    requires: [
        'TomTail.view.config.user.UserController',
        'TomTail.view.config.user.UserModel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Hidden'
    ],
    viewModel: 'user',
    controller: 'user',
    width: 650,
    //layout : 'fit',
    autoScroll: true,
	defaults: {
        xtype: 'textfield',
    },
	layout: {
            type: 'table',
            columns: 2,
             tdAttrs: { style: 'padding: 10px; vertical-align: top;' }
    },
       
    items: [{
		xtype:'fieldset',
		 border: false,
		 colspan:2,
            frame: false,
            style:'padding:0px; margin-left:-1px; margin-bottom:5px;',
            layout: {
                type: 'hbox',
            }, 
		items:[{
			xtype:'textfield',
        fieldLabel: 'User Name',
        name: 'userName'
    },{
        fieldLabel: 'Id',
		readOnly:true,
        name: 'userName',
		xtype:'textfield',
		readOnly:true,
		tabIndex:1,
		style:'left:489px !important;',
		labelWidth:20,
		width:80,
    }]
	},{
		xtype:'textareafield',
        fieldLabel: 'Address',
        name: 'useraddress'
    }, {
        fieldLabel: 'Mobile Number',
        name: 'mobilenumber',
		maxLength:10,
    }, {
        fieldLabel: 'LandLine',
        name: 'landLine',
		maxLength:12,
    }, {
        fieldLabel: 'Email',
		
        name: 'email'
    }, {
        fieldLabel: 'City',
		
        name: 'city'
    }, {
                    xtype: 'checkbox',
                    fieldLabel: 'Enable',
                    name: 'isactive',
                    checked: true,
                    value: true,
					inputValue:1,
					uncheckedValue:0
                }],
            
            buttons: [{
                text: 'Save',
                handler:function(){
			var win = this.up('window');
			console.log("wndow"+win);
			var form = win.down('form');
			console.log("form"+form);
			var formData = Ext.encode(form.getValues());
			var url='user/update';
			if(form.getForm().findField('userid').getValue()==null || form.getForm().findField('userid').getValue()==''){
				url='user/insert';
			}
			Ext.Ajax.request({
				url : url,
				method : 'POST',
				jsonData : formData,
				success:function(result, request){
					//Ext.Msg.alert('Product Details', "Successfully saved.");
					Ext.MessageBox.show({
                                    title:'Success',
                                    msg: 'Successfully saved...',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.INFO
                                })
					if (win) {
				win.close();
				
				Ext.getCmp('usergrid').store.load();
			}
				},
				 failure : function(response) {
             Ext.MessageBox.show({
                                    title:'Failure',
                                    msg: 'Incorrect Data',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.ERROR
                                })
        }
			});
			

		}

            }],
});