Ext.define('TomTail.view.config.user.UserGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.usergrid',
	requires:[
	          'TomTail.view.config.user.UserWindow'
	          ],
	border : false,
	title : 'Search Result',
	height : 288,
	header: {
        xtype: 'header',
        titlePosition: 0,
        defaults: {
            margin: '0 10px'
        },
        items: [
           
            {
                xtype: 'button',
                text: "Add New",
                handler: Ext.bind(function() {
                	Ext.widget('userwindow').show();
					Ext.ComponentQuery.query('#userPanel')[0].disable();
					Ext.ComponentQuery.query('#tabbar')[0].disable(); 
					Ext.ComponentQuery.query('#header')[0].disable(); 
                }, this)
            }
        ]
    },
	width : '100%',
	id:'usergrid',
	bind: {
        store: '{users}'
    },
			columns : [ {
				text : 'User Id',
				dataIndex : 'userid',
				sortable:false,
			}, {
				text : 'User Name',
				dataIndex : 'username',
				sortable:false,
				flex : 1
			},/* {
				text : 'Email',
				dataIndex : 'email',
				sortable:false,
				flex : 1
			}, {
				text : 'Phone',
				dataIndex : 'phone',
				sortable:false,
				flex : 1
			},{
				text : 'Address',
				dataIndex : 'address',
				sortable:false,
				flex : 1
			}*/],
			cls:'paging',
			dockedItems : [ {
				xtype : 'pagingtoolbar',
				dock : 'bottom',
				bind: {
        store: '{users}'
    },
				displayInfo : true,
				displayMsg : 'Displaying Users {0} - {1} of {2}'
			}]
		
	
});