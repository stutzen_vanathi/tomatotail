/**
 * This class is the View Model for the ticket search view.
 */
Ext.define('TomTail.view.config.user.UserModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.user',
    requires: [
        'TomTail.model.ModelUser' //,
        // 'TomTail.model.ModelRole'
    ],
    data: {
        defaultRole: 3
    },
    stores: {
		storeId:'userStore',
        users: {
            model: 'ModelUser',
           
           
            proxy: {
                 type: 'ajax',
            url: 'user/list',
            reader: {
                rootProperty: 'data.list'
            },
            listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }
            }, //,
            autoLoad:true

        },
       
    }

});