Ext.define('TomTail.view.config.user.UserSearchPanel', {
	extend : 'Ext.form.Panel',
	alias : 'widget.usersearchpanel',
	requires:[
'Ext.form.field.Hidden'

	          ],
	title : 'Search User ',
	layout : 'hbox',
	height:'50px',
	reference:'usersearchform',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			padding:7,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [ {
				fieldLabel : 'User',
				name : 'username',
				reference: 'usernameref',
				xtype : 'textfield',
				margin : '10px',
				listeners:{
					change:function(field, newValue, oldValue, options){
						 var grid = Ext.getCmp('usergrid');
    grid.store.clearFilter();

    if (newValue) {
        var category = new RegExp(Ext.String.escapeRegex(newValue), "i");
        grid.store.filter({
            filterFn: function(record) {
                return category.test(record.get('userid')) ||
                    category.test(record.get('username')) ||
                    category.test(record.get('isactive'));
            }
        });
    }
					}
				}
			}, {
				xtype : 'button',
				text : 'Clear',
				action : 'clearuser',
				width : 70,
				margin : '10px'
			}, /*{
				xtype : 'button',
				text : 'Search',
				action : 'searchuser',
				width : 70,
				margin : '10px',
				
			}, {
				xtype : 'hidden',
				value : 2,
				name : 'type'
			}, {
				xtype : 'hidden',
				value : 'config.StoreUser',
				name : 'store'
			}*/ ]
		});
		this.callParent(arguments);
	}
});