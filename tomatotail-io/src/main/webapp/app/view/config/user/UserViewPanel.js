Ext.define('TomTail.view.config.user.UserViewPanel', {
	extend: 'Ext.form.Panel',
	alias: 'widget.userviewpanel',
	width: 500,
	//title: 'Add/Edit User ',
	layout: 'fit',
	autoScroll:true,
    initComponent: function() {
		Ext.apply(this, {
				bodyStyle: 'padding: 10px;',
				layout:'hbox',
				height:'100px',
				defaults:{
					labelClsExtra:'vehicleLbl',
					labelWidth:'100px',
					padding:3
				},
				items: [{
					 height:'80px',
					 autoScroll:true,
					 flex:1,
					 border:0,
					items:[{
			            xtype: 'displayfield',
			            name: 'userid',
			            fieldLabel: 'User Id'

			        },
					   {
						 fieldLabel:'User Name',
						 xtype:'displayfield',
						 name:'username'
					 },{
						 fieldLabel:'Phone',
						 xtype:'displayfield',
						 name:'userphone'
					 },{
						 fieldLabel:'Email',
						 xtype:'displayfield',
						 name:'useremail'
					 }]
				 }
	            ]
		});

		this.callParent(arguments);
	}
});