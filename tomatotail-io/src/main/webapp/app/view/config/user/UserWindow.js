Ext.define('TomTail.view.config.user.UserWindow', {
	extend : 'Ext.window.Window',
	alias : 'widget.userwindow',
	title : 'Add User',
	width: 650,
	//modal: true,
	requires : [ 'TomTail.view.config.user.UserEditPanel' ],
	listeners:{
		close:function(){
			Ext.ComponentQuery.query('#userPanel')[0].enable();
			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
		Ext.ComponentQuery.query('#header')[0].enable(); 
		}
	},
	initComponent : function() {
		Ext.apply(this, {
			items : [ {
				xtype : 'usereditpanel'
			} ]
		});
		this.callParent(arguments);
	}
});