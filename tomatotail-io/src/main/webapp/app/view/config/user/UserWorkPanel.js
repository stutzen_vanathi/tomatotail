Ext.define('TomTail.view.config.user.UserWorkPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.userconfigpanel',
    requires:[
    'TomTail.view.config.user.UserSearchPanel',
    'TomTail.view.config.user.UserGrid',
    'TomTail.view.config.user.UserController',
    'TomTail.view.config.user.UserModel',
	'TomTail.view.config.user.UserEditPanel'
    ],
    viewModel: 'user',
    controller: 'user',
    layout: {
        type: 'vbox',
        align: 'center'
    },
    bodyStyle:'background:url(../../../../images/bg_page.png) !important',
    items:[{
        xtype: 'usersearchpanel',
        //collapsible:true,
        height:102,
        width:'100%'
        	 },{
        xtype: 'usergrid',
       // collapsible:true,
        flex:2,
        width:'100%'
        
    }],
    initComponent : function() {
    this.callParent(arguments);
    }
});