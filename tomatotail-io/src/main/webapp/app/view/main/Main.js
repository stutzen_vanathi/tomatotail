/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('TomTail.view.main.Main', {
    extend: 'Ext.tab.Panel',
    requires: [
        'TomTail.view.main.MainController',
        'TomTail.view.main.MainModel',
        'TomTail.view.config.brands.BrandWorkPanel',
        'TomTail.view.config.manufacturer.ManufacturerWorkPanel',
        "TomTail.view.config.competitorsAnalysis.CompetitorsAnalysisWorkPanel",
        'TomTail.view.config.products.ProductWorkPanel',
        'TomTail.view.config.category.CategoryWorkPanel',
        'TomTail.view.config.user.UserWorkPanel',
        'TomTail.view.config.competitors.CompetitorsWorkPanel',
        "TomTail.view.orders.Orders",
		"TomTail.view.config.profile.EditProfile",
		"TomTail.view.config.channelpartnerproduct.Product",
		'TomTail.view.config.billy.puppy.PuppyPanel',
		'TomTail.view.config.billy.history.HistoryPanel',
		'TomTail.view.config.feedback.FeedbackPanel',
		"TomTail.view.orders.NewOrdersPanel",
		"TomTail.view.orders.ProcessedOrdersPanel",
		"TomTail.view.config.orderrevision.OrderrevisionWorkPanel",
		"TomTail.view.config.outletproductrequest.RequestProduct",
		"TomTail.view.config.reviewproductrequest.ReviewProduct"
    ],

    xtype: 'app-main',
    id: 'main',
    controller: 'main',
    viewModel: {
        type: 'main'
    },

    layout: {
        type: 'fit'
    },

    ui: 'navigation',
    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,
    tabBarPosition: 0,
    header: {
		itemId:'header',

        title: {
            text: '<div style="padding-left: 27%; font-weight:bold;"></div>',
            flex: 0,
            //<img style="float:left;  padding-right:8px;" src="images/tt-logo.png" />&nbsp;<div>Admin</div></font>
        }
    },

    tabBar: {
        flex: 1,
        autoScroll: true,
		itemId:'tabbar',
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        },

    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top',
        },
        wide: {
            headerPosition: 'left',
        }
    },
    viewConfig: {
        autoScroll: true
    },

    defaults: {
        tabConfig: {
            plugins: 'responsive',

            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left',
                    // width: 180
                },
                tall: {
                    iconAlign: 'left',
                    textAlign: 'left',
                    //width: 120
                }
            }
        }
    },
	activeTab: 1,
    items: [{
		title:'HiddenTab',
		itemId: 'hiddenTab',
		hidden:true
	},{
        title: 'Home',
        icon: 'images/home.png',
        itemId: 'homeTab',
        cls:'bg-logo-home',
		//xtype:'newordersPanel',
        listeners: {
            activate: function() {
                destroyViewport();

            }
        }
    }, {
        title: 'Competitors Analysis',
        icon: 'images/icon-arrowup.png',
        itemId: 'CompetitorTab',
        padding:20,
        style:'background:url(../../../../images/bg_page.png);',
        xtype: 'competitorsanalysisconfigpanel',
        listeners: {
            activate: function() {
                destroyViewport();

            }
        }
    }, /* {
        title: 'Orders',
        icon: 'images/cart-icon.png',
        itemId: 'OrdersTab',
        xtype: 'ordersPanel',
        listeners: {
            activate: function() {
                destroyViewport();

            }
        }
    }, */{
        title: 'Product Price',
        icon: 'images/analysis2.png',
        itemId: 'PartnerproductTab',
        xtype: 'partnerproductPanel',
        style:'background:url(../../../../images/bg_page.png);',
        padding:20,
        listeners: {
            activate: function() {
                destroyViewport();

            }
        }
    },{
        title: 'Product Requests',
        icon: 'images/package_icon.png',
        xtype:'reviewproductPanel',
        style:'background:url(../../../../images/bg_page.png);',
        padding:20,
        listeners: {
            activate: function() {
                destroyViewport();

            }
        }
    },{
        title: 'Request Products',
        icon: 'images/list.png',
        xtype: 'requestproductPanel',
        style:'background:url(../../../../images/bg_page.png);',
        padding:20,
        listeners: {
            activate: function() {
                destroyViewport();

            }
        }
    },/*{
        title: 'Feedback',
        icon: 'images/edit_property.png',
        cls:'bg-logo-home',
        //style:'background:url(../../../../images/bg_page.png);',
        //itemId: '',
        //xtype: 'feedbackPanel',
        listeners: {
            activate: function() {
                destroyViewport();

            }
        }
    }*/],
	
	dockedItems:[{
			cls:'toolbar',
        tbar: ['->',{
            xtype: 'component',
            html: '<div style="margin-top:5px;"><img style="float:left;  padding-right:8px;" src="images/tt-logo.png" />&nbsp;<div style="float: right;margin-top: 17px;color: #EE2F25;font-weight: bold;font-family:"Open Sans", "Helvetica Neue", helvetica, arial, verdana, sans-serif">Admin</div></div>',
            style: "margin-left:4.7%;",
        },
        '->',  {
            icon:'images/logout.png',
			scale:'large',
			tooltip:'Logout',
			itemId:'logoutPanel',
			cls:'logout',
			listeners: {
        click: function() {
            window.open('swf/logout', '_self');
        }
    }
        }],
	}],

    listeners: {
        afterrender: function(panel) {
            var bar = panel.tabBar;
            bar.insert(3, [{
                xtype: 'component',
                width: 235,
                //flex: 1
            }, {
                xtype: 'panel',
                layout: {
                    type: 'accordion',
                    //titleCollapse: false,
                    animate: true,
                },
                items: [{
                    hidden: true
                }, {
                    bodyStyle: 'background-color:#444444',
                    icon: 'images/Bullet.png',
                    iconAlign: 'left',
                    title: '<div align="left" style="margin-left:18px">Configuration</div>',
                    layout: 'vbox',
                    itemId: 'Accordion',
                    items: [{
                        xtype: 'button',
                        text: 'Manufacturer',
                        scale: 'large',
                        itemId: 'manufacturerButton',
                        width: 238,
                        style: 'padding-left:10%; padding-top:5px',
                        iconAlign: 'left',
                        textAlign: 'left',
                        cls: 'button',
                        overCls: 'my-over',
                        toggleGroup: 'ratings',
                        pressedCls: 'pressed',
                        icon: 'images/manufacturer1.png',
                        handler: 'manufacturerView',
                    }, {
                        xtype: 'button',
                        text: 'Brands',
                        scale: 'large',
                        height:72,
                        width: 238,
                        itemId: 'brandsButton',
                        style: 'padding-left:10%; padding-top:5px',
                        iconAlign: 'left',
                        textAlign: 'left',
                        cls: 'button',
                        icon: 'images/brand.png',
                        overCls: 'my-over',
                        toggleGroup: 'ratings',
                        pressedCls: 'pressed',
                        handler: 'brandView',
                    }, {
                        xtype: 'button',
                        text: 'Category',
                        scale: 'large',
                        itemId: 'categoryButton',
                        height:72,
                        width: 238,
                        style: 'padding-left:10%; padding-top:5px',
                        iconAlign: 'left',
                        textAlign: 'left',
                        cls: 'button',
                        icon: 'images/category.png',
                        overCls: 'my-over',
                        toggleGroup: 'ratings',
                        pressedCls: 'pressed',
                        handler: 'categoryView'
                    }, {
                        xtype: 'button',
                        text: 'Competitors',
                        scale: 'large',
                        height:72,
                        width: 238,
                        itemId: 'competitorsButton',
                        style: 'padding-left:10%; padding-top:5px',
                        iconAlign: 'left',
                        textAlign: 'left',
                        cls: 'button',
                        icon: 'images/competitor.png',
                        overCls: 'my-over',
                        toggleGroup: 'ratings',
                        pressedCls: 'pressed',
                        handler: 'competitorsView'
                    }, {
                        xtype: 'button',
                        text: 'Products',
                        scale: 'large',
                        itemId: 'productsButton',
                        height:72,
                        width: 238,
                        style: 'padding-left:10%; padding-top:5px',
                        iconAlign: 'left',
                        textAlign: 'left',
                        cls: 'button',
                        icon: 'images/product1.png',
                        overCls: 'my-over',
                        toggleGroup: 'ratings',
                        pressedCls: 'pressed',
                        handler: 'productView'
                    }, {
                        xtype: 'button',
                        text: 'Users',
                        scale: 'large',
                        itemId: 'userButton',
                        height:72,
                        width: 238,
                        style: 'padding-left:10%; padding-top:5px',
                        iconAlign: 'left',
                        textAlign: 'left',
                        cls: 'button',
                        icon: 'images/user1.png',
                        overCls: 'my-over',
                        toggleGroup: 'ratings',
                        pressedCls: 'pressed',
                        handler: 'userView'
                    }, /*{
                        xtype: 'button',
                        text: 'History',
                        scale: 'large',
                        itemId: 'historyButton',
                        height:72,
                        width: 238,
                        style: 'padding-left:10%; padding-top:5px',
                        iconAlign: 'left',
                        textAlign: 'left',
                        cls: 'button',
                        icon: 'images/user1.png',
                        overCls: 'my-over',
                        toggleGroup: 'ratings',
                        pressedCls: 'pressed',
                        handler: 'historyView'
                    }*/],
                }],
            }]);
			
			bar.insert(3, [{
                xtype: 'component',
                width: 200,
                //flex: 1
            }, {
                xtype: 'panel',
                layout: {
                    type: 'accordion',
                    //titleCollapse: false,
                    animate: true,
                },
                items: [{
                    hidden: true
                }, {
                    bodyStyle: 'background-color:#444444',
                    icon: 'images/cart-icon.png',
                    iconAlign: 'left',
                    title: '<div align="left" style="margin-left:18px">Orders</div>',
                    layout: 'vbox',
                    itemId: 'OrderAccordion',
                    items: [{
                        xtype: 'button',
                        text: 'Unprocessed',
                        scale: 'large',
                        itemId: 'unprocessedButton',
                        width: 238,
                        style: 'padding-left:10%; padding-top:5px',
                        iconAlign: 'left',
                        textAlign: 'left',
                        cls: 'button',
                        overCls: 'my-over',
                        toggleGroup: 'ratings',
                        pressedCls: 'pressed',
                        icon: 'images/order-2.png',
                        handler: 'orderUnprocessed',
                    }, {
                        xtype: 'button',
                        text: 'Processed',
                        scale: 'large',
                        height:72,
                        width: 238,
                        itemId: 'processedButton',
                        style: 'padding-left:10%; padding-top:5px',
                        iconAlign: 'left',
                        textAlign: 'left',
                        cls: 'button',
                        icon: 'images/Review_icon.png',
                        overCls: 'my-over',
                        toggleGroup: 'ratings',
                        pressedCls: 'pressed',
                        handler: 'orderprocessed',
                    },{
                        xtype: 'button',
                        text: 'Orders',
                        scale: 'large',
                        itemId: 'orderrevisionButton',
                        height:72,
                        width: 238,
                        style: 'padding-left:10%; padding-top:5px',
                        iconAlign: 'left',
                        textAlign: 'left',
                        cls: 'button',
                        icon: 'images/Purchase_Order_icon.png',
                        overCls: 'my-over',
                        toggleGroup: 'ratings',
                        pressedCls: 'pressed',
                        handler: 'orderrevisionViewport'
                    }],
                }],
            }])
        },

    }


});

function destroyViewport() {
    if (Ext.ComponentQuery.query('#manufacturerPanel')[0] != undefined) {
        Ext.ComponentQuery.query('#manufacturerPanel')[0].destroy();
        console.log("manufacturerPanel destroyed");
    }

    if (Ext.ComponentQuery.query('#productPanel')[0] != undefined) {
        Ext.ComponentQuery.query('#productPanel')[0].destroy();
        console.log("productPanel destroyed");
    }
    if (Ext.ComponentQuery.query('#brandPanel')[0] != undefined) {
        Ext.ComponentQuery.query('#brandPanel')[0].destroy();
        console.log("brandPanel destroyed");
    }
    if (Ext.ComponentQuery.query('#categoryPanel')[0] != undefined) {
        Ext.ComponentQuery.query('#categoryPanel')[0].destroy();
        console.log("categoryPanel destroyed");
    }
    if (Ext.ComponentQuery.query('#userPanel')[0] != undefined) {
        Ext.ComponentQuery.query('#userPanel')[0].destroy();
        console.log("userPanel destroyed");
    }
    if (Ext.ComponentQuery.query('#competitorsPanel')[0] != undefined) {
        Ext.ComponentQuery.query('#competitorsPanel')[0].destroy();
        console.log("competitorsPanel destroyed");
    }
    if (Ext.ComponentQuery.query('#brandsButton')[0] != undefined) {
        Ext.ComponentQuery.query('#brandsButton')[0].enable();
        Ext.ComponentQuery.query('#brandsButton')[0].toggle(false);

    }
    if (Ext.ComponentQuery.query('#manufacturerButton')[0] != undefined) {
        Ext.ComponentQuery.query('#manufacturerButton')[0].enable();
        Ext.ComponentQuery.query('#manufacturerButton')[0].toggle(false);

    }
    if (Ext.ComponentQuery.query('#categoryButton')[0] != undefined) {
        Ext.ComponentQuery.query('#categoryButton')[0].enable();
        Ext.ComponentQuery.query('#categoryButton')[0].toggle(false);

    }
    if (Ext.ComponentQuery.query('#competitorsButton')[0] != undefined) {
        Ext.ComponentQuery.query('#competitorsButton')[0].enable();
        Ext.ComponentQuery.query('#competitorsButton')[0].toggle(false);

    }
    if (Ext.ComponentQuery.query('#productsButton')[0] != undefined) {
        Ext.ComponentQuery.query('#productsButton')[0].enable();
        Ext.ComponentQuery.query('#productsButton')[0].toggle(false);

    }
    if (Ext.ComponentQuery.query('#userButton')[0] != undefined) {
        Ext.ComponentQuery.query('#userButton')[0].enable();
        Ext.ComponentQuery.query('#userButton')[0].toggle(false);
    }
	if (Ext.ComponentQuery.query('#puppyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyButton')[0].enable();
			Ext.ComponentQuery.query('#puppyButton')[0].toggle(false);

    }
	if (Ext.ComponentQuery.query('#historyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#historyButton')[0].enable();
			Ext.ComponentQuery.query('#historyButton')[0].toggle(false);

    }		
	if (Ext.ComponentQuery.query('#historyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#historyPanel')[0].destroy();
            console.log("historyPanel destroyed");
    }
	if (Ext.ComponentQuery.query('#puppyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyPanel')[0].destroy();
            console.log("puppyPanel destroyed");
    }	
	if (Ext.ComponentQuery.query('#unprocessPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessPanel')[0].destroy();
        }
        if (Ext.ComponentQuery.query('#processedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#processedButton')[0].enable();
			Ext.ComponentQuery.query('#processedButton')[0].toggle(false);

        }
		if (Ext.ComponentQuery.query('#processedPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#processedPanel')[0].destroy();
            
        }
        if (Ext.ComponentQuery.query('#unprocessedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessedButton')[0].enable();
			Ext.ComponentQuery.query('#unprocessedButton')[0].toggle(false);

        }
        if (Ext.ComponentQuery.query('#orderrevisionButton')[0] != undefined) {
	        Ext.ComponentQuery.query('#orderrevisionButton')[0].enable();
	        Ext.ComponentQuery.query('#orderrevisionButton')[0].toggle(false);

	    }
	    if (Ext.ComponentQuery.query('#orderrevisionPanel')[0] != undefined) {
	        Ext.ComponentQuery.query('#orderrevisionPanel')[0].destroy();
	        console.log("orderrevisionPanel destroyed");
	    }

    Ext.ComponentQuery.query('#Accordion')[0].collapse();
	Ext.ComponentQuery.query('#OrderAccordion')[0].collapse();
	
}

