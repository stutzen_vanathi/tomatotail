/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('TomTail.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.window.MessageBox'
    ],

    alias: 'controller.main',
	
	manufacturerView: function() {
        var viewport = Ext.create('Ext.Viewport', {
            layout: 'fit',
            items: [{
                region: 'center',
                style: 'margin-left:16.5%;padding:15px 20px 0px 30px;margin-top:5%;background:url(../../../../images/bg_page.png) !important',
                itemId: 'manufacturerPanel',
                xtype: 'manufacturerconfigpanel'

            }]
        });
		Ext.ComponentQuery.query('#hiddenTab')[0].show();
        if (Ext.ComponentQuery.query('#brandPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#brandPanel')[0].destroy();
            console.log("brandPanel destroyed");
        }

        if (Ext.ComponentQuery.query('#categoryPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryPanel')[0].destroy();
            console.log("categoryPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#productPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#productPanel')[0].destroy();
            console.log("productPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#competitorsPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsPanel')[0].destroy();
            console.log("competitorsPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#userPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#userPanel')[0].destroy();
            console.log("userPanel destroyed");
        }

        if (Ext.ComponentQuery.query('#manufacturerButton')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerButton')[0].disable();

        }
        if (Ext.ComponentQuery.query('#brandsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#brandsButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#unprocessPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessPanel')[0].destroy();
        }
        if (Ext.ComponentQuery.query('#processedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#processedButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#processedPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#processedPanel')[0].destroy();
            
        }
        if (Ext.ComponentQuery.query('#unprocessedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessedButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#categoryButton')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#competitorsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#productsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#productsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#userButton')[0] != undefined) {
            Ext.ComponentQuery.query('#userButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#puppyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#historyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#historyButton')[0].enable();

        }		
		if (Ext.ComponentQuery.query('#historyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#historyPanel')[0].destroy();
            console.log("historyPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#puppyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyPanel')[0].destroy();
            console.log("puppyPanel destroyed");
        }
		  if (Ext.ComponentQuery.query('#orderrevisionButton')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionButton')[0].enable();

		    }
		    if (Ext.ComponentQuery.query('#orderrevisionPanel')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionPanel')[0].destroy();
		        console.log("orderrevisionPanel destroyed");
		    }


    },
    brandView: function() {
        var viewport = Ext.create('Ext.Viewport', {
            layout: 'fit',
            items: [{
                region: 'center',
                style: 'margin-left:16.5%;padding:15px 20px 0px 30px;margin-top:5%;background:url(../../../../images/bg_page.png) !important',
                itemId: 'brandPanel',
                xtype: 'brandconfigpanel'

            }]
        });
		Ext.ComponentQuery.query('#hiddenTab')[0].show();
        if (Ext.ComponentQuery.query('#manufacturerPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerPanel')[0].destroy();
            console.log("manufacturerPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#brandsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#brandsButton')[0].disable();

        }
        if (Ext.ComponentQuery.query('#manufacturerButton')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#categoryButton')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#competitorsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#productsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#productsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#userButton')[0] != undefined) {
            Ext.ComponentQuery.query('#userButton')[0].enable();

        }
		
		if (Ext.ComponentQuery.query('#unprocessPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessPanel')[0].destroy();
        }
        if (Ext.ComponentQuery.query('#processedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#processedButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#processedPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#processedPanel')[0].destroy();
            
        }
        if (Ext.ComponentQuery.query('#unprocessedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessedButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#categoryPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryPanel')[0].destroy();
            console.log("categoryPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#productPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#productPanel')[0].destroy();
            console.log("productPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#competitorsPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsPanel')[0].destroy();
            console.log("competitorsPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#userPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#userPanel')[0].destroy();
            console.log("userPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#puppyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#historyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#historyButton')[0].enable();

        }		
		if (Ext.ComponentQuery.query('#historyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#historyPanel')[0].destroy();
            console.log("historyPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#puppyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyPanel')[0].destroy();
            console.log("puppyPanel destroyed");
        }
		  if (Ext.ComponentQuery.query('#orderrevisionButton')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionButton')[0].enable();

		    }
		    if (Ext.ComponentQuery.query('#orderrevisionPanel')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionPanel')[0].destroy();
		        console.log("orderrevisionPanel destroyed");
		    }
    },
	
    productView: function() {
        var viewport = Ext.create('Ext.Viewport', {
            layout: 'fit',
            items: [{
                region: 'center',
                style: 'margin-left:16.5%;padding:15px 20px 0px 30px;margin-top:5%;background:url(../../../../images/bg_page.png) !important',
                itemId: 'productPanel',
                xtype: 'productconfigpanel'

            }]
        });
		Ext.ComponentQuery.query('#hiddenTab')[0].show();
        if (Ext.ComponentQuery.query('#manufacturerPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerPanel')[0].destroy();
            console.log("manufacturerPanel destroyed");
        }

        if (Ext.ComponentQuery.query('#categoryPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryPanel')[0].destroy();
            console.log("categoryPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#brandPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#brandPanel')[0].destroy();
            console.log("brandPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#competitorsPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsPanel')[0].destroy();
            console.log("competitorsPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#userPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#userPanel')[0].destroy();
            console.log("userPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#unprocessPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessPanel')[0].destroy();
        }
        if (Ext.ComponentQuery.query('#processedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#processedButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#processedPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#processedPanel')[0].destroy();
            
        }
        if (Ext.ComponentQuery.query('#unprocessedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessedButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#productsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#productsButton')[0].disable();

        }
        if (Ext.ComponentQuery.query('#manufacturerButton')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#categoryButton')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#competitorsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#brandsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#brandsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#userButton')[0] != undefined) {
            Ext.ComponentQuery.query('#userButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#puppyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#historyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#historyButton')[0].enable();

        }		
		if (Ext.ComponentQuery.query('#historyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#historyPanel')[0].destroy();
            console.log("historyPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#puppyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyPanel')[0].destroy();
            console.log("puppyPanel destroyed");
        }
		  if (Ext.ComponentQuery.query('#orderrevisionButton')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionButton')[0].enable();

		    }
		    if (Ext.ComponentQuery.query('#orderrevisionPanel')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionPanel')[0].destroy();
		        console.log("orderrevisionPanel destroyed");
		    }
    },
	
	 orderUnprocessed: function() {
        var viewport = Ext.create('Ext.Viewport', {
            layout: 'fit',
            items: [{
                region: 'center',
                style: 'margin-left:16.5%;padding:15px 20px 0px 30px;margin-top:5%;background:url(../../../../images/bg_page.png) !important',
                itemId: 'unprocessPanel',
				xtype: 'ordersPanel'
				              

            }]
        });
		Ext.ComponentQuery.query('#hiddenTab')[0].show();
        if (Ext.ComponentQuery.query('#manufacturerPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerPanel')[0].destroy();
            console.log("manufacturerPanel destroyed");
        }

        if (Ext.ComponentQuery.query('#categoryPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryPanel')[0].destroy();
            console.log("categoryPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#brandPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#brandPanel')[0].destroy();
            console.log("brandPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#competitorsPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsPanel')[0].destroy();
            console.log("competitorsPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#userPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#userPanel')[0].destroy();
            console.log("userPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#productPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#productPanel')[0].destroy();
            console.log("productPanel destroyed");
        }
		
        if (Ext.ComponentQuery.query('#processedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#processedButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#processedPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#processedPanel')[0].destroy();
            
        }
        if (Ext.ComponentQuery.query('#unprocessedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessedButton')[0].disable();

        }
        if (Ext.ComponentQuery.query('#manufacturerButton')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#categoryButton')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#competitorsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#brandsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#brandsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#productsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#productsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#userButton')[0] != undefined) {
            Ext.ComponentQuery.query('#userButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#puppyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#historyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#historyButton')[0].enable();

        }		
		if (Ext.ComponentQuery.query('#historyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#historyPanel')[0].destroy();
            console.log("historyPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#puppyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyPanel')[0].destroy();
            console.log("puppyPanel destroyed");
        }
		  if (Ext.ComponentQuery.query('#orderrevisionButton')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionButton')[0].enable();

		    }
		    if (Ext.ComponentQuery.query('#orderrevisionPanel')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionPanel')[0].destroy();
		        console.log("orderrevisionPanel destroyed");
		    }
    },

	
	orderprocessed: function() {
        var viewport = Ext.create('Ext.Viewport', {
            layout: 'fit',
            items: [{
                region: 'center',
                style: 'margin-left:16.5%;padding:15px 20px 0px 30px;margin-top:5%;background:url(../../../../images/bg_page.png) !important',
                itemId: 'processedPanel',
                xtype:'processedordersPanel',

            }]
        });
		Ext.ComponentQuery.query('#hiddenTab')[0].show();
        if (Ext.ComponentQuery.query('#manufacturerPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerPanel')[0].destroy();
            console.log("manufacturerPanel destroyed");
        }

        if (Ext.ComponentQuery.query('#categoryPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryPanel')[0].destroy();
            console.log("categoryPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#productPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#productPanel')[0].destroy();
            console.log("productPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#brandPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#brandPanel')[0].destroy();
            console.log("brandPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#competitorsPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsPanel')[0].destroy();
            console.log("competitorsPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#userPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#userPanel')[0].destroy();
            console.log("userPanel destroyed");
        }
       
        if (Ext.ComponentQuery.query('#manufacturerButton')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#categoryButton')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#unprocessPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessPanel')[0].destroy();
        }
        if (Ext.ComponentQuery.query('#processedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#processedButton')[0].disable();

        }

        if (Ext.ComponentQuery.query('#unprocessedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessedButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#competitorsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#brandsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#brandsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#userButton')[0] != undefined) {
            Ext.ComponentQuery.query('#userButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#productsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#productsButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#puppyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#historyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#historyButton')[0].enable();

        }		
		if (Ext.ComponentQuery.query('#historyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#historyPanel')[0].destroy();
            console.log("historyPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#puppyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyPanel')[0].destroy();
            console.log("puppyPanel destroyed");
        }
		  if (Ext.ComponentQuery.query('#orderrevisionButton')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionButton')[0].enable();

		    }
		    if (Ext.ComponentQuery.query('#orderrevisionPanel')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionPanel')[0].destroy();
		        console.log("orderrevisionPanel destroyed");
		    }
    },
	
    categoryView: function() {
        var viewport = Ext.create('Ext.Viewport', {
            layout: 'fit',
            items: [{
                region: 'center',
                style: 'margin-left:16.5%;padding:15px 20px 0px 30px;margin-top:5%;background:url(../../../../images/bg_page.png) !important',
                itemId: 'categoryPanel',
                xtype: 'categoryconfigpanel'

            }]
        });
		Ext.ComponentQuery.query('#hiddenTab')[0].show();
        if (Ext.ComponentQuery.query('#manufacturerPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerPanel')[0].destroy();
            console.log("manufacturerPanel destroyed");
        }

        if (Ext.ComponentQuery.query('#productPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#productPanel')[0].destroy();
            console.log("productPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#brandPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#brandPanel')[0].destroy();
            console.log("brandPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#competitorsPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsPanel')[0].destroy();
            console.log("competitorsPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#userPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#userPanel')[0].destroy();
            console.log("userPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#productsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#productsButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#unprocessPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessPanel')[0].destroy();
        }
        if (Ext.ComponentQuery.query('#processedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#processedButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#processedPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#processedPanel')[0].destroy();
            
        }
        if (Ext.ComponentQuery.query('#unprocessedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessedButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#manufacturerButton')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#categoryButton')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryButton')[0].disable();

        }
        if (Ext.ComponentQuery.query('#competitorsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#brandsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#brandsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#userButton')[0] != undefined) {
            Ext.ComponentQuery.query('#userButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#puppyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#historyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#historyButton')[0].enable();

        }		
		if (Ext.ComponentQuery.query('#historyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#historyPanel')[0].destroy();
            console.log("historyPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#puppyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyPanel')[0].destroy();
            console.log("puppyPanel destroyed");
        }
		  if (Ext.ComponentQuery.query('#orderrevisionButton')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionButton')[0].enable();

		    }
		    if (Ext.ComponentQuery.query('#orderrevisionPanel')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionPanel')[0].destroy();
		        console.log("orderrevisionPanel destroyed");
		    }

    },

    competitorsView: function() {
        var viewport = Ext.create('Ext.Viewport', {
            layout: 'fit',
            items: [{
                region: 'center',
                style: 'margin-left:16.5%;padding:15px 20px 0px 30px;margin-top:5%;background:url(../../../../images/bg_page.png) !important',
                itemId: 'competitorsPanel',
                xtype: 'competitorsconfigpanel'

            }]
        });
		Ext.ComponentQuery.query('#hiddenTab')[0].show();
        if (Ext.ComponentQuery.query('#manufacturerPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerPanel')[0].destroy();
            console.log("manufacturerPanel destroyed");
        }

        if (Ext.ComponentQuery.query('#productPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#productPanel')[0].destroy();
            console.log("productPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#brandPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#brandPanel')[0].destroy();
            console.log("brandPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#categoryPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryPanel')[0].destroy();
            console.log("categoryPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#userPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#userPanel')[0].destroy();
            console.log("userPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#productsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#productsButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#unprocessPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessPanel')[0].destroy();
        }
        if (Ext.ComponentQuery.query('#processedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#processedButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#processedPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#processedPanel')[0].destroy();
            
        }
        if (Ext.ComponentQuery.query('#unprocessedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessedButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#manufacturerButton')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#categoryButton')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#competitorsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsButton')[0].disable();

        }
        if (Ext.ComponentQuery.query('#brandsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#brandsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#userButton')[0] != undefined) {
            Ext.ComponentQuery.query('#userButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#puppyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#historyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#historyButton')[0].enable();

        }		
		if (Ext.ComponentQuery.query('#historyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#historyPanel')[0].destroy();
            console.log("historyPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#puppyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyPanel')[0].destroy();
            console.log("puppyPanel destroyed");
        }
		  if (Ext.ComponentQuery.query('#orderrevisionButton')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionButton')[0].enable();

		    }
		    if (Ext.ComponentQuery.query('#orderrevisionPanel')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionPanel')[0].destroy();
		        console.log("orderrevisionPanel destroyed");
		    }
    },

    userView: function() {
        var viewport = Ext.create('Ext.Viewport', {
            layout: 'fit',
            items: [{
                region: 'center',
                style: 'margin-left:16.5%;padding:15px 20px 0px 30px;margin-top:5%;background:url(../../../../images/bg_page.png) !important',
                itemId: 'userPanel',
                xtype: 'userconfigpanel'

            }]
        });
		Ext.ComponentQuery.query('#hiddenTab')[0].show();
        if (Ext.ComponentQuery.query('#manufacturerPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerPanel')[0].destroy();
            console.log("manufacturerPanel destroyed");
        }

        if (Ext.ComponentQuery.query('#productPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#productPanel')[0].destroy();
            console.log("productPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#brandPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#brandPanel')[0].destroy();
            console.log("brandPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#categoryPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryPanel')[0].destroy();
            console.log("categoryPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#competitorsPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsPanel')[0].destroy();
            console.log("competitorsPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#productsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#productsButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#unprocessPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessPanel')[0].destroy();
        }
        if (Ext.ComponentQuery.query('#processedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#processedButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#processedPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#processedPanel')[0].destroy();
            
        }
        if (Ext.ComponentQuery.query('#unprocessedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessedButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#manufacturerButton')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#categoryButton')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#competitorsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#brandsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#brandsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#userButton')[0] != undefined) {
            Ext.ComponentQuery.query('#userButton')[0].disable();

        }
		if (Ext.ComponentQuery.query('#puppyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#historyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#historyButton')[0].enable();

        }		
		if (Ext.ComponentQuery.query('#historyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#historyPanel')[0].destroy();
            console.log("historyPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#puppyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyPanel')[0].destroy();
            console.log("puppyPanel destroyed");
        }
		  if (Ext.ComponentQuery.query('#orderrevisionButton')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionButton')[0].enable();

		    }
		    if (Ext.ComponentQuery.query('#orderrevisionPanel')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionPanel')[0].destroy();
		        console.log("orderrevisionPanel destroyed");
		    }
    },

	historyView: function() { 
        var viewport = Ext.create('Ext.Viewport', {
            layout: 'fit',
            items: [{
                region: 'center',
                style: 'margin-left:16.5%;padding:15px 20px 0px 30px;margin-top:5%;background:url(../../../../images/bg_page.png) !important',
                itemId: 'historyPanel',
				html:'History panel',
                //xtype: ''

            }]
        });
		Ext.ComponentQuery.query('#hiddenTab')[0].show();
        if (Ext.ComponentQuery.query('#manufacturerPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerPanel')[0].destroy();
            console.log("manufacturerPanel destroyed");
        }
		
        if (Ext.ComponentQuery.query('#productPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#productPanel')[0].destroy();
            console.log("productPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#brandPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#brandPanel')[0].destroy();
            console.log("brandPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#competitorsPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsPanel')[0].destroy();
            console.log("competitorsPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#userPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#userPanel')[0].destroy();
            console.log("userPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#puppyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyPanel')[0].destroy();
            console.log("puppyPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#unprocessPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessPanel')[0].destroy();
        }
        if (Ext.ComponentQuery.query('#processedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#processedButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#processedPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#processedPanel')[0].destroy();
            
        }
        if (Ext.ComponentQuery.query('#unprocessedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessedButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#productsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#productsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#manufacturerButton')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#historyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#historyButton')[0].disable();

        }
		if (Ext.ComponentQuery.query('#categoryButton')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#competitorsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#brandsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#brandsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#userButton')[0] != undefined) {
            Ext.ComponentQuery.query('#userButton')[0].enable();

        }
		 if (Ext.ComponentQuery.query('#puppyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyButton')[0].enable();

        }		 
		  if (Ext.ComponentQuery.query('#orderrevisionButton')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionButton')[0].enable();

		    }
		    if (Ext.ComponentQuery.query('#orderrevisionPanel')[0] != undefined) {
		        Ext.ComponentQuery.query('#orderrevisionPanel')[0].destroy();
		        console.log("orderrevisionPanel destroyed");
		    }

    },
	
	
    
    orderrevisionViewport: function() {
        var viewport = Ext.create('Ext.Viewport', {
            layout: 'fit',
            items: [{
                region: 'center',
                style: 'margin-left:16.5%;padding:15px 20px 0px 30px;margin-top:5%;background:url(../../../../images/bg_page.png) !important',
                itemId: 'orderrevisionPanel',
				xtype:'orderrevisionworkpanel'

            }]
        });
		Ext.ComponentQuery.query('#hiddenTab')[0].show();
        if (Ext.ComponentQuery.query('#manufacturerPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerPanel')[0].destroy();
            console.log("manufacturerPanel destroyed");
        }
		
        if (Ext.ComponentQuery.query('#productPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#productPanel')[0].destroy();
            console.log("productPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#brandPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#brandPanel')[0].destroy();
            console.log("brandPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#unprocessPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessPanel')[0].destroy();
        }
        if (Ext.ComponentQuery.query('#processedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#processedButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#processedPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#processedPanel')[0].destroy();
            
        }
        if (Ext.ComponentQuery.query('#unprocessedButton')[0] != undefined) {
            Ext.ComponentQuery.query('#unprocessedButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#competitorsPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsPanel')[0].destroy();
            console.log("competitorsPanel destroyed");
        }
        if (Ext.ComponentQuery.query('#userPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#userPanel')[0].destroy();
            console.log("userPanel destroyed");
        }
		
        if (Ext.ComponentQuery.query('#productsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#productsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#manufacturerButton')[0] != undefined) {
            Ext.ComponentQuery.query('#manufacturerButton')[0].enable();

        }
        
        if (Ext.ComponentQuery.query('#competitorsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#competitorsButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#brandsButton')[0] != undefined) {
            Ext.ComponentQuery.query('#brandsButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#categoryButton')[0] != undefined) {
            Ext.ComponentQuery.query('#categoryButton')[0].enable();

        }
        if (Ext.ComponentQuery.query('#userButton')[0] != undefined) {
            Ext.ComponentQuery.query('#userButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#puppyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyButton')[0].enable();

        }
		if (Ext.ComponentQuery.query('#orderrevisionButton')[0] != undefined) {
            Ext.ComponentQuery.query('#orderrevisionButton')[0].disable();

        }
		if (Ext.ComponentQuery.query('#historyButton')[0] != undefined) {
            Ext.ComponentQuery.query('#historyButton')[0].enable();

        }		
		if (Ext.ComponentQuery.query('#historyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#historyPanel')[0].destroy();
            console.log("historyPanel destroyed");
        }
		if (Ext.ComponentQuery.query('#puppyPanel')[0] != undefined) {
            Ext.ComponentQuery.query('#puppyPanel')[0].destroy();
            console.log("puppyPanel destroyed");
        }

    },
    
  

});