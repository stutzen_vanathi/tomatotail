Ext.define('TomTail.view.orders.AddNewCustomer', {
    extend: 'Ext.window.Window',
    alias: 'widget.addcustomer',
    width: 400,
    layout: 'vbox',
    title: 'Add Customer',
    bodyStyle: 'padding:10px;',
    items: [{
        xtype: 'form',
        items: [{
            xtype: 'hidden',
            name: 'id'
        }, {
            xtype: 'textfield',
            fieldLabel: 'Name',
            allowBlank: false,
            name: 'name',
            listeners: {
                afterrender: function(field) {
                    Ext.defer(function() {
                        field.focus(true, 100);
                    }, 1);
                }
            }
        }, {
            xtype: 'textfield',
            fieldLabel: 'Mobile',
            allowBlank: false,
            name: 'mobile'
        }, {
            xtype: 'hidden',
            name: 'address',
        }, {
            xtype: 'textareafield',
            fieldLabel: 'Address',
            allowBlank: false,
            name: 'addressDetails'
        }, {
            xtype: 'textfield',
            fieldLabel: 'State',
            allowBlank: false,
            name: 'state'
        }, {
            xtype: 'textfield',
            fieldLabel: 'City',
            allowBlank: false,
            name: 'city'
        },{
            xtype: 'textfield',
            fieldLabel: 'Country',
            allowBlank: false,
            name: 'country'
        }, {
            xtype: 'textfield',
            fieldLabel: 'Pin Code',
            allowBlank: false,
            name: 'pincode'
        }, {
            xtype: 'textfield',
            fieldLabel: 'Email',
           // allowBlank: false,
            vtype: 'email',
            name: 'email'
        }, {
            xtype: 'textareafield',
            fieldLabel: 'Comments',
            name: 'comments'
        }, {
            xtype: 'checkbox',
            //colspan: 2,
            fieldLabel: 'Enable',
            name: 'isActive',
            checked: true,
            value: true,
            inputValue: 1,
            uncheckedValue: 0
        }],
    }],


    buttons: [{
        text: 'Save',
        handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            console.log("form" + form);
            if (form.isValid()) {
            		
                var url = 'customer/modify';
                if (form.getForm().findField('id').getValue() == null || form.getForm().findField('id').getValue() == '') {
                    url = 'customer/save';
                }
                var addrDetails = new Array();
                addrDetails.push({
                	id:form.getForm().findField('id').getValue(),
                    address: form.getForm().findField('addressDetails').getValue(),
                    city: form.getForm().findField('city').getValue(),
                    email: form.getForm().findField('email').getValue(),
                    pincode: form.getForm().findField('pincode').getValue(),
                    state: form.getForm().findField('state').getValue(),
                    country:form.getForm().findField('country').getValue(),
                    isActive: form.getForm().findField('isActive').getValue(),
                    comments: form.getForm().findField('comments').getValue(),
                });
                var custDetails = Ext.JSON.encode(addrDetails);
                /* var customerDetails = new Array();
				customerDetails.push({
							name: form.getForm().findField('name').getValue(),
							mobile: form.getForm().findField('mobile').getValue(),
							isActive: form.getForm().findField('isActive').getValue(),
							comments: form.getForm().findField('comments').getValue(),
							address: addrDetails
						});
					var formData = Ext.encode(customerDetails);
					console.log('formData'+formData) */
                var store=Ext.getStore('custpickerStore');
                Ext.Ajax.request({
                    url: url,
                    method: 'POST',
                    jsonData: {
                        "id": form.getForm().findField('id').getValue(),
                        "name": form.getForm().findField('name').getValue(),
                        "mobile": form.getForm().findField('mobile').getValue(),
                        "isActive": form.getForm().findField('isActive').getValue(),
                        "comments": form.getForm().findField('comments').getValue(),
                        "address": addrDetails
                    },

                    success: function(result, request) {
                        Ext.MessageBox.show({
                            title: 'Success',
                            msg: 'Successfully saved...',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        })
                        store.proxy.url="customer/listAll?name="+form.getForm().findField('name').getValue()+"&mobileNo="+form.getForm().findField('mobile').getValue();
                        	store.load();
                        if (win) {
                            win.close();
                            
                        }
                    },
                    failure: function(result, request) {
                        Ext.MessageBox.show({
                            title: 'Failure',
                            msg: 'Invalid Data',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR
                        })
                    }
                });
            }

        }
    }]
});