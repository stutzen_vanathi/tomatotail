Ext.define('TomTail.view.orders.BillingGrid', {
    extend: 'Ext.grid.Panel',

    alias: 'widget.billingGrid',
    //itemId: 'productgrid',
    width : '100%',
    autoScroll: true,
    controller: "orders-orders",
    viewModel: {
        type: "orders-orders"
    },
    requires: [
        'TomTail.view.config.products.ProductAddWindow',
        'Ext.grid.plugin.RowEditing',
		'Ext.data.identifier.Negative',
		'TomTail.view.orders.ProductCartWin'
    ],
	
    store: Ext.create('Ext.data.Store', {
        storeId: 'billingStore',
        fields: [{
            name: 'id',

        }, {
            name: 'productName',
        }, {
            name: 'sku',
        }, {
            name: 'mrpPrice',
        }, {
            name: 'currentPrice',
        },{
            name: 'requiredPrice',
        }, {
            name: 'requiredQty',
        }, {
            name: 'total',
            type: 'int',
            convert: function(val, row) {
                return row.data.currentPrice * row.data.requiredQty;
            }
        }],

    }),
 /*initComponent : function() {
		this.editing = Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToMoveEditor: 1,
        autoCancel: false
    });
		Ext.apply(this, {
    features : [{
    			ftype : 'summary',
    		}],
	plugins: [this.editing],
	selType: 'rowmodel',*/
	border : true,
			frame : true,
			split : true,
			autoScroll : true,
			/*features : [{
    			ftype : 'summary',
    		}],*/
    columns: [{
        xtype: 'rownumberer',
    }, {
    	text:'id',
    	hidden:true,
    },{
        text: 'SKU',
        menuDisabled:true,
        dataIndex: 'sku',
        sortable: false,
		editor: {
            xtype: 'combobox',
            editable:false,
            allowBlank: false,
            typeAhead: true,
            name: 'name',
            emptyText: 'Please select...',
            value: '',
            //forceSelection: true,
            selectOnFocus: true,
            store: new Ext.data.Store({
                // storeId:'',
                fields: ['name', 'sku'],
                proxy: {
                    type: 'ajax',
                    url: 'product/list',
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'sku',
            valueField: 'sku',
            listeners: {
                buffer: 50,
                change: function() {
                    var store = this.store;
                    //store.suspendEvents();
                    store.clearFilter();
                    //store.resumeEvents();
                    store.filter({
                        property: 'sku',
                        anyMatch: true,
                        value: this.getValue()
                    });
                }
            }

        },
		
    }, {
        text: 'Product',
        dataIndex: 'productName',
        sortable: false,
        menuDisabled:true,
		editor: {
            xtype: 'combobox',
            editable:false,
            allowBlank: false,
            typeAhead: true,
            name: 'name',
            emptyText: 'Please select...',
            value: '',
            //forceSelection: true,
            selectOnFocus: true,
            store: new Ext.data.Store({
                // storeId:'',
                fields: ['name', 'sku'],
                proxy: {
                    type: 'ajax',
                    url: 'product/list',
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'name',
            listeners: {
                buffer: 50,
                change: function() {
                    var store = this.store;
                    //store.suspendEvents();
                    store.clearFilter();
                    //store.resumeEvents();
                    store.filter({
                        property: 'name',
                        anyMatch: true,
                        value: this.getValue()
                    });
                }
            }

        },
        flex: 1,
		//renderer:Ext.util.Format.dateRenderer('m/d/Y g:i A')
    }, {
        text: 'Qty',
        dataIndex: 'requiredQty',
        sortable: false,
        flex: 1,
        menuDisabled:true,
		editor:{
			xtype:'numberfield',
			itemId:'qty',
			minValue:1,
			 name: 'requiredQty',
		}
    }, {
        text: 'MRP',
        dataIndex: 'mrpPrice',
        sortable: false,
        flex: 1,
        menuDisabled:true,
		editor:{
			xtype:'numberfield',
			itemId:'mrp',
			minValue:1,
			 name: 'mrpPrice',
		}
    },{
        text: 'TTPrice',
        dataIndex: 'currentPrice',
        sortable: false,
        menuDisabled:true,
        flex: 1,
		
		editor:{
			xtype:'numberfield',
			itemId:'cprice',
			minValue:1,
			 name: 'currentPrice',
		}
    }, {
        text: 'Sub Total',
        menuDisabled:true,
        dataIndex: 'total',
        sortable: false,
		//summaryType:'sum',
		itemId:'summary',
		 /*summaryType:function( records ) {
			 var i = 0,
                length = records.length,
				 sum = 0,
				 shipping=0,
                record;
         for (i; i < length; ++i) {
            var record = records[ i ];
            sum += record.get('currentPrice') * record.get('requiredQty');
			//shipping +=record.get('cost');
         }
         grandtot= Math.floor( sum+shipping );
         return '<strong>'+' Total :<span style="padding-left:62px;"></span>'+ sum +'</strong>';
		 }, */
        flex: 1,
		
    }, {
        dataIndex: 'requiredPrice',
        hidden: true,
        flex: 1,
		
    },{
        dataIndex: 'isActive',
        hidden: true,
        flex: 1,
		
    },{
        dataIndex: 'comments',
        hidden: true,
        flex: 1,
    },{
        xtype: 'actioncolumn',
        width: 30,
        itemId:'billinggridactionBtn',
        sortable: false,
        menuDisabled: true,
        items: [{
            icon: 'images/icon16_error.png',
            tooltip: 'Delete Product',
            
            style: 'cursor:pointer;',
            scope: this,
            handler: function(grid, rowIndex, e) {
                grid.getStore().removeAt(rowIndex);
                Ext.ComponentQuery.query('#processedtotal')[0].reset();
                Ext.ComponentQuery.query('#totalItems')[0].reset();
                Ext.ComponentQuery.query('#noOfItems')[0].reset();
                var store = Ext.getStore('billingStore');
           	 var storecount=store.getCount();
                var sum = 0;
				 var totalItems = 0;
       		 store.each(function(record){
    	            sum += record.get('currentPrice') * record.get('requiredQty');
    	           grandtot= parseFloat(sum).toFixed(2);
    	           
    	           Ext.ComponentQuery.query('#processedtotal')[0].setValue(grandtot);
    	           console.log(storecount)
    	          totalItems += record.get('requiredQty');
    	         Ext.ComponentQuery.query('#totalItems')[0].setValue(totalItems);
    	         Ext.ComponentQuery.query('#noOfItems')[0].setValue(storecount);
       		 });
       		Ext.ComponentQuery.query('#processacceptBtn')[0].setVisible(false);
            }
        }],
        renderer: function(val, metadata, record) {
            metadata.style = 'cursor: pointer;';
            return val;
        }
    }],
    selModel: 'cellmodel',
    plugins: {
        ptype: 'cellediting',
        clicksToEdit: 1,
    },
    listeners : {
        edit : function( editor, e, eOpts ){
             //var currentPrice = e.record.data.currentPrice; 
        	//Ext.ComponentQuery.query('#processacceptBtn')[0].setVisible(false);
        	 var store = Ext.getStore('billingStore');
        	 var storecount=store.getCount();
        	 var rec = e.record;

             var rowtotal = rec.get('currentPrice') * rec.get('requiredQty');
            
             rec.set('total',rowtotal);
            
				var sum = 0;
				 var totalItems = 0;
        		 store.each(function(record){
     	            sum += record.get('currentPrice') * record.get('requiredQty');
     	           grandtot= parseFloat(sum).toFixed(2);
     	           
     	           Ext.ComponentQuery.query('#processedtotal')[0].setValue(grandtot);
     	           console.log(record.get('requiredQty')+""+storecount)
     	          totalItems += record.get('requiredQty');
     	         Ext.ComponentQuery.query('#totalItems')[0].setValue(totalItems);
        		 });
				},
				beforeedit: function(plugin, edit) {
					var grid = Ext.ComponentQuery.query('processordersgrid')[0];
			    	var cust = grid.getSelectionModel();
			    	var record;
			    	
			    	console.log(cust.getSelection());
			    	console.log(cust.getSelection().length);
					for(var i=0;i<cust.getSelection().length;i++){
						record=cust.getSelection()[i];
						console.log(record.data.status);
		                if (record.data.status=="Approved" || record.data.status=="Rejected" || record.data.status=="Revised") {
		                	Ext.ComponentQuery.query('#billinggridtoolbarBtn')[0].setVisible(false);
							Ext.ComponentQuery.query('#billinggridactionBtn')[0].setVisible(false);
		                    return false;
		                }
					}
					
	            }
        },
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        itemId:'billinggridtoolbarBtn',
        items: [{
            xtype: 'combobox',
            //editable:false,
            width: '80%',
            itemId:'billingOutletProduct',
           // allowBlank: false,
            autoSelect: true,
            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'No matching products found.',

                // Custom rendering template for each item
                getInnerTpl: function() {
                    return '<div class="search-item" style="border-bottom:1px solid;">' +
                        '<span><strong>Product Name : {name}</strong> || <strong>SKU:</strong> {sku}<br/><strong>MRP:</strong> {mrp_Price}, <strong>TTP:</strong> {sellingprice}</span>' +
                        '</div>';
                }
            },
            //pageSize: 5,
            //typeAhead: true,
            name: 'productsearch',
            emptyText: 'Enter a product sku...',
            //forceSelection: true,
            selectOnFocus: true,
            store: new Ext.data.Store({
                // storeId:'',
            	fields: [{
                    name: 'productid'
                },{
                    name: 'name'
                },{
                    name: 'id',
        			mapping:'priceMapping.id',
                }, {
                    name: 'sellingprice',
        			mapping:'priceMapping.sellingprice',
                },{
                    name: 'purchaseprice',
                    mapping:'priceMapping.purchaseprice',
                },{
                    name: 'mrp_Price',
                    mapping:'priceMapping.mrp_Price',
                }],
                //pageSize: 5,
                proxy: {
                    type: 'ajax',
                    url: "channelPartner/skuList?partnerId=",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                        totalProperty:'data.total'
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'sku',
            valueField: 'sku',
            listeners: {
               // buffer: 50,
                change: function() {
                    var store = this.store;
                    //store.suspendEvents();
                    store.clearFilter();
                    //store.resumeEvents();
                    store.filter({
                        property: 'sku',
                        anyMatch: true,
                        value: this.getValue()
                    });
                },
                select: function(combo, selection) {
                    var post = selection[0];
                    if (post) {
						var win = Ext.widget('productcartwin');
						var formvalue=win.down('form').getForm();
						formvalue.loadRecord(post);
						formvalue.findField('name').setValue(post.get('name'));
						formvalue.findField('sku').setValue(post.get('sku'));
						formvalue.findField('mrp_price').setValue(post.get('mrp_Price'));
						formvalue.findField('price').setValue(post.get('sellingprice'));
						win.show();
						
					}
					combo.reset();
                }
            }

        },{
            text: 'Add new',
            handler : function(selection) {
				var cart = Ext.widget('productcartwin');
				Ext.ComponentQuery.query('#name')[0].setReadOnly(false);
				Ext.ComponentQuery.query('#sku')[0].setReadOnly(false);
				cart.show();
			}
        }]
    }],

   
});