Ext.define('TomTail.view.orders.CustomerPicker', {
	extend: 'Ext.window.Window',
	alias: 'widget.custpicker',
	height: 448,
	width: 720,
	layout:'fit',
	title: 'Search Customers',
		controller: "orders-orders",
    viewModel: {
        type: "orders-orders"
    },
	itemId:'custpickerWin',
	requires: ['TomTail.view.orders.CustomerPickerGrid','TomTail.view.orders.AddNewCustomer'],
	items: [{
				xtype: 'custpickergrid',
				flex: 1
			}],
			dockedItems: [{
				dock: 'top',
				xtype: 'toolbar',
				bodyStyle: 'padding: 5px;',
				items: [
				       {
	                    fieldLabel: 'Customer Name',
	                    emptyText: 'Customer Name',
	                    name: 'name',
	                    reference:'name',
	                    enableKeyEvents: true,
	                    xtype:'textfield',
	                    margin:'5px',
	                    itemId:'customername',
						listeners:{
			afterrender: function(field) {
   Ext.defer(function() {
       field.focus(true, 100);
   }, 1);
}
		}
				       },{
	                    fieldLabel: 'Customer Mobile',
	                    emptyText: 'Customer Mobile',
	                    name: 'mobile',
	                    reference:'mobile',
	                    enableKeyEvents: true,
	                    xtype:'textfield',
	                    itemId:'customermobile',
	                    margin:'5px'
				       },{
						   xtype:'button',
						   text:'Clear',
						   margin:'5px',
						   action: 'clear_customers'
					   },{
	                  	text:'Search',
	                	action: 'search_customers'
	             }],
	             
			},{
           	 dock:'bottom',
           	 buttons:[{
				 text:'Add New',
				 handler:function(){
					 Ext.widget('addcustomer').show();
				 }
			 },{
	              	text:'Edit',
	            	action: 'editcustomer'
	         },{
              	text:'Select',
            	action: 'select_customers'
         }]
         }],
         
         listeners:{
        	 close:function(){
        		 if(Ext.ComponentQuery.query('#unprocessedWindow')[0] == undefined){
        		 Ext.ComponentQuery.query('#unprocessPanel')[0].enable();
           		Ext.ComponentQuery.query('#tabbar')[0].enable(); 
           		Ext.ComponentQuery.query('#header')[0].enable(); 
           		
        		 }
        	 }
         }
});