Ext.define('TomTail.view.orders.CustomerPickerGrid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.custpickergrid',
    border: false,
	controller: "orders-orders",
    viewModel: {
        type: "orders-orders"
    },
	bind:{
				store:'{custpicker}' 
			 },
			 
	initComponent: function() {
		Ext.apply(this, {
             multiSelect: true,
			 itemId:'cust_picker_grid',
			 columns: [{
				text : 'Id',
				dataIndex : 'id',
				sortable:false,
				hidden:true,
			},{
				text: 'Customer Name',
				dataIndex:'name',
				flex:1
			},
			{
				text: 'Customer Mobile',
				dataIndex:'mobile',
				flex:1
			}],
			 dockedItems: [{
			        xtype: 'pagingtoolbar',
			        bind:{
						store:'{custpicker}' 
					},
			        dock: 'bottom',
			        displayInfo: true,
			        displayMsg: 'Displaying Customers {0} - {1} of {2}'
			    }]
		}); 
		this.callParent(arguments);
	}
});