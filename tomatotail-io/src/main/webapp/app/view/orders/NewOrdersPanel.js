Ext.define("TomTail.view.orders.NewOrdersPanel",{
    extend: "Ext.panel.Panel",
	alias:'widget.newordersPanel',
	requires: [
	'TomTail.view.orders.OrdersController',
	'TomTail.view.orders.OrdersModel',
	'TomTail.view.orders.BillingGrid',
	'TomTail.view.orders.OrdersAddWindow'
	],
    controller: "orders-orders",
    viewModel: {
        type: "orders-orders"
    },
	 layout: {
        type: 'border',
        align: 'stretch'
    },
	itemId:'orderpanel',
   items:[{
	   xtype:'orderswindow',
	   split:true,
	   collapsible:true,
	   region:'west',
	    flex:1,
   },{
	   xtype:'billingGrid',
	   split:true,
	    flex:2.5,
		region:'center',
		title:'Billing Grid',
   }],
});
