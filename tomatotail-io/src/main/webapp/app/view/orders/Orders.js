Ext.define("TomTail.view.orders.Orders",{
    extend: "Ext.panel.Panel",
	alias:'widget.ordersPanel',
	requires: [
	'TomTail.view.orders.OrdersController',
	'TomTail.view.orders.OrdersModel',
	'TomTail.view.orders.OrdersGrid',
	'TomTail.view.orders.OrdersSearchPanel',
	'TomTail.view.orders.OrdersViewPanel',
	'TomTail.view.orders.CustomerPickerGrid',
		'TomTail.view.orders.CustomerPicker',
		'TomTail.view.orders.BillingGrid'
	],
    controller: "orders-orders",
    viewModel: {
        type: "orders-orders"
    },
	 layout: {
        type: 'vbox',
        align: 'center'
    },
    bodyStyle:'background:url(../../../../images/bg_page.png) !important',
//	itemId:'orderpanel',
   items:[{
	   xtype:'orderssearchpanel',
	   height: 102,
		 width:'100%'
   },{
	   xtype:'ordersgrid',
	   style:'margin-top:5px;',
	    flex:2,
		 width:'100%'
   }],
});
