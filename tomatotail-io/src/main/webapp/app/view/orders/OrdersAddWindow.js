Ext.define('TomTail.view.orders.OrdersAddWindow', {
    extend: 'Ext.panel.Panel',

    alias: 'widget.orderswindow',
	requires:['TomTail.view.orders.AddNewCustomer'],
   controller: "orders-orders",
    viewModel: {
        type: "orders-orders"
    },
	
	//id:'ordersWindow',
   // modal: true,
    title: 'Shipment & Total Info',
	
	autoScroll:true,
	items:[{
	xtype:'form',
	bodyPadding:10,
    
	layout: {
            type: 'vbox',
			align:'left',
            //columns: 3,
             //tdAttrs: { style: 'padding: 10px; vertical-align: top;' }
    },
        
    items: [ {
        xtype: 'textfield',
        fieldLabel: 'Order Id',
        //id: 'orderId',
        hidden: true,
        name: 'ordersid'
    },{
		xtype:'combobox',
		editable:false,
		allowBlank:false,
        fieldLabel: 'Store Id',
		
        name: 'storeid',
		emptyText: 'Please select...',
		reference:'storeId',
		value:'',
		//forceSelection: true,
                        store:new Ext.data.Store({
                     		 // storeId:'',
                              fields: ['storeid', 'orderstatus'],
                               proxy : {
                           		type : 'ajax',
                           		url:   "order/orderList",
                           		reader : {
                           			type : 'json',
                           			rootProperty:'data.list',
                           		},

                               },
                           }),
                        queryMode: 'remote',
                        displayField: 'storeid',
                        valueField: 'storeid',
    },{
		xtype:'textareafield',
        fieldLabel: 'Billing Address',
		readOnly:true,
		allowBlank:false,
        name: 'address',
		//height:120,
		//width:'540px'
	},{
		xtype:'fieldset',
		layout:{	
		type:'hbox',
		align:'center',
		},
		border:false,
		frame:false,
		items:[{
		xtype:'button',
		text:'Find Customer',
		handler:function(){
			Ext.widget('custpicker').show();
		}
	},{
		flex:2,
		border:'0px',
	},{
		xtype:'button',
		text:'Add Customer',
		handler:function(){
			Ext.widget('addcustomer').show();
		}
	}]
	},{
        xtype: 'displayfield',
        fieldLabel: 'Your Savings: &#8377;',
		labelWidth:'n',
		style:'margin-top:1%;',
        labelSeparator: '',
		value:'300',
		height:45,
		fieldStyle:'font-weight:bold;font-size:21px;color: #666;',
        labelStyle: 'font-weight:bold;font-size:21px;',
        readOnly: true,
        name: 'savings',
    },{
        xtype: 'displayfield',
        fieldLabel: 'Shipping: &#8377;',
		labelWidth:'n',
		style:'margin-top:13px;',
        labelSeparator: '',
		value:'50',
		height:45,
		fieldStyle:'font-weight:bold;font-size:21px;color: #666;',
        labelStyle: 'font-weight:bold;font-size:21px;',
        readOnly: true,
        name: 'shipping',
    }, {
		flex:1,
		border:'0px',
	},{
        xtype: 'displayfield',
        fieldLabel: 'Total: <span style="color:rgb(238, 47, 37);">&#8377;</span>',
		style:'margin-top:13px;',
		labelWidth:'n',
        labelSeparator: '',
		height:45,
		value:'10000',
		fieldStyle:'font-weight:bold;font-size:36px;color:rgb(238, 47, 37);',
        labelStyle: 'font-weight:bold;font-size:36px;',
        readOnly: true,
        name: 'ordertotal',
    }],
	
}],

    
	
	/* listeners:{
		close:function(){
			Ext.ComponentQuery.query('ordersPanel')[0].enable();
			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
		Ext.ComponentQuery.query('#header')[0].enable(); 
		}
	}, */

});