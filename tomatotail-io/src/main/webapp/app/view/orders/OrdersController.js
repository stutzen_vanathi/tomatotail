Ext.define('TomTail.view.orders.OrdersController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.orders-orders',
		
	init: function() {
        var me = this;
        this.control({
        	 'custpicker #customername': { 
        	        specialkey: function(field, e) { 
        	            if(e.getKey() == e.ENTER) { 
        	                      this.searchCustomers() 
        	            } 
        	        } 
        	        }, 
        	        'custpicker #customermobile': { 
            	        specialkey: function(field, e) { 
            	            if(e.getKey() == e.ENTER) { 
            	                      this.searchCustomers() 
            	            } 
            	        } 
            	        }, 
            'orderssearchpanel #custmobilesearch': { 
                	        specialkey: function(field, e) { 
                	            if(e.getKey() == e.ENTER) { 
                	                      this.searchConfig() 
                	            } 
                	        } 
             }, 
            'ordersgrid dataview': {
                itemdblclick: function(view, rec, elm, inx, evt, obj) {
                    me.loadInfo(rec);
                }
            },
			 'processordersgrid dataview': {
                itemdblclick: function(view, rec, elm, inx, evt, obj) {
                    me.loadProcessedOrder(rec);
                }
            },
			'custpicker dataview':{
		 itemdblclick:function(view,rec,elm,inx,evt,obj){
			 var search = Ext.ComponentQuery.query('#custnamesearch')[0];
			 var unprocesswin= Ext.ComponentQuery.query('#name')[0]
				if(unprocesswin){
					this.loadCustomers(rec);
				}else if(search){
					 this.loadCustomerSearch(rec);
				}
		 }
	     },
	     'custpicker button[action=editcustomer]':{
             click:'editCustomer'
         },
			'orderssearchpanel button[action=searchOrder]':{
                click:'searchConfig'
            },
			'orderssearchpanel button[action=ClearOrder]':{
                click:'clearConfig'
            },
			'processedorderssearchpanel button[action=searchprocessOrder]':{
                click:'searchProcessingOrder'
            },
			
			'processedorderssearchpanel button[action=clearprocessedOrder]':{
               click:'clearprocessed'
            },
			
			'custpicker button[action=search_customers]':{
                click:'searchCustomers'
            },
			'custpicker button[action=select_customers]':{
                click:function(){
                	var search = Ext.ComponentQuery.query('#custnamesearch')[0];
       			 var unprocesswin= Ext.ComponentQuery.query('#name')[0]
       				if(unprocesswin){
       					this.loadCustomers();
       				}else if(search){
       					 this.loadCustomerSearch();
       				}
                }
            },
			'custpicker button[action=clear_customers]':{
                click:'clearCustomers'
            },
            'orderssearchpanel #custnamesearch':{
    			triggerclick:function(){
    				 Ext.widget('custpicker').show();
    				 Ext.ComponentQuery.query('#unprocessPanel')[0].disable();
	              		Ext.ComponentQuery.query('#tabbar')[0].disable(); 
	              		Ext.ComponentQuery.query('#header')[0].disable(); 
    			}
            },
    			
			'unprocessedwindow #name':{
			triggerclick:function(){
	    			 
	    			 var win = Ext.widget('custpicker');
	    			var custname = Ext.ComponentQuery.query('#name')[0].getValue();
	    			console.log(custname);
	    			var custmobile = Ext.ComponentQuery.query('#mobile')[0].getValue();
	    			win.show();
	    			if(custname!='' && custmobile!=''){
	    			var store=Ext.getStore('custpickerStore');
	    			store.proxy.url="customer/listAll?name="+custname+"&mobileNo="+custmobile;
                	store.load();
                	 Ext.ComponentQuery.query('#customername')[0].setValue(custname);
                	 Ext.ComponentQuery.query('#customermobile')[0].setValue(custmobile);
	    			}

			}
            },
			'ordersgrid #process':{
	    	      click:function(grid, rowIndex, rec) {
	                  var row = grid.getStore().getAt(rec);
	                  if(row.data.status == 'Initiated'){
	                	  Ext.ComponentQuery.query('#unprocessPanel')[0].disable();
	              		Ext.ComponentQuery.query('#tabbar')[0].disable(); 
	              		Ext.ComponentQuery.query('#header')[0].disable(); 
            var win = Ext.widget('processedwindow');
			console.log(row.data.customerId+"address"+row.data.addressId)
            var formvalue = win.down('form').getForm();
			formvalue.findField('refOrderId').setValue(row.data.id);
			formvalue.findField('name').setValue(row.data.name);
			formvalue.findField('mobile').setValue(row.data.mobile);
		    formvalue.findField('city').setValue(row.data.city);
			formvalue.findField('customerId').setValue(row.data.customerId);
			formvalue.findField('addressId').setValue(row.data.addressId);
			formvalue.findField('orderDetails').setValue(row.data.orderDetails);
            formvalue.findField('address').setValue(row.data.address);
            formvalue.findField('comments').setValue(row.data.comments);
            //Ext.ComponentQuery.query('#noOfItems')[0].setVisible(false);
			var store = Ext.getStore('billingStore');
				store.load();
				Ext.ComponentQuery.query('#processacceptBtn')[0].setVisible(false);
				Ext.ComponentQuery.query('#processrejectBtn')[0].setVisible(false);
			
				Ext.ComponentQuery.query('#processprintBtn')[0].setVisible(false);
            win.show();
	                  }
	    	},
	    	 
	    },

        });
    },
    
    loadInfo: function(rec) {
    	Ext.ComponentQuery.query('#unprocessPanel')[0].disable();
		Ext.ComponentQuery.query('#tabbar')[0].disable(); 
		Ext.ComponentQuery.query('#header')[0].disable(); 
          var win = Ext.widget('unprocessedwindow');
		var formvalue=win.down('form').getForm();
		
		formvalue.loadRecord(rec);
			formvalue.findField('address').setValue(rec.data.address+", "+ rec.data.city+"-"+ rec.data.pincode+", "+ rec.data.state);
			if(rec.data.dateTime !== null){
			var datetime = Ext.Date.format(new Date(rec.data.dateTime), 'Y-m-d H:i:s');
			Ext.ComponentQuery.query('#date')[0].setValue(datetime);
			formvalue.findField('name').setValue(rec.data.name);
			formvalue.findField('mobile').setValue(rec.data.mobile);
			console.log(datetime);
			}
			if(rec.data.status == "Processing" || rec.data.status == "Revised" || rec.data.status == "Approved" || rec.data.status == "Rejected"){
				//formvalue.setEditable(false);
				Ext.ComponentQuery.query('#unprocessSave')[0].setDisabled(true);
			}
        win.show();
		
		/* Ext.Ajax.request({
			url : 'order/listAll?orderId=5' + rec.get('orderid'),
			method : 'GET',
			success : function(response, options) {
				var jsonresp = response.responseText;
				var jsondata = Ext.util.JSON.decode(jsonresp);
				var shippingamount = jsondata.data.order.shippingamount;
				var total = jsondata.data.order.subtotal;
				var grandtotal = jsondata.data.order.grandtotal;
				console.log("shippingamount "+shippingamount);
				var list = new Array();
				 Ext.each(jsondata.data.order.orderItemList, function(record) {
					list.push(record);
				});
				
				Ext.ComponentQuery.query('ordersproductGrid')[0].store.loadData(list, false);
				
				Ext.each(jsondata.data.order.orderItemList, function(record) {
					 console.log("firstname " +shippingamount);
					 console.log("total " +total);
					formvalue.findField('summary').setValue("<strong>"+" Total :<span style='margin-left:58px;'></span>"+total+"<br>"+"Shipping :<span style='padding-right:45px;'></span>"+shippingamount+"<br />--------------------------------<br />"+"Grand Total :<span style='padding-right:18px;'></span>"+grandtotal+"</strong>");
                   
				});
				
				 Ext.each(jsondata.data.order.orderAddressList, function(record) {
					formvalue.findField('shippingAddress').setValue(record.firstname+" "+record.lastname+",<br>"+record.street+", "+record.city+",<br>"+record.postcode)+".";
                   
				});
				Ext.each(jsondata.data.order.partner, function(record) {
					formvalue.findField('channelpartnerAddress').setValue(record.name+",<br>"+record.address+",<br>"+record.phoneNumber)+".";                   
				});
				Ext.each(jsondata.data.order.orderPayment, function(record) {
					console.log("record" +record.method);
					 formvalue.findField('orderPayment').setValue(record.method);
				});
				
                    
			},
			failure : function(response, options) {
				Ext.MessageBox.alert('FAILED', 'Error Occured!');
			}
		}); */

    },
	
    loadProcessedOrder:function(rec){
    	Ext.ComponentQuery.query('#processedPanel')[0].disable();
		Ext.ComponentQuery.query('#tabbar')[0].disable(); 
		Ext.ComponentQuery.query('#header')[0].disable(); 
		 var win = Ext.widget('processedwindow');
		var formvalue=win.down('form').getForm();
    	    formvalue.loadRecord(rec);
			var datetime = Ext.Date.format(new Date(rec.data.dateTime), 'Y-m-d H:i:s');
			formvalue.findField('date').setValue(datetime);
			formvalue.findField('city').setValue(rec.data.unProcessedOrder.address.city);
			Ext.ComponentQuery.query('#noOfItems')[0].setValue(rec.data.orderDetails.length);
			var order = new Array();
				 Ext.each(rec.data.orderDetails, function(record) {
					order.push(record);
				});
				 Ext.ComponentQuery.query('billingGrid')[0].store.loadData(order, false);
				 var partnerProduct = Ext.ComponentQuery.query('#billingOutletProduct')[0];
          		 partnerProduct.clearValue();
          		partnerProduct.store.proxy.url="channelPartner/skuList?partnerId="+rec.data.outletId;
          		partnerProduct.store.load();	
				 Ext.Ajax.request({
						url : 'order/listAll?orderId='+rec.get('id'),
						method : 'GET',
						success : function(response, options) {
							var jsonresp = response.responseText;
							var jsondata = Ext.util.JSON.decode(jsonresp);
							formvalue.findField('orderDetails').setValue(jsondata.data.list[0].unProcessedOrder.orderDetails);
							var total = parseFloat(jsondata.data.totalPrice).toFixed(2);

							Ext.ComponentQuery.query('#processedtotal')[0].setVisible(true);
							Ext.ComponentQuery.query('#processedtotal')[0].setValue(total);
							 
						},
						failure : function(response, options) {
							Ext.MessageBox.alert('FAILED', 'Error Occured!');
						},
							 });
				 
				 var store = Ext.getStore('billingStore');
				 var totalItems = 0;
				 store.each(function(record){
	    	           
	    	          totalItems += record.get('requiredQty');
	    	         Ext.ComponentQuery.query('#totalItems')[0].setValue(totalItems);
	       		 });
				if(rec.data.status == 'Revised'){
					Ext.ComponentQuery.query('#processprintBtn')[0].setVisible(false);
					Ext.ComponentQuery.query('#processedsubmitBtn')[0].setVisible(false);
				}else if(rec.data.status == 'Processed'){
					
					Ext.ComponentQuery.query('#processacceptBtn')[0].setVisible(false);
					Ext.ComponentQuery.query('#processrejectBtn')[0].setVisible(false);
					Ext.ComponentQuery.query('#processprintBtn')[0].setVisible(false);
					//Ext.ComponentQuery.query('#processedsubmitBtn')[0].setVisible(false);
				}else if(rec.data.status == 'Approved'){
					
					Ext.ComponentQuery.query('#processacceptBtn')[0].setVisible(false);
					Ext.ComponentQuery.query('#processrejectBtn')[0].setVisible(false);
					Ext.ComponentQuery.query('#processedsubmitBtn')[0].setVisible(false);
				}else if(rec.data.status == 'Rejected'){
					Ext.ComponentQuery.query('#processacceptBtn')[0].setVisible(false);
					Ext.ComponentQuery.query('#processrejectBtn')[0].setVisible(false);
					Ext.ComponentQuery.query('#processedsubmitBtn')[0].setVisible(false);
					Ext.ComponentQuery.query('#processprintBtn')[0].setVisible(false);
				}
				
				 
				win.show();
	},
	
	searchConfig:function(){
    	var store=this.getViewModel().getStore('unprocessed');
		var orderdate = this.lookupReference('date').getValue();
		
		if (orderdate != null){
    	var date =Ext.Date.format(new Date(orderdate), 'Y-m-d H:i:s');
		}else{
			console.log(orderdate);
		}
		var custname =this.lookupReference('custname').getValue();
		var custmobile =this.lookupReference('custmobile').getValue();
		if(date == null || date == undefined){
			date = '';
		}
    	if(date!='' || custname!='' || custmobile!=''){
    		console.log('if');
    		url="unProcessedOrder/listAll?date="+date+"&name="+custname+"&mobile="+custmobile;
    	}else{
    		console.log('else');
    		url="unProcessedOrder/listAll";
    	}
    	console.log('url'+url);
    	store.proxy.url=url;
    	store.load();
    },
	
	searchProcessingOrder:function(){
    	var store=this.getViewModel().getStore('orders');
		var orderdate = this.lookupReference('processedDate').getValue();
		
		if (orderdate != null){
    	var date =Ext.Date.format(new Date(orderdate), 'Y-m-d H:i:s');
		}else{
			console.log(orderdate);
		}
		var orderStatus =this.lookupReference('processorderStatus').getValue();
		if(date == null || date == undefined){
			date = '';
		}
		var name =this.lookupReference('name').getValue();
		
		//var sourceType =this.lookupReference('sourceType').getValue();
		
		
    	if(date!='' || orderStatus !='' || name!=''){
    		console.log('if');
    		url="order/listAll?status="+orderStatus+"&date="+date+"&name="+name;
    	}else{
    		console.log('else');
    		url="order/listAll";
    	}
    	console.log('url'+url);
    	store.proxy.url=url;
    	store.load();
    },
	
	searchCustomers:function(){
    	var store=Ext.getStore('custpickerStore');
    	var  name =this.lookupReference('name').getValue();
		var mobile =this.lookupReference('mobile').getValue();
    	if(name!='' || mobile!=''){
    		console.log('if');
    		url="customer/listAll?name="+name+"&mobileNo="+mobile;
    	}else{
    		console.log('else');
    		url="customer/listAll";
    	}
    	console.log('url'+url);
    	store.proxy.url=url;
    	store.load();
    },
	

	clearConfig:function(){
		this.lookupReference('date').reset();
		this.lookupReference('custname').reset();
		this.lookupReference('custmobile').reset();
		var store=this.getViewModel().getStore('unprocessed');
		store.proxy.url="unProcessedOrder/listAll";
    	store.load();
	},
	clearprocessed:function(){
		this.lookupReference('processedDate').reset();
		this.lookupReference('processorderStatus').reset();
		this.lookupReference('name').reset();
		var store=this.getViewModel().getStore('orders');
		store.proxy.url='order/listAll?sourceType='+"Internal";
    	store.load();
	},
	
	clearCustomers:function(){
		this.lookupReference('name').reset();
		this.lookupReference('mobile').reset();
	},
	
	loadCustomers:function(rec){
		var grid = Ext.ComponentQuery.query('custpickergrid')[0];
    	var cust = grid.getSelectionModel();
    	var record;
    	console.log(cust.getSelection());
    	console.log(cust.getSelection().length);
		for(var i=0;i<cust.getSelection().length;i++){
			record=cust.getSelection()[i];
			
			Ext.ComponentQuery.query('#name')[0].setValue(record.data.name);
			Ext.ComponentQuery.query('#customerId')[0].setValue(record.data.id);
			//Ext.ComponentQuery.query('#address')[0].setValue(record.data.address.address);
			Ext.ComponentQuery.query('#addressId')[0].setValue(record.data.address.id);
			Ext.ComponentQuery.query('#address')[0].setValue(record.data.address.address+", "+ record.data.address.city+"-"+ record.data.address.pincode+", "+ record.data.address.state);
			Ext.ComponentQuery.query('#city')[0].setValue(record.data.address.city);
			Ext.ComponentQuery.query('#state')[0].setValue(record.data.address.state);
		}
		Ext.ComponentQuery.query('#custpickerWin')[0].destroy();
	    },
	    
	    loadCustomerSearch:function(rec){
	    	var grid = Ext.ComponentQuery.query('custpickergrid')[0];
	    	var cust = grid.getSelectionModel();
	    	var record;
	    	console.log(cust.getSelection());
	    	console.log(cust.getSelection().length);
			for(var i=0;i<cust.getSelection().length;i++){
				record=cust.getSelection()[i];
	    	Ext.ComponentQuery.query('#custnamesearch')[0].setValue(record.data.name);
			Ext.ComponentQuery.query('#custmobilesearch')[0].setValue(record.data.mobile);
			Ext.ComponentQuery.query('#custpickerWin')[0].destroy();
			var store=Ext.ComponentQuery.query('ordersgrid')[0].store;
			store.proxy.url="unProcessedOrder/listAll?name="+record.data.name+"&mobile="+record.data.mobile;
	    	store.load();
	    	if(Ext.ComponentQuery.query('#unprocessedWindow')[0] == undefined){
       		 Ext.ComponentQuery.query('#unprocessPanel')[0].enable();
          		Ext.ComponentQuery.query('#tabbar')[0].enable(); 
          		Ext.ComponentQuery.query('#header')[0].enable(); 
          		
       		 }
			}
	    },
	    
	    editCustomer:function(){
	    	var grid = Ext.ComponentQuery.query('custpickergrid')[0];
	    	var cust = grid.getSelectionModel();
	    	var record;
	    	console.log(cust.getSelection());
	    	console.log(cust.getSelection().length);
	    	if(cust.getSelection().length > 0){
	    		for(var i=0;i<cust.getSelection().length;i++){
	    			record=cust.getSelection()[i];
	    		var win = Ext.widget('addcustomer');
	    		var form = win.down('form').getForm();
	    		form.loadRecord(record);
	    		form.findField('addressDetails').setValue(record.data.address.address);
	    		form.findField('city').setValue(record.data.address.city);
                 form.findField('email').setValue(record.data.address.email);
                 form.findField('pincode').setValue(record.data.address.pincode);
                 form.findField('state').setValue(record.data.address.state);
                 form.findField('country').setValue(record.data.address.country);
                 console.log(record.data.address.country)
	    		}
	    		win.show();
	    	}else{
				Ext.Msg.alert('Alert','Select one customer from the grid');
			}
	    }
});
