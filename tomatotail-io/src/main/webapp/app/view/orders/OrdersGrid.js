Ext.define('TomTail.view.orders.OrdersGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ordersgrid',
    border: true,
    frame:true,
	requires:['TomTail.view.orders.UnprocessedWindow'],
    title: 'Search Result',
    height: 288,
    width: '100%',
    header: {
        xtype: 'header',
        titlePosition: 0,
        defaults: {
            margin: '0 10px'
        },
        items: [
           
            {
                xtype: 'button',
                text: "Add New",
                handler: Ext.bind(function() {
                	Ext.widget('unprocessedwindow').show();
                	Ext.ComponentQuery.query('#unprocessPanel')[0].disable();
					Ext.ComponentQuery.query('#tabbar')[0].disable(); 
					Ext.ComponentQuery.query('#header')[0].disable(); 
                }, this)
            }
        ]
    },
    viewConfig: {
		style: { overflow: 'auto', overflowX: 'hidden' },
        getRowClass: function(record, index) {
            var status = record.get('status');
            if(status == 'Approved' || status == 'Revised'){
            	return 'processed';
            }else if(status == 'Rejected'){
            	return 'rejected'
            }
        },
        loadMask:false
    },
    bind: {
        store: '{unprocessed}'
    },
	itemId:'unprocessedordergrid',
    columns: [{
        text: 'Id',
        dataIndex: 'id',
        sortable: false,
        menuDisabled:true,
        
    }, {
        text: 'Name',
        dataIndex: 'name',
        flex: 1,
        sortable: false,
        menuDisabled:true,
    }, {
        text: 'Mobile',
        dataIndex: 'mobile',
        
        sortable: false,
        flex: 1,
        menuDisabled:true,
		//renderer:Ext.util.Format.dateRenderer('m/d/Y g:i A')
    }, {
        text: 'Comments',
        dataIndex: 'comments',
        sortable: false,
        
        flex: 1,
        menuDisabled:true,
    }, /*{
        text: 'Enable',
        dataIndex: 'isActive',
        menuDisabled:true,
        sortable: false,
        
        flex: 1,
		renderer:function(val){
			if(val==1){
				return "True"
			}else{
				return "False"
			}
		}
    },*/{
        dataIndex: 'city',
        sortable: false,
        flex: 1,
        
		hidden:true
    },{
        dataIndex: 'state',
        sortable: false,
        flex: 1,
		hidden:true
    },{
        dataIndex: 'pincode',
        sortable: false,
        
        flex: 1,
		hidden:true
    },{
				flex:1,
				width: 85,
				
				text:'Status',
				sortable: false, 
				itemId:'process',
				 dataIndex: 'status',
				 menuDisabled:true,
				renderer: function(val,metaData){
					if(val=='Initiated'){
					return val + '<button type="button" class="x-btn x-unselectable x-btn-default-small x-border-box" style="margin-left: 40.5%;width:74px;margin-top: -15px;" ><span class="x-btn-inner x-btn-inner-default-small">Process</span></button>';
					}else{
						metaData.css = 'processed';
						return val 
					}
					
				}
			},/*{
	            xtype: 'widgetcolumn',
	           // padding:'10 px',
	            widget: {
	                xtype: 'button',
	                text: 'Process',
	                listeners:{
	                	click:function(btn) {
	  	                  var row = btn.getWidgetRecord();
	  	                  if(row.data.status == 'Initiated'){
	              var win = Ext.widget('processedwindow');
	  			console.log(row.data.customerId+"address"+row.data.addressId)
	              var formvalue = win.down('form').getForm();
	  			formvalue.findField('refOrderId').setValue(row.data.id);
	  			formvalue.findField('name').setValue(row.data.name);
	  			formvalue.findField('mobile').setValue(row.data.mobile);
	  			formvalue.findField('city').setValue(row.data.city);
	  			formvalue.findField('customerId').setValue(row.data.customerId);
	  			formvalue.findField('addressId').setValue(row.data.addressId);
	  			formvalue.findField('orderDetails').setValue(row.data.orderDetails);
	              formvalue.findField('address').setValue(row.data.address);
	              formvalue.findField('comments').setValue(row.data.comments);
	              //Ext.ComponentQuery.query('#noOfItems')[0].setVisible(false);
	  			var store = Ext.getStore('billingStore');
	  				store.load();
	              win.show();
	  	                  }else{
	  	                	Ext.MessageBox.show({
                                title: 'Alert',
                                msg: 'Ordered Details is on processing...',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            })
	  	                  }
	  	    	},
	  	    	render:function(){
	  	    		
	  	    	}
	                }
	            }
	        }*/],
	        cls:'paging',
	 dockedItems: [{
        xtype: 'pagingtoolbar',
        dock: 'bottom',
        
		bind: {
        store: '{unprocessed}'
    },
        displayInfo: true,
        displayMsg: 'Displaying Unprocessed orders {0} - {1} of {2}'
    }]
});