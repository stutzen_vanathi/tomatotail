Ext.define('TomTail.view.orders.OrdersModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.orders-orders',
	 requires: [
               'TomTail.model.ModelOrders',
			   'TomTail.model.ModelProductList',
			   'TomTail.model.ModelUnprocessed',
			   'TomTail.model.ModelProcessedOrderDetails'
           ],
    data: {
        name: 'TomTail'
    },
	stores: {
       orders:{
		   storeId:'OrdersStore',
    	  model:'TomTail.model.ModelProcessedOrderDetails',
		    autoLoad:true,
			pageSize:13,
    	   proxy : {
    		   type : 'ajax',
    		   url : 'order/listAll?sourceType='+"Internal",
   			reader : {
   				type : 'json',
   				rootProperty:'data.list',
				totalProperty:'data.total'
   			},
			 listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }
    	   },
    	  
       },
	   custpicker:{
		   storeId:'custpickerStore',
    	  // model : 'TomTail.model.ModelOrders',
		  fields: [{
            name: 'id'
        },{
            name: 'name'
        }, {
            name: 'address',
			mapping:'address[0]',
        },{
            name: 'city',
			mapping:'address[0].city',
        },{
            name: 'state'
        },{
            name: 'pincode'
        } ,{
            name: 'mobile'
        },{
            name: 'comments'
        },{
			name:'isActive',
		},],
		    autoLoad:false,
			pageSize:10,
    	   proxy : {
    		   type : 'ajax',
    		   url : 'customer/listAll',
   			reader : {
   				type : 'json',
   				rootProperty:'data.list',
				totalProperty:'data.total'
   			},
			 listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }
    	   },
    	   /*sorters: [{
    			property: 'name',
    	        direction:'DESC'
    	    }]*/
       },
	   unprocessed:{
		   storeId:'unprocessedStore',
		     model:'TomTail.model.ModelUnprocessed',
			  autoLoad: true,
			  pageSize:13,
        proxy: {
            type: 'ajax',
            url: 'unProcessedOrder/listAll',
            reader: {
                type: 'json',
                rootProperty: 'data.list',
				totalProperty:'data.total',
            },
			 listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }

        },
       
	   },
	   billinggrid:{
				//storeId: 'productStore',
				//fields:["productid","name","brandid","sku","weight","categoryid","comments","isactive"],
				 fields: [{
            name: 'name'
        }, {
            name: 'sku'
        }, {
            name: 'price'
        }, {
            name: 'ttprice'
        }, {
			name:'qtyordered',
		},{
            name: 'total',
            type: 'int',
            convert: function(val, row) {
                return row.data.ttprice * row.data.qtyordered;
            }
        },{
			name:'isActive',
		},],
				autoLoad: true,
				pageSize:4,

			},
	}

});
