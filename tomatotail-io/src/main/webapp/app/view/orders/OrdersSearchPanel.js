Ext.define('TomTail.view.orders.OrdersSearchPanel', {
	extend : 'Ext.form.Panel',
	alias : 'widget.orderssearchpanel',
	requires:[
'Ext.form.field.Hidden'

	          ],
	title : 'Search Unprocessed Orders',
	layout : 'hbox',
	//cls:'bg-logo',
	height:'50px',
	reference:'orderssearchform',
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			padding:7,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [ {
		xtype:'datefield',
		margin : '10px',
        fieldLabel: 'Date',
        name: 'date',
		format:'Y-m-d H:i:s',
		reference:'date',
    }, /*{
		xtype:'combobox',
		margin : '10px',
		editable:false,
		allowBlank:false,
        fieldLabel: 'Order Status',
        name: 'status',
		reference:'orderStatus',
		emptyText: 'Please select...',
		value:'',
						//forceSelection: true,
                        store:new Ext.data.Store({
                     		 // storeId:'',
                             fields: ['status'],
                                data: [{
                                    "status": "Initiated",
                                },{
                                    "status": "Processing",
                                }],

                           }),
                        queryMode: 'remote',
                        displayField: 'status',
                        valueField: 'status',
    },*/ {
        fieldLabel: 'Name',
        xtype: 'triggerfield',
        name: 'custname',
        itemId: 'custnamesearch',
        margin : '10px',
        reference:'custname',
        editable: false,
        triggerCls: 'x-form-search-trigger',
        onTriggerClick: function(event) {
            var me = this;
            if (!me.hideTrigger) {
                me.fireEvent("triggerclick", me, event);
            }
        },

    },{
    	xtype:'textfield',
    	fieldLabel:'Mobile',
    	margin : '10px',
    	itemId: 'custmobilesearch',
    	reference:'custmobile',
    		name:'custmobile'
    }, {
				xtype : 'button',
				text : 'Clear',
				width : 70,
				margin : '10px',
				action:'ClearOrder',
			},{
				xtype : 'button',
				text : 'Search',
				width : 70,
				margin : '10px',
				action:'searchOrder',
			}]
		});
		this.callParent(arguments);
	}
});