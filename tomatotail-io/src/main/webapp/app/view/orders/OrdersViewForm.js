Ext.define('TomTail.view.orders.OrdersViewForm', {
    extend: 'Ext.form.Panel',
    requires: ['TomTail.view.orders.ProductGrid', 'TomTail.view.orders.OrdersModel'],
    alias: 'widget.ordersViewForm',
    height: 470,
    controller: "orders-orders",
    viewModel: {
        type: "orders-orders"
    },
    autoScroll: true,
    layout: {
        type: 'table',
        columns: 2,
        tdAttrs: { style: 'padding: 10px; vertical-align: top;' }
    },
    bodyPadding: 10,
    defaults: {
        xtype: 'displayfield',
        labelWidth: 105,
    },

    items: [{
        fieldLabel: 'Order Date',
        name: 'createdat',
    }, {
        xtype: 'textfield',
        fieldLabel: 'Order Id',
        id: 'orderId',
        readOnly: true,
        name: 'orderid'
    }, {
        xtype: 'combobox',
        fieldLabel: 'Order Status',
        editable: false,
        name: 'orderstatus',
        emptyText: 'Please select...',
        forceSelection: true,
        autoLoad: true,
        store: new Ext.data.Store({
            // storeId:'',
            autoLoad: true,
            fields: ['storeid', 'orderstatus'],
            data: [{
                "orderstatus": "Shipping"
            }, {
                "orderstatus": "Delivered"
            }, {
                "orderstatus": "Pending"
            }, {
                "orderstatus": "Cancel"
            }],
            /*proxy : {
                           		type : 'ajax',
                           		url:   "order/orderList",
                           		reader : {
                           			type : 'json',
                           			rootProperty:'data.list',
                           		},
                               },*/
        }),
        queryMode: 'remote',
        displayField: 'orderstatus',
        valueField: 'orderstatus',
    }, {
        fieldLabel: 'Payment Type',
        labelWidth: 112,
        name: 'orderPayment'
    }, {
        fieldLabel: 'Shipping Address',
        labelWidth: 'n',
        labelStyle: 'white-space: nowrap;',
        style: 'padding-bottom:25px;',
        name: 'shippingAddress'
    }, {
        fieldLabel: 'ChannelPartner Address',
        labelWidth: 'n',
        labelStyle: 'white-space: nowrap;',
        style: 'padding-bottom:25px;',
        name: 'channelpartnerAddress'
    }, {
        fieldLabel: 'Total Product Qty',
        labelWidth: 'n',
        colspan: 2,
        name: 'totalqtyordered',
    }, {
        xtype: 'ordersproductGrid',
        colspan: 2,
    }, {
        fieldLabel: '',
        labelSeperator: '',
        labelWidth: 'n',
        style: 'margin-left:570px',
        colspan: 2,
        name: 'summary',
    }],

    buttons: [{
        xtype: 'button',
        text: 'Invoice Copy',
        style: 'margin-left:570px',
        handler: function() {
            var serverPath = window.location.href;
            var serverPathIndex = serverPath.lastIndexOf("/");
            var orderId = Ext.getCmp('orderId').getValue();
            window.open(serverPath.substring(0, serverPathIndex) + '/InvoiceCopy.jsp?rid=' + orderId);
          
        }

    }, {
        text: 'Save',
        handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            console.log("form" + form);
            if (form.isValid()) {
                var formData = Ext.encode(form.getValues());
                var url = 'order/updateByOrderId';
                Ext.Ajax.request({
                    url: url,
                    method: 'POST',
                    jsonData: formData,
                    success: function(result, request) {
                        Ext.MessageBox.show({
                            title: 'Success',
                            msg: 'Successfully saved...',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        })
                        if (win) {
                            win.close();
                            Ext.getCmp('ordergrid').store.load();
                        }
                    },
                    failure: function(result, request) {
                        Ext.MessageBox.show({
                            title: 'Failure',
                            msg: 'Invalid Data',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR
                        })
                    }
                });

            }
        }
    }, {
        text: 'Close',
        handler: function() {
            Ext.ComponentQuery.query('#ordersPanel')[0].close();
        }
    }],

});