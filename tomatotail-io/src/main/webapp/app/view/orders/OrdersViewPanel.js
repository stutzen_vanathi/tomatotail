Ext.define('TomTail.view.orders.OrdersViewPanel',{
    extend: 'Ext.window.Window',
	requires:['TomTail.view.orders.OrdersViewForm'],
    alias: 'widget.ordersView',
    modal: true,
    title: 'Orders',
	itemId:'ordersPanel',
	width:750,
	height:500,
	items:[{
		xtype:'ordersViewForm',
    }],

});