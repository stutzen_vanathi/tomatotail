Ext.define('TomTail.view.orders.ProcessedOrdersGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.processordersgrid',
    border: true,
    frame:true,
	requires:['TomTail.view.orders.ProcessedWindow'],
    title: 'Search Result',
    height: 288,
    width: '100%',
    viewConfig: {
		style: { overflow: 'auto', overflowX: 'hidden' },
        getRowClass: function(record, index) {
            var status = record.get('status');
            if(status == 'Revised' || status == 'Approved'){
            	return 'processed';
            }else if(status == 'Rejected'){
            	return 'rejected'
            }
        },
        loadMask:false
    },
    bind: {
        store: '{orders}'
    },
    columns: [{
        text: 'Id',
        dataIndex: 'id',
        sortable: false,
        menuDisabled:true,
    }, {
        text: 'Name',
        dataIndex: 'name',
        sortable: false,
        flex: 1,
        menuDisabled:true,
    }, {
        text: 'Status',
        dataIndex: 'status',
        sortable: false,
        flex: 1,
        menuDisabled:true,
    }, {
        text: 'Source Type',
        dataIndex: 'sourceType',
        sortable: false,
        flex: 1,
		//renderer:Ext.util.Format.dateRenderer('m/d/Y g:i A')
        menuDisabled:true,
    }],
    cls:'paging',
	 dockedItems: [/*{
        xtype: 'toolbar',
        dock: 'top',
        items: [{
            text: 'Add New',
            handler: function() {
				var store = Ext.getStore('billingStore');
				store.load();
                Ext.widget('processedwindow').show();
            }
        }]
	 },*/{
        xtype: 'pagingtoolbar',
        dock: 'bottom',
		bind: {
        store: '{orders}'
		}, 
        displayInfo: true,
        displayMsg: 'Displaying Orders {0} - {1} of {2}'
    }]
});