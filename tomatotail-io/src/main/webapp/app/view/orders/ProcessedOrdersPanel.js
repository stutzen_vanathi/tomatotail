Ext.define("TomTail.view.orders.ProcessedOrdersPanel",{
    extend: "Ext.panel.Panel",
	alias:'widget.processedordersPanel',
	requires: [
	'TomTail.view.orders.OrdersController',
	'TomTail.view.orders.OrdersModel',
	'TomTail.view.orders.ProcessedOrdersGrid',
	'TomTail.view.orders.ProcessedOrdersSearchPanel',
	],
    controller: "orders-orders",
    viewModel: {
        type: "orders-orders"
    },
	 layout: {
        type: 'vbox',
        align: 'center'
    },
    bodyStyle:'background:url(../../../../images/bg_page.png) !important',
//	itemId:'orderpanel',
   items:[{
	   xtype:'processedorderssearchpanel',
	   height: 102,
		 width:'100%'
   },{
	   xtype:'processordersgrid',
	    flex:2,
		 width:'100%'
   }],
});
