Ext.define('TomTail.view.orders.ProcessedOrdersSearchPanel', {
	extend : 'Ext.form.Panel',
	alias : 'widget.processedorderssearchpanel',
	requires:[
'Ext.form.field.Hidden',
'TomTail.model.ModelProcessedOrderDetails'
	          ],
	title : 'Search Processed Orders',
	layout : 'hbox',
	height:'50px',
	
	//cls:'bg-logo',
	
	initComponent : function() {
		Ext.apply(this, {
			border : false,
			frame : true,
			split : true,
			padding:7,
			fieldDefaults : {
				msgTarget : 'side',
				labelWidth : 100,
				bodyStyle : 'padding: 5px;'
			},
			items : [ {
		xtype:'datefield',
		margin : '10px',
		labelWidth:'n',
        fieldLabel: 'Date',
        name: 'date',
		reference:'processedDate',
		format:'Y-m-d TH:i:s.u',
		altFormats: 'Y-m-d TH:i:s.u',
    }, {
		xtype:'combobox',
		margin : '10px',
		editable:false,
		allowBlank:false,
        fieldLabel: 'Order Status',
		labelWidth:'n',
        name: 'status',
		reference:'processorderStatus',
		emptyText: 'Please select...',
		value:'',
						//forceSelection: true,
                       store:new Ext.data.Store({
                     		 // storeId:'',
                             fields: ['status'],
                                data: [{
                                    "status": "Processed",
                                },{
									"status": "Revised",
								},{
									"status": "Approved",
								},{
									"status": "Rejected",
								}],

                           }),
                        queryMode: 'local',
                        displayField: 'status',
                        valueField: 'status',
    },{
		xtype:'combobox',
		margin : '10px',
		editable:false,
		allowBlank:false,
        fieldLabel: 'Name',
		labelWidth:'n',
        name: 'name',
		reference:'name',
		emptyText: 'Please select...',
		value:'',
						//forceSelection: true,
                       store:new Ext.data.Store({
                     		fields: [{
            name: 'id'
        },{
            name: 'name'
        }, {
            name: 'address',
			mapping:'address[0]',
        },{
            name: 'city',
			mapping:'address[0].city',
        },{
            name: 'state'
        },{
            name: 'pincode'
        } ,{
            name: 'mobile'
        },{
            name: 'comments'
        },{
			name:'isActive',
		},],
                               proxy: {
                            type: 'ajax',
                            url: "customer/listAll",
                            reader: {
                                type: 'json',
                                rootProperty: 'data.list',
                            },

                        },

                           }),
                        queryMode: 'remote',
                        displayField: 'name',
                        valueField: 'name',
    },/*{
		xtype:'combobox',
		margin : '10px',
		editable:false,
		allowBlank:false,
        fieldLabel: 'SourceType',
		labelWidth:'n',
        name: 'name',
		reference:'sourceType',
		emptyText: 'Please select...',
						//forceSelection: true,
                        store:new Ext.data.Store({
                     		 // storeId:'',
                             fields: ['sourceType'],
                                data: [{
                                    "sourceType": "Internal",
                                },{
									"sourceType": "External",
								}],

                           }),
                        queryMode: 'remote',
                        displayField: 'sourceType',
                        valueField: 'sourceType',
    },{
		xtype:'fieldset',
		layout:'vbox',
		border:0,
		margin:0,
		padding:0,
		items:[]
	},*/{
		xtype : 'button',
		text : 'Clear',
		width : 70,
		margin : '10px',
		action:'clearprocessedOrder',
	},{
		xtype : 'button',
		text : 'Search',
		width : 70,
		margin : '10px',
		action:'searchprocessOrder',
	}]
		});
		this.callParent(arguments);
	}
});