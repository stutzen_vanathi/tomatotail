Ext.define('TomTail.view.orders.ProcessedProductItemGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.processedproductitemgrid',
    border: true,
	frame:true,
    height: 230,
	autoScroll:true,
    width: '100%',
    store: Ext.create('Ext.data.Store', {
        storeId: 'processedItemStore',
        fields: [{
            name: 'id',

        }, {
            name: 'productName',
        }, {
            name: 'sku',
        }, {
            name: 'price',
        }, {
            name: 'currentPrice',
        }, {
            name: 'requiredQty',
        },  {
            name: 'total',
            type: 'int',
            convert: function(val, row) {
                return row.data.currentPrice * row.data.requiredQty;
            }
        }],

    }),
    columns: [{
        text: 'SKU',
        dataIndex: 'sku',
        sortable: false,
		editor: {
            xtype: 'combobox',
            //editable:false,
            allowBlank: false,
            typeAhead: true,
            name: 'name',
            emptyText: 'Please select...',
            value: '',
            //forceSelection: true,
            selectOnFocus: true,
            store: new Ext.data.Store({
                // storeId:'',
                fields: ['name', 'sku'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: 'product/list',
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'sku',
            valueField: 'sku',
            listeners: {
                buffer: 50,
                change: function() {
                    var store = this.store;
                    //store.suspendEvents();
                    store.clearFilter();
                    //store.resumeEvents();
                    store.filter({
                        property: 'sku',
                        anyMatch: true,
                        value: this.getValue()
                    });
                }
            }

        },
		
    }, {
        text: 'Product',
        dataIndex: 'productName',
        sortable: false,
		editor: {
            xtype: 'combobox',
            //editable:false,
            allowBlank: false,
            typeAhead: true,
            name: 'name',
            emptyText: 'Please select...',
            value: '',
            //forceSelection: true,
            selectOnFocus: true,
            store: new Ext.data.Store({
                // storeId:'',
                fields: ['name', 'sku'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: 'product/list',
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'name',
            listeners: {
                buffer: 50,
                change: function() {
                    var store = this.store;
                    //store.suspendEvents();
                    store.clearFilter();
                    //store.resumeEvents();
                    store.filter({
                        property: 'name',
                        anyMatch: true,
                        value: this.getValue()
                    });
                }
            }

        },
        flex: 1,
		//renderer:Ext.util.Format.dateRenderer('m/d/Y g:i A')
    }, {
        text: 'Qty',
        dataIndex: 'requiredQty',
        sortable: false,
        flex: 1,
		editor:{
			xtype:'numberfield',
			minValue:1,
			 name: 'requiredQty',
		}
    }, {
        text: 'Price',
        dataIndex: 'mrpPrice',
        sortable: false,
        flex: 1,
		editor:{
			xtype:'numberfield',
			minValue:1,
			 name: 'mrpPrice',
		}
    },{
        text: 'TTPrice',
        dataIndex: 'currentPrice',
        sortable: false,
        flex: 1,
		
		editor:{
			xtype:'numberfield',
			minValue:1,
			 name: 'currentPrice',
		}
    },/* {
        text: 'Sub Total',
        dataIndex: 'total',
		//summaryType:'sum',
		hidden:true,
		itemId:'summary',
		 summaryType:function( records ) {
			 var i = 0,
                length = records.length,
				 sum = 0,
				 shipping=0,
                record;
         for (i; i < length; ++i) {
            var record = records[ i ];
            sum += record.get('currentPrice') * record.get('requiredQty');
			//shipping +=record.get('cost');
         }
         grandtot= Math.floor( sum+shipping );
         return '<strong>'+' Total :<span style="padding-left:62px;"></span>'+ sum +'</strong>';
		 }, 
        flex: 1,
		
    }, */{
        dataIndex: 'isActive',
        hidden: true,
        flex: 1,
		
    },{
        dataIndex: 'comments',
        hidden: true,
        flex: 1,
    },{
        xtype: 'actioncolumn',
        width: 30,
        sortable: false,
        menuDisabled: true,
        items: [{
            icon: 'images/icon16_error.png',
            tooltip: 'Delete Product',
            style: 'cursor:pointer;',
            scope: this,
            handler: function(grid, rowIndex) {
                grid.getStore().removeAt(rowIndex);
            }
        }],
        renderer: function(val, metadata, record) {
            metadata.style = 'cursor: pointer;';
            return val;
        }
    }],
	/* features : [{
				ftype : 'summary',
			}], */
	selModel: 'cellmodel',
    plugins: {
        ptype: 'cellediting',
        clicksToEdit: 1,
    },

		dockedItems : [{
				xtype : 'toolbar',
				dock : 'top',
				items : [ {
					text : 'Add',
					icon:'images/drop-add.png',
					itemId:'gridaddBtn',
					cls : 'x-btn-text-icon',
					handler : function() {
						 var rec = new TomTail.view.orders.ProcessedProductItemGrid({
            sku: '',
            productName: '',
			requiredQty:'',
            currentPrice: '',
			
        });
		var store = Ext.getStore('processedItemStore');
		var storecount=store.getCount();
                        store.insert(storecount, rec);
					}
				}]
			}],
	 
});