Ext.define('TomTail.view.orders.ProcessedWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.processedwindow',
    requires: ['TomTail.view.orders.BillingGrid','TomTail.model.ModelUnprocessed'],
    height: 560,
    width: 933,
    title: 'Processed Order',
    bodyStyle: 'padding:10px;',
    
    itemId:'processedWin',
    controller: "orders-orders",
    viewModel: {
        type: "orders-orders"
    },
    
    items: [{
        xtype: 'form',
        height: 470,
        width: 900,

        layout: {
            type: 'table',
            columns: 3,
            tdAttrs: {
                style: 'padding:10px; vertical-align: top;'
            }
        },
        items: [{
        	xtype:'fieldset',
        	border:0,
        	margin:0,
        	padding:0,
        	width:295,
        	layout:'hbox',
        	items:[{
                xtype: 'textfield',
                name: 'id',
                fieldLabel: 'Id',
                width:100,
                labelWidth:15,
                readOnly:true,
            },{
                xtype: 'textfield',
                name: 'refOrderId',
                width:155,
                labelWidth:75,
                style:'margin-left: 20px;',
                fieldLabel: 'RefOrder Id',
                readOnly:true,
            }]
        },{
            xtype: 'textfield',
            name: 'city',
            fieldLabel: 'City',
        }, {
            xtype: 'combobox',
            editable: false,
            allowBlank:false,
            colspan:1,
            itemId:'outletid',
            fieldLabel: 'Outlet',
            style: 'margin-left:5px;',
            name: 'outletId',
            emptyText: 'Please select...',
            //forceSelection: true,
            store: new Ext.data.Store({
                // storeId:'',
                fields: [ 'name','partnerId','city'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: "channelPartner/listAll",
                    reader: {
                        type: 'json',
                        rootProperty: 'data.list',
                    },

                },
            }),
            queryMode: 'remote',
            displayField: 'name',
            valueField: 'partnerId',
            listeners:{
          	   select: function(combo, selection) {
          		  var partnerProduct = Ext.ComponentQuery.query('#billingOutletProduct')[0];
          		 partnerProduct.clearValue();
          		partnerProduct.store.proxy.url="channelPartner/skuList?partnerId="+combo.getValue();
          		partnerProduct.store.load();								
                     }
                 }
        },{
        	xtype:'fieldset',
        	border:0,
        	margin:0,
        	padding:0,
        	layout:'vbox',
        	items:[{
        	xtype:'component',
        	html:'Order Details :',
        	style:'margin-bottom:18px;'
        },{
    		xtype:'textareafield',
            fieldLabel: '',
    		name:'orderDetails',
    		readOnly:true,
    		labelSeparator: '',
    		width:275,
    		height:370,
    	}]
        },{
            xtype: 'billingGrid',
            colspan:2,
            title: '',
            height:406,
        }, {
                    xtype: 'textfield',
                    name: 'name',
                    width: 277,
					hidden:true,
					
                    style: 'margin-bottom:7px;margin-left:5px;',
                    labelSeparator: '',
                },  {
                    xtype: 'textfield',
                    name: 'mobile',
                    width: 277,
					
                    style: 'margin-bottom:7px;margin-left:5px;',
                    fieldLabel: '',
					hidden:true,
                    labelSeparator: '',
                },  {
                    xtype: 'datefield',
                    name: 'date',
					width:277,
					value:new Date(),
                    style: 'margin-bottom:7px;margin-left:5px;',
                    itemId: 'date',
					hidden:true,
                   format: 'Y-m-d H:i:s',
                }, {
                    xtype: 'hidden',
					name:'customerId',
                },  {
                    xtype: 'hidden',
					name:'status',
                },   {
                    xtype: 'hidden',
                    name: 'addressId',
                }, {
                    xtype: 'textareafield',
                    style: 'margin-bottom:7px;margin-left:5px;',
                    name: 'address',
					width:277,
					hidden:true,
                },  {
                    xtype: 'numberfield',
                    name: 'noOfItems',
					width:277,
					hidden:true,
					
                    style: 'margin-bottom:7px;margin-left:5px;',
                    minValue: 1,
                },  
                  
                {
                    xtype: 'textareafield',
                    name: 'comments',
                    colspan: 2,
					hidden:true,
                    style: 'margin-bottom:7px;margin-left:5px;',
                    width: 277
                }, {
                    xtype: 'checkbox',
                    fieldLabel: 'Enable',
                    name: 'isActive',
                    checked: true,
					hidden:true,
                    inputValue: 1,
                    uncheckedValue: 0
                }],
    }],
    
    
    buttons: [{
    	xtype: 'displayfield',
        fieldLabel: 'Unique Items',
		labelWidth:'n',
		itemId:'noOfItems',
		fieldStyle:'font-weight:bold;font-size:20px;color: #666;',
        labelStyle: 'font-size:18px;',
        name: 'noOfItems',
    },'||',{
    	xtype: 'displayfield',
        fieldLabel: 'Total Items',
		labelWidth:'n',
		itemId:'totalItems',
		fieldStyle:'font-weight:bold;font-size:20px;color: #666;',
        labelStyle: 'font-size:18px;',
        name: 'totalItems',
    },'||',{
    	xtype: 'displayfield',
        fieldLabel: 'Total: <span style="color:rgb(238, 47, 37);">&#8377;</span>',
		labelWidth:'n',
		//hidden:true,
		itemId:'processedtotal',
        labelSeparator: '',
		fieldStyle:'font-weight:bold;font-size:20px;color:rgb(238, 47, 37);',
        labelStyle: 'font-weight:bold;font-size:18px;',
        name: 'totalPrice',
    },'->',{
    	text:'Accept',
    	itemId:'processacceptBtn',
    	handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            console.log("form" + form);
            if (form.isValid()) {

               /* var url = 'order/update';
                if (form.getForm().findField('id').getValue() == null || form.getForm().findField('id').getValue() == '') {
                    url = 'order/save';
                }*/
                console.log("url" + form.getForm().findField('isActive').getValue());
                var store_grid = Ext.ComponentQuery.query('billingGrid')[0].store;
                console.log("store_grid" + store_grid);
                var selectionne = store_grid.getRange();
                console.log("selectionne" + selectionne);
                var data_grid = new Array();
                Ext.each(selectionne, function(record) {
                    console.log("record" + record);
                    var idval = "" + record.data.id;

                    if (record.data.sku != null && record.data.productName != null) {
                        if (idval.indexOf("-") > -1) {
                            idval = 0;
                        } else {
                            idval = record.data.id;
                        }
                        data_grid.push({
                        	id:idval,
                            sku: record.data.sku,
                            productName: record.data.productName,
                            requiredQty: record.data.requiredQty,
                            availableQty: record.data.requiredQty,
                            mrpPrice: record.data.mrpPrice,
                            requiredPrice: record.data.requiredPrice,
							currentPrice: record.data.currentPrice,
                            isActive: record.data.isActive,
                            comments: record.data.comments,
                        });
                    }
                });
                console.log("data_grid" + data_grid);
              /*   var gridData = Ext.encode(data_grid);
                console.log("gridData" + gridData);
                form.getForm().findField('orderDetails').setValue(gridData);

                var formData = Ext.encode(form.getValues());
                console.log("formData" + formData); */
                var orderid = form.getForm().findField('id').getValue();
                if (store_grid.getCount() > 0) {
                    Ext.Ajax.request({
                        url: 'order/updateStatus',
                        method: 'POST',
                        jsonData: {
							"id":form.getForm().findField('id').getValue(),
                            "date": Ext.Date.format(new Date(form.getForm().findField('date').getValue()), 'Y-m-d H:i:s'),
                            "refOrderId": form.getForm().findField('refOrderId').getValue(),
                            "customerId": form.getForm().findField('customerId').getValue(),
                            "name": form.getForm().findField('name').getValue(),
                            "mobile": form.getForm().findField('mobile').getValue(),
                            "outletId": form.getForm().findField('outletId').getValue(),
                            "addressId": form.getForm().findField('addressId').getValue(),
                            "noOfItems": Ext.ComponentQuery.query('#noOfItems')[0].getValue(),
                            "isActive": form.getForm().findField('isActive').getValue(),
                            "comments": form.getForm().findField('comments').getValue(),
                            "orderDetails": data_grid
                        },
                        
                        params: {
                        	"orderId":orderid,
                            "statusMode": 1
                        },

                        success: function(result, request, response) {
                        	var jsondata = Ext.util.JSON.decode(result.responseText);
                        	Ext.MessageBox.show({
                                title: 'Success',
                                msg: jsondata.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            })
                            Ext.ComponentQuery.query('#processedPanel')[0].enable();
                			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
                			Ext.ComponentQuery.query('#header')[0].enable(); 
                            if(Ext.ComponentQuery.query('ordersgrid')[0]!=undefined){
                            	Ext.ComponentQuery.query('ordersgrid')[0].store.load();
                            }
                            if (win) {
                                win.destroy();
								Ext.ComponentQuery.query('processordersgrid')[0].store.load();
                            }
                            	
                        },
                        failure: function(result, response) {
                        	var jsondata = Ext.util.JSON.decode(result.responseText);
                        	Ext.MessageBox.show({
                                title: 'Failure',
                                msg: 'Invalid Data',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            })
                        }
                    });

                } else {
                    Ext.MessageBox.show({
                        title: 'Failure',
                        msg: 'Please add the products',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    })
                }
            }
        }
    },{
    	text:'Reject',
    	itemId:'processrejectBtn',
    	handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            console.log("form" + form);
            if (form.isValid()) {

               /* var url = 'order/update';
                if (form.getForm().findField('id').getValue() == null || form.getForm().findField('id').getValue() == '') {
                    url = 'order/save';
                }*/
                console.log("url" + form.getForm().findField('isActive').getValue());
                var store_grid = Ext.ComponentQuery.query('billingGrid')[0].store;
                console.log("store_grid" + store_grid);
                var selectionne = store_grid.getRange();
                console.log("selectionne" + selectionne);
                var data_grid = new Array();
                Ext.each(selectionne, function(record) {
                    console.log("record" + record);
                    var idval = "" + record.data.id;

                    if (record.data.sku != null && record.data.productName != null) {
                        if (idval.indexOf("-") > -1) {
                            idval = 0;
                        } else {
                            idval = record.data.id;
                        }
                        data_grid.push({
                        	id:idval,
                            sku: record.data.sku,
                            productName: record.data.productName,
                            requiredQty: record.data.requiredQty,
                            availableQty: record.data.requiredQty,
                            mrpPrice: record.data.mrpPrice,
                            requiredPrice: record.data.requiredPrice,
                            currentPrice: record.data.currentPrice,
                            isActive: record.data.isActive,
                            comments: record.data.comments,
                        });
                    }
                });
                console.log("data_grid" + data_grid);
                var orderid = form.getForm().findField('id').getValue();
                
              /*   var gridData = Ext.encode(data_grid);
                console.log("gridData" + gridData);
                form.getForm().findField('orderDetails').setValue(gridData);

                var formData = Ext.encode(form.getValues());
                console.log("formData" + formData); */
                if (store_grid.getCount() > 0) {
                    Ext.Ajax.request({
                        url: 'order/updateStatus',
                        method: 'POST',
                        jsonData: {
							"id":form.getForm().findField('id').getValue(),
                            "date": Ext.Date.format(new Date(form.getForm().findField('date').getValue()), 'Y-m-d H:i:s'),
                            "refOrderId": form.getForm().findField('refOrderId').getValue(),
                            "customerId": form.getForm().findField('customerId').getValue(),
                            "name": form.getForm().findField('name').getValue(),
                            "mobile": form.getForm().findField('mobile').getValue(),
                            "outletId": form.getForm().findField('outletId').getValue(),
                            "addressId": form.getForm().findField('addressId').getValue(),
                            "noOfItems": Ext.ComponentQuery.query('#noOfItems')[0].getValue(),
                            "isActive": form.getForm().findField('isActive').getValue(),
                            "comments": form.getForm().findField('comments').getValue(),
                            "orderDetails": data_grid
                        },
                        params: {
                        	"orderId":orderid,
                            "statusMode": 2
                            },

                        success: function(result, request) {
                        	var jsondata = Ext.util.JSON.decode(result.responseText);
                        	Ext.MessageBox.show({
                                title: 'Success',
                                msg: jsondata.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            })
                            Ext.ComponentQuery.query('#processedPanel')[0].enable();
                			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
                			Ext.ComponentQuery.query('#header')[0].enable(); 
                            if(Ext.ComponentQuery.query('ordersgrid')[0]!=undefined){
                            	Ext.ComponentQuery.query('ordersgrid')[0].store.load();
                            }
                            if (win) {
                                win.destroy();
                                Ext.ComponentQuery.query('processordersgrid')[0].store.load();
                            }
                            	
                        },
                        failure: function(result, response) {
                        	var jsondata = Ext.util.JSON.decode(result.responseText);
                        	Ext.MessageBox.show({
                                title: 'Failure',
                                msg: 'Invalid Data',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            })
                        }
                    });

                } else {
                    Ext.MessageBox.show({
                        title: 'Failure',
                        msg: 'Please add the products',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    })
                }
            }
        }
    },{
    	text:'Print',
    	itemId:'processprintBtn'
    },{
        text: 'Place Order',
		itemId:'processedsubmitBtn',
        handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            console.log("form" + form);
            if (form.isValid()) {

                var url = 'order/updateOrder';
                if (form.getForm().findField('id').getValue() == null || form.getForm().findField('id').getValue() == '') {
                    url = 'order/save';
                }
                console.log("url" + form.getForm().findField('isActive').getValue());
                var store_grid = Ext.ComponentQuery.query('billingGrid')[0].store;
                console.log("store_grid" + store_grid);
                var selectionne = store_grid.getRange();
                console.log("selectionne" + selectionne);
                var data_grid = new Array();
                Ext.each(selectionne, function(record) {
                    console.log("record" + record);
                    var idval = "" + record.data.id;

                    if (record.data.sku != null && record.data.productName != null) {
                        if (idval.indexOf("-") > -1) {
                            idval = 0;
                        } else {
                            idval = record.data.id;
                        }
                        data_grid.push({
                        	id:idval,
                            sku: record.data.sku,
                            productName: record.data.productName,
                            availability:1,
                            parentSku:record.data.sku,
                            requiredQty: record.data.requiredQty,
                            availableQty: record.data.requiredQty,
                            mrpPrice: record.data.mrpPrice,
                            requiredPrice: record.data.requiredPrice,
                            currentPrice: record.data.currentPrice,
                            isActive: 1 , //record.data.isActive,
                            comments: record.data.comments,
                        });
                    }
                });
                console.log("data_grid" + data_grid);
              /*   var gridData = Ext.encode(data_grid);
                console.log("gridData" + gridData);
                form.getForm().findField('orderDetails').setValue(gridData);

                var formData = Ext.encode(form.getValues());
                console.log("formData" + formData); */
                if (store_grid.getCount() > 0) {
                    Ext.Ajax.request({
                        url: url,
                        method: 'POST',
                        jsonData: {
							"id":form.getForm().findField('id').getValue(),
                            "date": Ext.Date.format(new Date(form.getForm().findField('date').getValue()), 'Y-m-d H:i:s'),
                            "sourceType": "Internal",
                            "status":form.getForm().findField('status').getValue(),
                            "refOrderId": form.getForm().findField('refOrderId').getValue(),
                            "customerId": form.getForm().findField('customerId').getValue(),
                            "name": form.getForm().findField('name').getValue(),
                            "mobile": form.getForm().findField('mobile').getValue(),
                            "outletId": form.getForm().findField('outletId').getValue(),
                            "addressId": form.getForm().findField('addressId').getValue(),
                            "noOfItems": Ext.ComponentQuery.query('#noOfItems')[0].getValue(),
                            "isActive": 1, // form.getForm().findField('isActive').getValue(),
                            "comments": form.getForm().findField('comments').getValue(),
                            "orderDetails": data_grid
                        },

                        success: function(result, request, response) {
                        	var jsondata = Ext.util.JSON.decode(result.responseText);
                        	Ext.MessageBox.show({
                                title: 'Success',
                                msg: jsondata.message,
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            })
                            if(Ext.ComponentQuery.query('ordersgrid')[0]!=undefined){
                            	Ext.ComponentQuery.query('ordersgrid')[0].store.load();
                            }
                            if(Ext.ComponentQuery.query('processordersgrid')[0]!=undefined){
                            	Ext.ComponentQuery.query('processordersgrid')[0].store.load();
                            }
                            if(Ext.ComponentQuery.query('#processedPanel')[0]!=undefined){
                        		Ext.ComponentQuery.query('#processedPanel')[0].enable();
                    			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
                    			Ext.ComponentQuery.query('#header')[0].enable(); 
                        		}
                    			if(Ext.ComponentQuery.query('#unprocessPanel')[0]!=undefined){
                    			Ext.ComponentQuery.query('#unprocessPanel')[0].enable();
                    			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
                    			Ext.ComponentQuery.query('#header')[0].enable(); 
                    			}

                            if (win) {
                                win.destroy();
								//Ext.ComponentQuery.query('processordersgrid')[0].store.load();
                            }
                            	
                        },
                        failure: function(result, response) {
                        	var jsondata = Ext.util.JSON.decode(result.responseText);
                        	Ext.MessageBox.show({
                                title: 'Failure',
                                msg: 'Invalid Data',
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR
                            })
                        }
                    });

                } else {
                    Ext.MessageBox.show({
                        title: 'Failure',
                        msg: 'Please add the products',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    })
                }
            }
        }
    }],
    

    listeners:{
    	close:function(){
    		if(Ext.ComponentQuery.query('#processedPanel')[0]!=undefined){
    		Ext.ComponentQuery.query('#processedPanel')[0].enable();
			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
			Ext.ComponentQuery.query('#header')[0].enable(); 
    		}
			if(Ext.ComponentQuery.query('#unprocessPanel')[0]!=undefined){
			Ext.ComponentQuery.query('#unprocessPanel')[0].enable();
			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
			Ext.ComponentQuery.query('#header')[0].enable(); 
			}
    	}
    }
	
});