Ext.define('TomTail.view.orders.ProductCartWin', {
	extend: 'Ext.window.Window',
	alias: 'widget.productcartwin',
	//requires:['TomTail.view.config.orderrevision.OrdersRevisedWindow'],
	width: 550,
	layout:'vbox',
	title: '<span style="font-size:large;">Product Info</span>',
	itemId:'productcartWin',
	bodyStyle:'padding:10px;',
	items: [
	{
		xtype:'form',
		width: 550,
		items:[{
		xtype:'textfield',
        fieldLabel: 'Product Name',
		labelWidth:148,
		readOnly:true,
		labelStyle:'font-weight:bold;font-size:15px;',
		fieldStyle:'font-weight:bold;font-size:15px;',
		allowBlank:false,
        name: 'name',
		itemId:'name',
	},{
		xtype:'textfield',
        fieldLabel: 'SKU',
		labelWidth:148,
		readOnly:true,
		labelStyle:'font-weight:bold;font-size:15px;',
		fieldStyle:'font-weight:bold;font-size:15px;',
		allowBlank:false,
        name: 'sku',
		itemId:'sku',
	},{
		xtype:'displayfield',
        fieldLabel: 'Parent SKU',
		labelWidth:148,
		labelStyle:'font-weight:bold;font-size:15px;',
		fieldStyle:'font-weight:bold;font-size:15px;',
		allowBlank:false,
		hidden:true,
        name: 'prntsku',
		itemId:'prntsku',
	}, {
		xtype:'numberfield',
        fieldLabel: 'MRP',
		activeItem: 0,
		labelWidth:148,
		minValue:1,
		labelStyle:'font-weight:bold;font-size:15px;',
		fieldStyle:'font-weight:bold;font-size:15px;',
		allowBlank:false,
        name: 'mrp_price',
		itemId:'mrp',
		
	},{
		xtype:'numberfield',
        fieldLabel: 'TT-Price',
		labelWidth:148,
		minValue:1,
		fieldStyle:'font-weight:bold;font-size:15px;',
		labelStyle:'font-weight:bold;font-size:15px;',
		allowBlank:false,
        name: 'price',
		itemId:'price',
	}, {
		xtype:'numberfield',
        fieldLabel: 'Qty',
		minValue:1,
		labelWidth:148,
		labelStyle:'font-weight:bold;font-size:15px;',
		fieldStyle:'font-weight:bold;font-size:15px;',
		value:1,
		allowBlank:false,
		itemId:'qtyordered',
        name: 'qtyordered',
		listeners:{
			afterrender: function(field) {
   Ext.defer(function() {
       field.focus(true, 100);
   }, 1);
}
		}
	},/*{
		xtype:'fieldset',
		border:0,
		padding:0,
		margin:0,
		layout:'hbox',
	items:[{
		xtype:'combobox',
		fieldLabel:'Configurable product',
		labelWidth:148,
		name:'configcb',
		allowBlank:false,
		itemId:'configcb',
		hidden:true,
		labelStyle:'font-weight:bold;font-size:15px;',
		store:new Ext.data.Store({
                     		 // storeId:'',
                              fields: [{
            name: 'id',
			//mapping:'options.id',
        }, {
            name: 'price',
			//mapping:'options.price',
        }, {
            name: 'label',
			//mapping:'options.label',
        }],
                               proxy : {
                           		type : 'ajax',
                           		url: "app/store/orders/productConfig.json",
                           		reader : {
                           			type : 'json',
                           			rootProperty:'attributes.attribute.options',
                           		},

                               },
                           }),
                        queryMode: 'remote',
                        displayField: 'label',
                        valueField: 'price',
		
	}]
	},*/ {
        xtype: 'textareafield',
        name: 'comments',
        itemId:'comments',
        fieldLabel:'Comments',
        labelStyle:'font-weight:bold;font-size:15px;',
		fieldStyle:'font-weight:bold;font-size:15px;',
        labelWidth:148,
    }, {
        xtype: 'checkbox',
        fieldLabel: 'Enable',
        name: 'isActive',
        itemId:'isActive',
        checked: true,
        labelStyle:'font-weight:bold;font-size:15px;',
		fieldStyle:'font-weight:bold;font-size:15px;',
        inputValue: 1,
        uncheckedValue: 0
    }]
	}],
			
			
           	 buttons:[{
              	text:'Add to Cart',
            	handler:function(){
            		
					var win = this.up('window');
					var form = win.down('form');
					if(form.isValid()){
					var pname = Ext.ComponentQuery.query('#name')[0].getValue();
					var psku = Ext.ComponentQuery.query('#sku')[0].getValue();
					var mrp = Ext.ComponentQuery.query('#mrp')[0].getValue();
					var ttp = Ext.ComponentQuery.query('#price')[0].getValue();
					var qty = Ext.ComponentQuery.query('#qtyordered')[0].getValue();
					var comments = Ext.ComponentQuery.query('#comments')[0].getValue();
					var isactive = Ext.ComponentQuery.query('#isActive')[0].getValue();
					//var cbbox = Ext.ComponentQuery.query('#configcb')[0].getValue();
					var sum = 0;
            		var totalItems=0;
						if (Ext.ComponentQuery.query('#orderrevisionWin')[0] != undefined) {
							var revisedstore = Ext.getStore('revisedItemStore');
							var flag = 0;
							revisedstore.each(function(record){
								if(record.get('sku') == psku){
									var reqqty = record.get('requiredQty');
									record.set('requiredQty',reqqty+qty);
									flag=1;
								}
			     	            
			        		 });
							console.log("Flag " +flag);
							if(flag != 1){
							var rec = new TomTail.view.config.orderrevision.RevisedProductItemGrid({
								 sku:psku,
		                         productName:pname,
		                         parentSku:Ext.ComponentQuery.query('#prntsku')[0].getValue(),
		                         requiredQty:qty,
		                         availability:1,
		                         mrpPrice:mrp,
		                         availableQty:qty,
		                         requiredPrice:ttp,
		                         currentPrice:ttp,
		                         comments:comments,
		                         isActive:isactive,
                       });
							}
                       
						var revisedstorecount=revisedstore.getCount();
						Ext.ComponentQuery.query('#revisednoOfItems')[0].setValue(revisedstorecount + 1);
						revisedstore.insert(revisedstorecount, rec);
						revisedtotalItems = Ext.ComponentQuery.query('#revisedtotalItems')[0].getValue() + qty;
						var reviseditems= Math.floor(revisedtotalItems);
						Ext.ComponentQuery.query('#revisedtotalItems')[0].setValue(reviseditems);
						if(Ext.ComponentQuery.query('#revisedtotal')[0].getValue() != ''){
						var revisedtotalValue = parseFloat(Ext.ComponentQuery.query('#revisedtotal')[0].getValue());
						}else{
							revisedtotalValue = 0;
						}
						sum += revisedtotalValue + ttp * qty;
						var revisedgrandtot= parseFloat(sum).toFixed(2);
		     	           Ext.ComponentQuery.query('#revisedtotal')[0].setValue(revisedgrandtot);
		     	          Ext.ComponentQuery.query('#revisedacceptBtn')[0].setDisabled(true);
						}
						
						if(Ext.ComponentQuery.query('#processedWin')[0] != undefined){
						//if(cbbox == null){
							var store = Ext.getStore('billingStore');
							var flag = 0;
							store.each(function(record){
								if(record.get('sku') == psku){
									var reqqty = record.get('requiredQty');
									record.set('requiredQty',reqqty+qty);
									flag=1;
								}
			     	            
			        		 });
							console.log("Flag " +flag);
							if(flag != 1){
						var rec = new TomTail.view.orders.BillingGrid({
							 sku:psku,
	                         productName:pname,
	                         requiredQty:qty,
	                         availability:1,
	                         requiredPrice:ttp,
	                         mrpPrice:mrp,
	                         currentPrice:ttp,
	                         comments:comments,
	                         isActive:isactive,
                        });
							}
						var storecount=store.getCount();
						if(Ext.ComponentQuery.query('#noOfItems')[0] != undefined){
						Ext.ComponentQuery.query('#noOfItems')[0].setValue(storecount + 1);
						}
						totalItems = Ext.ComponentQuery.query('#totalItems')[0].getValue() + qty;
						var items= Math.floor(totalItems);
						Ext.ComponentQuery.query('#totalItems')[0].setValue(items);
						if(Ext.ComponentQuery.query('#processedtotal')[0].getValue() != ''){
						var totalValue = parseFloat(Ext.ComponentQuery.query('#processedtotal')[0].getValue());
						}else{
							totalValue = 0;
						}
						sum += totalValue + ttp * qty;
						var grandtot= parseFloat(sum).toFixed(2);
		     	           Ext.ComponentQuery.query('#processedtotal')[0].setValue(grandtot);
                        store.insert(storecount, rec);
						//}
						/*else{
							var rec = new TomTail.view.orders.BillingGrid({
								 sku:psku,
		                         productName:pname,
		                         requiredQty:qty,
		                         mrpPrice:mrp,
		                         currentPrice:ttp,
		                         comments:comments,
		                         isActive:isactive,
                        });
                        var store = Ext.getStore('billingStore');
						var storecount=store.getCount();
						store.each(function(record){
		     	            sum += record.get('currentPrice') * record.get('requiredQty');
		     	           grandtot= parseFloat(sum).toFixed(2);
		     	           
		     	           Ext.ComponentQuery.query('#processedtotal')[0].setValue(grandtot);
		     	          totalItems += record.get('requiredQty') + Ext.ComponentQuery.query('#qtyordered')[0].getValue();
		     	         Ext.ComponentQuery.query('#totalItems')[0].setValue(totalItems);
		        		 });
                        store.insert(storecount, rec);
						}*/
						
						}
						
						win.destroy();
					}
				}
         }]
});