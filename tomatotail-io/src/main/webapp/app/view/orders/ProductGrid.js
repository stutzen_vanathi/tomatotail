Ext.define('TomTail.view.orders.ProductGrid', {
    extend: 'Ext.grid.Panel',

    alias: 'widget.ordersproductGrid',
    itemId: 'productgrid',
    width: 700,
    autoScroll: true,
    
	store: new Ext.data.Store({
	storeId:'ProductListStore',
		     fields: [{
            name: 'name'
        }, {
            name: 'sku'
        }, {
            name: 'price'
        }, {
            name: 'qtyordered'
        }, {
            name: 'total',
            type: 'int',
            convert: function(val, row) {
                return row.data.price * row.data.qtyordered;
            }
        },{
			name:'shippingamount'
		}],
			  autoLoad: false,
        proxy: {
            type: 'ajax',
            url: 'order/orderList',
            reader: {
                type: 'json',
                rootProperty: 'data.list',
            },
			 listeners: {
                exception: function(proxy, response, operation, eOpts) {
                    alert('Records not found!');
                }
            }

        },
	}),
	
	/*features : [{
				ftype : 'summary',
			}],*/
			
    columns: [{
                xtype: 'rownumberer'
            },{
        text: 'Product Name',
        dataIndex: 'name',
        flex: 2
    }, {
        text: 'SKU',
        dataIndex: 'sku',
		align:'right',
        //flex: 1
    }, {
        text: 'Price',
        dataIndex: 'price',
		align:'right',
        flex: 1
    }, {
        text: 'Quantity',
        dataIndex: 'qtyordered',
		align:'right',
        flex: 1
    }, {
        text: 'Sub Total',
        dataIndex: 'total',
		align:'right',
		//summaryType:'sum',
		/*summaryType:function( records ) {
			 var i = 0,
                length = records.length,
				 sum = 0,
				 shipping=0,
                record;
         for (i; i < length; ++i) {
            var record = records[ i ];
            sum += record.get('price') * record.get('qtyordered');
			shipping +=record.get('cost');
         }
         grandtot= Math.floor( sum+shipping );
         return '<strong>'+' Total :<span style="padding-left:62px;"></span>'+ sum + '<br />Shipping :<span style="padding-right:45px;"></span>' + shipping + '<br />---------------------------------<br /> Grand Total :<span style="padding-right:22px;"></span>'+grandtot+'</strong>';
      },*/
   
       
        flex: 1
    }],
	
});