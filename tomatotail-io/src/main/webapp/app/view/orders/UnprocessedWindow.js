Ext.define('TomTail.view.orders.UnprocessedWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.unprocessedwindow',
	
	requires:['Ext.form.field.Trigger'],
	
    controller: "orders-orders",
    viewModel: {
        type: "orders-orders"
    },
    height: 570,
    width: 630,
    overflowX: 'hidden',
    overflowY: 'auto',
    autoCreate:false,
    title: 'Unprocessed Order',
    bodyStyle: 'padding:10px;',
    itemId:'unprocessedWindow',
    items: [{
        xtype: 'form',
		width: 630,
		height: 680,
        layout: {
            type: 'table',
            columns: 2,
            tdAttrs: {
                style: 'padding:10px; vertical-align: top;'
            }
        },

        items: [{
                xtype: 'hidden',
                name: 'id',
            }, {
                xtype: 'hidden',
                name: 'customerId',
                itemId: 'customerId'
            }, {
                xtype: 'hidden',
                name: 'addressId',
                itemId: 'addressId'

            },{
                xtype: 'hidden',
                name: 'mobile',
                itemId: 'mobile'

            }, {
                xtype: 'datefield',
                fieldLabel: 'Date',
                name: 'date',
                itemId: 'date',
                readOnly:true,
                value:new Date(),
				allowBlank:false,
                format: 'Y-m-d H:i:s'
            }, {
                fieldLabel: 'Name',
                xtype: 'triggerfield',
                name: 'name',
                itemId: 'name',
				allowBlank:false,
                editable: false,
                triggerCls: 'x-form-search-trigger',
                onTriggerClick: function(event) {
                    var me = this;
                    if (!me.hideTrigger) {
                        me.fireEvent("triggerclick", me, event);
                    }
                },

            }, {
		xtype:'textareafield',
        fieldLabel: 'Address',
		name:'address',
		allowBlank:false,
		itemId:'address',
		//colspan:2,
	},{
    	xtype:'fieldset',
    	border:0,
    	margin:0,
    	padding:0,
    	width:295,
    	layout:'vbox',
    	items:[{
            xtype: 'textfield',
            name: 'city',
            fieldLabel: 'City',
            itemId:'city'
        },{
            xtype: 'textfield',
            name: 'state',
            fieldLabel: 'State',
            itemId:'state'
        }]
    },
            {
                xtype: 'textareafield',
                fieldLabel: 'Product Items',
                name: 'orderDetails',
                itemId: 'orderdetails',
				allowBlank:false,
                height: 350,
                colspan: 2,
                width: 570
            }, {
                xtype: 'textareafield',
                fieldLabel: 'Comments',
                name: 'comments',
                colspan: 2,
                width: 570
            }, {
                xtype: 'checkbox',
                fieldLabel: 'Is Active',
                name: 'isActive',
                checked: true,
                inputValue: 1,
                uncheckedValue: 0
            }
        ],
    }],

    buttons: [{
        text: 'Save',
        itemId:'unprocessSave',
        handler: function() {
            var win = this.up('window');
            console.log("wndow" + win);
            var form = win.down('form');
            console.log("form" + form);
            if (form.isValid()) {
                var formData = Ext.encode(form.getValues());
                var url = 'unProcessedOrder/update';
                if (form.getForm().findField('id').getValue() == null || form.getForm().findField('id').getValue() == '') {
                    url = 'unProcessedOrder/save';
                }
                Ext.Ajax.request({
                    url: url,
                    method: 'POST',
                    jsonData: {
                        "id": form.getForm().findField('id').getValue(),
                        "date": Ext.Date.format(new Date(form.getForm().findField('date').getValue()), 'Y-m-d H:i:s'),
                        "customerId": form.getForm().findField('customerId').getValue(),
                        "addressId": form.getForm().findField('addressId').getValue(),
                        "comments": form.getForm().findField('comments').getValue(),
                        "orderDetails": form.getForm().findField('orderDetails').getValue(),
                        "isActive": form.getForm().findField('isActive').getValue(),
                    },

                    success: function(result, request) {
                    	var jsondata = Ext.util.JSON.decode(result.responseText);
                    	Ext.MessageBox.show({
                            title: 'Success',
                            msg: jsondata.message,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        })
                        if (win) {
                        	Ext.ComponentQuery.query('#unprocessPanel')[0].enable();
                			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
                			Ext.ComponentQuery.query('#header')[0].enable(); 
                            win.close();
                            Ext.ComponentQuery.query('ordersgrid')[0].store.load();
                        }
                    },
                    failure: function(result, request) {
                    	var jsondata = Ext.util.JSON.decode(result.responseText);
                        Ext.MessageBox.show({
                            title: 'Failure',
                            msg: 'Invalid Data',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR
                        })
                    }
                });
            }

        }
    }],
    
    listeners:{
    	close:function(){
    		Ext.ComponentQuery.query('#unprocessPanel')[0].enable();
			Ext.ComponentQuery.query('#tabbar')[0].enable(); 
			Ext.ComponentQuery.query('#header')[0].enable(); 
    	}
    }
});